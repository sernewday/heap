<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $token int */
/* @var $use_link bool */
/* @var $link string */

?>
<div class="verify-email">
    <p>Hello <?= Html::encode($user->username) ?>,</p>

    <?php if (!$use_link): ?>
        <p>Use this code to verify your email: <?= $token ?></p>
    <?php else: ?>
        <p>Use this link to verify your email: <a href="<?= $link ?>"><?= $link ?></a></p>
    <?php endif; ?>

</div>
