<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $order \common\models\Orders */
/* @var $user \common\models\User */

?>
<p>Здравствуйте <?= Html::encode($user->info->name) ?>,</p>

<p>Статус вашего заказа №<?= $order->id ?> изменён на "<?= Html::encode($order->statusLabel) ?>"</p>
