<?php
return [
    'adminEmail' => 'shopping@sysale.com.ua',
    'supportEmail' => 'shopping@sysale.com.ua',
    'senderEmail' => 'shopping@sysale.com.ua',
    'senderName' => 'Shopping',
    'user.passwordResetTokenExpire' => 3600,
];
