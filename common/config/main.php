<?php
return [
    'name' => 'Shopping',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@images' => dirname(dirname(__DIR__)) . '/frontend/web/uploads'
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'mailer' => require dirname(__DIR__) . '/config/mail.php',
    ],
];
