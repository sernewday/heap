<?php

namespace common\models;

use common\models\Queries\TopProductsQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "top_products".
 *
 * @property int $id
 * @property int $product_id
 * @property int $due_to
 * @property float $cost
 * @property int $created_at
 * @property int $updated_at
 */
class TopProducts extends ActiveRecord
{
    /**
     * @var int Price for a product per day
     */
    public static $priceForOne = 200;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'top_products';
    }

    /**
     * {@inheritdoc}
     * @return TopProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TopProductsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'due_to', 'cost'], 'required'],
            [['product_id', 'due_to', 'created_at', 'updated_at'], 'integer'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'due_to' => 'Due To',
            'cost' => 'Cost',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
