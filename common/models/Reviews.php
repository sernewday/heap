<?php

namespace common\models;

use common\models\Queries\ReviewsQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string $module
 * @property int $record_id
 * @property string $comment
 * @property int|null $small_size
 * @property int|null $correct_color
 * @property int|null $perfect_quality
 * @property int|null $delivery_in_time
 * @property int|null $is_polite
 * @property int|null $is_online
 * @property int|null $order_id
 * @property int $user_id
 * @property int|null $reply_to
 * @property int $score
 * @property int $confirmed
 * @property int $created_at
 * @property Products $product
 * @property Reviews[] $answers
 * @property-read User $user
 * @property-read string $date
 * @property-read mixed $imagesLinks
 * @property-read ImageManager[] $images
 * @property-read mixed $imagesLinksData
 * @property-read ImageManager $image
 * @property-read Brands $brand
 * @property int $updated_at
 */
class Reviews extends ActiveRecord
{
    /**
     * Get brand's reviews
     * @param $brand_id
     * @param int $limit
     * @return Reviews[]
     */
    public static function getByBrand($brand_id, $limit = 10)
    {
        return self::find()->where([self::tableName() . '.record_id' => $brand_id])->orderBy(self::tableName() . '.created_at DESC')->limit($limit)->main()->brands()->confirmed()->cache(3600 * 24)->all();
    }

    /**
     * Get brand's reviews
     * @param $brand_id
     * @return ReviewsQuery
     */
    public static function getByBrandQuery($brand_id)
    {
        return self::find()->where([self::tableName() . '.record_id' => $brand_id])->orderBy(self::tableName() . '.created_at DESC')->main()->brands()->confirmed()->cache(3600 * 24);
    }

    /**
     * {@inheritdoc}
     * @return ReviewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReviewsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @param array $params
     * @return ReviewsQuery
     */
    public static function get($params = [])
    {
        $model = Reviews::find()->orderBy('created_at DESC');

        self::search($model, $params);

        return $model;
    }

    /**
     * @param ReviewsQuery $model
     * @param array $params
     */
    public static function search($model, $params)
    {
        foreach ($params as $key => $param) {
            $model->andWhere([Reviews::tableName() . '.' . $key => $param]);
        }
    }

    /**
     * Get user's reviews
     * @param int $user_id
     * @param int $limit
     * @return array|Reviews[]
     */
    public static function getByUser($user_id, $limit = 10)
    {
        return self::find()->joinWith(['product'])->where([self::tableName() . '.user_id' => $user_id])->orderBy(self::tableName() . '.created_at DESC')->products()->limit($limit)->main()->cache(3600 * 24)->all();
    }

    /**
     * @param int $user_id
     * @return ReviewsQuery
     */
    public static function getSellerReviewsByUser($user_id)
    {
        return self::find()->where([self::tableName() . '.user_id' => $user_id])->orderBy(self::tableName() . '.created_at DESC')->main()->brands()->cache(3600 * 24);
    }

    /**
     * @param int $user_id
     * @param int $limit
     * @return int
     */
    public static function getCountByUser($user_id, $limit = 10)
    {
        return self::find()->joinWith(['product'])->where([self::tableName() . '.user_id' => $user_id])->orderBy(self::tableName() . '.created_at DESC')->limit($limit)->main()->cache(3600 * 24)->count();
    }

    /**
     * @return Reviews|null
     */
    public static function getIds()
    {
        return self::find()->select('id')->orderBy('id DESC')->one();
    }

    /**
     * Get reviews by product id
     * @param int $product_id
     * @return ReviewsQuery
     */
    public static function getByProduct($product_id)
    {
        return self::find()
//            ->cache(3600 * 24)
            ->where(['record_id' => $product_id])
            ->andWhere([Reviews::tableName() . '.module' => Products::tableName()])
            ->andWhere([Reviews::tableName() . '.reply_to' => 0])
            ->orderBy('created_at DESC');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module', 'comment', 'user_id', 'score', 'record_id'], 'required'],
            [['user_id', 'reply_to', 'score', 'created_at', 'updated_at', 'record_id', 'order_id'], 'integer'],
            [['confirmed', 'is_polite', 'is_online', 'small_size', 'correct_color', 'perfect_quality', 'delivery_in_time'], 'boolean'],
            [['module', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Module',
            'comment' => 'Comment',
            'small_size' => 'Small Size',
            'correct_color' => 'Correct Color',
            'perfect_quality' => 'Perfect Quality',
            'delivery_in_time' => 'Delivery In Time',
            'user_id' => 'User ID',
            'reply_to' => 'Reply To',
            'score' => 'Score',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'record_id']);
    }

    /**
     * @return Reviews[]
     */
    public function getAnswers()
    {
        return self::find()->where(['reply_to' => $this->id])->cache(3600 * 24)->orderBy('created_at ASC')->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return date('d.m.Y', $this->created_at);
    }

    public function getImage()
    {
        return $this->hasOne(ImageManager::className(), ['record_id' => 'id'])->andWhere(['module' => self::tableName()])->orderBy('sort');
    }

    public function getImagesLinks()
    {
        return ArrayHelper::getColumn($this->images, 'imageUrl');
    }

    public function getImagesLinksData()
    {
        return ArrayHelper::toArray($this->images, [
                ImageManager::className() => [
                    'caption' => 'path',
                    'key' => 'id',
                ]]
        );
    }

    public function beforeDelete()
    {
        if ($this->getImages()) {
            foreach ($this->images as $one) {
                @unlink(Yii::getAlias('@images') . '/' . self::tableName() . '/' . $one->path);
            }
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function getImages()
    {
        return $this->hasMany(ImageManager::className(), ['record_id' => 'id'])->andWhere(['module' => self::tableName()])->orderBy('sort');
    }

    /**
     * @return ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'record_id']);
    }
}
