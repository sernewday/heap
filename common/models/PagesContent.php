<?php

namespace common\models;

use frontend\components\Lang;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pages_content".
 *
 * @property int $id
 * @property-read PagesContentInfo $info
 * @property int $page_id
 */
class PagesContent extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id'], 'required'],
            [['page_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getInfo()
    {
        if (!PagesContentInfo::find()->where(['record_id' => $this->id, 'lang' => Lang::getCurrentId()])->cache(3600 * 5)->exists())
            return $this->hasOne(PagesContentInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 1])->cache(3600 * 5);

        return $this->hasOne(PagesContentInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => Lang::getCurrentId()])->cache(3600 * 5);
    }

    /**
     * @param $lang
     * @return PagesContentInfo
     */
    public function getInfoByLang($lang)
    {
        return PagesContentInfo::findOne(['record_id' => $this->id, 'lang' => $lang]);
    }
}
