<?php

namespace common\models;

use common\models\Infos\CategoriesInfo;
use common\models\Queries\CategoriesQuery;
use frontend\components\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name_alt
 * @property int $parent_id
 * @property int $depth
 * @property string $path
 * @property int $created_at
 * @property CategoriesInfo $info
 * @property CategoriesInfo $infoRu
 * @property CategoriesInfo $infoUa
 * @property Categories[] $childs
 * @property string $image
 * @property-read CategoriesImportAssoc[] $importIds
 * @property-read CategoriesInfo[] $infos
 * @property-read null|bool|string|int $productsAmount
 * @property int $updated_at
 */
class Categories extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @param int $limit
     * @return Categories[]
     */
    public static function getTop($limit = 4)
    {
        return self::find()->limit($limit)->all();
    }

    /**
     * {@inheritdoc}
     * @return CategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoriesQuery(get_called_class());
    }

    /**
     * @param $import_id
     * @return array|Categories|null
     */
    public static function findByImportId($import_id)
    {
        return self::find()->joinWith(['importIds'])->where([CategoriesImportAssoc::tableName() . '.import_id' => $import_id])->one();
    }

    /**
     * @param $name
     * @return array|Categories|null
     */
    public static function findByName($name)
    {
        return self::find()->joinWith(['infos'])->where(['like', CategoriesInfo::tableName() . '.name', $name])->limit(1)->one();
    }

    public static function get($params = [])
    {
        return self::find();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_alt', 'parent_id'], 'required'],
            [['parent_id', 'depth', 'created_at', 'updated_at'], 'integer'],
            [['name_alt', 'path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_alt' => 'Name Alt',
            'parent_id' => 'Parent ID',
            'depth' => 'Depth',
            'path' => 'Path',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getInfo()
    {
        if (!CategoriesInfo::find()->where(['record_id' => $this->id, 'lang' => Lang::getCurrentId()])->cache(3600 * 5)->exists())
            return $this->hasOne(CategoriesInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 1])->cache(3600 * 5);

        return $this->hasOne(CategoriesInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => Lang::getCurrentId()])->cache(3600 * 5);
    }

    /**
     * @return ActiveQuery
     */
    public function getInfoRu()
    {
        return $this->hasOne(CategoriesInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 2])->cache(3600 * 5);
    }

    /**
     * @return ActiveQuery
     */
    public function getInfoUa()
    {
        return $this->hasOne(CategoriesInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 1])->cache(3600 * 5);
    }

    public function getInfos()
    {
        return $this->hasMany(CategoriesInfo::className(), ['record_id' => 'id'])->cache(3600 * 5);
    }

    /**
     * @return ActiveQuery
     */
    public function getChilds()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id'])->orderBy('path, depth ASC');
    }

    /**
     * Get category image by product image in the category
     * @return string
     */
    public function getImage()
    {
        $cache = Yii::$app->cache;
        $key = 'category_image_' . $this->id;
        $cache_data = $cache->get($key);
        if ($cache_data)
            return $cache_data;

        $assoc = ProductsCategoriesAssoc::find()->where(['category_id' => $this->id])->one();

        if ($assoc->product->image)
            $image = $assoc->product->image->imageUrl;
        else
            $image = null;

        $cache->set($key, $image, 3600 * 24);

        return $image;
    }

    public function beforeSave($insert)
    {
        $categories_exist = Categories::find()->where(['like', 'name_alt', $this->name_alt])->count();
        if ($categories_exist > 0)
            $this->name_alt .= '-' . ($categories_exist + 1);

        $depth = 1;
        $path = '-1';

        if ($this->path != '-1') {
            $parent_category = Categories::findOne($this->parent_id);
            $depth = $parent_category->depth + 1;

            $path = $parent_category->path . '.' . $parent_category->id;
            if ($parent_category->path == '-1')
                $path = $parent_category->id;
        }

        $this->depth = $depth;
        $this->path = $path;

        return parent::beforeSave($insert);
    }

    /**
     * @return ActiveQuery
     */
    public function getImportIds()
    {
        return $this->hasMany(CategoriesImportAssoc::className(), ['category_id' => 'id']);
    }

    /**
     * @param $import_id
     * @return bool
     */
    public function addImportId($import_id)
    {
        $import_assoc = new CategoriesImportAssoc(['category_id' => $this->id, 'import_id' => "$import_id"]);

        return $import_assoc->save();
    }

    /**
     * @return bool|int|string|null
     */
    public function getProductsAmount()
    {
        return ProductsCategoriesAssoc::find()->where(['category_id' => $this->id])->groupBy('product_id')->cache(3600 * 24)->count();
    }
}
