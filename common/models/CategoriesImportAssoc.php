<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories_import_assoc".
 *
 * @property int $category_id
 * @property string $import_id
 */
class CategoriesImportAssoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_import_assoc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'import_id'], 'required'],
            [['category_id'], 'integer'],
            [['import_id'], 'string', 'max' => 255],
            [['category_id', 'import_id'], 'unique', 'targetAttribute' => ['category_id', 'import_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'import_id' => 'Import ID',
        ];
    }
}
