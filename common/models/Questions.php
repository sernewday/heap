<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property string $module
 * @property int $record_id
 * @property string $question
 * @property string|null $answer
 * @property int $is_answered
 * @property int $is_viewed
 * @property int $from_id
 * @property int $created_at
 * @property-read User $user
 * @property-read string|false $date
 * @property-read Products $product
 * @property int $updated_at
 * @property-read string|false $answerDate
 * @property int $answered_at
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @param int $product_id
     * @return Queries\QuestionsQuery
     */
    public static function getByProduct($product_id)
    {
        return self::find()
            ->products()
            ->answered()
            ->andWhere([self::tableName() . '.record_id' => $product_id])
            ->cache(3600 * 24)
            ->orderBy('created_at DESC');
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Queries\QuestionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\Queries\QuestionsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * @param int $user_id
     * @return Questions[]
     */
    public static function getByUser($user_id)
    {
        return self::find()
            ->products()
            ->answered()
            ->andWhere([self::tableName() . '.from_id' => $user_id])
            ->orderBy('created_at DESC')
            ->cache(3600 * 24)
            ->all();
    }

    /**
     * @param int $user_id
     * @return int
     */
    public static function getCountByUser($user_id)
    {
        return self::find()
            ->products()
            ->answered()
            ->andWhere([self::tableName() . '.from_id' => $user_id])
            ->orderBy('created_at DESC')
            ->cache(3600 * 24)
            ->count();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module', 'record_id', 'question', 'from_id'], 'required'],
            [['record_id', 'is_answered', 'is_viewed', 'from_id', 'created_at', 'updated_at', 'answered_at'], 'integer'],
            [['module', 'question', 'answer'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Module',
            'record_id' => 'Record ID',
            'question' => 'Question',
            'answer' => 'Answer',
            'is_answered' => 'Is Answered',
            'is_viewed' => 'Is Viewed',
            'from_id' => 'From ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id'])->cache(3600 * 24);
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return date('d.m.Y', $this->created_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'record_id'])->cache(3600 * 24);
    }

    /**
     * @return false|string
     */
    public function getAnswerDate()
    {
        return date('d.m.Y', $this->answered_at);
    }

    public function setNew()
    {
        $this->is_answered = 0;
        $this->is_viewed = 0;
    }
}
