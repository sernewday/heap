<?php

namespace common\models;

use common\models\Infos\ProductsInfo;
use common\models\Queries\ProductsQuery;
use frontend\components\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $brand_id
 * @property int $category_id
 * @property int|null $discount
 * @property int|null $discount_due
 * @property int|null $discount_type
 * @property int|null $is_new
 * @property int $hide
 * @property float $price
 * @property int $currency_id
 * @property int $created_at
 * @property ProductsInfo $info
 * @property mixed $imagesLinks
 * @property ImageManager[] $images
 * @property mixed $imagesLinksData
 * @property ImageManager $image
 * @property string $currencyLabel
 * @property string $discountLabel
 * @property Brands $brand
 * @property float|int $discountPrice
 * @property Reviews[] $reviews
 * @property ProductsCategoriesAssoc[] $categoriesAssoc
 * @property ActiveQuery $categoryAssoc
 * @property Categories $category
 * @property float $averageScore
 * @property mixed $reviewsSummary
 * @property bool $isPerfectQuality
 * @property bool $isCorrectColor
 * @property bool $isDeliveryInTime
 * @property bool $isSmallSize
 * @property array $breadcrumbsData
 * @property Bag[] $bag
 * @property int $updated_at
 * @property int $status_id
 * @property int $amount_left
 * @property int $available
 * @property-read ProductParamsAssoc[] $characteristics
 * @property-read ProductSizes[] $sizes
 * @property-read ProductParamsAssoc[] $colors
 * @property string $vendor_code
 * @property-read bool $isLiked
 * @property-read null|bool|string|int $reviewsAmount
 * @property-read null|bool|string|int $ordersAmount
 * @property-read null|bool|string|int $likesAmount
 * @property string $import_id
 */
class Products extends ActiveRecord
{
    /**
     * Discount id - percent
     */
    const DISCOUNT_PERCENT = 1;

    /**
     * Discount id - cash
     */
    const DISCOUNT_CASH = 2;

    /**
     * Ukrainian currency
     */
    const CURRENCY_UAH = 1;

    // Products statuses
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_DELETED = 9;

    public static $attrs_list = ['id', 'brand_id', 'vendor_code', 'discount', 'is_new', 'hide', 'price', 'currency_id',
        'status_id', 'available', 'amount_left', 'created_at', 'updated_at'];

    /**
     * Need for defining product reviews summary
     * @var int
     */
    public static $percentPoint = 45;

    /**
     * @var string[]
     */
    public static $price_labels = [
        self::CURRENCY_UAH => 'грн',
    ];

    public static $discount_labels = [
        self::DISCOUNT_PERCENT => '%',
        self::DISCOUNT_CASH => 'грн',
    ];

    /**
     * @var int Limit of recently viewed products
     */
    public static $recentlyViewedLimit = 20;

    public static function getIds()
    {
        return self::find()->select('id')->orderBy('id DESC')->one();
    }

    /**
     * {@inheritdoc}
     * @return ProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductsQuery(get_called_class());
    }

    /**
     * Returns sell-out products
     * @param int $limit
     * @return Products[]
     */
    public static function getSale($limit = 15)
    {
        return self::find()->where(['!=', 'discount', ''])->joinWith(['image', 'brand'])->orderBy('created_at DESC')->limit($limit)->all();
    }

    /**
     * Returns new products
     * @param int $limit
     * @return Products[]
     */
    public static function getNewProducts($limit = 15)
    {
        return self::find()->where(['is_new' => 1])->joinWith(['image', 'brand'])->orderBy('created_at DESC')->limit($limit)->all();
    }

    /**
     * Get products by category
     * @param int $category_id
     * @param array $params
     * @param int $limit
     * @return ProductsQuery
     */
    public static function byCategory($category_id, $params, $limit = 15)
    {
        $query = self::find()->joinWith(['categoriesAssoc', 'image', 'characteristics'])->where([ProductsCategoriesAssoc::tableName() . '.category_id' => $category_id])->cache(3600 * 24)->orderBy('created_at DESC')->groupBy(Products::tableName() . '.id');

        self::search_params($query, $params);

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * Search products by params
     * @param ProductsQuery $query
     * @param array $params
     * @return int
     */
    public static function search_params($query, $params)
    {
        foreach ($params as $key => $value) {
            if (in_array($key, self::$attrs_list))
                $query->andWhere([self::tableName() . '.' . $key => $value]);
            else {
                if ($key == 'params') {

                    $query_params = new Query();

                    $query_params->select('product_id')
                        ->from('product_params_assoc')
                        ->groupBy('product_id');

                    $params_ids = [];
                    $values_ids = [];

                    $amount_having = 0;

                    $param_index = 0;
                    $params_condition = [];
                    $condition = '';
                    foreach ($value as $param_id => $value_id) {
                        foreach ($value_id as $value_item) {
                            $condition .= '(' . ProductParamsAssoc::tableName() . '.param_id = :p' . $param_index . ' AND ' . ProductParamsAssoc::tableName() . '.value_id = :v' . $param_index . ') OR ';
                            $params_condition[':p' . $param_index] = $param_id;
                            $params_condition[':v' . $param_index] = $value_item;

                            $param_index++;
                        }


                        $params_ids[] = $param_id;
                        $values_ids = array_merge($values_ids, $value_id);

                        $amount_having++;
                    }

                    Yii::debug('#condition');
                    Yii::debug($condition);
                    Yii::debug($params_condition);

//                    $query
//                        ->andWhere(new Expression(substr($condition, 0, -3), $params_condition))
//                        ->groupBy(ProductParamsAssoc::tableName() . '.product_id')
//                        ->having('COUNT(' . ProductParamsAssoc::tableName() . '.product_id) >= ' . $amount_having);

                    $query->andWhere([ProductParamsAssoc::tableName() . '.param_id' => $params_ids])
                        ->andWhere([ProductParamsAssoc::tableName() . '.value_id' => $values_ids])
                        ->groupBy(ProductParamsAssoc::tableName() . '.product_id')
                        ->having('COUNT(' . ProductParamsAssoc::tableName() . '.product_id) >= ' . $amount_having);

                    /*
                     * $query->andWhere([ProductParamsAssoc::tableName() . '.param_id' => $params_ids])
                        ->andWhere([ProductParamsAssoc::tableName() . '.value_id' => $values_ids])
                        ->groupBy(ProductParamsAssoc::tableName() . '.product_id')
                        ->having('COUNT(' . ProductParamsAssoc::tableName() . '.product_id) >= ' . $amount_having); */

//                    $query_params->where([ProductParamsAssoc::tableName() . '.param_id' => $params_ids])
//                        ->andWhere([ProductParamsAssoc::tableName() . '.value_id' => $values_ids])
//                        ->having('COUNT(' . ProductParamsAssoc::tableName() . '.product_id) >= ' . $amount_having);
//
//                    Yii::debug($params_ids);
//                    Yii::debug($values_ids);
//                    Yii::debug($query_params->createCommand()->rawSql . PHP_EOL . '#sql');
//
//                    $query->andWhere(Products::tableName() . '.id IN (' . $query_params->createCommand()->rawSql . ')');

                } else if ($key == 'price_start' && $value)
                    $query->andWhere(['>=', Products::tableName() . '.price', $value]);
                else if ($key == 'price_end' && $value)
                    $query->andWhere(['<=', Products::tableName() . '.price', $value]);

            }
        }
    }

    /**
     * Get products
     * @param array $params
     * @return ProductsQuery
     */
    public static function get($params = [])
    {
        $query = self::find()
            ->joinWith(['categoriesAssoc', 'image', 'characteristics'])
            ->cache(3600 * 24)
            ->orderBy('created_at DESC')
            ->groupBy(Products::tableName() . '.id');

        self::search_params($query, $params);

        return $query;
    }

    /**
     * @param int $brand_id
     * @param int $category_id
     * @param array $params
     * @param int $limit
     * @param null|string $order_by
     * @return ProductsQuery
     */
    public static function byBrand($brand_id, $category_id = 0, $params = [], $limit = 3, $order_by = null)
    {
        $query = self::find()->joinWith(['categoriesAssoc', 'image', 'characteristics'])->groupBy(self::tableName() . '.id');

        if ($order_by)
            $query->orderBy($order_by);

        self::search_params($query, $params);

        if ($category_id == 0) {
            $query->andWhere([self::tableName() . '.brand_id' => $brand_id]);
        } else {
            $query->andWhere([self::tableName() . '.brand_id' => $brand_id])
                ->andWhere([ProductsCategoriesAssoc::tableName() . '.category_id' => $category_id]);
        }

        return $query;
    }

    /**
     * Get recently viewed products
     * @return Products[]
     */
    public static function getRecentlyViewed()
    {
        $cookie = Yii::$app->request->cookies;
        $key = 'recently_viewed_products';
        if (!$cookie->has($key))
            return [];

        $recently_viewed = ($cookie->get($key))->value;

        return self::find()->where(['id' => $recently_viewed])->all();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'category_id', 'price', 'currency_id', 'status_id', 'amount_left', 'available'], 'required'],
            [['brand_id', 'category_id', 'discount', 'discount_due', 'discount_type', 'is_new', 'hide', 'currency_id', 'created_at', 'updated_at', 'status_id', 'amount_left'], 'integer'],
            [['vendor_code', 'import_id'], 'string'],
            [['price'], 'number'],
            [['available'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Brand ID',
            'category_id' => 'Category ID',
            'discount' => 'Discount',
            'discount_due' => 'Discount Due',
            'discount_type' => 'Discount Type',
            'is_new' => 'Is New',
            'hide' => 'Hide',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getInfo()
    {
        if (!ProductsInfo::find()->where(['record_id' => $this->id, 'lang' => Lang::getCurrentId()])->cache(3600 * 5)->exists())
            return $this->hasOne(ProductsInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 1])->cache(3600 * 5);

        return $this->hasOne(ProductsInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => Lang::getCurrentId()])->cache(3600 * 5);
    }

    public function getImage()
    {
        return $this->hasOne(ImageManager::className(), ['record_id' => 'id'])->andWhere([ImageManager::tableName() . '.module' => self::tableName()])->orderBy('sort');
    }

    public function getImagesLinks()
    {
        return ArrayHelper::getColumn($this->images, 'imageUrl');
    }

    public function getImagesLinksData()
    {
        return ArrayHelper::toArray($this->images, [
                ImageManager::className() => [
                    'caption' => 'path',
                    'key' => 'id',
                ]]
        );
    }

    public function beforeDelete()
    {
        if ($this->getImages()) {
            foreach ($this->images as $one) {
                @unlink(Yii::getAlias('@images') . '/' . self::tableName() . '/' . $one->path);
            }
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function getImages()
    {
        return $this->hasMany(ImageManager::className(), ['record_id' => 'id'])->andWhere([ImageManager::tableName() . '.module' => self::tableName()])->orderBy('sort');
    }

    /**
     * @return string
     */
    public function getCurrencyLabel()
    {
        return self::$price_labels[$this->currency_id];
    }

    /**
     * @return string
     */
    public function getDiscountLabel()
    {
        return self::$discount_labels[$this->discount_type];
    }

    /**
     * @return ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }

    /**
     * Get price with discount
     * @return float|int
     */
    public function getDiscountPrice()
    {
        if (!$this->discount)
            return $this->price;

        if ($this->discount_type == self::DISCOUNT_PERCENT)
            return round($this->price - ($this->price * $this->discount / 100), 2);
        else if ($this->discount_type == self::DISCOUNT_CASH)
            return $this->price - $this->discount;

        return $this->price;
    }

    /**
     * @return ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['record_id' => 'id'])->andWhere([Reviews::tableName() . '.module' => self::tableName(), Reviews::tableName() . '.reply_to' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategoriesAssoc()
    {
        return $this->hasMany(ProductsCategoriesAssoc::className(), ['product_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function getAverageScore()
    {
        $cache = Yii::$app->cache;
        $key = 'product_average_score_' . $this->id;
        $average_score_cache = $cache->get($key);
        if ($average_score_cache)
            return $average_score_cache;

        $reviews = Reviews::find()->products()->confirmed()->main()->andWhere(['record_id' => $this->id])->limit(1000)->all();
        $count_reviews = count($reviews);
        $score = 0;
        $average_score = 0;
        foreach ($reviews as $review) {
            $score += $review->score;
        }

        if ($count_reviews > 0)
            $average_score = round($score / $count_reviews, 1);

        $cache->set($key, $average_score, 3600 * 24 * 3);

        return $average_score;
    }

    /**
     * @return bool|int|string|null
     */
    public function getReviewsAmount()
    {
        return Reviews::find()->products()->confirmed()->andWhere(['record_id' => $this->id])->cache(3600 * 24)->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getLikesAmount()
    {
        return Likes::find()->where(['product_id' => $this->id])->cache(3600 * 24)->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getOrdersAmount()
    {
        return Orders::find()->joinWith(['products'])->where([OrderItems::tableName() . '.product_id' => $this->id])->cache(3600 * 24)->count();
    }

    public function getReviewsSummary()
    {
        $cache = Yii::$app->cache;
        $key = 'product_review_summary_' . $this->id;
        $product_review_summary = $cache->get($key);
        if ($product_review_summary)
            return $product_review_summary;

        $reviews = Reviews::find()->products()->confirmed()->main()->andWhere(['record_id' => $this->id])->limit(1000)->all();
        $count_reviews = count($reviews);
        $small_size = 0;
        $correct_color = 0;
        $perfect_quality = 0;
        $delivery_in_time = 0;
        foreach ($reviews as $review) {
            $small_size += $review->small_size;
            $correct_color += $review->correct_color;
            $perfect_quality += $review->perfect_quality;
            $delivery_in_time += $review->delivery_in_time;
        }

        $product_summary = [
            'small_size' => 0,
            'correct_color' => 0,
            'perfect_quality' => 0,
            'delivery_in_time' => 0,
        ];

        if ($count_reviews > 0) {
            $product_summary = [
                'small_size' => $small_size * 100 / $count_reviews,
                'correct_color' => $correct_color * 100 / $count_reviews,
                'perfect_quality' => $perfect_quality * 100 / $count_reviews,
                'delivery_in_time' => $delivery_in_time * 100 / $count_reviews,
            ];
        }

        $cache->set($key, $product_summary, 3600 * 24 * 3);

        return $product_summary;
    }

    /**
     * @return bool
     */
    public function getIsSmallSize()
    {
        return $this->reviewsSummary['small_size'] > self::$percentPoint;
    }

    /**
     * @return bool
     */
    public function getIsCorrectColor()
    {
        return $this->reviewsSummary['correct_color'] > self::$percentPoint;
    }

    /**
     * @return bool
     */
    public function getIsPerfectQuality()
    {
        return $this->reviewsSummary['perfect_quality'] > self::$percentPoint;
    }

    /**
     * @return bool
     */
    public function getIsDeliveryInTime()
    {
        return $this->reviewsSummary['delivery_in_time'] > self::$percentPoint;
    }

    public function getBreadcrumbsData()
    {
        $cache = Yii::$app->cache;
        $key = 'product_breadcrumbs_' . $this->id;
        $cached_breadcrumbs = $cache->get($key);
        if ($cached_breadcrumbs)
            return $cached_breadcrumbs;

        $category = Categories::findOne($this->category_id);

        $breadcrumbs = [];
        if ($category->path != '-1') {
            $categories_data = explode('.', $category->path);
            foreach ($categories_data as $category_id) {
                $category_item = Categories::findOne($category_id);
                $breadcrumbs[] = ['label' => $category_item->info->name, 'url' => Url::to(['/category/index', 'alias' => $category_item->name_alt])];
            }
        }

        $breadcrumbs[] = ['label' => $category->info->name, 'url' => Url::to(['/category/index', 'alias' => $category->name_alt])];
        $breadcrumbs[] = ['label' => $this->info->name, 'url' => '#'];

        $cache->set($key, $breadcrumbs, 3600 * 24);

        return $breadcrumbs;
    }

    /**
     * @return ActiveQuery
     */
    public function getBag()
    {
        return $this->hasMany(Bag::className(), ['product_id' => 'id']);
    }

    /**
     * Checks if user can change the order as a seller
     * @param int $user_id
     * @return bool
     */
    public function hasAdminPermissions($user_id)
    {
        return $this->brand->user_id == $user_id;
    }

    /**
     * Add a characteristic to the product and add param value if it does not exist
     * @param int $param_id
     * @param string $value_name
     * @return bool
     */
    public function addCharacteristicWithParam($param_id, $value_name)
    {
        $value = ProductParamsValues::findOne(['param_id' => $param_id, 'value' => $value_name]);
        if (!$value) {
            $value = new ProductParamsValues(['param_id' => $param_id, 'value' => $value_name]);

            $value->save();
        }

        return $this->addCharacteristic($param_id, $value->id);
    }

    /**
     * Add a characteristic to the product
     * @param int $param_id
     * @param int $value_id
     * @return bool
     */
    public function addCharacteristic($param_id, $value_id)
    {
        if (!ProductParamsAssoc::find()->where(['product_id' => $this->id, 'param_id' => $param_id, 'value_id' => $value_id])->exists()
            && ProductParams::find()->where(['id' => $param_id])->cache(3600 * 24)->exists()
            && ProductParamsValues::find()->where(['id' => $value_id, 'param_id' => $param_id])->cache(3600 * 24)->exists()) {
            $assoc_char = new ProductParamsAssoc([
                'product_id' => $this->id,
                'param_id' => $param_id,
                'value_id' => $value_id,
            ]);

            return $assoc_char->save();
        }

        return true;
    }

    /**
     * @param string $param
     * @param string $value
     * @return bool
     */
    public function addNewParam($param, $value)
    {
        $param_data = ProductParams::findOne(['name' => $param]);
        if (!$param_data) {
            $param_data = new ProductParams(['name' => $param]);
            $param_data->save();
        }

        $value_data = ProductParamsValues::findOne(['param_id' => $param_data->id, 'value' => $value]);
        if (!$value_data) {
            $value_data = new ProductParamsValues(['param_id' => $param_data->id, 'value' => $value]);
            $value_data->save();
        }

        return $this->addCharacteristic($param_data->id, $value_data->id);
    }

    /**
     * @return ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this->hasMany(ProductParamsAssoc::className(), ['product_id' => 'id'])->cache(3600 * 24);
    }

    /**
     * Associate the product with new category
     * @param int $category_id
     * @param bool $force
     * @return bool
     */
    public function addNewCategory($category_id, $force = false)
    {
        if ($this->category_id == $category_id && !$force)
            return true;

        $this->category_id = $category_id;

        $category = Categories::findOne($category_id);
        if ($category) {
            ProductsCategoriesAssoc::deleteAll(['product_id' => $this->id]);

            $data = explode('.', $category->path);

            foreach ($data as $item) {
                $assoc = new ProductsCategoriesAssoc([
                    'product_id' => $this->id,
                    'category_id' => $item,
                ]);

                $assoc->save();
            }

            $assoc = new ProductsCategoriesAssoc([
                'product_id' => $this->id,
                'category_id' => $this->category_id,
            ]);

            $assoc->save();

            return true;
        }

        return false;
    }

    /**
     * Add size to the product
     * @param int $size
     * @param string $size_international
     * @return bool
     */
    public function addSize($size, $size_international)
    {
        if (!ProductSizes::find()->where(['product_id' => $this->id, 'size' => $size, 'size_international' => $size_international])->exists()) {
            $product_size = new ProductSizes([
                'product_id' => $this->id,
                'size' => $size,
                'size_international' => $size_international
            ]);

            return $product_size->save();
        }

        return true;
    }

    /**
     * @return ActiveQuery
     */
    public function getSizes()
    {
        return $this->hasMany(ProductSizes::className(), ['product_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getColors()
    {
        return $this->hasMany(ProductParamsAssoc::className(), ['product_id' => 'id'])
            ->andWhere([ProductParamsAssoc::tableName() . '.param_id' => ProductParams::PARAM_COLOR]);
    }

    /**
     * @param $url
     * @return bool
     * @throws \yii\base\Exception
     */
    public function addNewImageByUrl($url)
    {
        $url_data = explode('/', $url);
        $file_name = $url_data[count($url_data) - 1];
        $dir_name = substr($file_name, 0, 2);

        $uploads_path = dirname(dirname(__DIR__)) . '/frontend/web/uploads/' . Products::tableName() . '/';
        $file_full_new_name = $uploads_path . $dir_name . '/' . $file_name;

        if (file_exists($file_full_new_name))
            return true;

        FileHelper::createDirectory($uploads_path . $dir_name);

        if (file_put_contents($file_full_new_name, file_get_contents($url))) {
            $im = new ImageManager(['module' => Products::tableName()]);
            $im->record_id = $this->id;
            $im->path = $dir_name . '/' . $file_name;

            return $im->save();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getIsLiked()
    {
        return Likes::find()->where(['user_id' => Yii::$app->user->getId(), 'product_id' => $this->id])->exists();
    }
}
