<?php

namespace common\models;

use yii\helpers\Url;

/**
 * This is the model class for table "image_manager".
 *
 * @property int $id
 * @property string $module
 * @property int $record_id
 * @property string $path
 * @property int|null $sort
 * @property string $imageUrlWithoutClass
 * @property string $imageUrl
 * @property string $extension
 * @property-read string $fullPath
 * @property string $attachment
 */
class ImageManager extends \yii\db\ActiveRecord
{
    public $attachment;

    public $_ext;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'sort'], 'integer'],
            [['sort'], 'default', 'value' => function ($model) {
                $count = ImageManager::find()->andWhere([
                    'module' => $model->module, 'record_id' => $model->record_id])->count();
                return ($count > 0) ? $count++ : 0;
            }],
            [['path', 'module'], 'string', 'max' => 255],
            [['attachment'], 'image'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Module',
            'record_id' => 'Record ID',
            'path' => 'Path',
            'sort' => 'Sort',
        ];
    }

    public function getExtension()
    {
        if ($this->_ext)
            return $this->_ext;

        $parts = explode('.', $this->path);

        $this->_ext = $parts[count($parts) - 1];
        return $this->_ext;
    }

    public function getImageUrl()
    {
        $path_ = dirname(dirname(__DIR__)) . '/frontend/web/uploads/';
        if (file_exists($path_ . $this->module . '/' . $this->path) && is_file($path_ . $this->module . '/' . $this->path))
            return '/uploads/' . $this->module . '/' . $this->path;
        else
            return '/assets/images/no-image.png';
    }

    public function getImageUrlWithoutClass()
    {
        if ($this->path) {
            $path = '/uploads/' . strtolower($this->path);
        } else {
            $path = Url::home(true) . 'web/no-image.png';
        }
        return $path;
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return dirname(dirname(__DIR__)) . '/frontend/web/uploads/' . $this->module . '/' . $this->path;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function removeSelf()
    {
        return $this->deleteImage() && $this->delete();
    }

    /**
     * @return bool
     */
    public function deleteImage()
    {
        return @unlink($this->fullPath);
    }
}
