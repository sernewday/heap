<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_sizes".
 *
 * @property int $id
 * @property int $product_id
 * @property int $size
 * @property string $size_international
 */
class ProductSizes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_sizes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'size', 'size_international'], 'required'],
            [['product_id', 'size'], 'integer'],
            [['size_international'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'size' => 'Size',
            'size_international' => 'Size International',
        ];
    }
}
