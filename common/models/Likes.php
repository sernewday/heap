<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "likes".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int $created_at
 * @property-read Products $product
 * @property int $updated_at
 */
class Likes extends \yii\db\ActiveRecord
{
    /**
     * @param $user_id
     * @return bool|int|string|null
     */
    public static function getAmountByUser($user_id)
    {
        return self::find()->where(['user_id' => $user_id])->count();
    }

    /**
     * @param $user_id
     * @param array $params
     * @return \yii\db\ActiveQuery
     */
    public static function getByUser($user_id, $params = [])
    {
        return self::find()
            ->where([self::tableName() . '.user_id' => $user_id])
            ->joinWith(['product'])
            ->orderBy(self::tableName() . '.created_at DESC');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'likes';
    }

    /**
     * @param $user_id
     * @param $product_id
     * @return bool|int
     */
    public static function like($user_id, $product_id)
    {
        if (self::find()->where(['user_id' => $user_id, 'product_id' => $product_id])->exists())
            return self::deleteAll(['user_id' => $user_id, 'product_id' => $product_id]);

        $like = new Likes(['user_id' => $user_id, 'product_id' => $product_id]);

        return $like->save();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
