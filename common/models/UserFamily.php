<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_family".
 *
 * @property int $id
 * @property int $user_id
 * @property string $full_name
 * @property int $gender
 * @property string $birthday
 * @property int $size_clothes
 * @property int $size_shoes
 * @property int $size_chest
 * @property int $size_waist
 * @property int $size_hips
 * @property int $created_at
 * @property-read string $personIcon
 * @property int $updated_at
 */
class UserFamily extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_family';
    }

    /**
     * @param $user_id
     * @return UserFamily[]
     */
    public static function getUserFamily($user_id)
    {
        return self::find()->where(['user_id' => $user_id])->orderBy('created_at')->all();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'full_name', 'gender', 'birthday', 'size_clothes', 'size_shoes', 'size_chest', 'size_waist', 'size_hips'], 'required'],
            [['user_id', 'gender', 'size_clothes', 'size_shoes', 'size_chest', 'size_waist', 'size_hips', 'created_at', 'updated_at'], 'integer'],
            [['birthday'], 'safe'],
            [['full_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'full_name' => 'Full Name',
            'gender' => 'Gender',
            'birthday' => 'Birthday',
            'size_clothes' => 'Size Clothes',
            'size_shoes' => 'Size Shoes',
            'size_chest' => 'Size Chest',
            'size_waist' => 'Size Waist',
            'size_hips' => 'Size Hips',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return string
     */
    public function getPersonIcon()
    {
        $date = explode('-', $this->birthday);

        $age = date('Y') - $date[0];
        if ($age <= 10)
            return 'child.svg';
        else if ($age >= 10 && $age < 18) {
            if ($this->gender == User::GENDER_MALE)
                return 'boy.svg';
            else
                return 'girl.svg';
        }

        if ($this->gender == User::GENDER_MALE)
            return 'men.svg';

        return 'woman.svg';
    }
}
