<?php

namespace common\models;

use common\models\Queries\OrdersQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property int $seller_id
 * @property string $full_name
 * @property string $phone
 * @property string $email
 * @property string $city
 * @property int|null $post_number
 * @property string|null $street
 * @property string|null $house
 * @property string|null $flat
 * @property int $delivery_type
 * @property int $payment_type
 * @property string $comment
 * @property int $status_id
 * @property int $created_at
 * @property User $user
 * @property OrderItems[] $products
 * @property Brands $seller
 * @property float|int $orderSum
 * @property-read string $statusLabel
 * @property-read string|false $time
 * @property-read string|false $date
 * @property-read string $deliveryLabel
 * @property-read string $fullAddress
 * @property-read string $paymentLabel
 * @property-read OrderHistory[] $history
 * @property-read string $statusClass
 * @property-read string $statusIcon
 * @property-read null|int $statusDate
 * @property-read string $payAccountName
 * @property int $updated_at
 */
class Orders extends ActiveRecord
{
    /**
     * Delivery by new post
     */
    const DELIVERY_NEW_POST = 1;

    /**
     * Delivery to address
     */
    const DELIVERY_ADDRESS = 2;

    /**
     * Pay after delivery
     */
    const PAY_AFTER = 1;

    /**
     * Pay online on the site
     */
    const PAY_ONLINE = 2;

    /**
     * Transfer from card to card
     */
    const PAY_TRANSFER = 3;

    // Possible statuses
    const STATUS_NEW = 1;
    const STATUS_PAYED = 2;
    const STATUS_PROCESSING = 3;
    const STATUS_DELIVERING = 4;
    const STATUS_DELIVERED = 5;
    const STATUS_SUCCESS = 6;
    const STATUS_CANCELED = 7;
    const STATUS_RETURNING = 8;
    const STATUS_RETURNED = 9;


//    ---------------------------------------------
    const ENABLE_TEST_MODE = true;

    const WAYFORPAY_CODE_OK = 1100;
//    ---------------------------------------------

    /**
     * @var string[] Labels for order statuses
     */
    public static $order_statuses = [
        self::STATUS_NEW => 'Новый заказ',
        self::STATUS_PROCESSING => 'В обработке',
        self::STATUS_PAYED => 'Оплачен',
        self::STATUS_DELIVERING => 'Доставляется',
        self::STATUS_DELIVERED => 'Заказ доставлен',
        self::STATUS_SUCCESS => 'Заказ выполнен',
        self::STATUS_CANCELED => 'Заказ отменён',
        self::STATUS_RETURNING => 'Возвращается',
        self::STATUS_RETURNED => 'Заказ возвращен',
    ];
    /**
     * @var string[] Classes for statuses
     */
    public static $status_classes = [
        self::STATUS_NEW => 'framed',
        self::STATUS_PAYED => 'paid',
        self::STATUS_PROCESSING => 'sent',
        self::STATUS_DELIVERING => 'delivered',
        self::STATUS_DELIVERED => 'delivered',
        self::STATUS_SUCCESS => 'done',
        self::STATUS_CANCELED => 'no-done',
        self::STATUS_RETURNING => 'no-done',
        self::STATUS_RETURNED => 'no-done',
    ];
    /**
     * @var string[] Files svg for statuses
     */
    public static $status_files = [
        self::STATUS_NEW => 'processing.svg',
        self::STATUS_PROCESSING => 'processing.svg',
        self::STATUS_PAYED => 'processing.svg',
        self::STATUS_DELIVERING => 'processing.svg',
        self::STATUS_DELIVERED => 'processing.svg',
        self::STATUS_SUCCESS => 'ok.svg',
        self::STATUS_CANCELED => 'error.svg',
        self::STATUS_RETURNING => 'error.svg',
        self::STATUS_RETURNED => 'error.svg',
    ];
    /**
     * @var string[] Labels for order deliveries
     */
    public static $order_deliveries = [
        self::DELIVERY_NEW_POST => 'Доставка в отделение Новой почты',
        self::DELIVERY_ADDRESS => 'Адресная доставка',
    ];
    /**
     * @var string[] Labels for order payment types
     */
    public static $order_payments = [
        self::PAY_AFTER => 'Наложенный платёж',
        self::PAY_ONLINE => 'Картой на сайте',
        self::PAY_TRANSFER => 'Перевод с карты на карту',
    ];
    private $merchantAccount = 'test_merch_n1';
    private $merchantSecretKey = 'flk3409refn54t54t*FNJRET';

    /**
     * @var Brands
     */
    private $_seller;

    /**
     * Get new order ID
     * @return int
     */
    public static function getNewOrderNum()
    {
        $last_order = self::find()->select(['id'])->orderBy('id DESC')->limit(1)->one();

        return $last_order ? ($last_order->id + 1) : 1;
    }

    /**
     * {@inheritdoc}
     * @return OrdersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrdersQuery(get_called_class());
    }

    /**
     * Get last n orders
     * @param int $seller_id
     * @param int $limit
     * @return Orders[]
     */
    public static function getLastBySeller($seller_id, $limit = 3)
    {
        return self::find()
            ->where(['seller_id' => $seller_id])
            ->joinMain()
            ->orderBy('created_at DESC')
            ->limit($limit)
            ->all();
    }

    /**
     * Get last n orders
     * @param int $limit
     * @return Orders[]
     */
    public static function getLast($limit = 3)
    {
        return self::find()
            ->joinMain()
            ->orderBy('created_at DESC')
            ->limit($limit)
            ->groupBy(Orders::tableName() . '.id')
            ->all();
    }

    /**
     * Get orders by seller
     * @param int $seller_id
     * @param array $params
     * @return OrdersQuery
     */
    public static function getBySeller($seller_id, $params = [])
    {
        $params['seller_id'] = $seller_id;

        $model = self::search($params);
        $model->joinMain()->orderBy('created_at DESC')->groupBy(self::tableName() . '.id');

        return $model;
    }

    /**
     * Get orders
     * @param array $params
     * @return OrdersQuery
     */
    public static function get($params = [])
    {
        $model = self::search($params);
        $model->joinMain()->orderBy('created_at DESC')->groupBy(self::tableName() . '.id');

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * Search orders
     * @param array $params
     * @return OrdersQuery
     */
    public static function search($params = [])
    {
        $model = self::find();

        foreach ($params as $key => $param) {
            $model->andWhere([Orders::tableName() . '.' . $key => $param]);
        }

        return $model;
    }

    public static function getUserOrders($user_id)
    {
        return self::find()
            ->where(['user_id' => $user_id])
            ->joinWith(['products', 'products.product', 'products.product.image'])
            ->orderBy('created_at DESC')
            ->all();
    }

    /**
     * Get amount of orders in the last month
     * @param int $seller_id
     * @return int
     */
    public static function getAmountMonth($seller_id)
    {
        $start_of_the_month = '01.' . date('m') . '.' . date('Y') . ' 03:00:00';

        return self::find()
            ->where(['seller_id' => $seller_id])
            ->andWhere(['>=', 'created_at', strtotime($start_of_the_month)])
            ->cache(3600)
            ->count();
    }

    /**
     * @param $id
     * @return Orders|null
     */
    public static function getOneForSeller($id)
    {
        return self::find()
            ->where([self::tableName() . '.id' => $id])
            ->joinWith(['products', 'products.product', 'products.product.image', 'user', 'user.info', 'history'])
            ->one();
    }

    /**
     * Get amount of active orders by user id
     * @param $user_id
     * @return int
     */
    public static function getAmountActiveByUser($user_id)
    {
        return self::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['status_id' => [self::STATUS_NEW, self::STATUS_PAYED, self::STATUS_PROCESSING, self::STATUS_DELIVERING, self::STATUS_DELIVERED]])
            ->count();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'full_name', 'phone', 'email', 'city', 'payment_type', 'seller_id'], 'required'],
            [['user_id', 'post_number', 'delivery_type', 'payment_type', 'created_at', 'updated_at', 'seller_id'], 'integer'],
            [['full_name', 'phone', 'email', 'city', 'comment', 'street', 'house', 'flat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'full_name' => 'Full Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'city' => 'City',
            'post_number' => 'Post Number',
            'street' => 'Street',
            'house' => 'House',
            'flat' => 'Flat',
            'delivery_type' => 'Delivery Type',
            'payment_type' => 'Payment Type',
            'comment' => 'Comment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return Brands
     */
    public function getSeller()
    {
        if (!$this->_seller)
            $this->_seller = Brands::find()->where(['id' => $this->products[0]->product->brand_id])->one();

        return $this->_seller;
    }

    /**
     * @return float|int
     */
    public function getOrderSum()
    {
        $sum = 0;
        foreach ($this->products as $product) {
            $sum += $product->price;
        }

        return $sum;
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        return self::$order_statuses[$this->status_id];
    }

    /**
     * Checks if user can view the order as a seller
     * @param int $user_id
     * @return bool
     */
    public function hasAdminPermissions($user_id)
    {
        return $this->seller->user_id == $user_id;
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return date('d.m.Y', $this->created_at);
    }

    /**
     * @return false|string
     */
    public function getTime()
    {
        return date('H:i', $this->created_at);
    }

    /**
     * @return string
     */
    public function getDeliveryLabel()
    {
        return self::$order_deliveries[$this->delivery_type];
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        if ($this->delivery_type == self::DELIVERY_NEW_POST)
            return $this->city . ', ' . Yii::t('app', 'отделение') . ' № ' . $this->post_number;

        return "{$this->city}, ул. {$this->street}, дом {$this->house}" . ($this->flat ? ", кв. {$this->flat}" : '');
    }

    /**
     * @return string
     */
    public function getPaymentLabel()
    {
        return self::$order_payments[$this->payment_type];
    }

    /**
     * @return ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(OrderHistory::className(), ['order_id' => 'id'])->orderBy(OrderHistory::tableName() . '.created_at ASC');
    }

    /**
     * @return string
     */
    public function getStatusClass()
    {
        return self::$status_classes[$this->status_id];
    }

    /**
     * @return string
     */
    public function getStatusIcon()
    {
        return '/assets/img/statuses/' . self::$status_files[$this->status_id];
    }

    /**
     * Setting a new status to the order
     * @param int $status
     * @param null|string $description
     * @return bool
     */
    public function setNewStatus($status, $description = null)
    {
        $order_history = new OrderHistory(['order_id' => $this->id]);
        $order_history->order_status = $status;
        $order_history->description = $description;

        $this->status_id = $status;

        if ($order_history->save() && $this->save()) {
            return Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'status_order-change'],
                    ['user' => $this->user, 'order' => $this]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($this->user->email)
                ->setSubject('Изменён статус заказа - ' . Yii::$app->name)
                ->send();
        }

        return false;
    }

    /**
     * @return int|null
     */
    public function getStatusDate()
    {
        $status = OrderHistory::findOne(['order_id' => $this->id, 'order_status' => $this->status_id]);
        if ($status)
            return $status->created_at;

        return null;
    }

    /**
     * Check if the order can be cancelled
     * @return bool
     */
    public function canBeCancelled()
    {
        if (in_array($this->status_id, [self::STATUS_NEW, self::STATUS_PAYED, self::STATUS_PROCESSING]))
            return true;

        return false;
    }

    /**
     * Check if the order can be returned
     * @return bool
     */
    public function canBeReturned()
    {
        if ($this->status_id == self::STATUS_SUCCESS && (time() - $this->statusDate < (3600 * 24 * 14)))
            return true;

        return false;
    }

    /**
     * Generates signature for payment
     * @return string
     */
    public function generateSignature()
    {
        $data_string = (self::ENABLE_TEST_MODE ? $this->merchantAccount : $this->seller->decryptedMerchantName) . ";" . Yii::$app->request->serverName . ";";

        if (self::ENABLE_TEST_MODE)
            $data_string .= "test_";

        $data_string .= "r_" . $this->id . ";{$this->created_at};" . (self::ENABLE_TEST_MODE ? 1 : $this->orderSum) . ";UAH;";

        $products_data = [];
        $prices_data = [];
        $amount_data = [];

        $products = $this->products;
        foreach ($products as $product) {
            $products_data[] = $product->product->info->name;
            $prices_data[] = self::ENABLE_TEST_MODE ? 1 : $product->price_for_one;
            $amount_data[] = $product->amount;
        }

        $data_string .= implode(';', $products_data) . ';' . implode(';', $amount_data) . ';' . implode(';', $prices_data);

//        return $data_string;
        return hash_hmac("md5", $data_string, self::ENABLE_TEST_MODE ? $this->merchantSecretKey : $this->seller->decryptedMerchantSecret);
    }

    /**
     * @return string
     */
    public function getPayAccountName()
    {
        return self::ENABLE_TEST_MODE ? $this->merchantAccount : $this->seller->decryptedMerchantName;
    }

    /**
     * Generates signature for answer
     * @param array $data
     * @return string
     */
    public function generateSecretAnswer($data)
    {
        return hash_hmac('md5', "{$data['orderReference']};{$data['status']};{$data['time']}", $this->merchantSecretKey);
    }
}
