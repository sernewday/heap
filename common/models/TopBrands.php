<?php

namespace common\models;

use common\models\Queries\TopBrandsQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "top_brands".
 *
 * @property int $id
 * @property int $brand_id
 * @property int $type
 * @property int $due_to
 * @property float $cost
 * @property int $created_at
 * @property int $updated_at
 */
class TopBrands extends ActiveRecord
{
    // Advertising types
    const TYPE_MAIN = 1;
    const TYPE_TOP30 = 2;

    /**
     * @var double[]
     */
    public static $pricesOneDay = [
        self::TYPE_MAIN => 300,
        self::TYPE_TOP30 => 290,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'top_brands';
    }

    /**
     * {@inheritdoc}
     * @return TopBrandsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TopBrandsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'type', 'due_to', 'cost'], 'required'],
            [['brand_id', 'type', 'due_to', 'created_at', 'updated_at'], 'integer'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Brand ID',
            'type' => 'Type',
            'due_to' => 'Due To',
            'cost' => 'Cost',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
