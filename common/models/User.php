<?php

namespace common\models;

use frontend\components\SMS;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $verification_phone_token
 * @property string $email
 * @property string $phone
 * @property string $auth_key
 * @property bool $confirmed_email
 * @property bool $confirmed_phone
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $authKey
 * @property UserInfo $info
 * @property-read string $fullName
 * @property string $password write-only password
 * @property-read null|string|int $role
 * @property-read AuthAssignment $assignment
 * @property-read null|bool|string|int $amountOfOrders
 * @property-read null|bool|string|int $amountOfReviews
 * @property-read UserFamily[] $family
 * @property integer $user_type
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    // User types
    const TYPE_CUSTOMER = 1;
    const TYPE_SELLER = 2;
    const TYPE_ADMIN = 3;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Returns user profile progress
     * @param int $user_id
     * @return int
     */
    public static function getProfileProgress($user_id)
    {
        $user = self::findOne($user_id);
        if (!$user)
            return 0;

        // Amount of items to be filled in. E.g. if there's 15 items and all of them are filled in, it's 100%
        $amount_items = 2;

        // Amount of filled in items
        $progress = 0;

        if ($user->email)
            $progress++;

        if ($user->phone)
            $progress++;

        $info = UserInfo::findOne(['user_id' => $user_id]);
        if ($info) {
            $attributes = $info->getAttributes();

            foreach ($attributes as $attribute) {
                $amount_items++;

                if ($attribute)
                    $progress++;
            }

            // Don't consider `user_id`
            $progress--;
            $amount_items--;
        }

        return round($progress * 100 / $amount_items);
    }

    /**
     * @return ActiveQuery
     */
    public static function getCustomers()
    {
        return self::find()
            ->joinWith(['assignment', 'info'])
            ->where([AuthAssignment::tableName() . '.item_name' => 'customer'])
            ->orderBy(self::tableName() . '.created_at DESC');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generates an auth key
     * @param bool $insert
     * @return bool
     * @throws Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    /**
     * @return ActiveQuery
     */
    public function getInfo()
    {
        return $this->hasOne(UserInfo::className(), ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->info->surname . ' ' . $this->info->name;
    }

    /**
     * @param bool $use_link
     * @return bool
     * @throws Exception
     */
    public function sendEmailVerification($use_link = false)
    {
        $token = $this->generateEmailVerificationToken();

        Yii::$app->session->set('verification_code', $this->verification_token);

        $this->status = self::STATUS_INACTIVE;
        $this->confirmed_email = 0;

        $this->save();

        $link = '';

        if ($use_link)
            $link = Url::to(['/profile/verify-email', 'u' => $this->id, 'c' => $token], true);

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html'],
                ['user' => $this, 'token' => $token, 'use_link' => $use_link, 'link' => $link]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }

    /**
     * Generates new token for email verification
     * @return int
     * @throws Exception
     */
    public function generateEmailVerificationToken()
    {
        $token = mt_rand(10000, 99999);
        $this->verification_token = Yii::$app->security->generatePasswordHash($token);
        return $token;
    }

    /**
     * @return ResponseInterface
     * @throws Exception
     * @throws GuzzleException
     */
    public function sendPhoneVerification()
    {
        $token = $this->generatePhoneVerificationToken();

        Yii::$app->session->set('verification_phone_code', $this->verification_phone_token);

        $this->confirmed_phone = 0;

        $this->save();

        return SMS::send([str_replace('+', '', $this->phone)], 'Код подтверждения: ' . $token);
    }

    /**
     * Generates new token for phone verification
     * @return int
     * @throws Exception
     */
    public function generatePhoneVerificationToken()
    {
        $token = mt_rand(10000, 99999);
        $this->verification_phone_token = Yii::$app->security->generatePasswordHash($token);
        return $token;
    }

    public function setEmailActive()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->confirmed_email = 1;
    }

    /**
     * @return int|string|null
     */
    public function getRole()
    {
        return self::getRoleById($this->id);
    }

    /**
     * @param int $user_id
     * @return int|string|null
     */
    public static function getRoleById($user_id)
    {
        return array_key_first(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()));
    }

    /**
     * @return ActiveQuery
     */
    public function getAssignment()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return bool|int|string|null
     */
    public function getAmountOfOrders()
    {
        return Orders::find()->where(['user_id' => $this->id])->cache(3600 * 24)->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getAmountOfReviews()
    {
        return Reviews::find()->where(['user_id' => $this->id])->cache(3600 * 24)->count();
    }

    /**
     * @return ActiveQuery
     */
    public function getFamily()
    {
        return $this->hasMany(UserFamily::className(), ['user_id' => 'id']);
    }
}
