<?php

namespace common\models\Infos;

/**
 * This is the model class for table "products_info".
 *
 * @property int $record_id
 * @property int $lang
 * @property string $name
 * @property string $description
 */
class ProductsInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'name', 'description'], 'required'],
            [['record_id', 'lang'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['record_id', 'lang'], 'unique', 'targetAttribute' => ['record_id', 'lang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }
}
