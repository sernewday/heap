<?php

namespace common\models\Infos;

use Yii;

/**
 * This is the model class for table "categories_info".
 *
 * @property int $record_id
 * @property int $lang
 * @property string $name
 */
class CategoriesInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'name'], 'required'],
            [['record_id', 'lang'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['record_id', 'lang'], 'unique', 'targetAttribute' => ['record_id', 'lang']],

            [['name'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'name' => 'Name',
        ];
    }
}
