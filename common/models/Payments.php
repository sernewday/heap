<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $payment_type
 * @property int|null $order_id
 * @property float|null $order_cost
 * @property float|null $balance_add
 * @property float|null $balance_charge
 * @property float $current_balance
 * @property int|null $brand_id
 * @property int $created_at
 * @property-read string $htmlClass
 * @property-read string|false $time
 * @property-read string|false $date
 * @property-read string $description
 * @property int $updated_at
 */
class Payments extends \yii\db\ActiveRecord
{
    // Types
    const TYPE_ADD_BALANCE = 1;
    const TYPE_PAY_FOR_SALES = 2;
    const TYPE_COMMISSION_FOR_SALES = 3;
    const TYPE_COMMISSION_RETURN = 4;
    const TYPE_ADVERTISING = 5;

    /**
     * @var string[]
     */
    public static $type_classes = [
        self::TYPE_ADD_BALANCE => 'green',
        self::TYPE_PAY_FOR_SALES => 'green',
        self::TYPE_COMMISSION_FOR_SALES => 'red',
        self::TYPE_COMMISSION_RETURN => 'orange',
        self::TYPE_ADVERTISING => 'orange',
    ];

    /**
     * @var string[]
     */
    public static $type_descriptions = [
        self::TYPE_ADD_BALANCE => 'Пополнение баланса',
        self::TYPE_PAY_FOR_SALES => 'Начисление за заказ',
        self::TYPE_COMMISSION_FOR_SALES => 'Комиссия за продажу',
        self::TYPE_COMMISSION_RETURN => 'Возврат комиссии',
        self::TYPE_ADVERTISING => 'Реклама бренда',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @param int $brand_id
     * @param int $limit
     * @return Payments[]
     */
    public static function getLastByBrand($brand_id, $limit = 3)
    {
        return self::find()->where(['brand_id' => $brand_id])->orderBy('created_at DESC')->limit($limit)->all();
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Queries\PaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\Queries\PaymentsQuery(get_called_class());
    }

    /**
     * @param int $limit
     * @return Payments[]
     */
    public static function getLast($limit = 3)
    {
        return self::find()->orderBy('created_at DESC')->limit($limit)->all();
    }

    /**
     * @return array
     */
    public static function getStatisticsForAll()
    {
        $cache = \Yii::$app->cache;
        $key = 'balance_stat_for_all';
        if ($cache->exists($key))
            return $cache->get($key);

        $add = self::find()->cache(3600 * 24)->sum('balance_add');
        $charge = self::find()->cache(3600 * 24)->sum('balance_charge');

        $data = [
            'common_add' => $add,
            'common_charge' => $charge,
            'sum_all' => $add + $charge,
        ];

        $cache->set($key, $data, 3600 * 24);

        return $data;
    }

    /**
     * Get common statistics by period
     * @param int|null $brand_id
     * @param int $period
     * @return array|mixed
     */
    public static function getDataByPeriod($period, $brand_id = null)
    {
        $cache = \Yii::$app->cache;
        $key = 'payment_balance_info_' . $brand_id . '_' . $period;

        if ($cache->exists($key))
            return $cache->get($key);

        if ($brand_id)
            $payments = self::find()->where(['brand_id' => $brand_id])->andWhere(['>=', 'created_at', time() - $period])->all();
        else
            $payments = self::find()->where(['>=', 'created_at', time() - $period])->all();

        $orders = [];
        $balance_add = 0;
        $balance_charge = 0;

        foreach ($payments as $payment) {
            if ($payment->order_id && !in_array($payment->order_id, $orders))
                $orders[] = $payment->order_id;

            if ($payment->balance_add)
                $balance_add += $payment->balance_add;

            if ($payment->balance_charge)
                $balance_charge += $payment->balance_charge;
        }

        $result = [
            'orders' => count($orders),
            'balance_add' => $balance_add,
            'balance_charge' => $balance_charge,
        ];

        $cache->set($key, $result, 3600 * 24);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_type', 'current_balance'], 'required'],
            [['payment_type', 'order_id', 'brand_id', 'created_at', 'updated_at'], 'integer'],
            [['order_cost', 'balance_add', 'balance_charge', 'current_balance'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_type' => 'Payment Type',
            'order_id' => 'Order ID',
            'order_cost' => 'Order Cost',
            'balance_add' => 'Balance Add',
            'balance_charge' => 'Balance Charge',
            'current_balance' => 'Current Balance',
            'brand_id' => 'Brand ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return string
     */
    public function getHtmlClass()
    {
        return self::$type_classes[$this->payment_type];
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return date('d.m.Y', $this->created_at);
    }

    /**
     * @return false|string
     */
    public function getTime()
    {
        return date('H:i', $this->created_at);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return self::$type_descriptions[$this->payment_type];
    }
}
