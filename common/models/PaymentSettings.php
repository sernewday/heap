<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_settings".
 *
 * @property int $seller_id
 * @property string $np_name
 * @property string|null $np_description
 * @property string $transfer_name
 * @property string|null $transfer_card
 * @property string $wayforpay_name
 * @property string|null $merchant_name
 * @property string|null $merchant_secret
 * @property string|null $wayforpay_description
 */
class PaymentSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seller_id', 'np_name', 'transfer_name', 'wayforpay_name'], 'required'],
            [['seller_id'], 'integer'],
            [['np_name', 'np_description', 'transfer_name', 'transfer_card', 'wayforpay_name', 'merchant_name', 'merchant_secret', 'wayforpay_description'], 'string', 'max' => 255],
            [['seller_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'seller_id' => 'Seller ID',
            'np_name' => 'Np Name',
            'np_description' => 'Np Description',
            'transfer_name' => 'Transfer Name',
            'transfer_card' => 'Transfer Card',
            'wayforpay_name' => 'Wayforpay Name',
            'merchant_name' => 'Merchant Name',
            'merchant_secret' => 'Merchant Secret',
            'wayforpay_description' => 'Wayforpay Description',
        ];
    }
}
