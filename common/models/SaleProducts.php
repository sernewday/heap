<?php

namespace common\models;

/**
 * This is the model class for table "sale_products".
 *
 * @property int $sale_id
 * @property-read Products $product
 * @property int $product_id
 */
class SaleProducts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'product_id'], 'required'],
            [['sale_id', 'product_id'], 'integer'],
            [['sale_id', 'product_id'], 'unique', 'targetAttribute' => ['sale_id', 'product_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sale_id' => 'Sale ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
