<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_params_assoc".
 *
 * @property int $id
 * @property int $product_id
 * @property int $param_id
 * @property-read ProductParamsValues $value
 * @property-read ProductParams $param
 * @property int $value_id
 */
class ProductParamsAssoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_params_assoc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'param_id', 'value_id'], 'required'],
            [['product_id', 'param_id', 'value_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'param_id' => 'Param ID',
            'value_id' => 'Value ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(ProductParamsValues::className(), ['id' => 'value_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(ProductParams::className(), ['id' => 'param_id']);
    }
}
