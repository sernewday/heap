<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages_content_info".
 *
 * @property int $record_id
 * @property int $lang
 * @property string $title
 * @property string $text
 */
class PagesContentInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages_content_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'title', 'text'], 'required'],
            [['record_id', 'lang'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['record_id', 'lang'], 'unique', 'targetAttribute' => ['record_id', 'lang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }
}
