<?php

namespace common\models;

/**
 * This is the model class for table "sellers_balance".
 *
 * @property int $seller_id
 * @property float $balance
 */
class SellersBalance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sellers_balance';
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Queries\SellersBalanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\Queries\SellersBalanceQuery(get_called_class());
    }

    /**
     * Get balance by brand
     * @param int $brand_id
     * @return SellersBalance|null
     */
    public static function getByBrand($brand_id)
    {
        return self::find()->where(['seller_id' => $brand_id])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seller_id'], 'required'],
            [['seller_id'], 'integer'],
            [['balance'], 'number'],
            [['seller_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'seller_id' => 'Seller ID',
            'balance' => 'Balance',
        ];
    }
}
