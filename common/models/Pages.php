<?php

namespace common\models;

use frontend\components\Lang;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property-read PagesInfo $info
 * @property-read PagesContent[] $content
 * @property string $name
 */
class Pages extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getInfo()
    {
        if (!PagesInfo::find()->where(['record_id' => $this->id, 'lang' => Lang::getCurrentId()])->cache(3600 * 5)->exists())
            return $this->hasOne(PagesInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 1])->cache(3600 * 5);

        return $this->hasOne(PagesInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => Lang::getCurrentId()])->cache(3600 * 5);
    }

    /**
     * @return ActiveQuery
     */
    public function getContent()
    {
        return $this->hasMany(PagesContent::className(), ['page_id' => 'id']);
    }
}
