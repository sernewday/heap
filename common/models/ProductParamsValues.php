<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_params_values".
 *
 * @property int $id
 * @property string $value
 * @property int $param_id
 * @property string $additional_value
 */
class ProductParamsValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_params_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'param_id'], 'required'],
            [['param_id'], 'integer'],
            [['value', 'additional_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'param_id' => 'Param ID',
        ];
    }
}
