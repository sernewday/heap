<?php

namespace common\models;

use common\components\ImageComponent;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

Yii::setAlias('@images', dirname(dirname(__DIR__)) . '/frontend/web/uploads');

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    /**
     * @var UploadedFile[]
     */
    public $materials;

    /**
     * @var UploadedFile[]
     */
    public $lessonMaterial;

    public function rules()
    {
        return [
//            [['imageFiles'], 'required', 'message' => 'Поле обязательно к заполнению'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, svg, gif', 'maxFiles' => 15],
            [['lessonMaterial'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, svg, gif, mp4', 'maxFiles' => 3],
            [['materials'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, odp', 'maxFiles' => 15],
        ];
    }

    public function upload_with_name($name)
    {
        if ($this->validate()) {
            $u_id = mt_rand(0, 100);
            $path_dir = Yii::getAlias("@images");
            FileHelper::createDirectory($path_dir);
            $full_names = [];
            foreach ($this->imageFiles as $file) {
                // $full_name=$path_dir .'/'.    $file->baseName . '.' . $file->extension;
                $tmp2 = $name . '.' . $file->extension;
                $full_name = $path_dir . '/' . $tmp2;
                $file->saveAs($full_name);
                $for_model = $tmp2;
                $full_names[] = $for_model;
            }
            return $full_names;
        } else {
            return false;
        }
    }

    public function upload_mats($module)
    {
        $tmp = mt_rand(0, 100);
        $path_dir = Yii::getAlias("@images") . "/" . $module . '/' . $tmp;
        FileHelper::createDirectory($path_dir);
        $full_names = [];
        foreach ($this->lessonMaterial as $file) {
            // $full_name=$path_dir .'/'.    $file->baseName . '.' . $file->extension;
            $tmp2 = uniqid() . '.' . $file->extension;
            $full_name = $path_dir . '/' . $tmp2;
            $file->saveAs($full_name);
            $for_model = $tmp . "/" . $tmp2;
            $full_names[] = $for_model;

            try {
                ImageComponent::optimize($full_name);
            } catch (\Exception $e) {
            }
        }
        return $full_names;
    }

    public function save_materials($table, $id)
    {
        if (isset($_FILES['UploadForm'])) {
            $class_name = $table;
            $this->lessonMaterial = UploadedFile::getInstances($this, 'lessonMaterial');
            $full_names_path_image = $this->upload_mats($class_name);
            foreach ($full_names_path_image as $item) {
                $manager = new ImageManager([
                    "path" => $item,
                    "module" => $class_name,
                    "record_id" => $id,
                ]);
                $manager->save();
            }
        }

        return true;
    }

    public function upload($module)
    {
        $tmp = mt_rand(0, 100);
        $path_dir = Yii::getAlias("@images") . "/" . $module . '/' . $tmp;
        FileHelper::createDirectory($path_dir);
        $full_names = [];
        foreach ($this->imageFiles as $file) {
            // $full_name=$path_dir .'/'.    $file->baseName . '.' . $file->extension;
            $tmp2 = uniqid() . '.' . $file->extension;
            $full_name = $path_dir . '/' . $tmp2;
            $file->saveAs($full_name);
            $for_model = $tmp . "/" . $tmp2;
            $full_names[] = $for_model;
        }
        return $full_names;
    }

    public function save($table, $id)
    {
        if (isset($_FILES['UploadForm'])) {
            $class_name = $table;
            $this->imageFiles = UploadedFile::getInstances($this, 'imageFiles');
            $full_names_path_image = $this->upload($class_name);
            foreach ($full_names_path_image as $item) {
                $manager = new ImageManager([
                    "path" => $item,
                    "module" => $class_name,
                    "record_id" => $id,
                ]);
                $manager->save();
            }
        }

        return true;
    }

    public function upload_files($module)
    {
        $this->materials = UploadedFile::getInstances($this, 'materials');

        $tmp = mt_rand(0, 100);
        $path_dir = Yii::getAlias("@images") . "/" . $module . '/' . $tmp;
        FileHelper::createDirectory($path_dir);
        $full_names = [];
        foreach ($this->materials as $file) {
            // $full_name=$path_dir .'/'.    $file->baseName . '.' . $file->extension;
            $tmp2 = uniqid() . '.' . $file->extension;
            $full_name = $path_dir . '/' . $tmp2;
            $file->saveAs($full_name);
            $for_model = $tmp . "/" . $tmp2;
            $full_names[] = $for_model;
        }
        return $full_names;
    }
}