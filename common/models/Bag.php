<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bag".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property Products $product
 * @property int $quantity
 * @property int $size
 * @property-read ProductParamsValues $colorData
 * @property int $color
 */
class Bag extends \yii\db\ActiveRecord
{
    /**
     * @param int $user_id
     * @return string
     */
    public static function renderBagProducts($user_id)
    {
        if (!Yii::$app->user->isGuest)
            $bag_products = self::getUserBag($user_id);
        else
            $bag_products = Yii::$app->session->get('bag') ?: [];

        return Yii::$app->controller->renderPartial('//includes/_bag', ['bag_products' => $bag_products]);
    }

    /**
     * @param $user_id
     * @return Bag[]
     */
    public static function getUserBag($user_id)
    {
        return self::find()->joinWith(['product', 'product.image'])->where(['user_id' => $user_id])->all();
    }

    /**
     * @param int $user_id
     * @return bool
     */
    public static function bagHasProducts($user_id)
    {
        return self::find()->where(['user_id' => $user_id])->exists();
    }

    /**
     * Get bag products grouped by brands
     * @param $user_id
     * @return Brands[]
     */
    public static function getByBrands($user_id)
    {
        return Brands::find()->joinWith(['products.bag', 'products.image'])->andWhere([Bag::tableName() . '.user_id' => \Yii::$app->user->getId()])->all();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bag';
    }

    /**
     * @param $user_id
     * @return int
     */
    public static function clearUserBag($user_id)
    {
        return self::deleteAll(['user_id' => $user_id]);
    }

    /**
     * @return bool
     */
    public static function saveFromSession()
    {
        if (Yii::$app->user->isGuest || !Yii::$app->session->has('bag'))
            return false;

        $bag = Yii::$app->session->get('bag');
        foreach ($bag as $item) {
            $item->user_id = Yii::$app->user->getId();
            $item->save();
        }

        Yii::$app->session->remove('bag');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id', 'quantity', 'size', 'color'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
        ];
    }

    public function loadAttrs($data)
    {
        $this->user_id = Yii::$app->user->getId() ?: 0;

        $this->setAttributes($data, false);

        return true;
    }

    /**
     * @return bool
     */
    public function saveBag()
    {
        if (!Yii::$app->user->isGuest) {
            if (self::find()->where(['user_id' => $this->user_id, 'product_id' => $this->product_id, 'size' => $this->size, 'color' => $this->color])->exists()) {
                self::updateAllCounters(['quantity' => 1], ['user_id' => $this->user_id, 'product_id' => $this->product_id, 'size' => $this->size, 'color' => $this->color]);
                return true;
            }
            return $this->save();
        }

        $session = Yii::$app->session;
        if (!$session->has('bag'))
            $session->set('bag', []);

        $bag = $session->get('bag');
        if (!isset($bag[$this->product_id])) {
            $bag[$this->product_id] = $this;

            $session->set('bag', $bag);
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id'])->cache(3600 * 24)->orderBy('`brand_id` ASC');
    }

    /**
     * Remove a product from the bag
     * @return int
     */
    public function remove()
    {
        return self::deleteAll(['user_id' => $this->user_id, 'id' => $this->product_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColorData()
    {
        return $this->hasOne(ProductParamsValues::className(), ['id' => 'color']);
    }
}
