<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "banners".
 *
 * @property int $id
 * @property int $position
 * @property string $url
 * @property string|null $label_top
 * @property string|null $label_main
 * @property string|null $label_edge
 * @property int $status
 * @property int $created_at
 * @property-read mixed $imagesLinks
 * @property-read ImageManager[] $images
 * @property-read mixed $imagesLinksData
 * @property-read ImageManager $image
 * @property int $updated_at
 * @property int $due_to
 */
class Banners extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_PAUSED = 2;

    public static function getIds()
    {
        return self::find()->select('id')->orderBy('id DESC')->one();
    }

    /**
     * @param $position
     * @return array|Banners[]
     */
    public static function getBanners($position)
    {
        return self::find()->where(['position' => $position])->orderBy('created_at DESC')->joinWith(['image'])->all();
    }

    /**
     * @param $position
     * @return Banners|ActiveRecord|array
     */
    public static function getBanner($position)
    {
        return self::find()->where(['position' => $position])->orderBy('created_at DESC')->joinWith(['image'])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'url', 'status'], 'required'],
            [['position', 'status', 'created_at', 'updated_at', 'due_to'], 'integer'],
            [['url', 'label_top', 'label_main', 'label_edge'], 'string', 'max' => 255],
            [['url'], 'url'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Position',
            'url' => 'Url',
            'label_top' => 'Label Top',
            'label_main' => 'Label Main',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getImage()
    {
        return $this->hasOne(ImageManager::className(), ['record_id' => 'id'])->andWhere(['module' => self::tableName()])->orderBy('sort');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banners';
    }

    public function getImagesLinks()
    {
        return ArrayHelper::getColumn($this->images, 'imageUrl');
    }

    public function getImagesLinksData()
    {
        return ArrayHelper::toArray($this->images, [
                ImageManager::className() => [
                    'caption' => 'path',
                    'key' => 'id',
                ]]
        );
    }

    public function beforeDelete()
    {
        if ($this->getImages()) {
            foreach ($this->images as $one) {
                @unlink(Yii::getAlias('@images') . '/' . self::tableName() . '/' . $one->path);
            }
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function getImages()
    {
        return $this->hasMany(ImageManager::className(), ['record_id' => 'id'])->andWhere(['module' => self::tableName()])->orderBy('sort');
    }
}
