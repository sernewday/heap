<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_info".
 *
 * @property int $brand_id
 * @property string|null $contact_person
 * @property string|null $email
 * @property string|null $country
 * @property string|null $city
 * @property string|null $address
 * @property string|null $return_name
 * @property string|null $return_phone
 * @property string|null $return_email
 * @property string|null $return_city
 * @property string|null $return_np
 * @property string|null $return_comment
 */
class CompanyInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id'], 'required'],
            [['brand_id', 'return_np'], 'integer'],
            [['contact_person', 'email', 'country', 'city', 'address', 'return_name', 'return_phone', 'return_email', 'return_city', 'return_comment'], 'string', 'max' => 255],
            [['brand_id'], 'unique'],
            [['return_email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'brand_id' => 'Brand ID',
            'contact_person' => 'Contact Person',
            'email' => 'Email',
            'country' => 'Country',
            'city' => 'City',
            'address' => 'Address',
        ];
    }
}
