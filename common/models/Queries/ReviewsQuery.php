<?php

namespace common\models\Queries;

use common\models\Brands;
use common\models\Products;
use common\models\Reviews;

/**
 * This is the ActiveQuery class for [[\common\models\Reviews]].
 *
 * @see \common\models\Reviews
 */
class ReviewsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Reviews[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Reviews|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return ReviewsQuery
     */
    public function main()
    {
        return $this->andWhere([Reviews::tableName() . '.reply_to' => 0]);
    }

    /**
     * @return ReviewsQuery
     */
    public function products()
    {
        return $this->andWhere([Reviews::tableName() . '.module' => Products::tableName()]);
    }

    /**
     * @return ReviewsQuery
     */
    public function brands()
    {
        return $this->andWhere([Reviews::tableName() . '.module' => Brands::tableName()]);
    }

    /**
     * @return ReviewsQuery
     */
    public function confirmed()
    {
        return $this->andWhere([Reviews::tableName() . '.confirmed' => 1]);
    }
}
