<?php

namespace common\models\Queries;

use common\models\Sales;

/**
 * This is the ActiveQuery class for [[\common\models\Sales]].
 *
 * @see \common\models\Sales
 */
class SalesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Sales[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Sales|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active()
    {
        return $this
            ->andWhere(['status_id' => Sales::STATUS_ACTIVE])
            ->andWhere(['<=', 'start_at', time()])
            ->andWhere(['>', 'due_to', time()]);
    }
}
