<?php

namespace common\models\Queries;

use common\models\Products;
use common\models\Questions;

/**
 * This is the ActiveQuery class for [[\common\models\Questions]].
 *
 * @see \common\models\Questions
 */
class QuestionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Questions[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Questions|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return QuestionsQuery
     */
    public function products()
    {
        return $this->andWhere([Questions::tableName() . '.module' => Products::tableName()]);
    }

    /**
     * @return QuestionsQuery
     */
    public function answered()
    {
        return $this->andWhere(['is_answered' => 1]);
    }

    /**
     * @return QuestionsQuery
     */
    public function not_answered()
    {
        return $this->andWhere(['is_answered' => 0]);
    }
}
