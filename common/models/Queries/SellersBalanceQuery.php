<?php

namespace common\models\Queries;

/**
 * This is the ActiveQuery class for [[\common\models\SellersBalance]].
 *
 * @see \common\models\SellersBalance
 */
class SellersBalanceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\SellersBalance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\SellersBalance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
