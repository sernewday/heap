<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_info".
 *
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string|null $city
 * @property string|null $street
 * @property string|null $house
 * @property string|null $flat
 * @property string|null $birthday
 * @property int|null $gender
 * @property int|null $size_clothes Размер верхней одежды
 * @property int|null $size_shoes Размер обуви
 * @property int|null $size_chest Обхват груди
 * @property int|null $size_waist Обхват талии
 * @property int|null $size_hips Обхват бедер
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'surname'], 'required'],
            [['user_id', 'gender', 'size_clothes', 'size_shoes', 'size_chest', 'size_waist', 'size_hips'], 'integer'],
            [['birthday'], 'safe'],
            [['name', 'surname', 'city', 'street', 'house', 'flat'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'city' => 'City',
            'street' => 'Street',
            'house' => 'House',
            'flat' => 'Flat',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'size_clothes' => 'Size Clothes',
            'size_shoes' => 'Size Shoes',
            'size_chest' => 'Size Chest',
            'size_waist' => 'Size Waist',
            'size_hips' => 'Size Hips',
        ];
    }
}
