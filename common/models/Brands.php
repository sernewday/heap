<?php

namespace common\models;

use common\models\Queries\BrandsQuery;
use frontend\components\Crypt;
use frontend\components\Lang;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "brands".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $creation_time
 * @property mixed $imagesLinks
 * @property ImageManager[] $images
 * @property mixed $imagesLinksData
 * @property ImageManager $image
 * @property BrandsInfo $info
 * @property Products[] $associatedProducts
 * @property float $averageScore
 * @property Products[] $products
 * @property-read SellersBalance $balance
 * @property-read null|bool|string|false $decryptedMerchantName
 * @property-read null|bool|string|false $decryptedMerchantSecret
 * @property-read null|bool|string|false $decryptedCardNum
 * @property-read null|bool|string|int $commonOrdersAmount
 * @property-read null|bool|string|int $commonProductsAmount
 * @property-read null|bool|string|int $unsuccessfulOrdersAmount
 * @property-read null|bool|string|int $successfulOrdersAmount
 * @property-read CompanyInfo $companyInfo
 * @property-read CompanyDocuments $documents
 * @property int $update_time
 * @property string $import_id
 */
class Brands extends ActiveRecord
{
    /**
     * @var PaymentSettings
     */
    private $_settings;

    /**
     * @param int $count
     * @return array|Brands[]
     */
    public static function getTop($count)
    {
        return self::find()->orderBy('creation_time DESC')->joinWith(['image'])->limit($count)->all();
    }

    /**
     * {@inheritdoc}
     * @return BrandsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandsQuery(get_called_class());
    }

    public static function getIds()
    {
        return self::find()->select('id')->orderBy('id DESC')->one();
    }

    /**
     * @param int $limit
     * @return Brands[]
     */
    public static function getLast($limit = 18)
    {
        return self::find()->joinWith(['image'])->orderBy('creation_time DESC')->limit($limit)->all();
    }

    /**
     * @param string $name
     * @param string $import_id
     * @return Brands
     */
    public static function createByName($name, $import_id = '0')
    {
        $brand = new Brands();
        $brand->name = $name;
        $brand->user_id = 0;
        $brand->import_id = $import_id;

        $brand->save();

        return $brand;
    }

    /**
     * Get brands by category
     * @param int $category_id
     * @return array
     */
    public static function byCategory($category_id)
    {
        return ProductsCategoriesAssoc::find()->select([Brands::tableName() . '.id', Brands::tableName() . '.name'])->joinWith(['product', 'product.brand'])->where([ProductsCategoriesAssoc::tableName() . '.category_id' => $category_id])->groupBy([Products::tableName() . '.brand_id', ProductsCategoriesAssoc::tableName() . '.product_id'])->asArray()->cache(3600 * 24)->all();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * @param array $params
     * @return BrandsQuery
     */
    public static function allQuery($params = [])
    {
        return self::find()->orderBy('creation_time DESC');
    }

    /**
     * Get a brand by user id
     * @param int $user_id
     * @return Brands
     */
    public static function getByUser($user_id)
    {
        return self::find()->where(['user_id' => $user_id])->one();
    }

    /**
     * @param $id
     * @return Queries\ReviewsQuery
     */
    public static function getReviews($id)
    {
        return Reviews::find()->where(['module' => self::tableName(), 'record_id' => $id])->orderBy('created_at DESC');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['creation_time', 'update_time', 'user_id'], 'integer'],
            [['name', 'import_id'], 'string', 'max' => 255],
            [['name'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'creation_time' => 'Creation Time',
            'update_time' => 'Update Time',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
            $this->creation_time = time();

        $this->update_time = time();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $balance = new SellersBalance(['seller_id' => $this->id, 'balance' => 0]);
            $balance->save();

            $company_info = new CompanyInfo(['brand_id' => $this->id]);
            $company_info->save();
        }
    }

    public function getImage()
    {
        return $this->hasOne(ImageManager::className(), ['record_id' => 'id'])->andWhere(['module' => self::tableName()])->orderBy('sort');
    }

    public function getImagesLinks()
    {
        return ArrayHelper::getColumn($this->images, 'imageUrl');
    }

    public function getImagesLinksData()
    {
        return ArrayHelper::toArray($this->images, [
                ImageManager::className() => [
                    'caption' => 'path',
                    'key' => 'id',
                ]]
        );
    }

    public function beforeDelete()
    {
        if ($this->getImages()) {
            foreach ($this->images as $one) {
                @unlink(Yii::getAlias('@images') . '/' . self::tableName() . '/' . $one->path);
            }
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function getImages()
    {
        return $this->hasMany(ImageManager::className(), ['record_id' => 'id'])->andWhere(['module' => self::tableName()])->orderBy('sort');
    }

    /**
     * @return ActiveQuery
     */
    public function getInfo()
    {
        if (!BrandsInfo::find()->where(['record_id' => $this->id, 'lang' => Lang::getCurrentId()])->cache(3600 * 5)->exists())
            return $this->hasOne(BrandsInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => 1])->cache(3600 * 5);

        return $this->hasOne(BrandsInfo::className(), ['record_id' => 'id'])->andWhere(['lang' => Lang::getCurrentId()])->cache(3600 * 5);
    }

    /**
     * @return ActiveQuery
     */
    public function getAssociatedProducts()
    {
        return $this->hasMany(Products::className(), ['brand_id' => 'id']);
    }

    /**
     * Returns brand's average score
     * @return float
     */
    public function getAverageScore()
    {
        $cache = Yii::$app->cache;
        $key = 'brand_average_score_' . $this->id;
        $average_score_cache = $cache->get($key);
        if ($average_score_cache)
            return $average_score_cache;

        $reviews = Reviews::getByBrand($this->id, 1000);
        $count_reviews = count($reviews);
        if ($count_reviews == 0)
            return 0;

        $score = 0;
        foreach ($reviews as $review) {
            $score += $review->score;
        }

        $average_score = round($score / $count_reviews, 1);

        $cache->set($key, $average_score, 3600 * 24 * 3);

        return $average_score;
    }

    /**
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['brand_id' => 'id'])->cache(3600 * 24);
    }

    /**
     * @return ActiveQuery
     */
    public function getBalance()
    {
        return $this->hasOne(SellersBalance::className(), ['seller_id' => 'id']);
    }

    public function addPayment($data)
    {
        $payment = new Payments($data);
        $payment->brand_id = $this->id;

        if ($payment->balance_add)
            $payment->current_balance = $this->balance->balance + $payment->balance_add;

        if ($payment->balance_charge)
            $payment->current_balance = $this->balance->balance - $payment->balance_charge;

        if ($payment->save()) {

            if ($payment->balance_add)
                $this->updateBalance($payment->balance_add);

            if ($payment->balance_charge)
                $this->updateBalance($payment->balance_charge * -1);

        }

        return false;
    }

    /**
     * @param double $sum
     * @return int
     */
    public function updateBalance($sum)
    {
        return SellersBalance::updateAllCounters(['balance' => $sum], ['seller_id' => $this->id]);
    }

    /**
     * @return bool|false|string|null
     */
    public function getDecryptedCardNum()
    {
        if (!$this->_settings) {
            $this->_settings = PaymentSettings::findOne(['seller_id' => $this->id]);
        }

        if ($this->_settings->transfer_card)
            return Crypt::decrypt($this->_settings->transfer_card);

        return null;
    }

    /**
     * @return bool|false|string|null
     */
    public function getDecryptedMerchantName()
    {
        if (!$this->_settings) {
            $this->_settings = PaymentSettings::findOne(['seller_id' => $this->id]);
        }

        if ($this->_settings->merchant_name)
            return Crypt::decrypt($this->_settings->merchant_name);

        return null;
    }

    /**
     * @return bool|false|string|null
     */
    public function getDecryptedMerchantSecret()
    {
        if (!$this->_settings) {
            $this->_settings = PaymentSettings::findOne(['seller_id' => $this->id]);
        }

        if ($this->_settings->merchant_secret)
            return Crypt::decrypt($this->_settings->merchant_secret);

        return null;
    }

    /**
     * @return bool|int|string|null
     */
    public function getCommonOrdersAmount()
    {
        return Orders::find()->where(['seller_id' => $this->id])->cache(3600 * 24)->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getCommonProductsAmount()
    {
        return Products::find()->where(['brand_id' => $this->id])->cache(3600 * 24)->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getSuccessfulOrdersAmount()
    {
        return Orders::find()
            ->where(['status_id' => Orders::STATUS_SUCCESS])
            ->andWhere(['seller_id' => $this->id])
            ->cache(3600 * 24)
            ->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getUnsuccessfulOrdersAmount()
    {
        return Orders::find()
            ->where(['status_id' => [Orders::STATUS_RETURNED, Orders::STATUS_CANCELED]])
            ->andWhere(['seller_id' => $this->id])
            ->cache(3600 * 24)
            ->count();
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanyInfo()
    {
        return $this->hasOne(CompanyInfo::className(), ['brand_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasOne(CompanyDocuments::className(), ['brand_id' => 'id']);
    }
}
