<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "brands_info".
 *
 * @property int $record_id
 * @property int $lang
 * @property string $description
 */
class BrandsInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brands_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'description'], 'required'],
            [['record_id', 'lang'], 'integer'],
            [['description'], 'string'],
            [['record_id', 'lang'], 'unique', 'targetAttribute' => ['record_id', 'lang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'description' => 'Description',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Queries\BrandsInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\Queries\BrandsInfoQuery(get_called_class());
    }
}
