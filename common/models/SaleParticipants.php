<?php

namespace common\models;

/**
 * This is the model class for table "sale_participants".
 *
 * @property int $sale_id
 * @property-read Brands $brand
 * @property int $brand_id
 */
class SaleParticipants extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_participants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'brand_id'], 'required'],
            [['sale_id', 'brand_id'], 'integer'],
            [['sale_id', 'brand_id'], 'unique', 'targetAttribute' => ['sale_id', 'brand_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sale_id' => 'Sale ID',
            'brand_id' => 'Brand ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
}
