<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_params".
 *
 * @property int $id
 * @property-read ProductParamsValues[] $values
 * @property string $name
 */
class ProductParams extends \yii\db\ActiveRecord
{
    // Param ids
    const PARAM_COUNTRY = 1;
    const PARAM_STYLE = 2;
    const PARAM_SEASON = 3;
    const PARAM_GENDER = 4;
    const PARAM_MATERIAL = 5;
    const PARAM_COLOR = 6;
    const PARAM_CLASP = 7;
    const PARAM_LENGTH = 8;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_params';
    }

    /**
     * Get values map by a param
     * @param int $param_id
     * @param bool $add_empty
     * @return array
     */
    public static function getValuesMap($param_id, $add_empty = false)
    {
        $param_values = ProductParamsValues::find()->where(['param_id' => $param_id])->cache(3600 * 24)->asArray()->all();
        if (!$param_values)
            return [];

        return ArrayHelper::map($param_values, 'id', 'value');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(ProductParamsValues::className(), ['param_id' => 'id']);
    }

    /**
     * Add a new value
     * @param string $value
     * @return bool
     */
    public function addValue($value)
    {
        if (ProductParamsValues::find()->where(['param_id' => $this->id, 'value' => $value])->exists())
            return true;

        $param_value = new ProductParamsValues(['param_id' => $this->id]);
        $param_value->value = $value;

        return $param_value->save();
    }
}
