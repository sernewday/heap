<?php

namespace common\models;

/**
 * This is the model class for table "sales_info".
 *
 * @property int $record_id
 * @property int $lang
 * @property string $name
 * @property string $label_name
 * @property string|null $comment
 */
class SalesInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sales_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'name', 'label_name'], 'required'],
            [['record_id', 'lang'], 'integer'],
            [['comment'], 'string'],
            [['name', 'label_name'], 'string', 'max' => 255],
            [['record_id', 'lang'], 'unique', 'targetAttribute' => ['record_id', 'lang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'name' => 'Name',
            'label_name' => 'Label Name',
            'comment' => 'Comment',
        ];
    }
}
