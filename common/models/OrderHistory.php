<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order_history".
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_status
 * @property string|null $description
 * @property int $created_at
 * @property-read string|false $time
 * @property-read string|false $date
 * @property int $updated_at
 */
class OrderHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_status'], 'required'],
            [['order_id', 'order_status', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'order_status' => 'Order Status',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return date('d.m.Y', $this->created_at);
    }

    /**
     * @return false|string
     */
    public function getTime()
    {
        return date('H:i', $this->created_at);
    }
}
