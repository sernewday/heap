<?php


namespace backend\assets;


use kartik\file\FileInputAsset;

class FileInputCustomAsset extends FileInputAsset
{
    public $depends = [
        'backend\assets\AppAsset'
    ];
}