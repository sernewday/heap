<?php

/* @var $items array */
/* @var $mainClass string */
/* @var $activeClass string */

use backend\components\Helper;
use backend\widgets\Sidebar;
use yii\helpers\Url;

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

?>

<ul class="<?= $mainClass ?>">
    <?php foreach ($items as $item): ?>
        <li class="<?= Sidebar::returnActiveClass(Helper::splitUrl($item['url'][0]), [$controller, $action], $activeClass) ?>">
            <a href="<?= Url::to($item['url']) ?>">
                <i class="<?= $item['icon'] ?>"></i>
                <?= $item['label'] ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
