<?php

namespace backend\widgets;

class Sidebar extends \yii\base\Widget
{
    public $items;
    public $mainClass = 'list-unstyled navbar__list';
    public $activeClass = 'active';

    public function run()
    {
        return $this->render('sidebar', [
            'items' => $this->items,
            'mainClass' => $this->mainClass,
            'activeClass' => $this->activeClass,
        ]);
    }

    /**
     * Return active class
     * @param array $item_url
     * @param array $current_url
     * @param string $activeClass
     * @return string
     */
    public static function returnActiveClass($item_url, $current_url, $activeClass)
    {
        if (($item_url[0] == $current_url[0] && $item_url[1] == $current_url[1]) || ($item_url[0] == $current_url[0]))
            return $activeClass;

        return '';
    }
}