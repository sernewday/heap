<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Brands */
/* @var $form yii\widgets\ActiveForm */
/* @var $model_upload \common\models\UploadForm */

\backend\assets\FileInputCustomAsset::register($this);
?>

<div class="brands-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_upload, 'imageFiles[]')->widget(FileInput::classname(), [
        'language' => 'ru',
        'name' => 'ImageManager[attachment]',
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'previewFileType' => 'any',
            'deleteUrl' => Url::toRoute([Yii::$app->controller->id . '/delete-image']),
            'initialPreview' => $model->imagesLinks,
            'initialPreviewAsData' => true,
            'overwriteInitial' => false,
            'initialPreviewConfig' => $model->imagesLinksData,
            'maxFileCount' => 1
        ],
    ])->label("Фото") ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
