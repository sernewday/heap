<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brands';
?>
<div class="brands-index">

    <div class="table-data__tool">
        <div class="table-data__tool-left">
            <button class="au-btn-filter">
                <i class="zmdi zmdi-filter-list"></i>filters</button>
        </div>
        <div class="table-data__tool-right">
            <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="<?= Url::to(['create']) ?>">
                <i class="zmdi zmdi-plus"></i>add item
            </a>
        </div>
    </div>

    <?= \backend\components\Grid::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'creation_time:datetime',
            'update_time:datetime',

            ['class' => 'backend\components\CustomActionColumn'],
        ],
    ]); ?>


</div>
