<?php


namespace backend\components;

/**
 * Class Helper
 * @package backend\components
 */
class Helper
{
    /**
     * Splits url to controller and action
     * @param string $url
     * @return array
     */
    public static function splitUrl($url)
    {
        $parts = explode('/', $url);

        $controller = $parts[1];
        $action = $parts[2];

        if (count($parts) > 3)
        {
            $controller = $parts[2];
            $action = $parts[3];
        }

        return [$controller, $action];
    }
}