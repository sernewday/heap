<?php

namespace backend\components;

use Closure;
use yii\grid\Column;
use yii\helpers\Html;

class Grid extends \yii\grid\GridView
{
    public $tableOptions = [
        'class' => 'table table-data2',
    ];

    public function renderTableRow($model, $key, $index)
    {
        $cells = [];
        /* @var $column Column */
        foreach ($this->columns as $column) {
            $cells[] = $column->renderDataCell($model, $key, $index);
        }
        if ($this->rowOptions instanceof Closure) {
            $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
        } else {
            $options = $this->rowOptions;
        }
        $options['data-key'] = is_array($key) ? json_encode($key) : (string) $key;
        $options['class'] = 'tr-shadow';

        return Html::tag('tr', implode('', $cells), $options) . Html::tag('tr', '', ['class' => 'spacer']);
    }
}