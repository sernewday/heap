<?php


namespace frontend\assets;


class CustomPjaxAsset extends \yii\widgets\PjaxAsset
{
    public $depends = [
        'frontend\assets\AppAsset',
        'yii\web\YiiAsset',
    ];
}