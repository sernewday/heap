<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/css/style.css',
    ];
    public $js = [
        'assets/js/app.js',
        'js/modal.js',
        'js/modal__controller.js',
        'js/register.js',
        'js/bag.js',
        'js/product__like.js',
        'js/slider.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
