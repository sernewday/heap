<?php


namespace frontend\components;


use yii\i18n\MissingTranslationEvent;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        if ($event->language != 'ru') {
            $curl = curl_init();

            $data_params = [
                'from' => 'ru',
                'to' => 'uk',
                'text' => $event->message,
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://microsoft-azure-translation-v1.p.rapidapi.com/translate?" . http_build_query($data_params),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 100,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "x-rapidapi-host: microsoft-azure-translation-v1.p.rapidapi.com",
                    "x-rapidapi-key: a955baa75cmsh52331eb4c09fd98p133baajsn483d4103645c"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $event->translatedMessage = $event->message;
            }
        } else {
            $response = $event->message;
        }

        $f_path = dirname(__DIR__) . '/messages/' . $event->language . '/app.php';
        if (!file_exists(dirname(__DIR__) . '/messages/' . $event->language . '/app.php'))
            file_put_contents($f_path, '<?php return [];');

        $current_translation = require dirname(__DIR__) . '/messages/' . $event->language . '/app.php';
        $current_translation[$event->message] = strip_tags($response);

        $new_file = "<?php \n return " . var_export($current_translation, true) . ';';
        
        file_put_contents(dirname(__DIR__) . '/messages/' . $event->language . '/app.php', $new_file);

//        $event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";
        $event->translatedMessage = strip_tags($response);
    }
}