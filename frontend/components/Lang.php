<?php


namespace frontend\components;


use Yii;

/**
 * Class Lang
 * @package frontend\components
 */
class Lang
{
    /**
     * IDs of the possible languages
     *
     * @var int[]
     */
    public static $langs = [
        'uk' => 1,
        'ru' => 2,
    ];

    /**
     * Returns current lang id
     * @return string
     */
    public static function getCurrentLang()
    {
        $cookies = Yii::$app->request->cookies;

        $lang = $cookies->get('language');

        if (!$lang->value)
            return 'uk';

        return $lang->value;
    }

    /**
     * Returns the id of a lang
     * @return int
     */
    public static function getCurrentId()
    {
        return self::$langs[self::getCurrentLang()];
    }

    /**
     * Returns a string according to the current language
     * @param array $array
     * @return mixed
     */
    public static function getStringFromArray($array)
    {
        if (Yii::$app->language == 'uk')
            return $array[0];

        return $array[1] ?: $array[0];
    }
}