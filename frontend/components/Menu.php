<?php


namespace frontend\components;

use common\models\Categories;
use Yii;

/**
 * Class Menu
 * @package frontend\components
 */
class Menu
{
    /**
     * @param int $depth
     * @return array|Categories[]
     */
    public static function get($depth = 2)
    {
        $cache = Yii::$app->cache;

        // if (Lang::getCurrentId() == 1)
            $cur_relation = 'infoUa';
        // else
        //     $cur_relation = 'infoRu';

        $result = $cache->get('menu_structure_' . $cur_relation);
        if ($result)
            return $result;

        $all_categories = Categories::find()->where(['!=', 'name_alt', 'all'])->andWhere(['<=', 'depth', $depth])->with([$cur_relation])->orderBy('path, depth, created_at ASC')->asArray()->indexBy('id')->cache(3600 * 24)->all();

        foreach ($all_categories as &$item) {
            $all_categories[$item['id']]['info'] = $all_categories[$item['id']][$cur_relation];
            unset($all_categories[$item['id']][$cur_relation]);

            $item['_childs'] = [];

            if ($item['parent_id'] == -1)
                continue;

            $path = explode('.', $item['path']);

            if (count($path) == 1) {
                if (!isset($all_categories[$path[0]]['_childs_count']))
                    $all_categories[$path[0]]['_childs_count'] = 0;

                $all_categories[$path[0]]['_childs'][$item['id']] = $item;
                $all_categories[$path[0]]['_childs_count'] += 1;
                unset($all_categories[$item['id']]);
            } else {
                $all_categories[$path[0]]['_childs'][$path[1]]['_childs'][$item['id']] = $item;
                unset($all_categories[$item['id']]);
            }
        }

        $cache->set('menu_structure_' . $cur_relation, $all_categories, 3600 * 24);

        return $all_categories;
    }
}