<?php


namespace frontend\components;

/**
 * Class Csv
 * @package frontend\components
 */
class Csv
{
    /**
     * @param string $csv_string
     * @param string $delimiter
     * @return array
     */
    public static function parse($csv_string, $delimiter = ',')
    {
        $rows = explode("\n", $csv_string);
        $result = [];

        foreach ($rows as $row) {
            $result[] = explode($delimiter, $row);
        }

        return $result;
    }
}