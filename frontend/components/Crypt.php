<?php


namespace frontend\components;

/**
 * Class Crypt
 * @package frontend\components
 */
class Crypt
{
    /**
     * @var string Private key to encrypt and decrypt
     */
    private static $key = 'd416c2b30fea0c8474b6138e';

    /**
     * Encrypts the string
     * @param string $string
     * @return string
     */
    public static function encrypt($string)
    {
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($string, $cipher, self::$key, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, self::$key, $as_binary = true);
        return base64_encode($iv . $hmac . $ciphertext_raw);
    }

    /**
     * Decrypts the string
     * @param string $string
     * @return bool|false|string
     */
    public static function decrypt($string)
    {
        $c = base64_decode($string);
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $plaintext = openssl_decrypt($ciphertext_raw, $cipher, self::$key, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, self::$key, $as_binary = true);
        if (hash_equals($hmac, $calcmac)) {
            return $plaintext;
        }

        return false;
    }
}