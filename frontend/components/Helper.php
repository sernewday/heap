<?php


namespace frontend\components;

/**
 * Class Helper
 * @package frontend\components
 */
class Helper
{
    public static $monthsLabel = [
        1 => 'январь',
        2 => 'февраль',
        3 => 'март',
        4 => 'апрель',
        5 => 'май',
        6 => 'июнь',
        7 => 'июль',
        8 => 'август',
        9 => 'сентябрь',
        10 => 'октябрь',
        11 => 'ноябрь',
        12 => 'декабрь',
    ];

    /**
     * @param float|int $number
     * @return string
     */
    public static function formatPrice($number)
    {
        return self::formatNumber($number, ' ', 2, '.');
    }

    /**
     * Formats big numbers
     * For example 357283 will be 357 283 etc.
     * @param int|float $number
     * @param string $symbol
     * @param int $decimals
     * @param string $decimal_point
     * @return string
     */
    public static function formatNumber($number, $symbol = ' ', $decimals = 0, $decimal_point = '.')
    {
        return number_format($number, $decimals, $decimal_point, $symbol);
    }

    /**
     * @param string $text
     * @return string
     */
    public static function transliterate($text)
    {
        $cyr = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',
            'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'і', 'ї', 'є'
        ];
        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
            'A', 'B', 'V', 'G', 'D', 'E', 'Io', 'Zh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P',
            'R', 'S', 'T', 'U', 'F', 'H', 'Ts', 'Ch', 'Sh', 'Sht', 'A', 'I', 'Y', 'e', 'Yu', 'Ya', 'i', 'ii', 'ye'
        ];

        $symbols = ['-', '/', '|', '\\', '#', '@', '%', '$', '^', '&', '*', '(', ')', ' ', '=', '+', ':', ';', '"', '\''];

        return str_replace($symbols, '_', str_replace($cyr, $lat, $text));
    }

    /**
     * Get decimal part of a number
     * @param $number
     * @return float
     */
    public static function getFraction($number)
    {
        $whole = floor($number);
        return $number - $whole;
    }

    /**
     * @return string[]
     */
    public static function getMonthsList()
    {
        $list = self::$monthsLabel;

        foreach ($list as &$item) {
            $item = \Yii::t('app', $item);
        }

        return $list;
    }

    /**
     * Get name part of the full name
     * @param string $full_name
     * @return string
     */
    public static function getNameByFIO($full_name)
    {
        $data = explode(' ', $full_name);

        return $data[1] ?: $data[0];
    }

    /**
     * @param string $string_date
     * @param string $format
     * @return false|string
     */
    public static function getFormattedDateFromString($string_date, $format = 'd.m.Y')
    {
        $unix_time = strtotime($string_date);

        return date($format, $unix_time);
    }
}