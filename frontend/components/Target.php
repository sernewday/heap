<?php


namespace frontend\components;


class Target
{
    const TARGET_KEY = 'target';
    const TARGET_PARAM = 't';

    const KEY_MALE = 'm';
    const KEY_FEMALE = 'f';
    const KEY_KID = 'k';

    public static $labels = [
        'm' => 'Мужская',
        'f' => 'Женская',
        'k' => 'Детская',
    ];

    public static $genderId = [
        'm' => '642',
        'f' => '641',
        'k' => '643',
    ];

    /**
     * @return mixed|null
     */
    public static function get()
    {
        return \Yii::$app->session->get(self::TARGET_KEY);
    }

    /**
     * @return string
     */
    public static function getLabel()
    {
        return self::$labels[self::get()];
    }

    
    /**
     * @return string
     */
    public static function getGenderId()
    {
        return self::$genderId[self::get()];
    }
}