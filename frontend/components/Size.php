<?php


namespace frontend\components;

/**
 * Class Size
 * @package frontend\components
 */
class Size
{
    /**
     * @var string[] List of possible international sizes
     */
    public static $international_sizes = ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL'];

    /**
     * @return array
     */
    public static function getClothesSize()
    {
        $sizes = [];

        for ($i = 40; $i <= 60; $i++)
            $sizes[$i] = $i . ' URK';

        return $sizes;
    }

    /**
     * @return array
     */
    public static function getShoesSize()
    {
        $sizes = [];

        for ($i = 15; $i <= 50; $i++)
            $sizes[$i] = $i . ' URK';

        return $sizes;
    }

    /**
     * @return array
     */
    public static function getChestSize()
    {
        $sizes = [];

        for ($i = 50; $i <= 140; $i++)
            $sizes[$i] = $i . ' cm';

        return $sizes;
    }

    /**
     * @return array
     */
    public static function getWaistSize()
    {
        $sizes = [];

        for ($i = 50; $i <= 140; $i++)
            $sizes[$i] = $i . ' cm';

        return $sizes;
    }

    /**
     * @return array
     */
    public static function getHipsSize()
    {
        $sizes = [];

        for ($i = 80; $i <= 150; $i++)
            $sizes[$i] = $i . ' cm';

        return $sizes;
    }
}