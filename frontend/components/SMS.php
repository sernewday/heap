<?php


namespace frontend\components;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class SMS
{
    public static $count_limit = 40;
    private static $token = '4808adf25644416457e4e6a2b1c9133c5aa0ebcf';
    private static $sender = 'Market';
    private static $api_url = 'https://api.turbosms.ua/message/';

    /**
     * @param array $recipients
     * @param string $text
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public static function send($recipients, $text)
    {
        $client = new Client([
            'verify' => false,
            'base_uri' => self::$api_url
        ]);

        $response = $client->request('GET', 'send.json',
            [
                'query' =>
                    [
                        'token' => self::$token,
                        'recipients' => $recipients,
                        'sms' => [
                            'sender' => self::$sender,
                            'text' => $text,
                        ],
                    ],
            ]
        );

        return $response;
    }
}