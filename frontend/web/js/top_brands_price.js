let days_option = document.querySelectorAll('div.options__conteiner.field-advertisingbrandform-days div.options__opt'),
    types_option = document.querySelectorAll('div.options__conteiner.field-advertisingbrandform-type div.options__opt')
days_input = document.querySelector('#advertisingbrandform-days'),
    type_input = document.querySelector('#advertisingbrandform-type'),
    price_text = document.querySelector('div.seller-cab-adv-logo__cost > span');

function changePrice() {
    price_text.innerHTML = price_for_days[type_input.value] * days_input.value + ' <span>грн</span>';
}

if (days_option) {
    for (let i = 0; i < days_option.length; i++) {
        days_option[i].addEventListener('click', function (event) {
            changePrice();
        });
    }
}

if (types_option) {
    for (let i = 0; i < types_option.length; i++) {
        types_option[i].addEventListener('click', function (event) {
            changePrice();
        });
    }
}

changePrice();
