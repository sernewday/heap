let $ = require('jquery') 
let jQuery = require('jquery') 

import '../../lib/slick/slick.min.js'  

$(document).ready(function(){
  $('.main-slider-baner__body').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 3000,
    prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
  });
  $('.product-zoom__slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    speed: 1000,
    prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
  });
  $('.zoom').each(function(){
    $(this).click(function() {
      $('.product-zoom__slider').slick('slickGoTo', `${$(this).data('index')}`)
    })

  })
  $('.product__slider-categori').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    speed: 1000,
    initialSlide: 1,
    autoplaySpeed: 3000,
    prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
  });
  $('.product__slider-categori').find('.slider__button').click((e) => e.preventDefault())
  console.log()
  if($(window).width() > 1100){
    // $('.product').each(function(){
    //   if($(this).find('.product__slider-img').children().length >= 2){
    //     $(this).append(function(){
    //       return $(`
    //       <div class="product__button">
    //         <button class="slider__button product-slider__prev slick-arrow" style=""><i class="fas fa-chevron-left"></i></button>
    //         <button class="slider__button product-slider__next slick-arrow" style=""><i class="fas fa-chevron-right"></i></button>
    //       </div>
    //       `)
    //     })
    //     $(this).find('.product__slider-img').slick({
    //       infinite: true,
    //       slidesToShow: 1,
    //       slidesToScroll: 1,
    //       arrows: true,
    //       dots: false,
    //       speed: 1000,
    //       prevArrow: $(this).find('.product-slider__prev'),
    //       nextArrow: $(this).find('.product-slider__next'),
    //     });
    //     $(this).hover(function(){
    //       $(this).find('.product__button').toggleClass("hover")
    //     })
    //   }
    // })
    $('.slider-product__body').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      speed: 1000,
      autoplaySpeed: 3000,
      prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
      responsive: [
        {
          breakpoint: 1520,
          settings: {
            slidesToShow: 4,
          }
        },
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 4,
          }
        },
        {
          breakpoint: 480,
          settings: {
          }
        }
      ]
    });  
    $('.product-categori-slider__body').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      speed: 1000,
      autoplaySpeed: 4000,
      prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
      responsive: [
        {
          breakpoint: 1570,
          settings: {
            slidesToShow: 4,
          }
        },
        {
          breakpoint: 1340,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 480,
          settings: {
          }
        }
      ]
    });
    $('.ariticle-slider-article__body').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      speed: 1000,
      autoplaySpeed: 4000,
      prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>'
    });
    $('.ariticle-slider-newsletter__body').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      speed: 1000,
      autoplaySpeed: 4000,
      prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>'
    });
    $('.blog-slider__body').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      speed: 1000,
      autoplaySpeed: 4000,
      prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>'
    });
  }
  else{
    $('.main-blog').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      speed: 1000,
      autoplaySpeed: 3000,
      prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
      responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
          }
        },
      ]
    });
  }
  console.log()
  if($('.product-img-slider__examlpe').length + $('.product-img-slider__video').length == 4){
    $('.product-img-slider__container').addClass('slider-three')
    $('.product-img-slider__container').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      speed: 1000,
      autoplaySpeed: 4000,
      prevArrow: $('.product-img-slider__prev'),
      nextArrow: $('.product-img-slider__next'),
      responsive: [
        {
          breakpoint: 1420,
          settings: {
            arrows: false,
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 1100,
          settings: {
            arrows: false,
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false,
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 460,
          settings: {
            slidesToShow: 1,
          }
        },
      ]
    });
  }
  else if($('.product-img-slider__examlpe').length + $('.product-img-slider__video').length > 4){
    $('.product-img-slider__container').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      speed: 1000,
      autoplaySpeed: 4000,
      prevArrow: $('.product-img-slider__prev'),
      nextArrow: $('.product-img-slider__next'),
      responsive: [
        {
          breakpoint: 1420,
          settings: {
            arrows: false,
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 1100,
          settings: {
            arrows: false,
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false,
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 460,
          settings: {
            slidesToShow: 1,
          }
        },
      ]
    });
  }
  else{
    $('.product-img-slider__next').css('display', 'none')
    $('.product-img-slider__prev').css('display', 'none')
    $('.product-img-slider__container').addClass('no-slider')
  }
  $('.review-slider__body').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    speed: 1000,
    autoplaySpeed: 4000,
    prevArrow: '<button class="slider__button slider__prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button class="slider__button slider__next"><i class="fas fa-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          arrows: false,
          dots: true,
        }
      },
    ]
  });
})