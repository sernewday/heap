let reg_form = $("#registration-form"),
    reg_form_modal = new Modal('registration'),
    email_form_modal = new Modal('confirm-mail-done');

email_form_modal.init();

reg_form.on('beforeSubmit', function (event) {
    event.preventDefault();

    if (reg_form.find('.has-error').length > 0)
        return false;

    reg_form_modal.close();
    email_form_modal.open();

    let data = reg_form.serialize();
    $.ajax({
        url: reg_form.attr('action'),
        type: 'POST',
        data: data,
        success: function (data) {
            if (data.status === 'success') {
                // do something

            } else {
                // do something
            }

            return true;
        },
        error: function(jqXHR, errMsg) {
            alert(errMsg);
        }
    });

    return false;
});

let submit_email_form = $("#confirm-email-form_"),
    submit_email_input = $('#submit-email-input-field');

submit_email_form.on('beforeSubmit', function (event) {
    event.preventDefault();

    if (submit_email_form.find('.has-error').length > 0)
        return false;

    $.ajax({
        url: '/site/verify-account',
        type: 'POST',
        data: {code: submit_email_input.val()},
        success: function (data) {
            if (data.status === 'success')
                document.location.reload();

            return true;
        },
        error: function(jqXHR, errMsg) {
            alert(errMsg);
        }
    });

    return false;
});
