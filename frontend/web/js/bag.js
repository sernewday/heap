/**
 * Class Bag
 * Controls bag modal
 */
class Bag {

    constructor() {
        this.bag_modal = new Modal('bag');

        Bag.set_bag_counter();
    }

    /**
     * Set amount of products in the bag to the badge
     */
    static set_bag_counter() {
        let length = document.querySelectorAll('a.product-bag').length,
            element_counter = document
                .querySelector('body > div:nth-child(2) > header > div.header-button-container > div > div > div.header-button > div.header-button__example.bag.value > span');

        if (length <= 0)
            element_counter.style.display = 'none';
        else
            element_counter.removeAttribute('style');

        element_counter.innerHTML = length;
    }

    /**
     * Initialize add product to the bag
     */
    init_add_products() {
        let parent = this;
        $('.buy_product[data-id]').bind('click', function (event) {
            $.post('/bag/add-product', {
                product_id: $(this).attr('data-id'),
                quantity: 1,
                color: $('#value_color').val(),
                size: $('#value_size').val()
            }, function (data) {
                if (data.status === 'success') {

                    parent.load_products_html_data(data.products);

                    parent.bag_modal.open();

                    return true;
                }

                console.log('Error');
                return false;
            });
        });
    }

    /**
     * Remove a product from the bag
     */
    init_remove_products() {
        let parent = this;
        $('.product-bag__remove').bind('click', function (event) {
            event.preventDefault();

            $.post('/bag/remove-product', {
                product_id: $(this).attr('data-id'),
                quantity: 1,
            }, function (data) {
                if (data.status === 'success') {
                    parent.load_products_html_data(data.products);

                    parent.init_remove_products();
                    return true;
                }

                console.log('Error');
                return false;
            });
        });
    }

    /**
     * Load products html
     * @param products
     */
    load_products_html_data(products) {
        this.bag_modal.modal.querySelector('div.modal-bag__order.order-model-bag').innerHTML = products;

        Bag.set_bag_counter();
        this.init_remove_products();
    }
}

let bag = new Bag();
bag.init_add_products();
bag.init_remove_products();
