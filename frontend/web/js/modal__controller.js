
let open_buttons = document.querySelectorAll('.open-modal');

if (open_buttons) {
    for (let i = 0; i < open_buttons.length; i++) {
        open_buttons[i].addEventListener('click', function (event) {
            event.preventDefault();

            let modal = new Modal(open_buttons[i].getAttribute('data-target'));
            modal.init();
            modal.open();

            return false;
        });
    }
}

// -----------------------------------------

let resend_email_btn = document.querySelector('.reset-email-code'),
    resend_email_counter = document.querySelector('.modal-resend_email_counter');

var resend_email_seconds = 0,
    resend_interval_id = 0;
if (resend_email_btn) {
    resend_email_btn.addEventListener('click', function (event) {
        $.post('/profile/resend-email-verification', {}, function (data) {});

        resend_email_seconds = 60;
        resend_email_counter.style.display = 'block';
        resend_email_btn.style.display = 'none';
        resend_interval_id = setInterval(function () {
            resend_email_counter.innerText = resend_email_seconds;

            if (resend_email_seconds <= 0) {
                resend_email_seconds = 0;
                resend_email_counter.style.display = 'none';
                resend_email_btn.style.display = 'block';
                clearInterval(resend_interval_id);
            }

            resend_email_seconds -= 1;
        }, 1000);
    });
}

// ------------------------------------------

let resend_phone_btn = document.querySelector('.reset-phone-code'),
    resend_phone_counter = document.querySelector('.modal-resend_phone_counter'),
    confirm_phone_code_btn = document.querySelector('.confirm-phone-code');

var resend_phone_seconds = 0,
    resend__phone_interval_id = 0;
if (resend_phone_btn) {
    resend_phone_btn.addEventListener('click', function (event) {
        $.post('/profile/resend-phone-verification', {}, function (data) {});

        resend_phone_seconds = 300;
        resend_phone_counter.style.display = 'block';
        resend_phone_btn.style.display = 'none';
        resend__phone_interval_id = setInterval(function () {
            resend_phone_counter.innerText = resend_phone_seconds;

            if (resend_phone_seconds <= 0) {
                resend_phone_seconds = 0;
                resend_phone_counter.style.display = 'none';
                resend_phone_btn.style.display = 'block';
                clearInterval(resend__phone_interval_id);
            }

            resend_phone_seconds -= 1;
        }, 1000);
    });
}

if (confirm_phone_code_btn) {
    confirm_phone_code_btn.addEventListener('click', function (event) {
        event.preventDefault();

        $.ajax({
            url: '/site/verify-account-phone',
            type: 'POST',
            data: {code: $('#submit-phone-input-field').val()},
            success: function (data) {
                if (data.status === 'success')
                    document.location.reload();

                return true;
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });

        return false;
    });
}
