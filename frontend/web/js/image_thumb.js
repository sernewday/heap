let file_inputs = document.querySelectorAll('input.image-product-thumb');

if (file_inputs) {
    for (let i = 0; i < file_inputs.length; i++) {
        file_inputs[i].onchange = function (evt) {
            let tgt = evt.target || window.event.srcElement,
                files = tgt.files;

            // FileReader support
            if (FileReader && files && files.length) {
                let fr = new FileReader();
                fr.onload = function () {

                    let image_thumb = document.createElement('img');
                    image_thumb.src = fr.result;
                    image_thumb.setAttribute('alt', 'Image Thumb');
                    image_thumb.setAttribute('data-key', '0');

                    file_inputs[i].parentNode.appendChild(image_thumb);
                }
                fr.readAsDataURL(files[0]);
            }

            // Not supported
            else {
                // fallback -- perhaps submit the input to an iframe and temporarily store
                // them on the server until the user's session ends.
            }
        }
    }
}