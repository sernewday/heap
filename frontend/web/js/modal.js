import Modal from './class/Modals';


const start_modal = (list) => {
  list.forEach(item => {
    if(document.querySelector(`.${item}`)){
      new Modal(item).init()
    }
  })
}

start_modal([
  'clothing', 
  'shoes', 
  'review-modal', 
  'question', 
  'review-seller', 
  'review-product', 
  'return', 
  'chat', 
  'add-promo', 
  'cab-answer', 
  'cab-ask-delete', 
  'add-sales', 
  'remove-product', 
  'categori-add', 
  'add-seller', 
  'baner-select', 
  'baner-back',
  'add-baner',
  'add-product'
])

if(document.querySelector('.video')){
  new Modal('video').init_video()
}

if(document.querySelector('.bag')){
  new Modal('bag').init_bag()
}

if(document.querySelector('.login')){
  new Modal('login').init_login()
}




const filter_container = document.querySelectorAll('.filter-catalog-filter')

const filter_funk = () => {
  filter_container.forEach(container => {
    container.querySelectorAll('.filter-catalog-filter__example').forEach(element => {
      element.querySelector('input').addEventListener('click', function(){
        const div = document.createElement('div')
        div.classList.add('filter-catalog-filter__select')
        div.id = this.dataset.id
        div.innerHTML =` 
        ${element.querySelector('.checkbox-filter__text').innerHTML}
        <div class="filter-catalog-filter__remove"></div>
        `
        this.classList.add('check')
        container.classList.add('select')
        div.querySelector('.filter-catalog-filter__remove').addEventListener('click', () => {
          this.checked = false
          div.remove()
          this.classList.remove('check')
          if(container.querySelectorAll('.check').length == 0){
            container.classList.remove('select')
          }
        })
        if(this.checked){
          document.querySelector('.catalog-filter__filter-select').append(div)
        }
        else{
          this.classList.remove('check')
          document.querySelector(`#${this.dataset.id}`).remove()
          if(container.querySelectorAll('.check').length == 0){
            container.classList.remove('select')
          }
        }
      })
    })
  })
}

if(filter_container){
  filter_funk()
}


const seller_cab_filter = document.querySelectorAll('.seller-cab-filter')

const seller_cab_filter_funk = () => {
  seller_cab_filter.forEach(container => {
    container.querySelectorAll('.seller-cab-filter__value').forEach(element => {
      element.querySelector('input').addEventListener('click', function(){
        const div = document.createElement('div')
        div.classList.add('seller-cab-filter__result-example')
        div.id = this.dataset.id
        div.innerHTML =`
        ${element.querySelector('.checkbox__text').innerHTML}
        <span class="seller-cab-filter__result-close"></span>
        `
        this.classList.add('check')
        container.classList.add('select')
        div.querySelector('.seller-cab-filter__result-close').addEventListener('click', () => {
          this.checked = false
          div.remove()
          this.classList.remove('check')
          if(container.querySelectorAll('.check').length == 0){
            container.classList.remove('select')
          }
        })
        console.log()
        if(this.checked){
          container.parentNode.parentNode.parentNode.querySelector('.seller-cab-filter__result-container').append(div)
        }
        else{
          this.classList.remove('check')
          document.querySelector(`#${this.dataset.id}`).remove()
          if(container.querySelectorAll('.check').length == 0){
            container.classList.remove('select')
          }
        }
      })
    })
  })
}

if(seller_cab_filter){
  seller_cab_filter_funk()
}


