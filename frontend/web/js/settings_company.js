
let close_buttons = document.querySelectorAll('.seller-cab-product-editing-cart__close');

if (close_buttons) {
    for (let i = 0; i < close_buttons.length; i++) {
        close_buttons[i].addEventListener('click', function (event) {
            event.preventDefault();

            let img = close_buttons[i].parentNode.querySelector('img');

            if (img && confirm('Вы действительно хотите удалить это изображение?')) {
                let image_key = img.getAttribute('data-key');

                close_buttons[i].parentNode.querySelector('input').value = '';
                img.remove();

                close_buttons[i].parentNode.querySelector('.seller-cab-company-cart__cross').removeAttribute('style');

                if (image_key && image_key != 0) {
                    (async () => {
                        let data = {key: image_key};
                        const rawResponse = await fetch('/seller/products/delete-image', {
                            method: 'POST',
                            headers: {
                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                            },
                            body: Object.keys(data).map(function(k) {
                                return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
                            }).join('&')
                        });
                        const content = await rawResponse.json();

                        console.log(content);
                    })();
                }
            }

            return false;
        });
    }
}

// ------------------------------------------------------

let file_inputs = document.querySelectorAll('input.image-product-thumb');

if (file_inputs) {
    for (let i = 0; i < file_inputs.length; i++) {
        file_inputs[i].onchange = function (evt) {
            let tgt = evt.target || window.event.srcElement,
                files = tgt.files;

            // FileReader support
            if (FileReader && files && files.length) {
                let fr = new FileReader();
                fr.onload = function () {

                    let image_thumb = document.createElement('img');
                    image_thumb.src = fr.result;
                    image_thumb.setAttribute('alt', 'Image Thumb');
                    image_thumb.setAttribute('data-key', '0');

                    file_inputs[i].parentNode.appendChild(image_thumb);

                    close_buttons[i].parentNode.querySelector('.seller-cab-company-cart__cross').style = 'z-index: -1;';
                }
                fr.readAsDataURL(files[0]);
            }

            // Not supported
            else {
                // fallback -- perhaps submit the input to an iframe and temporarily store
                // them on the server until the user's session ends.
            }
        }
    }
}

