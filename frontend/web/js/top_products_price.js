let days_option = document.querySelectorAll('div.options__conteiner.field-advertisingproductsform-days div.options__opt'),
    products__checkbox = document.querySelectorAll('li.seller-cab-order-goods__table_item')
days_input = document.querySelector('#advertisingproductsform-days'),
    price_text = document.querySelector('#w0 > div.seller-cab-adv-goods__footer > div > span');

function changePrice() {
    let checked_amount = 0,
        checked_products = document.querySelectorAll('input[name="AdvertisingProductsForm[products][]"]:checked');

    if (checked_products)
        checked_amount = checked_products.length;

    price_text.innerHTML = (price_for_one * checked_amount * days_input.value) + ' грн';
}

if (days_option) {
    for (let i = 0; i < days_option.length; i++) {
        days_option[i].addEventListener('click', function (event) {
            changePrice();
        });
    }
}

if (products__checkbox) {
    for (let i = 0; i < products__checkbox.length; i++) {
        products__checkbox[i].addEventListener('click', function (event) {
            changePrice();
        });
    }
}

changePrice();
