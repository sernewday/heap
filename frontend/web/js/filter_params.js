function reloadItems(form) {
    $.pjax.reload({
        container: '#products_container',
        method: 'get',
        data: form.serialize(),
        url: pjax_url,
        timeout: 10000
    });
}

function initRemove(form) {
    let remove_buttons = document.querySelectorAll('.filter-catalog-filter__remove');

    if (remove_buttons) {
        for (let i = 0; i < remove_buttons.length; i++) {
            remove_buttons[i].addEventListener('click', function (event) {
                reloadItems(form);
            });
        }
    }
}

let form = $('#catalog-filter-form'),
    checkboxes = document.querySelectorAll('.checkbox-filter__input'),
    price_button = document.querySelectorAll('.price-confirm'),
    clear_all = document.querySelector('.catalog-filter__remove_all');

if (clear_all) {
    clear_all.addEventListener('click', function (event) {
        let chose_params = document.querySelectorAll('.filter-catalog-filter__select');

        if (chose_params) {
            for (let i = 0; i < chose_params.length; i++) {
                chose_params[i].remove();
            }
        }

        if (checkboxes) {
            for (let i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = false;
            }
        }

        reloadItems(form);
    });
}

if (checkboxes) {
    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener('change', function (event) {
            reloadItems(form);

            initRemove(form);
        });
    }
}

if (price_button) {
    for (let i = 0; i < price_button.length; i++) {
        price_button[i].addEventListener('click', function (event) {
            event.preventDefault();

            reloadItems(form);

            return false;
        });
    }
}

initRemove(form);
