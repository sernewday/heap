/*
* Variable pjax_url should be defined at the top of the document with var
*/

var default_limit = 12,
    current_limit = default_limit;

function reloadItems(data) {
    $.pjax.reload({container: '#' + $('.pjax-more-container').attr('id'), method: 'get', data: data, url: pjax_url, timeout: 10000});
}

function moreItemsEvent() {
    let more_button = document.querySelector('a.more-products-button')

    if (!more_button)
        return false;

    more_button.addEventListener('click', function (event) {
        event.preventDefault();

        current_limit += default_limit;

        reloadItems({limit: current_limit});

        return false;
    });
}

moreItemsEvent();
