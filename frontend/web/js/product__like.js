function initLikes() {
    if (document.querySelector('html').getAttribute('data-guest') == 'true')
        return true;

    let likes = document.querySelectorAll('label.product__like');
    let counter_likes = document.querySelector('#counter-likes');

    if (likes) {
        for (let i = 0; i < likes.length; i++) {
            likes[i].addEventListener('click', function (event) {
                event.preventDefault();

                let checkbox = likes[i].querySelector('input');
                checkbox.checked = !checkbox.checked;

                if (counter_likes) {
                    counter_likes.innerText = parseInt(counter_likes.innerText) + (checkbox.checked ? 1 : -1);
                }

                $.post('/profile/add-like', {
                    product_id: likes[i].getAttribute('data-id'),
                }, function (data) {
                    console.log(data);
                });

                return false;
            });
        }
    }
}

initLikes();
