
let btn_add = document.querySelector('a.admin-editing__add');

if (btn_add) {
    btn_add.addEventListener('click', function (event) {
        event.preventDefault();

        let items = document.querySelectorAll('ul.admin-editing__menu'),
            item_copy = items[1],
            next_index = items.length - 1;

        items[items.length - 1].outerHTML += item_copy.outerHTML;

        let new_items = document.querySelectorAll('ul.admin-editing__menu'),
            new_elem = new_items[new_items.length - 1];

        let items_replace = [
            new_elem.querySelector('input[type="text"]'),
            new_elem.querySelector('textarea'),
            new_elem.querySelector('input[type="hidden"]'),
        ];

        for (let i = 0; i < items_replace.length; i++) {
            items_replace[i].setAttribute('name', items_replace[i].getAttribute('name').replace(/\d+/, next_index));
            items_replace[i].setAttribute('id', items_replace[i].getAttribute('id').replace(/\d+/, next_index));
            items_replace[i].value = '';
        }

        let span_val = new_elem.querySelector('div.admin-editing__number span');

        span_val.innerText = span_val.innerText.replace(/\d+/, next_index + 2);

        return false;
    });
}
