<?php


namespace frontend\controllers;


use common\models\Orders;
use common\models\Payments;
use yii\filters\VerbFilter;
use yii\web\Response;

class CallbackController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'order' => ['post']
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionOrder()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = json_decode(file_get_contents('php://input'), true);

        $order_num = $data['orderNo'];
        if (Orders::ENABLE_TEST_MODE)
            $order_num = str_replace('test_', '', $order_num);

        $status = $data['transactionStatus'];
        $code = $data['reasonCode'];
        $signature = $data['merchantSignature'];
        $ref = $data['orderReference'];
        $amount = $data['amount'];

        $status_return = 'accept';
        $time = time();

        $order = Orders::findOne($order_num);

        if ($order && $status == 'Approved' && $code == Orders::WAYFORPAY_CODE_OK) {
            $order->setNewStatus(Orders::STATUS_PAYED);

            $order->seller->addPayment([
                'payment_type' => Payments::TYPE_PAY_FOR_SALES,
                'order_id' => $order->id,
                'order_cost' => $order->orderSum,
                'balance_add' => $amount,
            ]);

            $order->seller->addPayment([
                'payment_type' => Payments::TYPE_COMMISSION_FOR_SALES,
                'order_id' => $order->id,
                'order_cost' => $order->orderSum,
                'balance_charge' => $amount * 10 / 100,
            ]);
        }

        $data = [
            'orderReference' => $ref,
            'status' => $status_return,
            'time' => $time,
        ];

        $data['signature'] = $order->generateSecretAnswer($data);

        \Yii::debug($data);

        return $data;
    }
}