<?php


namespace frontend\controllers;

use common\models\Likes;
use common\models\Orders;
use common\models\Products;
use common\models\Questions;
use common\models\Reviews;
use common\models\User;
use common\models\UserFamily;
use frontend\models\FamilyForm;
use frontend\models\ProfileForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class ProfileController
 * @package frontend\controllers
 */
class ProfileController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['*'],
                        'actions' => ['verify-email'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['add-like'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $model = new ProfileForm();
        $model_family = new FamilyForm();

        $model->loadUserData();

        if (($model->load(\Yii::$app->request->post()) && $model->validate() && $model->saveInfo())
            || ($model_family->load(\Yii::$app->request->post()) && $model_family->validate()) && $model_family->saveInfo())
            return $this->redirect(['/profile/index']);

        return $this->render('index', [
            'user' => User::findOne(\Yii::$app->user->getId()),
            'model' => $model,
            'model_family' => $model_family,
            'profileProgress' => User::getProfileProgress(\Yii::$app->user->getId()),
            'family' => UserFamily::getUserFamily(\Yii::$app->user->getId()),
        ]);
    }

    public function actionResendEmailVerification()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $user = User::findOne(\Yii::$app->user->getId());

        $user->sendEmailVerification(false);

        return true;
    }

    public function actionResendPhoneVerification()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $user = User::findOne(\Yii::$app->user->getId());

        $user->sendPhoneVerification();

        return true;
    }

    public function actionVerifyEmail($u, $c)
    {
        $user = User::findOne($u);
        if (!$user)
            return $this->redirect(['/profile/index']);

        if (\Yii::$app->security->validatePassword($c, $user->verification_token)) {
            $user->setEmailActive();
            $user->save();
        }

        return $this->redirect(['/profile/index']);
    }

    public function actionOrders()
    {
        return $this->render('orders', [
            'orders' => Orders::getUserOrders(\Yii::$app->user->getId()),
        ]);
    }

    public function actionReviews()
    {
        return $this->render('reviews', [
            'reviews' => Reviews::getByUser(\Yii::$app->user->getId()),
        ]);
    }

    public function actionQuestions()
    {
        return $this->render('questions', [
            'questions' => Questions::getByUser(\Yii::$app->user->getId()),
        ]);
    }

    public function actionLikes()
    {
        $params = \Yii::$app->request->queryParams;
        $params['status_id'] = Products::STATUS_ACTIVE;

        unset($params['limit']);
        $dataProvider = new ActiveDataProvider([
            'query' => Likes::getByUser(\Yii::$app->user->getId(), $params),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('likes', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddLike()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return Likes::like(Yii::$app->user->getId(), Yii::$app->request->post('product_id'));
    }
}