<?php


namespace frontend\controllers;


use common\models\Sales;
use Yii;
use yii\web\NotFoundHttpException;

class SalesController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index', [
            'sales' => Sales::find()->limit(Yii::$app->request->queryParams['limit'] ?: 12)->orderBy('created_at DESC')->all(),
        ]);
    }

    public function actionView($id)
    {
        $sale = Sales::findOne($id);
        if (!$sale)
            throw new NotFoundHttpException();

        return $this->render('view', [
            'sale' => $sale,
        ]);
    }
}