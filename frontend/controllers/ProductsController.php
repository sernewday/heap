<?php


namespace frontend\controllers;


use common\models\Orders;
use common\models\Products;
use common\models\Questions;
use common\models\Reviews;
use yii\data\ActiveDataProvider;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;

/**
 * Class ProductsController
 * @package frontend\controllers
 */
class ProductsController extends BaseController
{
    /**
     * Product cart
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $product = $this->findProduct($id);

        $cookie = \Yii::$app->request->cookies;
        $key = 'recently_viewed_products';
        if (!$cookie->has($key))
            $recently_viewed = [];
        else
            $recently_viewed = ($cookie->get($key))->value;

        if (!in_array($id, $recently_viewed))
            array_unshift($recently_viewed, $id);

        if (count($recently_viewed) > Products::$recentlyViewedLimit) {
            $removing_size = count($recently_viewed) - Products::$recentlyViewedLimit;
            for ($i = 0; $i < $removing_size; $i++)
                array_shift($recently_viewed);
        }

        \Yii::$app->getResponse()->getCookies()->add(new Cookie([
            'name' => $key,
            'value' => $recently_viewed,
            'expire' => time() + (3600 * 24 * 7),
        ]));

        $reviewsDataProvider = new ActiveDataProvider([
            'query' => Reviews::getByProduct($product->id),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        $questionsDataProvider = new ActiveDataProvider([
            'query' => Questions::getByProduct($product->id),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('view', [
            'product' => $product,
            'recently_viewed' => Products::getRecentlyViewed(),
            'reviewsDataProvider' => $reviewsDataProvider,
            'questionsDataProvider' => $questionsDataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return Products
     * @throws NotFoundHttpException
     */
    private function findProduct($id)
    {
        $model = Products::find()->where([Products::tableName() . '.id' => $id])
            ->joinWith(['image', 'sizes', 'characteristics', 'characteristics.value', 'characteristics.param'])
            ->cache(3600 * 24)
            ->one();
        if (!$model || $model->status_id != Products::STATUS_ACTIVE)
            throw new NotFoundHttpException();

        return $model;
    }
}