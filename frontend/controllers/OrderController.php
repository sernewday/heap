<?php


namespace frontend\controllers;


use common\models\Bag;
use common\models\Orders;
use common\models\User;
use frontend\models\OrderForm;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class OrderController extends BaseController
{
    public $layout = 'order';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['pay', 'thank']))
            $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $user = User::findOne(\Yii::$app->user->getId());
        if ($user->status != User::STATUS_ACTIVE)
            return $this->redirect(['/profile/index']);

        $bag = Bag::getByBrands(\Yii::$app->user->getId());
        $model = new OrderForm();

        $model->fillWithUserInfo(\Yii::$app->user->getId());

        if ($model->load(\Yii::$app->request->post()) && $model->processOrders(\Yii::$app->request->post('OrderForm'), $bag)) {
            $pay_ids = [];
            foreach ($model->_orders as $order) {
                if ($order->payment_type == Orders::PAY_ONLINE)
                    $pay_ids[] = $order->id;
            }

            if ($pay_ids)
                return $this->redirect(['/order/pay', 'o' => implode(',', $pay_ids)]);

            return $this->redirect(['/order/thank']);
        }

        return $this->render('index', [
            'model' => $model,
            'bag' => $bag,
        ]);
    }

    public function actionCancel($id)
    {
        $order = Orders::findOne($id);
        if (!$order)
            return $this->redirect(\Yii::$app->request->referrer ?: ['/']);

        if ($order->canBeCancelled() && $order->user_id == \Yii::$app->user->getId())
            $order->setNewStatus(Orders::STATUS_CANCELED, 'Отменён покупателем');

        return $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }

    public function actionThank()
    {
        $this->layout = 'main';

        return $this->render('thank');
    }

    /**
     * @param string $o
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPay($o)
    {
        $orders = explode(',', $o);
        if (!$orders)
            throw new NotFoundHttpException();

        $order_id = array_shift($orders);

        return $this->renderPartial('pay', [
            'order_ids' => $orders,
            'order' => Orders::findOne($order_id),
        ]);
    }

    public function actionTest()
    {
        echo "<pre>" . print_r(hash_hmac('md5', "test_merch_n1;shopping.sysale.com.ua;test_r_9;1", 'flk3409refn54t54t*FNJRET'), true) . "</pre>";
        exit();

        return $this->renderPartial('test');
    }
}