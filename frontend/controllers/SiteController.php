<?php

namespace frontend\controllers;

use common\models\Bag;
use common\models\Brands;
use common\models\Categories;
use common\models\LoginForm;
use common\models\Products;
use common\models\User;
use frontend\components\Lang;
use frontend\models\EmailVerifyForm;
use frontend\models\PhoneVerifyForm;
use frontend\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'login'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'signup' => ['post'],
                    'login' => ['post'],
                    'validate-signup' => ['post'],
                    'validate-code' => ['post'],
                    'validate-phone-code' => ['post'],
                    'verify-account' => ['post'],
                    'verify-login' => ['post'],
                ],
            ],
        ];
    }

    public function actionChangeLang()
    {
        $lang = Lang::getCurrentLang();

        if ($lang == 'uk')
            $new_lang = 'ru';
        else
            $new_lang = 'uk';

        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'language',
            'value' => $new_lang,
        ]));

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['messages', 'validate-code', 'verify-account', 'validate-phone-code'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'sale' => Products::getSale(),
            'last_brands' => Brands::getLast(),
            'top_categories' => Categories::getTop(),
            'new_products' => Products::find()->orderBy('created_at DESC')->limit(12)->joinWith(['image', 'category'])->groupBy(Products::tableName() . '.id')->all(),
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
//        $model->email = 'admin@gmail.com';
//        $model->password = 'admin1';
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Bag::saveFromSession();
            return $this->goHome();
        }

        return $this->goHome();
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionSignup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {

            Yii::$app->user->login(User::findOne(Yii::$app->session->get('uid')),3600 * 24 * 30);

            Bag::saveFromSession();

            return [
                'status' => 'success',
                'message' => Yii::t('app', 'Регистрация прошла успешно'),
            ];
        }

        return [
            'status' => 'error',
            'message' => Yii::t('app', 'Произошла ошибка при регистрации'),
        ];
    }

    public function actionValidateCode()
    {
        $model = new EmailVerifyForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionValidatePhoneCode()
    {
        $model = new PhoneVerifyForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionVerifyAccount()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!\Yii::$app->security->validatePassword(Yii::$app->request->post('code'), \Yii::$app->session->get('verification_code')))
            return ['status' => 'error'];

        $user = User::findOne(Yii::$app->session->get('uid') ?: Yii::$app->user->getId());
        $user->status = User::STATUS_ACTIVE;
        $user->confirmed_email = 1;

        if ($user->save()) {
            Yii::$app->user->login($user,3600 * 24 * 30);

            return ['status' => 'success'];
        }

        return ['status' => 'error'];
    }

    public function actionVerifyAccountPhone()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!\Yii::$app->security->validatePassword(Yii::$app->request->post('code'), \Yii::$app->session->get('verification_phone_code')))
            return ['status' => 'error'];

        $user = User::findOne(Yii::$app->session->get('uid') ?: Yii::$app->user->getId());
        $user->confirmed_phone = 1;

        if ($user->save()) {
            return ['status' => 'success'];
        }

        return ['status' => 'error'];
    }

    public function actionValidateLogin()
    {
        $model = new LoginForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * @return array
     */
    public function actionValidateSignup()
    {
        $model = new SignupForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionMessages()
    {
        $data = require dirname(__DIR__) . '/messages/uk/app.php';

        if (Yii::$app->request->isPost) {
            $new_arr = [];
            foreach (Yii::$app->request->post('messages') as $item) {
                $new_arr[$item['key']] = $item['value'];
            }

            $new_file = "<?php \n return " . var_export($new_arr, true) . ';';

            file_put_contents(dirname(__DIR__) . '/messages/uk/app.php', $new_file);

            return $this->redirect(Url::current());
        }

        return $this->renderPartial('messages', [
            'data' => $data
        ]);
    }
}
