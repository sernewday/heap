<?php


namespace frontend\controllers;


use common\models\Reviews;
use common\models\UploadForm;
use frontend\models\ProductReviewForm;
use frontend\models\QuestionsForm;
use frontend\models\SellerReviewForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Class ReviewsController
 * @package frontend\controllers
 */
class ReviewsController extends BaseController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                    'add-question' => ['post'],
                    'add-seller' => ['post'],
                ],
            ],
        ];
    }

    public function actionAdd()
    {
        $model = new ProductReviewForm();
        $model_upload = new UploadForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $model_upload->save(Reviews::tableName(), $model->_review->id);
        }

        return $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }

    public function actionAddQuestion()
    {
        $model = new QuestionsForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate())
            $model->save();

        return $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }

    public function actionAddSeller()
    {
        $model = new SellerReviewForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->save();
        }

        return $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }
}