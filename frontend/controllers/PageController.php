<?php


namespace frontend\controllers;

use common\models\Pages;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends BaseController
{
    public function actionPartner()
    {
        $page = Pages::findOne(['name' => 'partner']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('partner', [
            'page' => $page,
        ]);
    }

    public function actionReturn()
    {
        $page = Pages::findOne(['name' => 'return']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('return', [
            'page' => $page,
        ]);
    }

    public function actionFaq()
    {
        $page = Pages::findOne(['name' => 'questions']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('faq', [
            'page' => $page,
        ]);
    }

    public function actionDelivery()
    {
        $page = Pages::findOne(['name' => 'delivery']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('delivery', [
            'page' => $page,
        ]);
    }

    public function actionGuarantee()
    {
        $page = Pages::findOne(['name' => 'guarantee']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('guarantee', [
            'page' => $page,
        ]);
    }

    public function actionAbout()
    {
        $page = Pages::findOne(['name' => 'about']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('about', [
            'page' => $page,
        ]);
    }

    public function actionPartnerProgram()
    {
        $page = Pages::findOne(['name' => 'partner_program']);
        if (!$page)
            throw new NotFoundHttpException();

        return $this->render('partner_program', [
            'page' => $page,
        ]);
    }
}