<?php


namespace frontend\controllers;


use common\models\Bag;
use common\models\Brands;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;

class BagController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add-product' => ['post'],
                    'remove-product' => ['post'],
                    'remove-bag-product' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['add-product', 'remove-product'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->layout = 'order';

        if (!Bag::bagHasProducts(\Yii::$app->user->getId()) || \Yii::$app->user->isGuest)
            return $this->redirect(\Yii::$app->request->referrer ?: ['/']);


        return $this->render('index', [
            'bag' => Bag::getByBrands(\Yii::$app->user->getId()),
        ]);
    }

    public function actionAddProduct()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Bag();

        if ($model->loadAttrs(\Yii::$app->request->post()) && $model->validate() && $model->saveBag())
            return ['status' => 'success', 'products' => Bag::renderBagProducts($model->user_id)];

        return ['status' => 'error'];
    }

    public function actionRemoveProduct()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Bag();

        if ($model->loadAttrs(\Yii::$app->request->post()) && $model->validate() && $model->remove())
            return ['status' => 'success', 'products' => Bag::renderBagProducts($model->user_id)];

        return ['status' => 'error'];
    }

    public function actionRemoveBagProduct()
    {
        $model = new Bag();

        if ($model->loadAttrs(\Yii::$app->request->get()) && $model->validate())
            $model->remove();

        return $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }
}