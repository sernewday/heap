<?php


namespace frontend\controllers;


use common\models\Categories;
use common\models\Products;
use yii\data\ActiveDataProvider;

class CategoryController extends BaseController
{
    public function actionIndex($alias)
    {
        $category = Categories::findOne(['name_alt' => $alias]);

        $params = \Yii::$app->request->queryParams;
        $params['status_id'] = Products::STATUS_ACTIVE;

        $dataProvider = new ActiveDataProvider([
            'query' => Products::byCategory($category->id, $params),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'category' => $category,
            'products' => $dataProvider,
        ]);
    }
}