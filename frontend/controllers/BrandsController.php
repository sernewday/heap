<?php


namespace frontend\controllers;


use common\models\Brands;
use common\models\Categories;
use common\models\Products;
use common\models\Reviews;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class BrandsController extends BaseController
{
    public function actionIndex($l = null)
    {
        $brands = Brands::find()->orderBy('name');
        if ($l) {
            $condition = 'name LIKE :p';
            $params = [':p' => $l . '%'];

            $brands->andWhere(new Expression($condition, $params));
        }

        return $this->render('index', [
            'brands' => $brands->all(),
            'top_brands' => Brands::getTop(30),
            'recently_viewed' => Products::getRecentlyViewed(),
        ]);
    }

    /**
     * @param $id
     * @param int $category
     * @return string
     */
    public function actionView($id, $category = 0)
    {
        $params = \Yii::$app->request->queryParams;
        $params['status_id'] = Products::STATUS_ACTIVE;

        unset($params['id']);
        $dataProvider = new ActiveDataProvider([
            'query' => Products::byBrand($id, $category, $params),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('view', [
            'brand' => Brands::find()->where([Brands::tableName() . '.id' => $id])->joinWith(['image'])->one(),
            'reviews' => Reviews::getByBrand($id),
            'brand_products' => $dataProvider,
            'category' => $category == 0 ? false : Categories::findOne($category),
        ]);
    }
}