<?php


namespace frontend\controllers;


use frontend\components\Lang;
use frontend\components\Target;
use Yii;

/**
 * Class BaseController
 * Main controller. Defines the current language
 * @package frontend\controllers
 */
class BaseController extends \yii\web\Controller
{
    /**
     * Sets up a language
     */
    public function init()
    {
        parent::init();

        \Yii::$app->language = Lang::getCurrentLang();

        if (!Yii::$app->session->has(Target::TARGET_KEY))
            Yii::$app->session->set(Target::TARGET_KEY, 'f');

        if (Yii::$app->request->get(Target::TARGET_PARAM) && in_array(Yii::$app->request->get(Target::TARGET_PARAM), ['k', 'f', 'm'])) {
            Yii::$app->session->set(Target::TARGET_KEY, Yii::$app->request->get(Target::TARGET_PARAM));
        }

    }
}