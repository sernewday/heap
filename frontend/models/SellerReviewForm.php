<?php


namespace frontend\models;


use common\models\Brands;
use common\models\Orders;
use common\models\Reviews;
use yii\base\Model;

/**
 * Class SellerReviewForm
 * @package frontend\models
 */
class SellerReviewForm extends Model
{
    /**
     * @var Reviews
     */
    public $_review;

    public $record_id;
    public $module;

    public $comment;

    public $is_polite;
    public $is_online;
    public $perfect_quality;
    public $delivery_in_time;

    public $order_id;

    public $score;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'module', 'comment', 'is_polite', 'is_online', 'perfect_quality', 'delivery_in_time'], 'required'],

            [['module', 'comment'], 'string', 'max' => 255],

            [['is_polite', 'is_online', 'perfect_quality', 'delivery_in_time'], 'boolean'],

            [['score', 'order_id'], 'integer'],
            [['record_id'], 'integer', 'min' => 1],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'comment' => \Yii::t('app', 'Отзыв'),
            'is_polite' => \Yii::t('app', 'Общение'),
            'is_online' => \Yii::t('app', 'Контактность'),
            'perfect_quality' => \Yii::t('app', 'Качество'),
            'delivery_in_time' => \Yii::t('app', 'Доставка'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {

        $review = new Reviews();
        $review->setAttributes($this->getAttributes());

        $review->module = Brands::tableName();
        $review->reply_to = 0;
        $review->user_id = \Yii::$app->user->getId();

        $review->confirmed = 0;
        if ($review->order_id != 0 && Orders::find()
                ->where([Orders::tableName() . '.id' => Orders::STATUS_SUCCESS])
                ->exists())
            $review->confirmed = 1;

        if ($review->save()) {
            $this->_review = $review;
            return true;
        }

        return false;
    }
}