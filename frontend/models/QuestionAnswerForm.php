<?php


namespace frontend\models;


use common\models\Questions;
use yii\base\Model;

/**
 * Class QuestionAnswerForm
 * @package frontend\models
 */
class QuestionAnswerForm extends Model
{
    public $id;
    public $answer;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['id', 'answer'], 'required'],

            [['id'], 'integer'],

            [['answer'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'answer' => \Yii::t('app', 'Ответ'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->id == 0)
            return false;

        $question = Questions::findOne($this->id);
        $question->answer = $this->answer;
        $question->is_answered = 1;
        $question->is_viewed = 0;
        $question->answered_at = time();

        return $question->save();
    }
}