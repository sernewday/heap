<?php


namespace frontend\models;


use common\models\Categories;
use common\models\Infos\CategoriesInfo;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class ModuleCategoryForm extends Model
{
    public $name;

    /**
     * @var Categories
     */
    public $_category;

    /**
     * @var CategoriesInfo
     */
    public $_info;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'trim'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app' ,'Название категории'),
        ];
    }

    public function loadData($id, $lang)
    {
        $this->_category = Categories::findOne($id);
        if (!$this->_category)
            throw new NotFoundHttpException();

        $this->_info = CategoriesInfo::findOne(['record_id' => $id, 'lang' => $lang]);

        $this->name = $this->_info->name;
    }

    public function save($lang)
    {
        if (!$this->_category)
            return false;

        if (!$this->_info)
            $this->_info = new CategoriesInfo(['record_id' => $this->_category->id, 'lang' => $lang]);

        $this->_info->name = $this->name;

        return $this->_info->save();
    }
}