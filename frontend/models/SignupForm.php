<?php

namespace frontend\models;

use common\models\User;
use common\models\UserInfo;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $repeat_password;

    public $name;
    public $surname;
    public $phone;

    public $agree;

    public $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name', 'surname', 'phone'], 'trim'],
            [['email', 'name', 'surname', 'phone', 'password', 'repeat_password'], 'required'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'Такая почта уже используется')],

            [['password', 'repeat_password'], 'required'],
            [['password', 'repeat_password'], 'string', 'min' => 6],
            [['repeat_password'], 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false, 'message' => "Пароли не совпадают"],

            [['name', 'surname'], 'string', 'min' => 2, 'max' => 255],

            [['phone'], 'match', 'pattern' => '/\+380\d\d\d\d\d\d\d\d\d/', 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],
            [['phone'], 'string', 'min' => 13, 'max' => 13, 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],
            [['phone'], 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'Такой телефон уже используется')],

            [['agree'], 'boolean'],
//            [['agree'], 'required', 'requiredValue' => 1, 'message' => 'Для продолжения регистрации согласитесь с условиями Договора публичной оферты'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status = User::STATUS_INACTIVE;
        $user->user_type = User::TYPE_CUSTOMER;

        $user->phone = $this->phone;

        $result = $user->save();

        // Assign role customer
        $auth = Yii::$app->authManager;
        $authorRole = $auth->getRole('customer');
        $auth->assign($authorRole, $user->id);

        // Set user info
        $user_info = new UserInfo();
        $user_info->user_id = $user->id;

        $user_info->name = $this->name;
        $user_info->surname = $this->surname;

        $user_info->save();

        Yii::$app->session->set('uid', $user->id);

        $user->sendEmailVerification();

        return $result;
    }
}
