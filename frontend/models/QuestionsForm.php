<?php


namespace frontend\models;


use common\models\Questions;
use yii\base\Model;

/**
 * Class QuestionsForm
 * @package frontend\models
 */
class QuestionsForm extends Model
{
    public $record_id;
    public $module;

    public $question;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'module', 'question'], 'required'],

            [['record_id'], 'integer'],

            [['module', 'question'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
          'question' => \Yii::t('app', 'Вопрос'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $question = new Questions();

        $question->setAttributes($this->getAttributes());
        $question->setNew();
        $question->from_id = \Yii::$app->user->getId();

        return $question->save();
    }
}