<?php


namespace frontend\models;


use common\models\Brands;
use common\models\Infos\ProductsInfo;
use common\models\ProductParams;
use common\models\Products;
use frontend\components\Size;
use Yii;
use yii\base\Model;

/**
 * Class ModuleProductsForm
 * @package frontend\models
 */
class ModuleProductsForm extends Model
{
    /**
     * @var string[] List of the characteristics associated with param id
     */
    public static $characteristics_list = [
        ProductParams::PARAM_COUNTRY => 'country_char',
        ProductParams::PARAM_STYLE => 'style_char',
        ProductParams::PARAM_SEASON => 'season_char',
        ProductParams::PARAM_GENDER => 'gender_char',
        ProductParams::PARAM_MATERIAL => 'material_char',
        ProductParams::PARAM_COLOR => 'color_char',
        ProductParams::PARAM_LENGTH => 'length_char',
        ProductParams::PARAM_CLASP => 'clasp_char',
    ];
    /**
     * @var bool
     */
    public $isNew = true;
    /**
     * @var Products
     */
    public $_product;
    /**
     * @var ProductsInfo
     */
    public $_info;
    public $name;
    public $vendor_code;
    public $description;
    public $price;
    public $price_discount;
    public $available;
    public $amount_left;
    public $category_id;
    public $images;
    public $producer_char;
    public $country_char;
    public $style_char;
    public $season_char;
    public $gender_char;
    public $material_char;
    public $color_char = [];
    public $length_char;
    public $clasp_char;
    public $size = [];
    public $size_international = [];

    public function rules()
    {
        return [
            [['name', 'description', 'price', 'available', 'amount_left', 'category_id'], 'required'],

            [['name', 'vendor_code'], 'string', 'max' => 255],

            [['price', 'price_discount'], 'double', 'min' => 1],

            [['available'], 'boolean'],

            [['amount_left'], 'integer', 'min' => 0],

            [['category_id'], 'integer'],

            [['images'], 'image', 'extensions' => 'png, jpg, jpeg, svg'],

            [['producer_char', 'country_char', 'style_char', 'season_char', 'gender_char', 'material_char',
                'length_char', 'clasp_char'], 'integer'],
            [['color_char'], 'each', 'rule' => ['integer']],

            [['size'], 'each', 'rule' => ['integer', 'min' => 1]],
            [['size_international'], 'each', 'rule' => ['string']],
            [['size_international'], 'each', 'rule' => ['in', 'range' => Size::$international_sizes]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app', 'Название товара'),
            'vendor_code' => \Yii::t('app', 'Артикул'),
            'description' => \Yii::t('app', 'Описание'),
            'price' => \Yii::t('app', 'Стандартная цена'),
            'price_discount' => \Yii::t('app', 'Со скидкой (если есть)'),
            'available' => \Yii::t('app', 'Наличие'),
            'amount_left' => \Yii::t('app', 'Остатки'),
            'category_id' => \Yii::t('app', 'Категория'),
            'producer_char' => \Yii::t('app', 'Производитель'),
            'country_char' => \Yii::t('app', 'Страна'),
            'style_char' => \Yii::t('app', 'Стиль'),
            'season_char' => \Yii::t('app', 'Сезон'),
            'gender_char' => \Yii::t('app', 'Пол'),
            'material_char' => \Yii::t('app', 'Материал'),
            'color_char' => \Yii::t('app', 'Цвет'),
            'length_char' => \Yii::t('app', 'Длина'),
            'clasp_char' => \Yii::t('app', 'Застежка'),
            'size' => \Yii::t('app', 'Размер'),
            'size_international' => \Yii::t('app', 'Шкала размера (международная)'),
        ];
    }

    /**
     * @param int $product_id
     * @param int $lang
     * @return bool
     */
    public function loadProduct($product_id, $lang)
    {
        $product = Products::find()->joinWith(['image', 'images', 'characteristics'])->where([Products::tableName() . '.id' => $product_id])->one();
        $info = ProductsInfo::findOne(['record_id' => $product_id, 'lang' => $lang]);
        if (!$product)
            return false;

        $this->_product = $product;
        $this->_info = $info;

        $this->setAttributes($product->getAttributes());
        $this->setAttributes($info->getAttributes());

        $this->producer_char = $product->brand_id;

        $this->price_discount = $product->discount;

        $this->isNew = false;

        foreach ($product->characteristics as $characteristic) {
            if (isset(self::$characteristics_list[$characteristic->param_id])) {
                $value = self::$characteristics_list[$characteristic->param_id];

                if (!is_array($this->$value))
                    $this->$value = $characteristic->value_id;
                else
                    $this->$value[] = $characteristic->value_id;
            }
        }

        foreach ($product->sizes as $size) {
            $this->size[] = $size->size;
            $this->size_international[] = $size->size_international;
        }

        return true;
    }

    /**
     * @param int $lang
     * @return bool
     */
    public function save($lang = 1)
    {
        if ($this->isNew) {

            $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
            if (!$brand && Yii::$app->controller->module->id != 'admin')
                return false;

            $this->_product = new Products();
            $this->_product->brand_id = $brand->id;
            $this->_product->currency_id = Products::CURRENCY_UAH;
            $this->_product->status_id = Products::STATUS_ACTIVE;
        }

        // Check if category is changed
        $change_category = false;
        if ($this->category_id != $this->_product->category_id)
            $change_category = true;

        $this->_product->setAttributes($this->getAttributes());

        if ($this->producer_char != $this->_product->brand_id && Yii::$app->controller->module->id == 'admin')
            $this->_product->brand_id = $this->producer_char;

        if ($this->_product->save()) {
            // Add new category if changed
            if ($change_category)
                $this->_product->addNewCategory($this->category_id);

            if ($this->isNew) {
                $this->_info = new ProductsInfo(['record_id' => $this->_product->id, 'lang' => 1]);
            }

            $this->_info->setAttributes($this->getAttributes());

            // Add characteristics
            foreach (self::$characteristics_list as $param_id => $value) {
                if (!$this->$value)
                    continue;

                if (!is_array($this->$value))
                    $this->_product->addCharacteristic($param_id, $this->$value);
                else {
                    foreach ($this->$value as $item_value) {
                        if (!$item_value)
                            continue;

                        $this->_product->addCharacteristic($param_id, $item_value);
                    }
                }
            }

            // Add sizes
            foreach ($this->size as $size_key => $size) {
                if (!$size || !$this->size_international[$size_key])
                    continue;

                $this->_product->addSize($size, $this->size_international[$size_key]);
            }

            return $this->_info->save();
        }

        \Yii::debug($this->_product->errors);

        return false;
    }
}