<?php


namespace frontend\models;


use common\models\Bag;
use common\models\Brands;
use common\models\OrderItems;
use common\models\Orders;
use common\models\User;
use Yii;
use yii\base\Model;

class OrderForm extends Model
{
    public $full_name;
    public $phone;
    public $email;

    public $delivery_type;
    public $city;
    public $post_number;
    public $street;
    public $house;
    public $flat;

    public $payment_type;
    public $comment;

    /**
     * @var Orders[]
     */
    public $_orders = [];

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'phone', 'email', 'city', 'payment_type', 'delivery_type'], 'required'],

            [['full_name', 'email', 'city', 'street', 'house', 'flat', 'comment'], 'trim'],
            [['full_name', 'email', 'city', 'street', 'house', 'flat', 'comment'], 'string', 'max' => 255],

            [['post_number'], 'post_number_validation'],

            [['full_name'], 'match', 'pattern' => '/^[а-яА-Я\s\-ії]+$/ui', 'message' => Yii::t('app', 'Это поле может содержать только буквы')],

            [['email'], 'email'],
            [['email'], 'string', 'max' => 255],

            [['phone'], 'match', 'pattern' => '/\+380\d\d\d\d\d\d\d\d\d/', 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],
            [['phone'], 'string', 'min' => 13, 'max' => 13, 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],

            [['city', 'street', 'house', 'flat'], 'match', 'pattern' => '/^[a-zA-Zа-яА-Я0-9іїё\/\-\s]+$/iu', 'message' => Yii::t('app', 'Разрешены только буквы, цифры и некоторые символы (/ -)')],

            [['delivery_type', 'post_number', 'payment_type'], 'integer'],
            [['delivery_type'], 'in', 'range' => [Orders::DELIVERY_NEW_POST, Orders::DELIVERY_ADDRESS]],
            [['payment_type'], 'in', 'range' => [Orders::PAY_AFTER, Orders::PAY_ONLINE, Orders::PAY_TRANSFER]],
        ];
    }

    public function post_number_validation($attribute_name, $params)
    {
        if ($this->delivery_type == Orders::DELIVERY_NEW_POST && $this->post_number == '') {
            $this->addError($attribute_name, Yii::t('app', 'Вам необходимо указать номер отделения'));
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'full_name' => Yii::t('app', 'ФИО'),
            'phone' => Yii::t('app', 'Телефонный номер'),
            'email' => Yii::t('app', 'E-Mail'),
            'city' => Yii::t('app', 'Город'),
            'post_number' => Yii::t('app', 'Отделение Новой почты'),
            'street' => Yii::t('app', 'Улица'),
            'house' => Yii::t('app', 'Дом'),
            'flat' => Yii::t('app', 'Квартира'),
            'comment' => Yii::t('app', 'Комментарий к заказу'),
        ];
    }

    /**
     * Fills the form with user info
     * @param int $user_id
     */
    public function fillWithUserInfo($user_id)
    {
        $user = User::findOne($user_id);

        $this->setAttributes($user->getAttributes());
        $this->setAttributes($user->info->getAttributes());

        if ($user->info) {
            $this->full_name = trim($user->info->surname . ' ' . $user->info->name);
        }
    }

    /**
     * @param array $data
     * @param Brands[] $bag
     * @return bool
     */
    public function processOrders($data, $bag)
    {
        $index = 0;
        foreach ($bag as $item) {
            // Load payment type and comment for a specific order
            $this->payment_type = $data['payment_type'][$index];
            $this->comment = $data['comment'][$index];

            // Check if the form is valid
            if ($this->post_number_validation('post_number', []) && $this->validate()) {
                $order = new Orders();
                $order->setAttributes($this->getAttributes());

                $order->user_id = Yii::$app->user->getId();
                $order->status_id = Orders::STATUS_NEW;
                $order->seller_id = $item->products[0]->brand_id;

                if ($order->save()) {
                    $this->_orders[] = $order;

                    $order->setNewStatus(Orders::STATUS_NEW);

                    // Saving order products
                    foreach ($item->products as $product) {
                        if (!$product->bag) continue;

                        foreach ($product->bag as $product_bag) {
                            $order_item = new OrderItems();
                            $order_item->order_id = $order->id;
                            $order_item->product_id = $product->id;
                            $order_item->amount = $product_bag->quantity;
                            $order_item->price = $order_item->amount * $product->discountPrice;
                            $order_item->price_for_one = $product->discountPrice;
                            $order_item->size = "$product_bag->size";
                            $order_item->color = "$product_bag->color";

                            $order_item->save();
                        }
                    }
                }
            } else {
                return false;
            }

            $index++;
        }

        // Clear user's bag
        Bag::clearUserBag(Yii::$app->user->getId());

        return true;
    }
}