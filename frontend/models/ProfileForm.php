<?php


namespace frontend\models;


use common\models\User;
use common\models\UserInfo;
use Yii;

/**
 * Class ProfileForm
 * @package frontend\models
 */
class ProfileForm extends \yii\base\Model
{
    public $name;
    public $surname;
    public $phone;
    public $email;
    public $new_password;

    public $city;
    public $street;
    public $house;
    public $flat;

    public $birthday_year;
    public $birthday_month;
    public $birthday_day;

    public $gender;

    public $size_clothes;
    public $size_shoes;
    public $size_chest;
    public $size_waist;
    public $size_hips;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'phone', 'email'], 'required'],
            [['name', 'surname', 'phone', 'email'], 'string'],

            [['email'], 'email'],

            [['name', 'surname'], 'match', 'pattern' => '/^[а-яА-Я\s\-і]+$/ui', 'message' => Yii::t('app', 'Это поле может содержать только буквы')],

            [['phone'], 'match', 'pattern' => '/\+380\d\d\d\d\d\d\d\d\d/', 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],
            [['phone'], 'string', 'min' => 13, 'max' => 13, 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],

            [['new_password'], 'string', 'min' => 6, 'max' => 30],

            [['city', 'street', 'house', 'flat'], 'string'],
            [['city', 'street', 'house', 'flat'], 'match', 'pattern' => '/^[a-zA-Zа-яА-Я0-9іїё\/\-\s]+$/iu', 'message' => Yii::t('app', 'Разрешены только буквы, цифры и некоторые символы (/ -)')],

            [['birthday_year'], 'integer', 'min' => date('Y') - 60, 'max' => date('Y') - 13],
            [['birthday_month'], 'integer', 'min' => 1, 'max' => 12],
            [['birthday_day'], 'integer', 'min' => 1, 'max' => 31],

            [['gender'], 'integer'],
            [['gender'], 'in', 'range' => [1, 2]],

            [['size_clothes'], 'integer', 'min' => 40, 'max' => 60],
            [['size_shoes'], 'integer', 'min' => 15, 'max' => 50],
            [['size_chest'], 'integer', 'min' => 50, 'max' => 140],
            [['size_waist'], 'integer', 'min' => 50, 'max' => 140],
            [['size_hips'], 'integer', 'min' => 80, 'max' => 150],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Имя'),
            'surname' => Yii::t('app', 'Фамилия'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'E-Mail'),
            'new_password' => Yii::t('app', 'Пароль'),
            'city' => Yii::t('app', 'Город'),
            'street' => Yii::t('app', 'Улица'),
            'house' => Yii::t('app', 'Дом'),
            'flat' => Yii::t('app', 'Квартира'),
            'birthday_year' => Yii::t('app', 'Год'),
            'birthday_month' => Yii::t('app', 'Месяц'),
            'birthday_day' => Yii::t('app', 'День'),
            'gender' => Yii::t('app', 'Пол'),
            'size_clothes' => Yii::t('app', 'Размер верхней одежды'),
            'size_shoes' => Yii::t('app', 'Размер обуви'),
            'size_chest' => Yii::t('app', 'Обхват груди'),
            'size_waist' => Yii::t('app', 'Обхват талии'),
            'size_hips' => Yii::t('app', 'Обхват бедер'),
        ];
    }

    public function loadUserData()
    {
        $this->email = Yii::$app->user->identity->email;
        $this->phone = Yii::$app->user->identity->phone;

        $info = UserInfo::findOne(['user_id' => Yii::$app->user->getId()]);
        if ($info) {
            $this->setAttributes($info->getAttributes());

            if ($info->birthday) {
                $date = strtotime($info->birthday);
                $this->birthday_year = date('Y', $date);
                $this->birthday_month = date('n', $date);
                $this->birthday_day = date('d', $date);
            }
        }
    }

    public function saveInfo()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);

        if ($user->email != $this->email) {
            $user_check = User::findOne(['email' => $this->email]);
            if ($user_check) {
                $this->addError('email', Yii::t('app', 'Такая почта уже используется'));
                return false;
            }

            $user->generateEmailVerificationToken();
            $user->confirmed_email = 0;
            $user->status = User::STATUS_INACTIVE;

            $user->username = $this->email;
            $user->email = $this->email;
        }

        if ($user->phone != $this->phone) {
            $user_check = User::findOne(['phone' => $this->phone]);
            if ($user_check) {
                $this->addError('email', Yii::t('app', 'Такая почта уже используется'));
                return false;
            }

            $user->generatePhoneVerificationToken();
            $user->confirmed_phone = 0;

            $user->phone = $this->phone;
        }

        if ($user->save()) {
            $user_info = UserInfo::findOne(['user_id' => $user->id]);
            if (!$user_info)
                $user_info = new UserInfo(['user_id' => $user->id]);

            $user_info->setAttributes($this->getAttributes());

            $user_info->birthday = $this->birthday_year . '-' . sprintf('%02d', $this->birthday_month) . '-' . sprintf('%02d', $this->birthday_day);

            return $user_info->save();
        }

        return false;
    }
}