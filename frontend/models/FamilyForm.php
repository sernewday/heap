<?php


namespace frontend\models;


use common\models\UserFamily;
use Yii;
use yii\base\Model;

class FamilyForm extends Model
{
    public $full_name;
    public $gender;

    public $birthday_year;
    public $birthday_month;
    public $birthday_day;

    public $size_clothes;
    public $size_shoes;
    public $size_chest;
    public $size_waist;
    public $size_hips;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'gender', 'birthday_year', 'birthday_month', 'birthday_day'], 'required'],
            [['full_name'], 'string'],

            [['full_name'], 'match', 'pattern' => '/^[а-яА-Я\s\-і]+$/ui', 'message' => Yii::t('app', 'Это поле может содержать только буквы')],

            [['birthday_year'], 'integer', 'min' => date('Y') - 70, 'max' => date('Y')],
            [['birthday_month'], 'integer', 'min' => 1, 'max' => 12],
            [['birthday_day'], 'integer', 'min' => 1, 'max' => 31],

            [['gender'], 'integer'],
            [['gender'], 'in', 'range' => [1, 2]],

            [['size_clothes'], 'integer', 'min' => 40, 'max' => 60],
            [['size_shoes'], 'integer', 'min' => 15, 'max' => 50],
            [['size_chest'], 'integer', 'min' => 50, 'max' => 140],
            [['size_waist'], 'integer', 'min' => 50, 'max' => 140],
            [['size_hips'], 'integer', 'min' => 80, 'max' => 150],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'full_name' => Yii::t('app', 'ФИО'),
            'birthday_year' => Yii::t('app', 'Год'),
            'birthday_month' => Yii::t('app', 'Месяц'),
            'birthday_day' => Yii::t('app', 'День'),
            'gender' => Yii::t('app', 'Пол'),
            'size_clothes' => Yii::t('app', 'Размер верхней одежды'),
            'size_shoes' => Yii::t('app', 'Размер обуви'),
            'size_chest' => Yii::t('app', 'Обхват груди'),
            'size_waist' => Yii::t('app', 'Обхват талии'),
            'size_hips' => Yii::t('app', 'Обхват бедер'),
        ];
    }

    /**
     * @return bool
     */
    public function saveInfo()
    {
        $family_info = new UserFamily(['user_id' => Yii::$app->user->getId()]);

        $family_info->setAttributes($this->getAttributes());

        $family_info->birthday = $this->birthday_year . '-' . sprintf('%02d', $this->birthday_month) . '-' . sprintf('%02d', $this->birthday_day);

        $family_info->save();

        Yii::debug($family_info->errors);

        return true;
    }
}