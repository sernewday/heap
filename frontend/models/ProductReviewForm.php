<?php


namespace frontend\models;


use common\models\OrderItems;
use common\models\Orders;
use common\models\Reviews;
use yii\base\Model;

/**
 * Class ProductReviewForm
 * @package frontend\models
 */
class ProductReviewForm extends Model
{
    /**
     * @var Reviews
     */
    public $_review;

    public $record_id;
    public $module;

    public $comment;

    public $small_size;
    public $correct_color;
    public $perfect_quality;
    public $delivery_in_time;

    public $score;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'module', 'comment', 'small_size', 'correct_color', 'perfect_quality', 'delivery_in_time'], 'required'],

            [['module', 'comment'], 'string', 'max' => 255],

            [['small_size', 'correct_color', 'perfect_quality', 'delivery_in_time'], 'boolean'],

            [['score'], 'integer'],
            [['record_id'], 'integer', 'min' => 1],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'comment' => \Yii::t('app', 'Отзыв'),
            'small_size' => \Yii::t('app', 'Размер'),
            'correct_color' => \Yii::t('app', 'Цвет'),
            'perfect_quality' => \Yii::t('app', 'Качество'),
            'delivery_in_time' => \Yii::t('app', 'Доставка'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {

        $review = new Reviews();
        $review->setAttributes($this->getAttributes());

        $review->reply_to = 0;
        $review->user_id = \Yii::$app->user->getId();

        $review->confirmed = 0;
        if (Orders::find()
            ->where([Orders::tableName() . '.user_id' => $review->user_id, OrderItems::tableName() . '.product_id' => $review->record_id])
            ->andWhere([Orders::tableName() . '.status_id' => Orders::STATUS_SUCCESS])
            ->joinWith(['products'])
            ->exists())
            $review->confirmed = 1;

        if ($review->save()) {
            $this->_review = $review;
            return true;
        }

        return false;
    }
}