<?php


namespace frontend\models;


use yii\base\Model;

class PhoneVerifyForm extends Model
{
    public $code;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'compare_codes'],
        ];
    }

    /**
     * @param $attribute_name
     * @param $param
     * @return bool
     */
    public function compare_codes($attribute_name, $param)
    {
        $code_session = \Yii::$app->session->get('verification_phone_code');
        if (!$code_session) {
            $this->addError($attribute_name, 'Ошибка');
            return false;
        }

        if (!\Yii::$app->security->validatePassword($this->code, $code_session)) {
            $this->addError($attribute_name, 'Неверный код');
            return false;
        }

        return true;
    }
}