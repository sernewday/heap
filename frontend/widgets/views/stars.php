<?php

/* @var $this \yii\web\View */
/* @var $whole int */
/* @var $fraction int|float */

?>

<?php for ($i = 0; $i < 5; $i++): ?>
    <div class="stars__example">
        <i class="fas fa-star-half <?= ($i < $whole || ($i == $whole && $fraction > 4)) ? 'active' : '' ?>"></i>
        <i class="fas fa-star-half <?= $i < $whole ? 'active' : '' ?>"></i>
    </div>
<?php endfor; ?>
