<?php

/* @var $this \yii\web\View */
/* @var $param_id int */
/* @var $name string */
/* @var $name_input string */
/* @var $key string */
/* @var $value_key string */
/* @var $values \common\models\ProductParamsValues[]|\common\models\Brands[] */

$count_values = count($values);
?>

<div class="catalog-filter__filter filter-catalog-filter">
    <div class="filter-catalog-filter__name">
        <?= $name ?>
        <i class="fas fa-chevron-up"></i>
    </div>
    <div class="filter-catalog-filter__body">
        <?php for ($i = 0; $i < $count_values; $i++): ?>

            <?php
            if ($i % 8 == 0) {
                if ($i != 0 && ($i + 1) != $count_values) echo '</div>';

                if (($i + 1) != $count_values || $count_values == 1) echo '<div class="filter-catalog-filter__column">';
            }
            ?>

            <div class="filter-catalog-filter__example">
                <label class="checkbox-filter">
                    <input class="checkbox-filter__input" type="checkbox" name="<?= $name_input ?>" value="<?= $values[$i]->$key ?>" data-id="value-<?= $values[$i]->$key ?>">
                    <span class="checkbox-filter__fake">
                        <svg fill="none" height="8"
                             viewBox="0 0 9 8"
                             width="9"
                             xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
fill="#FFF" stroke-width="0.2"/>
</svg>
                    </span>
                    <span class="checkbox-filter__text"><?= $values[$i]->$value_key ?></span>
                </label>
            </div>

            <?php
            if (($i + 1) == $count_values) {
                echo '</div>';
            }
            ?>

        <?php endfor; ?>
    </div>
</div>
