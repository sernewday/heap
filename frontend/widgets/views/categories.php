<?php

/* @var $this \yii\web\View */

use common\models\Categories;
use yii\helpers\Url;

/* @var $category Categories|false */
/* @var $main_categories Categories[] */

$category_parent_ids = $category ? explode('.', $category->path) : [];
?>

<?php foreach ($main_categories as $main_category): ?>
    <?php $isCurrent = ($category->id == $main_category->id || in_array($main_category->id, $category_parent_ids)); ?>
    <div class="catalog-menu__container <?= $isCurrent ? 'active' : '' ?>">
        <div class="catalog-menu__title">
            <p><?= Yii::t('app', $main_category->info->name) ?></p>
            <span><?= $main_category->productsAmount ?></span>
        </div>
        <ul class="catalog-menu">
            <?php if ($isCurrent): ?>
                <?php foreach ($main_category->childs as $category_child): ?>
                    <?php $isSubActive = ($category_child->id == $category->id || $category->parent_id == $category_child->id); ?>
                    <li class="catalog-menu__item <?= $isSubActive ? 'active' : '' ?>">
                        <a class="catalog-menu__link <?= $isSubActive ? 'active__link' : '' ?>"
                           href="<?= Url::to(['/category/index', 'alias' => $category_child->name_alt]) ?>">
                            <?= Yii::t('app',$category_child->info->name) ?><span><?= $category_child->productsAmount ?></span>
                        </a>
                        <?php if ($isSubActive): ?>
                            <ul class="subcatalog-menu">
                                <?php foreach ($category_child->childs as $sub_child): ?>
                                    <li class="subcatalog-menu__item">
                                        <a class="subcatalog-menu__link"
                                           href="<?= Url::to(['/category/index', 'alias' => $sub_child->name_alt]) ?>">
                                           <?= Yii::t('app',$sub_child->info->name) ?><span><?= $sub_child->productsAmount ?></span></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
<?php endforeach; ?>
