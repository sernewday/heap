<?php

/* @var $this View */

/* @var $menu array */

use common\models\Brands;
use yii\helpers\Url;
use frontend\components\Target;
use yii\web\View;


$__index__ = 0;
$__limit__ = 6;
$genderId = Target::getGenderId(); 
?>

<ul class="header-menu">
    <!-- <li class="header-menu__item"><a class="header-menu__link stocks" href="<?= Url::to(['/sales/index']) ?>">% <?= Yii::t('app', 'Акции') ?></a></li> -->
    <?php foreach ($menu[$genderId]['_childs'] as $menu_item): ?> 
    <?php if ($__index__ >= $__limit__) break; ?>
        <?php $brands = Brands::byCategory($menu_item['id']); ?>
        <li class="header-menu__item">
            <a class="header-menu__link"
               href="<?= Url::to(['/category/index', 'alias' => $menu_item['name_alt']]) ?>"><?= Yii::t('app',$menu_item['info']['name']) ?></a>
            <!-- <div class="header-menu-lvl2">
                <div class="header-menu-lvl2__blur"></div>
                <nav>
                    <div class="header-menu-lvl2-container">
                        <div class="container">
                            <div class="header-menu-lvl2__body">
                                <div class="header-menu-lvl2-column">
                                    <div class="header-menu-lvl2__title"><?= Yii::t('app', 'Категории') ?></div>
                                    <div class="header-menu-lvl2-row">
                                        <?php if ($menu_item['_childs']): ?>
                                            <?php
                                            $counter = 0;
                                            ?>
                                            <?php foreach ($menu_item['_childs'] as $child_item): ?>
                                                <?php
                                                if ($counter == 0) echo '<div class="header-menu-lvl2__link-container">';
                                                if ($counter == 11) echo '</div><div class="header-menu-lvl2__link-container">';
                                                ?>

                                                <a class="header-menu-lvl2__link"
                                                   href="<?= Url::to(['/category/index', 'alias' => $child_item['name_alt']]) ?>"><?= $child_item['info']['name'] ?></a>

                                                <?php if ($counter > 19): ?>
                                                    <a class="header-menu-lvl2__link all"
                                                       href="#"><?= Yii::t('app', 'Все категории') ?></a>
                                                    <?php echo '</div>'; ?>
                                                    <?php break; endif; ?>

                                                <?php $counter += 1; ?>
                                            <?php endforeach; ?>

                                            <?php if ($counter <= 19 && $menu_item['_childs']): ?>
                                                <a class="header-menu-lvl2__link all"
                                                   href="#"><?= Yii::t('app', 'Все категории') ?></a>
                                                <?php echo '</div>'; ?>
                                                <?php endif; ?>
                                        <?php endif; ?>

                                    </div>
                                </div>
                                <div class="header-menu-lvl2-column">
                                    <div class="header-menu-lvl2__title"><?= Yii::t('app', 'Бренды') ?></div>
                                    <div class="header-menu-lvl2-row">
                                        <?php if ($brands): ?>
                                            <?php
                                            $counter = 0;
                                            $used_brands = [];
                                            ?>
                                            <?php foreach ($brands as $brand): ?>
                                                <?php
                                                if (in_array($brand['id'], $used_brands))
                                                    continue;

                                                $used_brands[] = $brand['id'];

                                                if ($counter == 0) echo '<div class="header-menu-lvl2__link-container">';
                                                if ($counter == 11) echo '</div><div class="header-menu-lvl2__link-container">';
                                                ?>

                                                <a class="header-menu-lvl2__link"
                                                   href="<?= Url::to(['/brands/view', 'id' => $brand['id'], 'category' => $menu_item['id']]) ?>"><?= $brand['name'] ?></a>

                                                <?php if ($counter > 19): ?>
                                                    <a class="header-menu-lvl2__link all"
                                                       href="<?= Url::to(['/brands/index']) ?>"><?= Yii::t('app', 'Все бренды') ?></a>
                                                    <?php echo '</div>'; ?>
                                                    <?php break; endif; ?>

                                                <?php $counter += 1; ?>
                                            <?php endforeach; ?>

                                            <?php if ($counter <= 19 && $brands): ?>
                                                <a class="header-menu-lvl2__link all"
                                                   href="#"><?= Yii::t('app', 'Все бренды') ?></a>
                                                <?php echo '</div>'; ?>
                                                <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="header-menu-lvl2-promo">
                                    <div class="header-menu-lvl2-promo__img"><img
                                                src="/assets/img/header/promo.png" alt="">
                                    </div>
                                    <div class="header-menu-lvl2-promo__type">Промокод1</div>
                                    <div class="header-menu-lvl2-promo__name">
                                        <p>Закажите на суму от 2500 грн и получите <span>промокод на скидку 35%</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </li>
    <?php $__index__++; ?> -->
    <?php endforeach; ?>
    <!-- <li class="header-menu__item"><a class="header-menu__link" href="<?= Url::to(['/brands/index']) ?>"><?= Yii::t('app', 'Бренды') ?></a></li> -->
    <li class="header-menu__item"><a class="header-menu__link" href="<?= Url::to(['/sales/index']) ?>"><?= Yii::t('app', 'Скидки') ?></a></li>
    <li class="header-menu__item"><a class="header-menu__link" href="<?= Url::to(['/category/Domashnyaya_odezhda']) ?>"><?= Yii::t('app', 'Новинки') ?></a></li>
    <!-- <li class="header-menu__item"><a class="header-menu__link" href="#">Блог</a></li> -->
</ul>
