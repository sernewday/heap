<?php

/* @var $itemTemplate string */
/* @var $separator string */
/* @var $links array */

/* @var $activeItemClass string */

use yii\helpers\Url;

$count_links = count($links);
?>

<?php for ($i = 0; $i < $count_links; $i++): ?>
    <a class="<?= $i == ($count_links - 1) ? $activeItemClass : '' ?>" href="<?= Url::to($links[$i]['url']) ?>"><?= $links[$i]['label'] ?></a>

    <?= $i != ($count_links - 1) ? $separator : '' ?>
<?php endfor; ?>
