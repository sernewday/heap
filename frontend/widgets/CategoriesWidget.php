<?php


namespace frontend\widgets;


use common\models\Categories;
use yii\base\Widget;

class CategoriesWidget extends Widget
{
    public $main_categories;
    public $category = false;

    public function init()
    {
        parent::init();

        if (!$this->main_categories)
            $this->main_categories = Categories::find()->mainItems()->orderPath()->all();
    }

    public function run()
    {
        return $this->render('categories', [
            'main_categories' => $this->main_categories,
            'category' => $this->category,
        ]);
    }
}