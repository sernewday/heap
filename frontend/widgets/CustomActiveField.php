<?php


namespace frontend\widgets;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveField;
use yii\widgets\ActiveForm;

class CustomActiveField extends ActiveField
{
    public $labelOptions = ['class' => 'input-block-small__title'];
    public $options = ['class' => 'input-block-small'];
    private $_skipLabelFor = false;
    public $template = "{label}\n{input}\n{hint}\n{error}";
    public $inputOptions = ['class' => 'input-block-small__input'];

    /**
     * {@inheritDoc}
     */
    public function label($label = null, $options = [], $need_encode = true)
    {
        if ($label === false) {
            $this->parts['{label}'] = '';
            return $this;
        }

        $options = array_merge($this->labelOptions, $options);
        $label_text = $label;
        if ($label !== null) {
            $options['label'] = $label;
            $label_text = $label;
        }

        if ($this->_skipLabelFor) {
            $options['for'] = null;
        }

        $attribute = Html::getAttributeName($this->attribute);

        if (!$label_text)
            $label_text = $this->model->getAttributeLabel($attribute);

        if ($need_encode)
            $this->parts['{label}'] = Html::tag('span', Html::encode($label_text), $options);
        else
            $this->parts['{label}'] = Html::tag('span', $label_text, $options);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function textInput($options = [])
    {
        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $attribute = Html::getAttributeName($this->attribute);

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        if (!isset($options['placeholder']))
            $options['placeholder'] = Html::encode($this->model->getAttributeLabel($attribute));

        $this->parts['{input}'] = Html::activeTextInput($this->model, $this->attribute, $options);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function searchInput($options = [])
    {
        $this->inputOptions = ['class' => 'input-block-menu__input'];
        $this->options = ['class' => 'input-block-menu__lable'];

        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $attribute = Html::getAttributeName($this->attribute);

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        $options['placeholder'] = Html::encode($this->model->getAttributeLabel($attribute));

        $this->parts['{input}'] = Html::activeTextInput($this->model, $this->attribute, $options) . '<img alt="" src="/assets/img/header/search.png">';

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function passwordInput($options = [])
    {
        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        $attribute = Html::getAttributeName($this->attribute);
        $options['placeholder'] = Html::encode($this->model->getAttributeLabel($attribute));

        $this->parts['{input}'] = Html::activePasswordInput($this->model, $this->attribute, $options);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function dropDownList($items, $options = [])
    {
        $this->options = ['class' => 'options__conteiner'];
        $this->labelOptions = ['class' => 'options__title'];

        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = CustomHtml::activeDropDownList($this->model, $this->attribute, $items, $options);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function textarea($options = [])
    {
        $options = array_merge($this->inputOptions, $options);

        if (!isset($options['placeholder']))
            $options['placeholder'] = Html::encode($this->model->getAttributeLabel($this->attribute));

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeTextarea($this->model, $this->attribute, $options);

        return $this;
    }
}