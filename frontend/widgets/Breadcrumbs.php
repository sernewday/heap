<?php
namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class Breadcrumbs
 * @package frontend\widgets
 */
class Breadcrumbs extends Widget
{
    public $itemTemplate = '{link}';
    public $separator = '<span><i class="fas fa-chevron-right"></i></span>';
    public $homeLink;
    public $links;
    public $activeItemClass = 'active';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        if (!$this->homeLink)
            $this->homeLink = ['label' => \Yii::t('app', 'Главная'), 'url' => ['/']];

        array_unshift($this->links, $this->homeLink);
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        return $this->render('breadcrumbs', [
            'links' => $this->links,
            'homeLink' => $this->homeLink,
            'separator' => $this->separator,
            'itemTemplate' => $this->itemTemplate,
            'activeItemClass' => $this->activeItemClass,
        ]);
    }
}