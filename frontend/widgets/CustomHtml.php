<?php


namespace frontend\widgets;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CustomHtml extends Html
{
    /**
     * {@inheritDoc}
     */
    public static function dropDownList($name, $selection = null, $items = [], $options = [])
    {
        if (!empty($options['multiple'])) {
            return static::listBox($name, $selection, $items, $options);
        }

        $options['class'] = 'options__body';

        $field_id = $options['id'] ?: null;
        unset($options['unselect']);
        unset($options['id']);

        $selectValue = '<div class="options__value"><span class="value">' . ($selection ? $items[$selection] : $items[array_key_first($items)]) . '</span><i class="fas fa-chevron-up"></i></div>';

        $selectOptions = static::renderSelectOptions($selection, $items, $options);

        $selectOptions .= static::input('text', $name, $selection ?: array_key_first($items), ['id' => $field_id]);
        return static::tag('div', static::tag('div', $selectValue . "\n" . static::tag('div', $selectOptions, ['class' => 'options__content']) . "\n", $options), ['class' => 'options']);
    }

    /**
     * {@inheritDoc}
     */
    public static function renderSelectOptions($selection, $items, &$tagOptions = [])
    {
        if (ArrayHelper::isTraversable($selection)) {
            $selection = array_map('strval', ArrayHelper::toArray($selection));
        }

        $lines = [];
        $encodeSpaces = ArrayHelper::remove($tagOptions, 'encodeSpaces', false);
        $encode = ArrayHelper::remove($tagOptions, 'encode', true);
        if (isset($tagOptions['prompt'])) {
            $promptOptions = ['value' => ''];
            if (is_string($tagOptions['prompt'])) {
                $promptText = $tagOptions['prompt'];
            } else {
                $promptText = $tagOptions['prompt']['text'];
                $promptOptions = array_merge($promptOptions, $tagOptions['prompt']['options']);
            }
            $promptText = $encode ? static::encode($promptText) : $promptText;
            if ($encodeSpaces) {
                $promptText = str_replace(' ', '&nbsp;', $promptText);
            }
            $lines[] = static::tag('option', $promptText, $promptOptions);
        }

        $options = isset($tagOptions['options']) ? $tagOptions['options'] : [];
        $groups = isset($tagOptions['groups']) ? $tagOptions['groups'] : [];
        unset($tagOptions['prompt'], $tagOptions['options'], $tagOptions['groups']);
        $options['encodeSpaces'] = ArrayHelper::getValue($options, 'encodeSpaces', $encodeSpaces);
        $options['encode'] = ArrayHelper::getValue($options, 'encode', $encode);

        foreach ($items as $key => $value) {
            $attrs = isset($options[$key]) ? $options[$key] : [];
            $attrs['class'] = 'options__opt';
            $attrs['data-value'] = (string)$key;

            $text = $encode ? static::encode($value) : $value;
            if ($encodeSpaces) {
                $text = str_replace(' ', '&nbsp;', $text);
            }

            if (!array_key_exists('selected', $attrs)) {
                $attrs['selected'] = $selection !== null &&
                    (!ArrayHelper::isTraversable($selection) && !strcmp($key, $selection)
                        || ArrayHelper::isTraversable($selection) && ArrayHelper::isIn((string)$key, $selection));
            }

            $lines[] = static::tag('div', $text, $attrs);
        }

        return implode("\n", $lines);
    }
}