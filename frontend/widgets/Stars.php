<?php


namespace frontend\widgets;


use frontend\components\Helper;
use Symfony\Component\Console\Exception\MissingInputException;

class Stars extends \yii\base\Widget
{
    public $score;

    private $whole;
    private $fraction;

    public function init()
    {
        parent::init();

        $this->whole = floor($this->score);
        $this->fraction = Helper::getFraction($this->score) * 10;
    }

    public function run()
    {
        return $this->render('stars', [
            'whole' => $this->whole,
            'fraction' => $this->fraction,
        ]);
    }
}