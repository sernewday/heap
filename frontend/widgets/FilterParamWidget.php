<?php


namespace frontend\widgets;


use common\models\ProductParamsValues;
use yii\base\Widget;

class FilterParamWidget extends Widget
{

    public $param_id;
    public $name;
    public $name_input;

    public $values;

    public $key = 'id';
    public $value = 'value';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        if (!$this->values)
            $this->values = ProductParamsValues::find()->where(['param_id' => $this->param_id])->all();
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        return $this->render('_filter-values', [
            'name' => $this->name,
            'name_input' => $this->name_input,
            'values' => $this->values,
            'key' => $this->key,
            'value_key' => $this->value,
        ]);
    }

}