<?php


namespace frontend\widgets;


use yii\base\Widget;

/**
 * Class Menu
 * @package frontend\widgets
 */
class Menu extends Widget
{
    /**
     * @var array
     */
    public $menu;

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->menu = \frontend\components\Menu::get();

        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        return $this->render('menu', [
            'menu' => $this->menu,
        ]);
    }
}