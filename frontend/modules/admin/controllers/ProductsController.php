<?php


namespace frontend\modules\admin\controllers;

use common\models\ImageManager;
use common\models\Products;
use common\models\UploadForm;
use frontend\models\ModuleProductsForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ProductsController
 * @package frontend\modules\admin\controllers
 */
class ProductsController extends ModuleController
{

    public function beforeAction($action)
    {
        if (in_array($action->id, ['delete-image'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        unset($params['limit']);

        $dataProvider = new ActiveDataProvider([
            'query' => Products::get($params),
            'pagination' => [
                'pageSize' => Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new ModuleProductsForm();
        $model_upload = new UploadForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save(1) &&
            $model_upload->save(Products::tableName(), $model->_product->id))
            return $this->refresh();

        return $this->render('create', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    /**
     * @param $id
     * @param int $lang
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id, $lang = 1)
    {
        $model = new ModuleProductsForm();
        $model_upload = new UploadForm();
        if (!$model->loadProduct($id, $lang))
            throw new NotFoundHttpException('Товар не найден');

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save($lang) &&
            $model_upload->save(Products::tableName(), $model->_product->id))
            return $this->redirect(['/admin/products/update', 'id' => $model->_product->id]);

        return $this->render('update', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (($model = ImageManager::findOne(Yii::$app->request->post('key')))) {
            return $model->removeSelf();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
