<?php


namespace frontend\modules\admin\controllers;


use common\models\Brands;
use common\models\Orders;
use common\models\Payments;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class SellersController
 * @package frontend\modules\admin\controllers
 */
class SellersController extends ModuleController
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Brands::allQuery(),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'payments_info' => Payments::getDataByPeriod(3600 * 24 * 30),
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $brand = Brands::find()->where([Brands::tableName() . '.id' => $id])->joinWith(['image', 'companyInfo', 'documents'])->one();
        if (!$brand)
            throw new NotFoundHttpException();

        return $this->render('view', [
            'brand' => $brand,
            'reviews' => Brands::getReviews($brand->id)->limit(5)->all(),
        ]);
    }
}