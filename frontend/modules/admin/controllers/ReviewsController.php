<?php


namespace frontend\modules\admin\controllers;

use common\models\Brands;
use common\models\Questions;
use common\models\Reviews;
use common\models\UploadForm;
use frontend\models\QuestionAnswerForm;
use yii\data\ActiveDataProvider;

/**
 * Class ReviewsController
 * @package frontend\modules\admin\controllers
 */
class ReviewsController extends ModuleController
{
    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['answer-question']))
            $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $params = \Yii::$app->request->queryParams;
        unset($params['limit']);

        $dataProvider = new ActiveDataProvider([
            'query' => Reviews::get($params),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSeller($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Brands::getReviews($id),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('seller', [
            'dataProvider' => $dataProvider,
            'brand' => Brands::findOne($id),
        ]);
    }

    public function actionQuestions()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Questions::find()->not_answered()->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('questions', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAnswerQuestion()
    {
        $model = new QuestionAnswerForm();
        $model_upload = new UploadForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()
            && $model_upload->save(Questions::tableName(), $model->id))
            $this->redirect(\Yii::$app->request->referrer ?: ['/']);

        $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }
}