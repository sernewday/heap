<?php


namespace frontend\modules\admin\controllers;


use common\models\Brands;
use common\models\Payments;
use common\models\SellersBalance;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

class BalanceController extends ModuleController
{
    public function actionIndex()
    {
        $paymentsDataProvider = new ActiveDataProvider([
            'query' => Payments::find()->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'paymentsDataProvider' => $paymentsDataProvider,
            'statistics' => Payments::getStatisticsForAll(),
        ]);
    }
}