<?php


namespace frontend\modules\admin\controllers;

use common\models\Sales;
use common\models\UploadForm;
use frontend\modules\admin\models\SalesForm;
use yii\web\NotFoundHttpException;

/**
 * Class SalesController
 * @package frontend\modules\admin\controllers
 */
class SalesController extends ModuleController
{
    public function actionIndex($status_id)
    {
        return $this->render('index', [
            'sales' => Sales::get()->where([Sales::tableName() . '.status_id' => $status_id])->all(),
        ]);
    }

    public function actionUpdate($id, $lang = 1)
    {
        $model = new SalesForm();
        $model_upload = new UploadForm();

        if (!$model->loadData($id, $lang))
            throw new NotFoundHttpException();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save($lang)
            && $model_upload->save(Sales::tableName(), $model->_sale->id))
            $this->refresh();

        return $this->render('update', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    public function actionCreate()
    {
        $model = new SalesForm();
        $model_upload = new UploadForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save(1)
            && $model_upload->save(Sales::tableName(), $model->_sale->id))
            $this->refresh();

        return $this->render('create', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }
}