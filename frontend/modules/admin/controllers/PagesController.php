<?php


namespace frontend\modules\admin\controllers;


use frontend\modules\admin\models\PagesForm;

/**
 * Class PagesController
 * @package frontend\modules\admin\controllers
 */
class PagesController extends ModuleController
{
    public function actionUpdate($name, $lang = 1)
    {
        $model = new PagesForm();
        $model->loadData($name, $lang);

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save($lang))
            $this->refresh();

        return $this->render('update', [
            'model' => $model,
            'lang' => $lang,
        ]);
    }
}