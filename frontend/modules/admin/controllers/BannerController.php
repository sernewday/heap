<?php


namespace frontend\modules\admin\controllers;

use common\models\Banners;
use common\models\UploadForm;
use frontend\modules\admin\models\BannerForm;
use frontend\modules\admin\models\SetBannerForm;
use yii\web\Response;

/**
 * Class BannerController
 * @package frontend\modules\admin\controllers
 */
class BannerController extends ModuleController
{
    public function actionIndex()
    {
        $banners = Banners::find()->orderBy('position ASC, created_at DESC')->joinWith(['image'])->all();

        return $this->render('index', [
            'banners' => $banners,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = new BannerForm();
        $model_upload = new UploadForm();

        $model->loadData($id);

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()
            && $model_upload->save(Banners::tableName(), $model->_banner->id))
            return $this->refresh();

        return $this->render('update', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['set-banner'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionSetBanner()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new SetBannerForm();

        $model->setAttributes(\Yii::$app->request->post());
        if ($model->validate() && $model->setBanner())
            return true;

        return false;
    }
}