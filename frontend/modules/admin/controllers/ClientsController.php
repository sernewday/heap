<?php


namespace frontend\modules\admin\controllers;

use common\models\Orders;
use common\models\Reviews;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class ClientsController
 * @package frontend\modules\admin\controllers
 */
class ClientsController extends ModuleController
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::getCustomers(),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $user = User::find()->where([User::tableName() . '.id' => $id])->joinWith(['info', 'family'])->one();
        if (!$user)
            throw new NotFoundHttpException();

        return $this->render('view', [
            'user' => $user,
            'profileProgress' => User::getProfileProgress($id),
            'reviews' => Reviews::getSellerReviewsByUser($id)->limit(5)->all(),
        ]);
    }
}