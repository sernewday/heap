<?php


namespace frontend\modules\admin\controllers;


use common\models\Categories;
use frontend\models\ModuleCategoryForm;
use Yii;
use yii\data\ActiveDataProvider;

class CategoryController extends ModuleController
{
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        unset($params['limit']);

        $dataProvider = new ActiveDataProvider([
            'query' => Categories::get($params)->where(['depth' => 1])->orderBy('created_at ASC'),
            'pagination' => [
                'pageSize' => Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id, $lang = 1)
    {
        $model = new ModuleCategoryForm();
        $model->loadData($id, $lang);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save($lang))
            $this->refresh();

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}