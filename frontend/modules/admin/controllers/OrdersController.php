<?php


namespace frontend\modules\admin\controllers;

use common\models\Orders;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class OrdersController
 * @package frontend\modules\admin\controllers
 */
class OrdersController extends ModuleController
{
    public function actionIndex()
    {
        $params = \Yii::$app->request->queryParams;
        unset($params['limit']);

        $dataProvider = new ActiveDataProvider([
            'query' => Orders::get($params),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $order = Orders::getOneForSeller($id);
        if (!$order)
            throw new NotFoundHttpException();

        return $this->render('view', [
            'order' => $order,
        ]);
    }
}