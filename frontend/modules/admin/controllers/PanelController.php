<?php


namespace frontend\modules\admin\controllers;


use common\models\Brands;
use common\models\Orders;
use common\models\Payments;

class PanelController extends ModuleController
{
    public function actionIndex()
    {
        return $this->render('index', [
            'payments_info' => Payments::getDataByPeriod(3600 * 24 * 30),
            'brands' => Brands::getLast(3),
            'last_orders' => Orders::getLast(3),
            'last_payments' => Payments::getLast(3),
        ]);
    }
}