<?php


namespace frontend\modules\admin\models;


use common\models\Banners;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class BannerForm extends Model
{
    public $url;
    public $label_top;
    public $label_main;
    public $label_edge;
    public $due_to;

    /**
     * @var Banners
     */
    public $_banner;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['url', 'due_to', 'label_main'], 'required'],

            [['due_to'], 'safe'],

            [['label_top', 'label_main', 'label_edge'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'url' => \Yii::t('app', 'Ссылка'),
            'due_to' => \Yii::t('app', 'Дата окончания'),
            'label_main' => \Yii::t('app', 'Название'),
            'label_top' => \Yii::t('app', 'Название ярлыка'),
            'label_edge' => \Yii::t('app', 'Надпись сбоку'),
        ];
    }

    public function loadData($id)
    {
        $this->_banner = Banners::findOne($id);
        if (!$this->_banner)
            throw new NotFoundHttpException();

        $this->setAttributes($this->_banner->getAttributes());

        $this->due_to = date('Y-m-d', $this->_banner->due_to);
    }

    /**
     * @return bool
     */
    public function save()
    {
        $this->_banner->setAttributes($this->getAttributes());

        $this->_banner->due_to = strtotime($this->due_to);

        return $this->_banner->save();
    }
}