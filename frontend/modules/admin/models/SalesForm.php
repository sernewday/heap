<?php


namespace frontend\modules\admin\models;


use common\models\Sales;
use common\models\SalesInfo;
use Yii;
use yii\base\Model;

class SalesForm extends Model
{
    public $name;
    public $label_name;
    public $category_id;
    public $start_at;
    public $due_to;
    public $status_id;
    public $comment;

    public $value;
    public $value_type;

    public $participants = [];
    public $products = [];

    public $is_new = true;

    /**
     * @var Sales
     */
    public $_sale;

    /**
     * @var SalesInfo
     */
    public $_info;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['name', 'label_name', 'category_id', 'start_at', 'due_to', 'status_id', 'value', 'value_type'], 'required'],

            [['name', 'label_name', 'comment', 'start_at', 'due_to'], 'string', 'max' => 255],

            [['category_id', 'status_id', 'value_type'], 'integer'],

            [['value'], 'double', 'min' => 1],

            [['participants', 'products'], 'each', 'rule' => ['integer']],

            ['due_to', 'check_date'],

            ['value', 'check_value'],
        ];
    }

    /**
     * Check if due_to is valid
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function check_date($attribute, $params)
    {
        if (strtotime($this->due_to) <= strtotime($this->start_at)) {
            $this->addError($attribute, Yii::t('app', 'Дата окончания акции не может быть меньше чем дата начала'));

            return false;
        }

        return true;
    }

    /**
     * Check if value is valid
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function check_value($attribute, $params)
    {
        if ($this->value_type == Sales::TYPE_PERCENT && ($this->value >= 100 || $this->value <= 0)) {
            $this->addError($attribute, Yii::t('app', 'Неверное значение скидки'));

            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
            'label_name' => Yii::t('app', 'Название ярлыка'),
            'category_id' => Yii::t('app', 'Категория'),
            'start_at' => Yii::t('app', 'Дата начала'),
            'due_to' => Yii::t('app', 'Дата окончания'),
            'status_id' => Yii::t('app', 'Статус акции'),
            'comment' => Yii::t('app', 'Дополнительная информация'),
            'value' => Yii::t('app', 'Скидка'),
            'value_type' => Yii::t('app', 'Тип скидки'),
        ];
    }

    public function loadData($id, $lang)
    {
        $this->_sale = Sales::find()
            ->where([Sales::tableName() . '.id' => $id])
            ->one();
        if (!$this->_sale)
            return false;

        $this->is_new = false;

        $this->_info = SalesInfo::findOne(['record_id' => $id, 'lang' => $lang]);

        $this->setAttributes($this->_sale->getAttributes());
        $this->setAttributes($this->_info->getAttributes());

        $this->start_at = date('Y-m-d', $this->_sale->start_at);
        $this->due_to = date('Y-m-d', $this->_sale->due_to);

        return true;
    }

    /**
     * @param int $lang
     * @return bool
     */
    public function save($lang)
    {
        if ($this->is_new)
            $this->_sale = new Sales(['sale_type' => 1]);

        $this->start_at = strtotime($this->start_at);
        $this->due_to = strtotime($this->due_to);

        $this->_sale->setAttributes($this->getAttributes());

        if ($this->_sale->save()) {
            if (!$this->_info)
                $this->_info = new SalesInfo(['record_id' => $this->_sale->id, 'lang' => $lang]);

            $this->_info->setAttributes($this->getAttributes());

            return $this->_info->save();
        }

        return false;
    }
}