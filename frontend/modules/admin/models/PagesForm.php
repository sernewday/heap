<?php

namespace frontend\modules\admin\models;

use common\models\Pages;
use common\models\PagesContent;
use common\models\PagesContentInfo;
use common\models\PagesInfo;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * Class PagesForm
 * @package frontend\modules\admin\models
 */
class PagesForm extends Model
{
    public $title;

    public $content = [];

    /**
     * @var Pages
     */
    public $_page;

    /**
     * @var PagesInfo
     */
    public $_info;

    /**
     * @var PagesContent[]
     */
    public $_content;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'safe'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => \Yii::t('app', 'Название'),
            'content' => '',
        ];
    }

    public function loadData($name, $lang)
    {
        $this->_page = Pages::findOne(['name' => $name]);
        if (!$this->_page)
            throw new NotFoundHttpException();

        $this->_info = PagesInfo::findOne(['record_id' => $this->_page->id, 'lang' => $lang]);
        $this->_content = PagesContent::find()->where(['page_id' => $this->_page->id])->all();

        $this->title = $this->_info->title;
    }

    /**
     * @param int $lang
     * @return bool
     */
    public function save($lang)
    {
        if (!$this->_info)
            $this->_info = new PagesInfo(['record_id' => $this->_page->id, 'lang' => $lang]);

        $this->_info->title = $this->title;

        foreach ($this->content as $item) {
            if ($item['id'] && $item['id'] != 0)
                $content_item = PagesContent::findOne($item['id']);
            else
                $content_item = new PagesContent(['page_id' => $this->_page->id]);

            if ($content_item->save()) {
                $content_item_info = PagesContentInfo::findOne(['record_id' => $content_item->id, 'lang' => $lang]);
                if (!$content_item_info)
                    $content_item_info = new PagesContentInfo(['record_id' => $content_item->id, 'lang' => $lang]);

                $content_item_info->setAttributes($item);

                $content_item_info->save();
            }
        }

        return $this->_info->save();
    }
}