<?php


namespace frontend\modules\admin\models;


use common\models\Banners;
use common\models\ImageManager;
use common\models\Sales;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\Url;

class SetBannerForm extends Model
{
    public $id;
    public $position;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['id', 'position'], 'required'],
            [['id', 'position'], 'integer'],
        ];
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function setBanner()
    {
        $sale = Sales::findOne($this->id);
        if (!$sale)
            return false;

        $banner = Banners::findOne(['position' => $this->position]);
        if (!$banner || $this->position == 1)
            $banner = new Banners();

        $banner->position = $this->position;
        $banner->label_main = $sale->info->label_name;
        $banner->status = Banners::STATUS_ACTIVE;
        $banner->url = Url::to(['/sales/view', 'id' => $sale->id], true);
        $banner->due_to = $sale->due_to;

        if ($banner->save()) {
            $uploads_dir = dirname(dirname(dirname(__DIR__))) . '/web/uploads/' . Banners::tableName() . '/';

            try {
                if ($banner->image)
                    $banner->image->removeSelf();
            } catch (\Exception $exception) {}

            FileHelper::createDirectory($uploads_dir . (explode('/', $sale->image->path))[0] . '/');

            try {
                copy($sale->image->fullPath, $uploads_dir . $sale->image->path . '_' . $banner->id . '.' . $sale->image->extension);
            } catch (\Exception $exception) {
                \Yii::debug($exception->getMessage());
                return false;
            }

            $im = new ImageManager(['module' => Banners::tableName(), 'record_id' => $banner->id]);
            $im->path = $sale->image->path . '_' . $banner->id . '.' . $sale->image->extension;

            $im->save();

            return true;
        }

        return false;
    }
}