<?php

/* @var $this \yii\web\View */

use common\models\Brands;
use common\models\Categories;
use common\models\ProductParams;
use frontend\widgets\CustomActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model \frontend\modules\admin\models\BannerForm */
/* @var $model_upload \common\models\UploadForm */

$this->registerJsFile('/js/sales__editing.js', ['depends' => ['frontend\assets\AppAsset']]);
?>

<?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab-product-editing', 'enctype' => 'multipart/form-data']]) ?>
<div class="seller-cab-product-editing__title">
    <a class="seller-cab-product-editing__link hover-button" href="#">
        <div class="hover-button__front">
            <svg fill="none" height="8" viewBox="0 0 17 8" width="17"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                        d="M4.79117 6.77032C5.10028 7.08607 5.05287 7.55525 4.68512 7.82148C4.31736 8.08795 3.76628 8.05199 3.45031 7.74115L0.207926 4.47766L0.181892 4.45524C0.173122 4.44754 0.16408 4.43259 0.155584 4.42535C0.144623 4.41811 0.138321 4.4069 0.138321 4.39522C0.129276 4.38798 0.120506 4.3728 0.111738 4.36556C0.102968 4.35809 0.102968 4.34314 0.0944748 4.33544C0.0854301 4.3282 0.0854301 4.31325 0.0766621 4.30578C0.0678921 4.29831 0.0678921 4.28336 0.059124 4.27612C0.0506287 4.26094 0.0506287 4.25347 0.0415859 4.23875C0.0415859 4.23105 0.0328159 4.21633 0.0328159 4.20863C0.0328159 4.19368 0.0243206 4.18621 0.0243206 4.17126C0.0243206 4.16402 0.0152779 4.14884 0.0152779 4.1416C0.0152779 4.12665 0.00678253 4.11147 0.00678253 4.09653V4.07411C-0.00226212 4.02717 -0.00226212 3.97929 0.00678253 3.93235V3.90993C0.00678253 3.89498 0.0152779 3.88027 0.0152779 3.86532C0.0152779 3.85762 0.0243206 3.8429 0.0243206 3.8352C0.0243206 3.82025 0.0328159 3.81278 0.0328159 3.79783C0.0328159 3.79059 0.0415859 3.77541 0.0415859 3.76817C0.0506287 3.75322 0.0506287 3.74575 0.059124 3.7308C0.0678921 3.72333 0.0678921 3.70838 0.0766621 3.70068C0.0854301 3.68596 0.0854301 3.67849 0.0944748 3.67102C0.102968 3.66331 0.102968 3.6486 0.111738 3.64136C0.120506 3.63365 0.129276 3.61871 0.138321 3.61123C0.146814 3.60399 0.146814 3.59629 0.155584 3.58134C0.16408 3.57387 0.173122 3.55892 0.181892 3.55145L0.207926 3.52926L3.45031 0.265543C3.76244 -0.0497352 4.31572 -0.0901376 4.68594 0.175864C5.05644 0.442099 5.1033 0.913382 4.79117 1.22913L2.76714 3.26793H16.1236C16.6073 3.26793 17 3.60212 17 4.01455C17 4.42675 16.6073 4.76141 16.1236 4.76141H2.76714L4.79117 6.77032Z"
                        fill="#353535"/>
            </svg>
            <span><?= Yii::t('app', 'Назад к товарам') ?></span>
        </div>
        <div class="hover-button__back">
            <svg fill="none" height="8" viewBox="0 0 17 8" width="17"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                        d="M4.79117 6.77032C5.10028 7.08607 5.05287 7.55525 4.68512 7.82148C4.31736 8.08795 3.76628 8.05199 3.45031 7.74115L0.207926 4.47766L0.181892 4.45524C0.173122 4.44754 0.16408 4.43259 0.155584 4.42535C0.144623 4.41811 0.138321 4.4069 0.138321 4.39522C0.129276 4.38798 0.120506 4.3728 0.111738 4.36556C0.102968 4.35809 0.102968 4.34314 0.0944748 4.33544C0.0854301 4.3282 0.0854301 4.31325 0.0766621 4.30578C0.0678921 4.29831 0.0678921 4.28336 0.059124 4.27612C0.0506287 4.26094 0.0506287 4.25347 0.0415859 4.23875C0.0415859 4.23105 0.0328159 4.21633 0.0328159 4.20863C0.0328159 4.19368 0.0243206 4.18621 0.0243206 4.17126C0.0243206 4.16402 0.0152779 4.14884 0.0152779 4.1416C0.0152779 4.12665 0.00678253 4.11147 0.00678253 4.09653V4.07411C-0.00226212 4.02717 -0.00226212 3.97929 0.00678253 3.93235V3.90993C0.00678253 3.89498 0.0152779 3.88027 0.0152779 3.86532C0.0152779 3.85762 0.0243206 3.8429 0.0243206 3.8352C0.0243206 3.82025 0.0328159 3.81278 0.0328159 3.79783C0.0328159 3.79059 0.0415859 3.77541 0.0415859 3.76817C0.0506287 3.75322 0.0506287 3.74575 0.059124 3.7308C0.0678921 3.72333 0.0678921 3.70838 0.0766621 3.70068C0.0854301 3.68596 0.0854301 3.67849 0.0944748 3.67102C0.102968 3.66331 0.102968 3.6486 0.111738 3.64136C0.120506 3.63365 0.129276 3.61871 0.138321 3.61123C0.146814 3.60399 0.146814 3.59629 0.155584 3.58134C0.16408 3.57387 0.173122 3.55892 0.181892 3.55145L0.207926 3.52926L3.45031 0.265543C3.76244 -0.0497352 4.31572 -0.0901376 4.68594 0.175864C5.05644 0.442099 5.1033 0.913382 4.79117 1.22913L2.76714 3.26793H16.1236C16.6073 3.26793 17 3.60212 17 4.01455C17 4.42675 16.6073 4.76141 16.1236 4.76141H2.76714L4.79117 6.77032Z"
                        fill="#353535"/>
            </svg>
            <span><?= Yii::t('app', 'Назад к товарам') ?></span>
        </div>
    </a>
    <div class="seller-cab-product-editing__name"><?= $this->title ?></div>
    <div class="seller-cab-product-editing__progres">
        <div class="personal-cart-progres__container">
            <div class="personal-cart-progres__text">Прогресс заполнения аккаунта</div>
            <div class="personal-cart-progres__bar_value">
                <div class="personal-cart-progres__bar" data-value="98"><span></span></div>
                <div class="personal-cart-progres__value"></div>
            </div>
        </div>
    </div>
</div>
<div class="seller-cab-product-editing__body">
    <div class="seller-cab-product-editing__content">
        <div class="seller-cab-product-editing__container info">
            <div class="title-seller-cab">
                <p><?= Yii::t('app', 'Общая информация') ?></p>
                <div class="title-seller-cab__dop">
                    <span>

                    </span>
                    <div class="title-seller-cab__leng">
                        <a class="active" href="#">UKR</a>/
                        <a href="#">RUS</a></div>
                </div>
            </div>
            <div class="seller-cab-product-editing__row">
                <?= $form->field($model, 'label_main')->textInput() ?>
                <?= $form->field($model, 'label_top')->textInput() ?>
            </div>
            <div class="seller-cab-product-editing__row">
                <?= $form->field($model, 'url')->textInput() ?>
                <?= $form->field($model, 'label_edge')->textInput() ?>
            </div>

            <div class="seller-cab-product-editing__row">
                <?= $form->field($model, 'due_to')->textInput(['type' => 'date']) ?>
            </div>
        </div>


    </div>
    <div class="seller-cab-product-editing__cart">
        <div class="seller-cab-product-editing-cart">
            <div class="seller-cab-product-editing-cart__body">
                <div class="seller-cab-product-editing-cart__title">
                    <div class="seller-cab-product-editing-cart__name">
                        <?= Html::encode($model->_banner->label_main) ?>
                    </div>
                    <div class="seller-cab-product-editing-cart__order">
                        <p><?= Yii::t('app', 'Баннер создан') ?></p>
                        <span><?= date('d.m.Y', $model->_banner->created_at) ?></span>
                        <span><?= date('H:i', $model->_banner->created_at) ?></span>
                    </div>
                </div>
                <label class="admin-goods-sales-example-cart__img">
                    <input type="file" class="image-product-thumb"
                           name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>">

                    <?php if ($model->_banner->image): ?>
                        <img alt="sale image" src="<?= $model->_banner->image->imageUrl ?>"
                             data-key="<?= $model->_banner->image->id ?>">
                    <?php endif; ?>
                    <div class="admin-goods-sales-example-cart__cross" <?= $model->_banner->image ? 'style="z-index: -1;"' : '' ?>></div>
                    <div class="admin-goods-sales-example-cart__close"></div>
                </label>
                <div class="seller-cab-product-editing-cart__btn">
                    <button type="submit" class="seller-cab-product-editing-cart__save hover-button">
                        <div class="hover-button__front"><span>Сохранить изменения</span></div>
                        <div class="hover-button__back"><span>Сохранить изменения</span></div>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php CustomActiveForm::end() ?>
