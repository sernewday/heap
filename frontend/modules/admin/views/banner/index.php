<?php

/* @var $this \yii\web\View */
/* @var $banners \common\models\Banners[] */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Баннеры');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="title">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
                        </div>
                        <ul class="admin-banner__table">
                            <li class="admin-banner__item">
                                <div class="admin-banner__value">Позиция</div>
                                <div class="admin-banner__value">баннер</div>
                                <div class="admin-banner__value">название</div>
                                <div class="admin-banner__value">название ярлыка</div>
                                <div class="admin-banner__value">Статус</div>
                                <div class="admin-banner__value"></div>
                            </li>
                            <?php foreach ($banners as $banner): ?>
                                <li class="admin-banner__item">
                                    <a href="<?= Url::to(['/admin/banner/update', 'id' => $banner->id]) ?>" class="admin-banner__container">
                                        <div class="admin-banner">
                                            <div class="admin-banner__value">
                                                <div class="admin-banner__text">
                                                    <?= $banner->position ?>
                                                </div>
                                            </div>
                                            <div class="admin-banner__value">
                                                <div class="admin-banner__img">
                                                    <img alt="" src="<?= $banner->image->imageUrl ?>">
                                                </div>
                                            </div>
                                            <div class="admin-banner__value">
                                                <div class="admin-banner__text"><?= $banner->label_main ?></div>
                                            </div>
                                            <div class="admin-banner__value">
                                                <div class="admin-banner__text"><?= $banner->label_top ?></div>
                                            </div>
                                            <div class="admin-banner__value">
                                                <div class="options__conteiner">
                                                    <div class="options">
                                                        <div class="options__body">
                                                            <div class="options__value"><span class="value">
                                    <div class="options__img"><svg fill="none" height="16" viewBox="0 0 16 16"
                                                                   width="16" xmlns="http://www.w3.org/2000/svg">
<path d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
      fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
<path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
      stroke="#35CB44" stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.1"/>
</svg>
                                    </div>Активная</span><i class="fas fa-chevron-up"></i></div>
                                                            <div class="options__content">
                                                                <div class="options__opt">
                                                                    <div class="options__img">
                                                                        <svg fill="none" height="16" viewBox="0 0 16 16"
                                                                             width="16" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                                                  fill="#35CB44" stroke="#35CB44"
                                                                                  stroke-width="0.2"/>
                                                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                                                  stroke="#35CB44" stroke-linecap="round"
                                                                                  stroke-linejoin="bevel"
                                                                                  stroke-width="1.1"/>
                                                                        </svg>
                                                                    </div>
                                                                    Активная
                                                                </div>
                                                                <div class="options__opt">
                                                                    <div class="options__img">
                                                                        <svg fill="none" height="16" viewBox="0 0 16 16"
                                                                             width="16" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                                                  stroke="#A4A4A4" stroke-linecap="round"
                                                                                  stroke-linejoin="bevel"
                                                                                  stroke-width="1.1"/>
                                                                        </svg>
                                                                    </div>
                                                                    Остановленная
                                                                </div>
                                                                <div class="options__opt">
                                                                    <div class="options__img">
                                                                        <svg fill="none" height="16" viewBox="0 0 16 16"
                                                                             width="16" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                                                  stroke="#EE4A2E" stroke-linecap="round"
                                                                                  stroke-linejoin="bevel"
                                                                                  stroke-width="1.1"/>
                                                                        </svg>
                                                                    </div>
                                                                    Завершённая
                                                                </div>
                                                            </div>
                                                            <input type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="admin-banner__value">
                                                <div class="admin-banner__editing">
                                                    <svg fill="none" height="9" viewBox="0 0 9 9" width="9"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M8.23619 0.40437C7.69698 -0.13479 6.82279 -0.13479 6.28358 0.40437L0.809762 5.87771C0.772248 5.91522 0.745162 5.96171 0.73102 6.01277L0.0111961 8.61126C-0.0184071 8.7178 0.0116755 8.83188 0.0898182 8.91014C0.168081 8.98828 0.282179 9.01836 0.388726 8.98887L2.98745 8.269C3.03851 8.25485 3.08501 8.22777 3.12252 8.19026L8.59622 2.7168C9.13459 2.17728 9.13459 1.30389 8.59622 0.76437L8.23619 0.40437ZM1.18969 6.65619L2.34397 7.81049L0.747319 8.25282L1.18969 6.65619ZM8.16236 2.28298L7.83697 2.60835L6.39205 1.16356L6.71756 0.838191C7.01707 0.538711 7.50271 0.538711 7.80221 0.838191L8.16236 1.19819C8.46139 1.49803 8.46139 1.98326 8.16236 2.28298Z"
                                                              fill="#6D6D6D"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
