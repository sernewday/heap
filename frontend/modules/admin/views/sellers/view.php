<?php

/* @var $this \yii\web\View */
/* @var $brand \common\models\Brands */
/* @var $reviews \common\models\Reviews[] */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $brand->name;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Продавцы'), 'url' => ['/admin/sellers/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="admin-sellers-example">
                            <div class="title">
                                <div class="title__line"></div>
                                <div class="title__text"><?= $brand->name ?><span></span></div>
                            </div>
                            <div class="admin-sellers-example__body">
                                <div class="admin-sellers-example__container">
                                    <div class="admin-sellers-example__content">
                                        <div class="title-seller-cab"><?= Yii::t('app', 'Счёт') ?>
                                            <div class="title-seller-cab__edit">
                                                <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10.9816 0.539161C10.2626 -0.17972 9.09706 -0.17972 8.37811 0.539161L1.07968 7.83694C1.02966 7.88696 0.993549 7.94895 0.974693 8.01702L0.0149281 11.4817C-0.0245427 11.6237 0.0155673 11.7758 0.119758 11.8802C0.224108 11.9844 0.376238 12.0245 0.518301 11.9852L3.98327 11.0253C4.05134 11.0065 4.11334 10.9704 4.16336 10.9203L11.4616 3.62241C12.1795 2.90305 12.1795 1.73852 11.4616 1.01916L10.9816 0.539161ZM1.58625 8.87492L3.12529 10.414L0.996426 11.0038L1.58625 8.87492ZM10.8832 3.04398L10.4493 3.4778L8.52273 1.55141L8.95675 1.11759C9.35609 0.718282 10.0036 0.718282 10.403 1.11759L10.8832 1.59759C11.2819 1.99737 11.2819 2.64435 10.8832 3.04398Z"
                                                          fill="#6D6D6D"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <ul class="admin-sellers-example__menu">
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'Название') ?>:</div>
                                                <div class="admin-sellers-example__value"><?= $brand->name ?></div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'Контактное лицо') ?>:</div>
                                                <div class="admin-sellers-example__value"><?= Html::encode($brand->companyInfo->contact_person) ?></div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'О нас') ?>:</div>
                                                <div class="admin-sellers-example__value">
                                                    <?= Html::encode($brand->info->description) ?>
                                                </div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value">E-mail:</div>
                                                <div class="admin-sellers-example__value"><?= $brand->companyInfo->email ?></div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'Телефон') ?>:</div>
                                                <div class="admin-sellers-example__value"><?= $brand->documents->phone ?>
                                                </div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value">Другие виды связи:</div>
                                                <div class="admin-sellers-example__value">Сайт компании -
                                                    https://www.youtube.com/?gl=UA&hl=ru
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="admin-sellers-example__content">
                                        <div class="title-seller-cab"><?= Yii::t('app', 'Место расположения') ?>
                                            <div class="title-seller-cab__edit">
                                                <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10.9816 0.539161C10.2626 -0.17972 9.09706 -0.17972 8.37811 0.539161L1.07968 7.83694C1.02966 7.88696 0.993549 7.94895 0.974693 8.01702L0.0149281 11.4817C-0.0245427 11.6237 0.0155673 11.7758 0.119758 11.8802C0.224108 11.9844 0.376238 12.0245 0.518301 11.9852L3.98327 11.0253C4.05134 11.0065 4.11334 10.9704 4.16336 10.9203L11.4616 3.62241C12.1795 2.90305 12.1795 1.73852 11.4616 1.01916L10.9816 0.539161ZM1.58625 8.87492L3.12529 10.414L0.996426 11.0038L1.58625 8.87492ZM10.8832 3.04398L10.4493 3.4778L8.52273 1.55141L8.95675 1.11759C9.35609 0.718282 10.0036 0.718282 10.403 1.11759L10.8832 1.59759C11.2819 1.99737 11.2819 2.64435 10.8832 3.04398Z"
                                                          fill="#6D6D6D"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <ul class="admin-sellers-example__menu">
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'Страна') ?>:</div>
                                                <div class="admin-sellers-example__value"><?= Html::encode($brand->companyInfo->country) ?></div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'Город') ?>:</div>
                                                <div class="admin-sellers-example__value"><?= Html::encode($brand->companyInfo->city) ?></div>
                                            </li>
                                            <li class="admin-sellers-example__item">
                                                <div class="admin-sellers-example__value"><?= Yii::t('app', 'Адрес') ?>:</div>
                                                <div class="admin-sellers-example__value"><?= Html::encode($brand->companyInfo->address) ?></div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="admin-sellers-example__content">
                                        <div class="title-seller-cab"><?= Yii::t('app', 'Отзывы о продавце') ?><span>(5)</span></div>
                                    </div>
                                    <div class="admin-sellers-example">
                                        <?php foreach ($reviews as $review): ?>
                                            <?= $this->render('//includes/_review', ['model' => $review]) ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <a class="more-link hover-button" href="<?= Url::to(['/admin/reviews/seller', 'id' => $brand->id]) ?>">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Смотреть все') ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Смотреть все') ?></span></div>
                                    </a>
                                </div>
                                <div class="admin-sellers-example-cart">
                                    <div class="admin-sellers-example-cart__body">
                                        <div class="admin-sellers-example-cart__title">
                                            <div class="admin-sellers-example-cart__logo"><img
                                                    alt="" src="./assets/img/brand/stradivarius.png"></div>
                                            <div class="admin-sellers-example-cart__text">
                                                <span><?= Yii::t('app', 'Создано') ?>:</span>
                                                <p><?= date('d.m.Y', $brand->creation_time) ?></p>
                                                <p><?= date('H:i', $brand->creation_time) ?></p>
                                            </div>
                                            <div class="admin-sellers-example-cart__text">
                                                <span>Рейтинг:</span>
                                                <p>
                                                    <?php for ($i = 1; $i <= 5; $i++): ?>
                                                        <i class="fas fa-star <?= $i < $brand->averageScore ? 'active' : '' ?>"></i>
                                                    <?php endfor; ?>
                                                </p>
                                                <span><?= $brand->averageScore ?></span>
                                            </div>
                                        </div>
                                        <div class="admin-sellers-example-cart__info">
                                            <div class="admin-sellers-example-cart__name"><?= Yii::t('app', 'Данные продавца') ?></div>
                                            <div class="admin-sellers-example-cart__statistic">
                                                <div class="admin-sellers-example-cart__img">
                                                    <svg fill="none" height="13" viewBox="0 0 12 13" width="12"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path clip-rule="evenodd" d="M10.1505 4.47541L7.93395 1.45628C7.92599 1.44543 7.91765 1.43503 7.90895 1.42509L7.26797 0.610547C6.60975 -0.225896 5.33353 -0.198457 4.71186 0.665504L2.70458 3.45515C2.57545 3.63027 2.61273 3.8769 2.78784 4.00603C2.96295 4.13516 3.20958 4.09788 3.33871 3.92277L5.47794 1.02175C5.71554 0.699538 6.19604 0.695968 6.4384 1.01461L7.18988 2.00262L9.30504 4.88363H1.44241C0.53715 4.89196 -0.140929 5.71593 0.0251183 6.60587L0.975777 11.701C1.08712 12.2977 1.60797 12.7304 2.21504 12.7304H9.88508C10.4996 12.7304 11.0246 12.2873 11.1278 11.6815L11.9776 6.6932C12.1094 5.91954 11.646 5.20106 10.9479 4.9649C10.6463 4.86289 10.3389 4.73203 10.1505 4.47541ZM5.54688 7.34535C5.54688 7.09041 5.7535 6.88379 6.00844 6.88379C6.26338 6.88379 6.47 7.09041 6.47 7.34535V10.3764C6.47 10.6315 6.26338 10.838 6.00844 10.838C5.7535 10.838 5.54688 10.6313 5.54688 10.3764V7.34535ZM8.39206 6.88379C8.13711 6.88379 7.93049 7.09041 7.93049 7.34535V10.3764C7.93049 10.6313 8.13711 10.838 8.39206 10.838C8.64702 10.838 8.85365 10.6315 8.85362 10.3764V7.34535C8.85362 7.09041 8.647 6.88379 8.39206 6.88379ZM3.16016 7.34535C3.16016 7.09041 3.36678 6.88379 3.62172 6.88379C3.87666 6.88379 4.08328 7.09041 4.08328 7.34535V10.3764C4.08328 10.6315 3.87666 10.838 3.62172 10.838C3.36678 10.838 3.16016 10.6313 3.16016 10.3764V7.34535Z"
                                                              fill="#A4A4A4"
                                                              fill-rule="evenodd"/>
                                                    </svg>
                                                </div>
                                                <p><?= Yii::t('app', 'Заказы') ?></p>
                                                <span><?= $brand->commonOrdersAmount ?></span>
                                            </div>
                                            <div class="admin-sellers-example-cart__statistic">
                                                <div class="admin-sellers-example-cart__img">
                                                    <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M2.78613 3.19129L8.06788 0.926881L5.88091 0.0284516C5.78853 -0.00948386 5.68497 -0.00948386 5.5926 0.0284516L0.339844 2.18634L2.78613 3.19129Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M9.05289 1.33008L3.76953 3.59383L5.73876 4.40282L11.1357 2.18571L9.05289 1.33008Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M5.3581 5.06834L3.22074 4.1903V5.85278C3.22074 6.06228 3.05089 6.23213 2.84139 6.23213C2.63189 6.23213 2.46203 6.06228 2.46203 5.85278V3.87862L0 2.86719V9.54544C0 9.69926 0.0928944 9.83787 0.2352 9.89634L5.3581 12.0009V5.06834Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M6.11719 5.06834V12.0009L11.2401 9.89634C11.3824 9.83789 11.4753 9.69926 11.4753 9.54544C11.4753 9.29018 11.4753 3.14661 11.4753 2.86719L6.11719 5.06834Z"
                                                              fill="#A4A4A4"/>
                                                    </svg>
                                                </div>
                                                <p><?= Yii::t('app', 'Товары') ?></p>
                                                <span><?= $brand->commonProductsAmount ?></span>
                                            </div>
                                            <div class="admin-sellers-example-cart__statistic">
                                                <div class="admin-sellers-example-cart__img">
                                                    <svg fill="none" height="13" viewBox="0 0 12 13" width="12"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path clip-rule="evenodd" d="M11.3323 3.25541C10.3348 1.3237 8.31853 0 5.99883 0C2.69124 0 0.000347603 2.69106 0.000347603 5.99882C0.000347603 7.45504 0.515819 8.83229 1.4605 9.92194L0.127534 11.2374C-0.142627 11.5041 0.0445194 11.9673 0.425765 11.9691L3.64478 11.9976C5.04109 12.0068 5.71928 12.0003 5.93094 11.9982L5.99883 11.9976C9.85751 11.9976 12.7388 8.37815 11.832 4.59909L11.3323 3.25541ZM3.78942 6.69798C4.16716 6.69798 4.47337 6.39177 4.47337 6.01403C4.47337 5.63629 4.16716 5.33008 3.78942 5.33008C3.41168 5.33008 3.10547 5.63629 3.10547 6.01403C3.10547 6.39177 3.41168 6.69798 3.78942 6.69798ZM6.69603 6.01403C6.69603 6.39177 6.38981 6.69798 6.01208 6.69798C5.63434 6.69798 5.32812 6.39177 5.32812 6.01403C5.32812 5.63629 5.63434 5.33008 6.01208 5.33008C6.38981 5.33008 6.69603 5.63629 6.69603 6.01403ZM8.23473 6.69798C8.61247 6.69798 8.91868 6.39177 8.91868 6.01403C8.91868 5.63629 8.61247 5.33008 8.23473 5.33008C7.857 5.33008 7.55078 5.63629 7.55078 6.01403C7.55078 6.39177 7.857 6.69798 8.23473 6.69798Z"
                                                              fill="#A4A4A4"
                                                              fill-rule="evenodd"/>
                                                    </svg>
                                                </div>
                                                <p>Чат с продавцом </p><span>5</span>
                                            </div>
                                            <div class="admin-sellers-example-cart__statistic">
                                                <div class="admin-sellers-example-cart__img">
                                                    <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path clip-rule="evenodd" d="M0 6C0 9.30848 2.69157 12 6.00012 12C9.30843 12 12 9.30848 12 6C12 2.69152 9.30843 0 6.00012 0C2.69157 0 0 2.69152 0 6ZM7.1753 5.83198C7.34269 5.92667 7.48338 6.07361 7.59714 6.27279C7.71074 6.47201 7.76772 6.71351 7.76769 6.99714C7.76769 7.47801 7.61134 7.8564 7.29872 8.13205C7.10093 8.30649 6.8534 8.42557 6.55642 8.48967V9.4016C6.55642 9.6671 6.35657 9.88235 6.11006 9.88235C5.86355 9.88235 5.6637 9.6671 5.6637 9.4016V8.531C5.31758 8.4988 5.01313 8.41359 4.75072 8.27497C4.40896 8.09449 4.23822 7.88618 4.23828 7.65005C4.23828 7.52506 4.28055 7.39489 4.36549 7.25949C4.45024 7.12412 4.54813 7.05642 4.65904 7.05642C4.73064 7.05642 4.82038 7.09125 4.92811 7.16091C5.03569 7.23054 5.17754 7.30165 5.35373 7.37439C5.52993 7.44714 5.73855 7.48341 5.98002 7.48341C6.45627 7.48341 6.69439 7.33824 6.69439 7.04777C6.69439 6.88567 6.6154 6.75747 6.45749 6.66274C6.29944 6.56818 6.10937 6.49055 5.88697 6.42969C5.66456 6.36903 5.44052 6.29641 5.21492 6.21197C4.98918 6.1275 4.79747 5.9856 4.63956 5.78642C4.48151 5.58724 4.40266 5.33224 4.40266 5.0214C4.40266 4.59049 4.5412 4.21962 4.81847 3.90878C5.01716 3.68592 5.29929 3.54332 5.66376 3.48021V2.5984C5.66376 2.33289 5.86361 2.11765 6.11012 2.11765C6.35663 2.11765 6.55648 2.33289 6.55648 2.5984V3.46932C6.78899 3.50004 7.00427 3.55694 7.20206 3.64029C7.51465 3.77226 7.67106 3.93192 7.67106 4.11937C7.67106 4.23752 7.62754 4.37453 7.54049 4.53079C7.45346 4.68701 7.34541 4.76512 7.21653 4.76512C7.16486 4.76512 7.02467 4.72181 6.79592 4.63492C6.56698 4.54817 6.35257 4.50473 6.15284 4.50473C5.93363 4.50473 5.76605 4.55206 5.65003 4.64662C5.53401 4.74115 5.47599 4.8458 5.47599 4.96067C5.47599 5.07557 5.53401 5.17014 5.65003 5.24433C5.76608 5.31871 5.90943 5.37616 6.08034 5.41668C6.25108 5.4572 6.43452 5.51285 6.63031 5.5838C6.82617 5.65478 7.00788 5.73742 7.1753 5.83198Z"
                                                              fill="#A4A4A4"
                                                              fill-rule="evenodd"/>
                                                    </svg>
                                                </div>
                                                <p>Финансовые операции</p>
                                                <div class="admin-sellers-example-cart__cost">245 <span>грн</span></div>
                                            </div>
                                            <div class="admin-sellers-example-cart__statistic">
                                                <div class="admin-sellers-example-cart__img">
                                                    <svg fill="none" height="9" viewBox="0 0 11 9" width="11"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M9.71094 0.293353V7.91054C9.71094 8.0289 9.63252 8.13574 9.51199 8.18124C9.38652 8.22792 9.24988 8.19863 9.16072 8.11777C9.13709 8.09609 6.81828 6.05937 4.51172 5.87773V2.32636C6.82215 2.14492 9.13709 0.107806 9.16072 0.0861265C9.25311 0.00233741 9.39146 -0.0234438 9.51199 0.0226499C9.63252 0.0679624 9.71094 0.174994 9.71094 0.293353Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M11 4.10254C11 4.53555 10.7405 4.91426 10.3555 5.11719V3.08789C10.7405 3.29082 11 3.66953 11 4.10254Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M5.00358 7.49699C5.06431 7.42146 5.08163 7.32418 5.05016 7.23492C4.95994 6.97801 4.89807 6.73561 4.86464 6.51316C4.64657 6.47357 4.42805 6.44785 4.21121 6.4465C4.23053 6.63207 4.26243 6.82512 4.31752 7.03123H3.83919C3.77274 6.84498 3.7102 6.65008 3.66169 6.44529H1.93449C1.84724 6.44529 1.76474 6.42959 1.67969 6.42188C1.84164 7.58373 2.2579 8.55494 2.27972 8.6048C2.32882 8.71609 2.44715 8.78906 2.57902 8.78906H4.19035C4.44082 8.78906 4.59531 8.54008 4.46666 8.34561C4.46258 8.33936 4.27949 8.05041 4.07842 7.61719H4.74329C4.8462 7.61717 4.94282 7.57254 5.00358 7.49699Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M1.93359 5.85938H3.86719V2.34375H1.93359C0.867346 2.34375 0 3.13225 0 4.10156C0 5.07088 0.867346 5.85938 1.93359 5.85938Z"
                                                              fill="#A4A4A4"/>
                                                    </svg>
                                                </div>
                                                <p>Реклама</p><span>5</span>
                                            </div>
                                        </div>
                                        <a class="admin-sellers-example-cart__link" href="#">зайти в кабинет
                                            продавца</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
