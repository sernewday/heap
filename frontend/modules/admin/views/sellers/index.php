<?php

/* @var $this \yii\web\View */
/* @var $payments_info array */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use frontend\components\Helper;
use frontend\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Продавцы');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="admin-seller-page">
                            <div class="admin-seller-page__title">
                                <div class="admin__title">
                                    <div class="title">
                                        <div class="title__line"></div>
                                        <div class="title__text">
                                            <?= Yii::t('app', 'Продавцы') ?>
                                            <span>(60)</span>
                                        </div>
                                    </div>
                                    <div class="admin__options">
                                        <div class="options__conteiner">
                                            <div class="options">
                                                <div class="options__body">
                                                    <div class="options__value">
                                                        <div class="options__img">
                                                            <img alt="" src="/assets/img/time.png">
                                                        </div>
                                                        <span class="value">За месяц</span><i class="fas fa-chevron-up"></i>
                                                    </div>
                                                    <div class="options__content">
                                                        <div class="options__opt">За неделю</div>
                                                        <div class="options__opt">За месяц</div>
                                                        <div class="options__opt">За пол года</div>
                                                        <div class="options__opt">За год</div>
                                                    </div>
                                                    <input type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="admin-seller-page__btn hover-button add-seller">
                                    <div class="hover-button__front"><span></span>Добавить продавца</div>
                                    <div class="hover-button__back"><span></span>Добавить продавца</div>
                                </div>
                            </div>
                            <?php Pjax::begin(['id' => 'products_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'admin-seller-page__body pjax-more-container']]); ?>
                            <div class="admin-seller-page__content">
                                <div class="seller-cab-title green">
                                    <div class="seller-cab-title__body">
                                        <div class="seller-cab-title__time">
                                            <img alt="" src="/assets/img/time.png">
                                            <span><?= Yii::t('app', 'Показатели за месяц') ?></span>
                                        </div>
                                        <div class="seller-cab-title__content">
                                            <div class="seller-cab-title__text">
                                                <p><?= Yii::t('app', 'К-во заказов') ?></p>
                                                <span><?= $payments_info['orders'] ?></span>
                                            </div>
                                            <div class="seller-cab-title__text">
                                                <p><?= Yii::t('app', 'Общие начисления') ?>:</p>
                                                <span><?= Helper::formatPrice($payments_info['balance_add']) ?> грн</span>
                                            </div>
                                            <div class="seller-cab-title__text">
                                                <p><?= Yii::t('app', 'Общее списание') ?>:</p>
                                                <span><?= Helper::formatPrice($payments_info['balance_charge']) ?> грн</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="seller-cab-filter-container">
                                    <div class="seller-cab-filter-body">
                                        <div class="seller-cab-filter__filter">
                                            <div class="seller-cab-filter__filter-container">
                                                <div class="seller-cab-filter">
                                                    <div class="seller-cab-filter__content">
                                                        <div class="seller-cab-filter__img">
                                                            <svg fill="none" height="14" viewBox="0 0 14 14"
                                                                 width="14" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M13.5862 2.23344L12.9661 1.61331L13.6803 0.89909C13.8405 0.738986 13.8405 0.47923 13.6803 0.319126C13.5201 0.158915 13.2605 0.158915 13.1003 0.319126L12.386 1.03324L11.766 0.413116C11.4508 0.0980345 10.9995 -0.0505347 10.5584 0.0153655L7.989 0.399872C7.69143 0.444411 7.41042 0.586144 7.19766 0.798905L0.413892 7.58257C-0.137554 8.13401 -0.137982 9.03077 0.412718 9.58157L4.41789 13.5866C4.69303 13.8619 5.05457 13.9995 5.41622 13.9995C5.77851 13.9995 6.14091 13.8615 6.41679 13.5855L13.2006 6.8018C13.4133 6.58894 13.5551 6.30793 13.5996 6.01047L13.9841 3.44111C14.0501 2.99999 13.9014 2.54862 13.5862 2.23344ZM13.1728 3.31967L12.7883 5.88913C12.7696 6.0141 12.71 6.13223 12.6205 6.22173L5.83683 13.0054C5.72447 13.1178 5.57536 13.1796 5.41675 13.1797C5.41665 13.1797 5.41643 13.1797 5.41622 13.1797C5.25814 13.1797 5.10958 13.1183 4.99796 13.0066L0.992789 9.0015C0.881068 8.88968 0.819547 8.741 0.819654 8.58271C0.819868 8.4241 0.881709 8.27489 0.993964 8.16264L7.77773 1.37898C7.86724 1.28947 7.98537 1.22987 8.11044 1.21118L10.6798 0.826675C10.7087 0.822296 10.7378 0.820266 10.7667 0.820266C10.9226 0.820266 11.0747 0.882001 11.186 0.993294L11.8061 1.61331L11.1329 2.28652C10.9624 2.2031 10.7739 2.15867 10.5784 2.15867C10.2414 2.15867 9.92435 2.29004 9.68585 2.52844C9.19378 3.02061 9.19378 3.82145 9.68595 4.31351C9.92435 4.5519 10.2414 4.68328 10.5784 4.68328C10.9156 4.68328 11.2326 4.5519 11.471 4.31351C11.7094 4.07512 11.8408 3.75811 11.8408 3.42103C11.8408 3.22557 11.7964 3.03706 11.7129 2.86659L12.3862 2.19338L13.0062 2.81351C13.1382 2.94552 13.2005 3.13478 13.1728 3.31967ZM11.0204 3.42092C11.0204 3.53905 10.9745 3.65002 10.891 3.73344C10.8075 3.81686 10.6966 3.86289 10.5784 3.86289C10.4604 3.86289 10.3494 3.81696 10.266 3.73344C10.0937 3.56116 10.0937 3.28068 10.266 3.1084C10.3494 3.02499 10.4604 2.97906 10.5784 2.97906C10.6966 2.97906 10.8075 3.02499 10.891 3.1084C10.9745 3.19193 11.0204 3.3029 11.0204 3.42092Z"
                                                                      fill="#353535"/>
                                                                <path d="M5.02558 6.24321C4.86547 6.08299 4.60572 6.08299 4.44561 6.24321L2.55758 8.13113C2.39747 8.29134 2.39747 8.5511 2.55758 8.7112C2.63768 8.79131 2.74268 8.83136 2.84767 8.83136C2.95255 8.83136 3.05754 8.79131 3.13765 8.7112L5.02558 6.82328C5.18579 6.66307 5.18579 6.40342 5.02558 6.24321Z"
                                                                      fill="#353535"/>
                                                                <path d="M7.91612 6.08297C7.75601 5.92286 7.49626 5.92286 7.33615 6.08297L3.92173 9.49739C3.76152 9.6576 3.76152 9.91725 3.92173 10.0775C4.00184 10.1576 4.10683 10.1976 4.21172 10.1976C4.31671 10.1976 4.4217 10.1576 4.5018 10.0775L7.91612 6.66304C8.07633 6.50294 8.07633 6.24318 7.91612 6.08297Z"
                                                                      fill="#353535"/>
                                                                <path d="M8.70156 7.4482L5.28714 10.8626C5.12692 11.0228 5.12692 11.2825 5.28714 11.4427C5.36724 11.5228 5.47223 11.5629 5.57722 11.5629C5.68211 11.5629 5.7871 11.5228 5.86721 11.4427L9.28152 8.02827C9.44173 7.86817 9.44173 7.60841 9.28152 7.4482C9.12142 7.2881 8.86166 7.2881 8.70156 7.4482Z"
                                                                      fill="#353535"/>
                                                            </svg>
                                                        </div>
                                                        <div class="seller-cab-filter__text">продавец</div>
                                                        <i class="fas fa-chevron-up"></i>
                                                        <div class="seller-cab-filter__body">
                                                            <div class="seller-cab-filter__options">
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="input-block-small">
                                                                        <label class="input-block-small__lable">
                                                                            <input class="input-block-small__input"
                                                                                   placeholder="Поиск" type="text">
                                                                            <div class="input-block-small__search">
                                                                                <img alt=""
                                                                                     src="/assets/img/header/search.png"></div>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="seller-cab-filter">
                                                    <div class="seller-cab-filter__content">
                                                        <div class="seller-cab-filter__img">
                                                            <svg fill="none" height="12" viewBox="0 0 12 12"
                                                                 width="12" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M11.2607 3.94668L7.70462 3.4274L6.11069 0.181772C5.99164 -0.0605906 5.57569 -0.0605906 5.45664 0.181772L3.86319 3.4274L0.307097 3.94668C0.0150135 3.98953 -0.101627 4.32912 0.103217 4.52955L2.68666 7.06099L2.07599 10.6398C2.02731 10.924 2.34493 11.1374 2.60762 10.9978L5.78391 9.31966L8.9602 10.9982C9.22047 11.1365 9.54099 10.9273 9.49183 10.6402L8.88115 7.06145L11.4646 4.53001C11.6694 4.32912 11.5523 3.98953 11.2607 3.94668Z"
                                                                      fill="#353535"/>
                                                            </svg>
                                                        </div>
                                                        <div class="seller-cab-filter__text">Рейтинг</div>
                                                        <i class="fas fa-chevron-up"></i>
                                                        <div class="seller-cab-filter__body">
                                                            <div class="seller-cab-filter__options">
                                                                <div class="seller-cab-filter__column">
                                                                    <div class="seller-cab-filter__value">
                                                                        <div class="checkbox">
                                                                            <label class="checkbox__body">
                                                                                <input class="checkbox__input"
                                                                                       type="checkbox"><span
                                                                                        class="checkbox__fake"><svg
                                                                                            fill="none" height="8"
                                                                                            viewBox="0 0 9 8" width="9"
                                                                                            xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox__text"><i class="fas fa-star active"></i><i class="fas fa-star"></i><i
                                                                                            class="fas fa-star"></i><i
                                                                                            class="fas fa-star"></i><i
                                                                                            class="fas fa-star"></i></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seller-cab-filter__value">
                                                                        <div class="checkbox">
                                                                            <label class="checkbox__body">
                                                                                <input class="checkbox__input"
                                                                                       type="checkbox"><span
                                                                                        class="checkbox__fake"><svg
                                                                                            fill="none" height="8"
                                                                                            viewBox="0 0 9 8" width="9"
                                                                                            xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox__text"><i class="fas fa-star active"></i><i class="fas fa-star active"></i><i
                                                                                            class="fas fa-star"></i><i
                                                                                            class="fas fa-star"></i><i
                                                                                            class="fas fa-star"></i></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seller-cab-filter__value">
                                                                        <div class="checkbox">
                                                                            <label class="checkbox__body">
                                                                                <input class="checkbox__input"
                                                                                       type="checkbox"><span
                                                                                        class="checkbox__fake"><svg
                                                                                            fill="none" height="8"
                                                                                            viewBox="0 0 9 8" width="9"
                                                                                            xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox__text"><i class="fas fa-star active"></i><i class="fas fa-star active"></i><i
                                                                                            class="fas fa-star active"></i><i
                                                                                            class="fas fa-star"></i><i
                                                                                            class="fas fa-star"></i></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seller-cab-filter__value">
                                                                        <div class="checkbox">
                                                                            <label class="checkbox__body">
                                                                                <input class="checkbox__input"
                                                                                       type="checkbox"><span
                                                                                        class="checkbox__fake"><svg
                                                                                            fill="none" height="8"
                                                                                            viewBox="0 0 9 8" width="9"
                                                                                            xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox__text"><i class="fas fa-star active"></i><i class="fas fa-star active"></i><i
                                                                                            class="fas fa-star active"></i><i
                                                                                            class="fas fa-star active"></i><i
                                                                                            class="fas fa-star"></i></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seller-cab-filter__value">
                                                                        <div class="checkbox">
                                                                            <label class="checkbox__body">
                                                                                <input class="checkbox__input"
                                                                                       type="checkbox"><span
                                                                                        class="checkbox__fake"><svg
                                                                                            fill="none" height="8"
                                                                                            viewBox="0 0 9 8" width="9"
                                                                                            xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox__text"><i class="fas fa-star active"></i><i class="fas fa-star active"></i><i
                                                                                            class="fas fa-star active"></i><i
                                                                                            class="fas fa-star active"></i><i
                                                                                            class="fas fa-star active"></i></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="seller-cab-filter">
                                                    <div class="seller-cab-filter-input">
                                                        <div class="seller-cab-filter__img">
                                                            <svg fill="none" height="9" viewBox="0 0 8 9" width="8"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M0.0146618 4.01081C0.044405 4.0809 0.113812 4.12664 0.190834 4.12664H2.28011V8.81256C2.28011 8.91602 2.36553 9 2.47078 9H5.52138C5.62663 9 5.71205 8.91602 5.71205 8.81256V4.12663H7.80933C7.88635 4.12663 7.95576 4.08088 7.98551 4.01116C8.01487 3.94106 7.99886 3.86046 7.94432 3.80685L4.1406 0.0551062C4.10477 0.0198736 4.05633 0 4.00562 0C3.95491 0 3.90647 0.0198736 3.87063 0.0547371L0.0558443 3.80648C0.00130892 3.86011 -0.0150814 3.94069 0.0146618 4.01081Z"
                                                                      fill="#353535"/>
                                                            </svg>
                                                        </div>
                                                        <div class="seller-cab-filter__text">статистика</div>
                                                        <div class="input-block">
                                                            <label class="input-block__lable"><span
                                                                        class="input-block__title"></span>
                                                                <input class="input-block__input" placeholder="От"
                                                                       type="text">
                                                                <div class="help-block"></div>
                                                            </label>
                                                        </div>
                                                        <div class="input-block">
                                                            <label class="input-block__lable"><span
                                                                        class="input-block__title"></span>
                                                                <input class="input-block__input" placeholder="До"
                                                                       type="text">
                                                                <div class="help-block"></div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="seller-cab-filter">
                                                    <div class="seller-cab-filter-input">
                                                        <div class="seller-cab-filter__img">
                                                            <svg fill="none" height="12" viewBox="0 0 5 12"
                                                                 width="5" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M4.75838 6.42159C4.59723 6.11377 4.39792 5.88668 4.16078 5.74034C3.9236 5.5942 3.66617 5.46648 3.38871 5.35678C3.11134 5.24714 2.85146 5.16113 2.60959 5.09851C2.36746 5.03589 2.16439 4.94711 1.99998 4.83215C1.83562 4.71749 1.75342 4.57135 1.75342 4.39377C1.75342 4.21624 1.83562 4.05451 1.99998 3.90841C2.16434 3.76227 2.40174 3.68913 2.71229 3.68913C2.99524 3.68913 3.29899 3.75626 3.62332 3.89034C3.94739 4.02461 4.14599 4.09155 4.21919 4.09155C4.40176 4.09155 4.55484 3.97083 4.67812 3.7294C4.80145 3.48791 4.86311 3.27617 4.86311 3.09358C4.86311 2.80388 4.64152 2.55713 4.19869 2.35318C3.91848 2.22437 3.61351 2.13643 3.28411 2.08895V0.74298C3.28411 0.332657 3.00099 0 2.65177 0C2.30255 0 2.01942 0.332657 2.01942 0.74298V2.10579C1.50309 2.20332 1.10341 2.4237 0.821936 2.76812C0.429141 3.24851 0.23287 3.82166 0.23287 4.48762C0.23287 4.96801 0.344572 5.3621 0.568483 5.66992C0.792183 5.97775 1.06377 6.19704 1.38357 6.32759C1.70317 6.45809 2.02057 6.57032 2.33564 6.66407C2.65071 6.75812 2.91997 6.8781 3.14388 7.02424C3.36758 7.17063 3.47949 7.36877 3.47949 7.61929C3.47949 8.0682 3.14215 8.29255 2.46746 8.29255C2.12538 8.29255 1.82983 8.23649 1.58022 8.12406C1.33062 8.01164 1.12966 7.90174 0.977254 7.79414C0.824641 7.68648 0.697513 7.63265 0.596081 7.63265C0.438946 7.63265 0.30028 7.73728 0.18021 7.94649C0.0598868 8.15574 2.21467e-08 8.35691 2.21467e-08 8.55008C-8.45044e-05 8.91501 0.241788 9.23694 0.725956 9.51587C1.0977 9.73009 1.529 9.86179 2.01934 9.91154V11.257C2.01934 11.6673 2.30246 12 2.65168 12C3.0009 12 3.28403 11.6673 3.28403 11.257V9.84768C3.70476 9.74862 4.05542 9.56458 4.33562 9.29499C4.7785 8.86898 5 8.28421 5 7.54103C5.00004 7.1027 4.91932 6.72947 4.75838 6.42159Z"
                                                                      fill="#353535"/>
                                                            </svg>
                                                        </div>
                                                        <div class="seller-cab-filter__text">баланс</div>
                                                        <div class="input-block">
                                                            <label class="input-block__lable"><span
                                                                        class="input-block__title"></span>
                                                                <input class="input-block__input" placeholder="От"
                                                                       type="text">
                                                                <div class="help-block"></div>
                                                            </label>
                                                        </div>
                                                        <div class="input-block">
                                                            <label class="input-block__lable"><span
                                                                        class="input-block__title"></span>
                                                                <input class="input-block__input" placeholder="До"
                                                                       type="text">
                                                                <div class="help-block"></div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seller-cab-filter__result">
                                            <div class="seller-cab-filter__result-container">
                                                <div class="seller-cab-filter__result-example">Заказ доставлен<span
                                                            class="seller-cab-filter__result-close"></span></div>
                                            </div>
                                            <div class="seller-cab-filter__remove-all">Сбросить фильтры</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="admin-seller__menu">
                                <li class="admin-seller__item">
                                    <div class="admin-seller__value"><?= Yii::t('app', 'Продавец') ?></div>
                                    <div class="admin-seller__value"><?= Yii::t('app', 'Рейтинг') ?></div>
                                    <div class="admin-seller__value"><?= Yii::t('app', 'К-во заказов') ?>
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                    <div class="admin-seller__value"><?= Yii::t('app', 'Статистика') ?>
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                    <div class="admin-seller__value">
                                        <?= Yii::t('app', 'К-во успешных заказов') ?>
                                        <i class="fas fa-sort-down"></i></div>
                                    <div class="admin-seller__value">
                                        <?= Yii::t('app', 'К-во неуспешных заказов') ?>
                                        <i class="fas fa-sort-down"></i></div>
                                    <div class="admin-seller__value"><?= Yii::t('app', 'Баланс') ?><i class="fas fa-sort-down"></i></div>
                                    <div class="admin-seller__value"><?= Yii::t('app', 'Доход') ?><i class="fas fa-sort-down"></i></div>
                                    <div class="admin-seller__value"></div>
                                </li>
                                <?php $widget = ListView::begin([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => '_item',
                                    'options' => [
                                        'tag' => false,
                                    ],
                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'layout' => '{items}'
                                ]) ?>

                                <?php $widget->end() ?>
                            </ul>
                            <div class="paginations">
                                <div class="paginations__page"><?= $widget->renderSummary() ?></div>
                                <?php if ($dataProvider->totalCount > 12): ?>
                                    <a class="more-link hover-button more-products-button" href="#">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                    </a>
                                <?php endif; ?>
                                <ul class="paginations__menu">
                                    <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                    <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                                </ul>
                            </div>
                            <?php Pjax::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
