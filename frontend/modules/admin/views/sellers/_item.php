<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\Brands */

use yii\helpers\Url;

?>

<li class="admin-seller__item">
    <a class="admin-seller" href="<?= Url::to(['/admin/sellers/view', 'id' => $model->id]) ?>">
        <div class="admin-seller__value">
            <div class="admin-seller__text"><?= $model->name ?></div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text">
                <?php for ($i = 1; $i <= 5; $i++): ?>
                    <i class="fas fa-star <?= $i < $model->averageScore ? 'active' : '' ?>"></i>
                <?php endfor; ?>
                <span><?= $model->averageScore ?></span>
            </div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text"><?= $model->commonOrdersAmount ?></div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text posetive">
                <div class="admin-seller__svg">
                    <svg fill="none" height="9" viewBox="0 0 8 9" width="8"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.0146618 4.01081C0.044405 4.0809 0.113812 4.12664 0.190834 4.12664H2.28011V8.81256C2.28011 8.91602 2.36553 9 2.47078 9H5.52138C5.62663 9 5.71205 8.91602 5.71205 8.81256V4.12663H7.80933C7.88635 4.12663 7.95576 4.08088 7.98551 4.01116C8.01487 3.94106 7.99886 3.86046 7.94432 3.80685L4.1406 0.0551062C4.10477 0.0198736 4.05633 0 4.00562 0C3.95491 0 3.90647 0.0198736 3.87063 0.0547371L0.0558443 3.80648C0.00130892 3.86011 -0.0150814 3.94069 0.0146618 4.01081Z"
                              opacity="0.35"/>
                    </svg>
                </div>
                85 %
            </div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text"><?= $model->successfulOrdersAmount ?></div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text"><?= $model->unsuccessfulOrdersAmount ?></div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text green">
                <span>
                    <?= $model->balance->balance ?>
                    <span>грн</span>
                </span>
            </div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__text green">
                <span>
                    245
                    <span>грн</span>
                </span>
            </div>
        </div>
        <div class="admin-seller__value">
            <div class="admin-seller__arrow"><i class="fas fa-chevron-right"></i></div>
        </div>
    </a>
</li>
