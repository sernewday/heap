<?php

/* @var $this \yii\web\View */

use frontend\components\Helper;
use frontend\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $paymentsDataProvider \yii\data\ActiveDataProvider */
/* @var $statistics array */

$this->title = Yii::t('app', 'Финансовые операции');

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$js = <<< JS
$('#products_container').on('pjax:complete', function () {
    moreItemsEvent();
});
JS;
$this->registerJs($js);

$models = $paymentsDataProvider->models;
$add_balance = 0;
$charge_balance = 0;

$orders = [];
foreach ($models as $model_item) {
    if ($model_item->order_id && !in_array($model_item->order_id, $orders))
        $orders[] = $model_item->order_id;

    if ($model_item->balance_add)
        $add_balance += $model_item->balance_add;

    if ($model_item->balance_charge)
        $charge_balance += $model_item->balance_charge;
}
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="seller-cab-balancs">
                            <div class="title">
                                <div class="title__line"></div>
                                <div class="title__text"><?= $this->title ?><span></span></div>
                            </div>
                            <div class="seller-cab-filter-container">
                                <div class="seller-cab-filter-mob__btn">фильтры <i class="fas fa-chevron-up"></i></div>
                                <div class="seller-cab-filter-body">
                                    <div class="seller-cab-filter__filter">
                                        <div class="seller-cab-filter__filter-container">
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter-input">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="12" viewBox="0 0 12 12"
                                                             width="12" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M10.125 0.9375H9.51562V0.46875C9.51562 0.209859 9.30577 0 9.04688 0C8.78798 0 8.57812 0.209859 8.57812 0.46875V0.9375H6.44531V0.46875C6.44531 0.209859 6.23545 0 5.97656 0C5.71767 0 5.50781 0.209859 5.50781 0.46875V0.9375H3.39844V0.46875C3.39844 0.209859 3.18858 0 2.92969 0C2.6708 0 2.46094 0.209859 2.46094 0.46875V0.9375H1.875C0.841125 0.9375 0 1.77863 0 2.8125V10.125C0 11.1589 0.841125 12 1.875 12H9.67969C9.93858 12 10.1484 11.7901 10.1484 11.5312C10.1484 11.2724 9.93858 11.0625 9.67969 11.0625H1.875C1.35806 11.0625 0.9375 10.6419 0.9375 10.125V2.8125C0.9375 2.29556 1.35806 1.875 1.875 1.875H2.46094V2.34375C2.46094 2.60264 2.6708 2.8125 2.92969 2.8125C3.18858 2.8125 3.39844 2.60264 3.39844 2.34375V1.875H5.50781V2.34375C5.50781 2.60264 5.71767 2.8125 5.97656 2.8125C6.23545 2.8125 6.44531 2.60264 6.44531 2.34375V1.875H8.57812V2.34375C8.57812 2.60264 8.78798 2.8125 9.04688 2.8125C9.30577 2.8125 9.51562 2.60264 9.51562 2.34375V1.875H10.125C10.6419 1.875 11.0625 2.29556 11.0625 2.8125V9.23438C11.0625 9.49327 11.2724 9.70312 11.5312 9.70312C11.7901 9.70312 12 9.49327 12 9.23438V2.8125C12 1.77863 11.1589 0.9375 10.125 0.9375Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M9.05078 5.44063C9.33728 5.44063 9.56953 5.20837 9.56953 4.92188C9.56953 4.63538 9.33728 4.40312 9.05078 4.40312C8.76428 4.40312 8.53203 4.63538 8.53203 4.92188C8.53203 5.20837 8.76428 5.44063 9.05078 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M7.01172 5.44063C7.29822 5.44063 7.53047 5.20837 7.53047 4.92188C7.53047 4.63538 7.29822 4.40312 7.01172 4.40312C6.72522 4.40312 6.49297 4.63538 6.49297 4.92188C6.49297 5.20837 6.72522 5.44063 7.01172 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M4.97266 7.47969C5.25915 7.47969 5.49141 7.24744 5.49141 6.96094C5.49141 6.67444 5.25915 6.44219 4.97266 6.44219C4.68616 6.44219 4.45391 6.67444 4.45391 6.96094C4.45391 7.24744 4.68616 7.47969 4.97266 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M9.05078 7.47969C9.33728 7.47969 9.56953 7.24744 9.56953 6.96094C9.56953 6.67444 9.33728 6.44219 9.05078 6.44219C8.76428 6.44219 8.53203 6.67444 8.53203 6.96094C8.53203 7.24744 8.76428 7.47969 9.05078 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M2.93359 5.44063C3.22009 5.44063 3.45234 5.20837 3.45234 4.92188C3.45234 4.63538 3.22009 4.40312 2.93359 4.40312C2.6471 4.40312 2.41484 4.63538 2.41484 4.92188C2.41484 5.20837 2.6471 5.44063 2.93359 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M2.93359 7.47969C3.22009 7.47969 3.45234 7.24744 3.45234 6.96094C3.45234 6.67444 3.22009 6.44219 2.93359 6.44219C2.6471 6.44219 2.41484 6.67444 2.41484 6.96094C2.41484 7.24744 2.6471 7.47969 2.93359 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M7.01172 7.47969C7.29822 7.47969 7.53047 7.24744 7.53047 6.96094C7.53047 6.67444 7.29822 6.44219 7.01172 6.44219C6.72522 6.44219 6.49297 6.67444 6.49297 6.96094C6.49297 7.24744 6.72522 7.47969 7.01172 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M2.93359 9.51875C3.22009 9.51875 3.45234 9.2865 3.45234 9C3.45234 8.7135 3.22009 8.48125 2.93359 8.48125C2.6471 8.48125 2.41484 8.7135 2.41484 9C2.41484 9.2865 2.6471 9.51875 2.93359 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M7.01172 9.51875C7.29822 9.51875 7.53047 9.2865 7.53047 9C7.53047 8.7135 7.29822 8.48125 7.01172 8.48125C6.72522 8.48125 6.49297 8.7135 6.49297 9C6.49297 9.2865 6.72522 9.51875 7.01172 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M4.97266 9.51875C5.25915 9.51875 5.49141 9.2865 5.49141 9C5.49141 8.7135 5.25915 8.48125 4.97266 8.48125C4.68616 8.48125 4.45391 8.7135 4.45391 9C4.45391 9.2865 4.68616 9.51875 4.97266 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M9.05078 9.51875C9.33728 9.51875 9.56953 9.2865 9.56953 9C9.56953 8.7135 9.33728 8.48125 9.05078 8.48125C8.76428 8.48125 8.53203 8.7135 8.53203 9C8.53203 9.2865 8.76428 9.51875 9.05078 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                            <path
                                                                d="M4.97266 5.44063C5.25915 5.44063 5.49141 5.20837 5.49141 4.92188C5.49141 4.63538 5.25915 4.40312 4.97266 4.40312C4.68616 4.40312 4.45391 4.63538 4.45391 4.92188C4.45391 5.20837 4.68616 5.44063 4.97266 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">Дата</div>
                                                    <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="От"
                                                                type="text">
                                                            <div class="help-block"></div>
                                                        </label></div>
                                                    <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="До"
                                                                type="text">
                                                            <div class="help-block"></div>
                                                        </label></div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="9" viewBox="0 0 13 9"
                                                             width="13" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M0.4 3.1875C0.179086 3.1875 0 3.36659 0 3.5875V7.61347C0 7.83438 0.179086 8.01347 0.4 8.01347H12.0464C12.2673 8.01347 12.4464 7.83438 12.4464 7.61347V3.5875C12.4464 3.36659 12.2673 3.1875 12.0464 3.1875H0.4ZM3.83393 5.8159C3.83393 6.01729 3.67067 6.18054 3.46929 6.18054H1.95775C1.75637 6.18054 1.59311 6.01729 1.59311 5.8159C1.59311 5.61452 1.75637 5.45126 1.95775 5.45126H3.46929C3.67067 5.45126 3.83393 5.61452 3.83393 5.8159ZM9.83593 6.61458C9.75834 6.61458 9.68336 6.60351 9.61245 6.58286C9.39006 6.51809 9.11446 6.51809 8.89208 6.58286C8.82117 6.60351 8.74618 6.61458 8.66859 6.61458C8.22745 6.61458 7.86984 6.25697 7.86984 5.81585C7.86984 5.37473 8.22745 5.01712 8.66859 5.01712C8.74619 5.01712 8.82119 5.0282 8.89211 5.04886C9.11448 5.11363 9.39005 5.11364 9.61242 5.04886C9.68334 5.0282 9.75833 5.01712 9.83593 5.01712C10.2771 5.01712 10.6347 5.37473 10.6347 5.81585C10.6347 6.25697 10.2771 6.61458 9.83593 6.61458Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M12.0464 0H0.4C0.179086 0 0 0.179086 0 0.4V1.25595C0 1.47687 0.179086 1.65595 0.4 1.65595H12.0464C12.2673 1.65595 12.4464 1.47687 12.4464 1.25595V0.4C12.4464 0.179086 12.2673 0 12.0464 0Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">тип операции</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__column">
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Наличние</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Наличние</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Наличние</span></label></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="11" viewBox="0 0 17 11"
                                                             width="17" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M16.253 0H0.531667C0.238054 0 0 0.238054 0 0.531667V9.83979C0 10.1334 0.238054 10.3715 0.531667 10.3715H16.253C16.5466 10.3715 16.7847 10.1334 16.7847 9.83979V0.531667C16.7847 0.238021 16.5466 0 16.253 0ZM15.7213 9.30813H1.06333V1.06333H15.7213V9.30813Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M2.62515 4.17303V6.95332C2.62515 7.24693 2.86321 7.48499 3.15682 7.48499C3.45043 7.48499 3.68849 7.24693 3.68849 6.95332V3.41906C3.68849 2.994 3.21225 2.74029 2.85945 2.97831L2.49164 3.22647C2.12177 3.47605 2.20883 4.03856 2.62515 4.17303Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M6.76007 4.31893L7.55531 3.95046C7.78937 3.88483 8.04989 4.00013 8.15669 4.22051C8.28858 4.49276 8.1624 4.81199 7.88913 4.92397C7.84231 4.94314 7.93558 4.89214 6.60914 5.65146C6.30413 5.82608 6.11469 6.15285 6.11469 6.50429C6.11469 7.04044 6.55085 7.48122 7.08694 7.48691C9.22368 7.5095 8.63599 7.50438 8.74112 7.50438C9.03211 7.50438 9.26957 7.27005 9.27266 6.97833C9.27578 6.68472 9.04025 6.44417 8.74664 6.44108L7.39514 6.42676L8.32775 5.89287C9.12678 5.54217 9.5152 4.58563 9.11359 3.75683C8.71863 2.94172 7.74229 2.69223 7.14001 2.97096L6.31303 3.35413C6.0466 3.47757 5.9307 3.79365 6.05414 4.06005C6.17759 4.32644 6.4936 4.44235 6.76007 4.31893Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M11.6809 3.88361H12.9011L12.4592 4.33241C12.1289 4.66782 12.3673 5.2371 12.838 5.2371C13.1996 5.2371 13.5022 5.54846 13.4594 5.93598C13.3914 6.55222 12.5503 6.69779 12.2757 6.13708C12.1465 5.87338 11.828 5.76432 11.5643 5.89351C11.3006 6.02267 11.1916 6.34118 11.3208 6.60485C11.6385 7.25348 12.3349 7.62735 13.0521 7.53809C14.5502 7.35174 15.0888 5.37637 13.8067 4.47941L14.0965 4.18503C14.5947 3.67905 14.2357 2.82031 13.525 2.82031H11.6809C11.3873 2.82031 11.1492 3.05837 11.1492 3.35198C11.1492 3.64559 11.3873 3.88361 11.6809 3.88361Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">номер транзакции</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__value">
                                                                <div class="input-block-small"><label
                                                                        class="input-block-small__lable"><input
                                                                            class="input-block-small__input" placeholder="Поиск"
                                                                            type="text">
                                                                        <div class="input-block-small__search"><img
                                                                                alt=""
                                                                                src="/assets/img/header/search.png"></div>
                                                                    </label></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="14" viewBox="0 0 14 14"
                                                             width="14" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M13.5862 2.23344L12.9661 1.61331L13.6803 0.89909C13.8405 0.738986 13.8405 0.47923 13.6803 0.319126C13.5201 0.158915 13.2605 0.158915 13.1003 0.319126L12.386 1.03324L11.766 0.413116C11.4508 0.0980345 10.9995 -0.0505347 10.5584 0.0153655L7.989 0.399872C7.69143 0.444411 7.41042 0.586144 7.19766 0.798905L0.413892 7.58257C-0.137554 8.13401 -0.137982 9.03077 0.412718 9.58157L4.41789 13.5866C4.69303 13.8619 5.05457 13.9995 5.41622 13.9995C5.77851 13.9995 6.14091 13.8615 6.41679 13.5855L13.2006 6.8018C13.4133 6.58894 13.5551 6.30793 13.5996 6.01047L13.9841 3.44111C14.0501 2.99999 13.9014 2.54862 13.5862 2.23344ZM13.1728 3.31967L12.7883 5.88913C12.7696 6.0141 12.71 6.13223 12.6205 6.22173L5.83683 13.0054C5.72447 13.1178 5.57536 13.1796 5.41675 13.1797C5.41665 13.1797 5.41643 13.1797 5.41622 13.1797C5.25814 13.1797 5.10958 13.1183 4.99796 13.0066L0.992789 9.0015C0.881068 8.88968 0.819547 8.741 0.819654 8.58271C0.819868 8.4241 0.881709 8.27489 0.993964 8.16264L7.77773 1.37898C7.86724 1.28947 7.98537 1.22987 8.11044 1.21118L10.6798 0.826675C10.7087 0.822296 10.7378 0.820266 10.7667 0.820266C10.9226 0.820266 11.0747 0.882001 11.186 0.993294L11.8061 1.61331L11.1329 2.28652C10.9624 2.2031 10.7739 2.15867 10.5784 2.15867C10.2414 2.15867 9.92435 2.29004 9.68585 2.52844C9.19378 3.02061 9.19378 3.82145 9.68595 4.31351C9.92435 4.5519 10.2414 4.68328 10.5784 4.68328C10.9156 4.68328 11.2326 4.5519 11.471 4.31351C11.7094 4.07512 11.8408 3.75811 11.8408 3.42103C11.8408 3.22557 11.7964 3.03706 11.7129 2.86659L12.3862 2.19338L13.0062 2.81351C13.1382 2.94552 13.2005 3.13478 13.1728 3.31967ZM11.0204 3.42092C11.0204 3.53905 10.9745 3.65002 10.891 3.73344C10.8075 3.81686 10.6966 3.86289 10.5784 3.86289C10.4604 3.86289 10.3494 3.81696 10.266 3.73344C10.0937 3.56116 10.0937 3.28068 10.266 3.1084C10.3494 3.02499 10.4604 2.97906 10.5784 2.97906C10.6966 2.97906 10.8075 3.02499 10.891 3.1084C10.9745 3.19193 11.0204 3.3029 11.0204 3.42092Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M5.02558 6.24321C4.86547 6.08299 4.60572 6.08299 4.44561 6.24321L2.55758 8.13113C2.39747 8.29134 2.39747 8.5511 2.55758 8.7112C2.63768 8.79131 2.74268 8.83136 2.84767 8.83136C2.95255 8.83136 3.05754 8.79131 3.13765 8.7112L5.02558 6.82328C5.18579 6.66307 5.18579 6.40342 5.02558 6.24321Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M7.91612 6.08297C7.75601 5.92286 7.49626 5.92286 7.33615 6.08297L3.92173 9.49739C3.76152 9.6576 3.76152 9.91725 3.92173 10.0775C4.00184 10.1576 4.10683 10.1976 4.21172 10.1976C4.31671 10.1976 4.4217 10.1576 4.5018 10.0775L7.91612 6.66304C8.07633 6.50294 8.07633 6.24318 7.91612 6.08297Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M8.70156 7.4482L5.28714 10.8626C5.12692 11.0228 5.12692 11.2825 5.28714 11.4427C5.36724 11.5228 5.47223 11.5629 5.57722 11.5629C5.68211 11.5629 5.7871 11.5228 5.86721 11.4427L9.28152 8.02827C9.44173 7.86817 9.44173 7.60841 9.28152 7.4482C9.12142 7.2881 8.86166 7.2881 8.70156 7.4482Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">id заказа</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__value">
                                                                <div class="input-block-small"><label
                                                                        class="input-block-small__lable"><input
                                                                            class="input-block-small__input" placeholder="Поиск"
                                                                            type="text">
                                                                        <div class="input-block-small__search"><img
                                                                                alt=""
                                                                                src="/assets/img/header/search.png"></div>
                                                                    </label></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="seller-cab-filter__result">
                                        <div class="seller-cab-filter__result-container">
                                            <div class="seller-cab-filter__result-example">Заказ доставлен<span> </span>
                                            </div>
                                        </div>
                                        <div class="seller-cab-filter__remove-all">Сбросить фильтры</div>
                                    </div>
                                </div>
                            </div>
                            <div class="admin-balance-title">
                                <div class="admin-balance-title__body">
                                    <div class="admin-balance-title__name">
                                        <img alt="" src="/assets/img/admin-balance-title.png">
                                        <?= Yii::t('app', 'Результаты за все время') ?>
                                    </div>
                                    <div class="admin-balance-title__info">
                                        <div class="admin-balance-title__text">
                                            <p><?= Yii::t('app', 'Сума начисления') ?></p>
                                            <span><?= Helper::formatPrice($statistics['common_add']) ?></span>
                                        </div>
                                        <div class="admin-balance-title__text">
                                            <p><?= Yii::t('app', 'Сума списания') ?></p>
                                            <span><?= Helper::formatPrice($statistics['common_charge']) ?></span>
                                        </div>
                                        <div class="admin-balance-title__text">
                                            <p><?= Yii::t('app', 'Общая сума') ?></p>
                                            <span><?= Helper::formatPrice($statistics['sum_all']) ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php Pjax::begin(['id' => 'products_container', 'timeout' => 10000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'seller-cab-balancs__body pjax-more-container']]); ?>
                            <ul class="seller-cab-table-transactions">
                                <li class="seller-cab-table-transactions__item">
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Номер транзакции') ?></div>
                                    <div class="seller-cab-table-transactions__value">Дата</div>
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Тип операции') ?></div>
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'ID Заказа') ?></div>
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Стоимость заказа') ?></div>
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Начисление') ?></div>
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Списание') ?></div>
                                    <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Баланс') ?></div>
                                    <div class="seller-cab-table-transactions__value"></div>
                                </li>
                                <?php $widget = ListView::begin([
                                    'dataProvider' => $paymentsDataProvider,
                                    'itemView' => '_item',
                                    'options' => [
                                        'tag' => false,
                                    ],
                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'layout' => '{items}'
                                ]) ?>

                                <?php $widget->end() ?>
                            </ul>
                            <div class="paginations">
                                <div class="paginations__page"><?= $widget->renderSummary() ?></div>

                                <?php if ($paymentsDataProvider->totalCount > 12): ?>
                                    <a class="more-link hover-button more-products-button" href="#">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                    </a>
                                <?php endif; ?>

                                <ul class="paginations__menu">
                                    <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                    <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                                </ul>
                            </div>
                            <?php Pjax::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
