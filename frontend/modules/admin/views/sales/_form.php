<?php

/* @var $this View */

/* @var $model SalesForm */

/* @var $model_upload UploadForm */

use common\models\Categories;
use common\models\Sales;
use common\models\UploadForm;
use frontend\modules\admin\models\SalesForm;
use frontend\widgets\CustomActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->registerJsFile('/js/sales__editing.js', ['depends' => ['frontend\assets\AppAsset']]);

$css = <<< CSS
.modal-baner__baner:hover, .modal-banner__selected {
    cursor: pointer;
    background: #d4d4d4;
}
CSS;

$this->registerCss($css);

$js = <<< JS

$('.modal-baner__baner').on('click', function (event) {
    $('.modal-banner__selected').each(function (item) {
        $(this).removeClass('modal-banner__selected');
    });
    
    $(this).addClass('modal-banner__selected');
});

$('.modal-baner__btn.select').on('click', function (event) {
    let position = $('.modal-banner__selected').attr('data-position'),
        sale_id = $('.modal-baner-select').attr('data-id');
    
    $.post('/admin/banner/set-banner', {
        id: sale_id,
        position: position
    }, function (data) {
        if (data)
            alert('Баннер добавлен');
    });
});

JS;
$this->registerJs($js);
?>

<?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab__content', 'enctype' => 'multipart/form-data']]) ?>
<div class="admin-goods-sales-example__title">
    <a class="admin-goods-sales-example__link hover-button"
       href="<?= Yii::$app->request->referrer ?: Url::to(['/admin/sales/index', 'status_id' => Sales::STATUS_ACTIVE]) ?>">
        <div class="hover-button__front">
            <i class="fas fa-long-arrow-alt-left"></i>
            <span><?= Yii::t('app', 'Назад') ?></span>
        </div>
        <div class="hover-button__back">
            <i class="fas fa-long-arrow-alt-left"></i>
            <span><?= Yii::t('app', 'Назад') ?></span>
        </div>
    </a>
    <button type="submit" class="admin-goods-sales-example__btn hover-button">
        <div class="hover-button__front"><?= Yii::t('app', 'Сохранить изменения') ?></div>
        <div class="hover-button__back"><?= Yii::t('app', 'Сохранить изменения') ?></div>
    </button>
</div>
<div class="admin-goods-sales-example">
    <div class="admin-goods-sales-example__body">
        <div class="admin-goods-sales-example__content">
            <div class="title-seller-cab"><?= Yii::t('app', 'Общая информация') ?></div>
            <div class="admin-goods-sales-example__row">
                <?= $form->field($model, 'name')->textInput() ?>

                <?= $form->field($model, 'label_name')->textInput() ?>
            </div>
            <div class="admin-goods-sales-example__row">

                <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::find()->where(['depth' => 3])->all(), 'id', 'info.name')) ?>

                <div class="admin-goods-sales-example__data">
                    <?= $form->field($model, 'start_at')->textInput(['type' => 'date']) ?>

                    <?= $form->field($model, 'due_to')->textInput(['type' => 'date']) ?>
                </div>
            </div>

            <div class="admin-goods-sales-example__row">
                <?= $form->field($model, 'value')->textInput() ?>
            </div>

            <div class="admin-goods-sales-example__row">
                <?= $form->field($model, 'value_type')->dropDownList([
                    Sales::TYPE_UAH => 'грн',
                    Sales::TYPE_PERCENT => '%',
                ]) ?>
            </div>

        </div>
        <div class="admin-goods-sales-example__content">
            <div class="title-seller-cab"><?= Yii::t('app', 'Актуальная информация') ?></div>
            <div class="admin-goods-sales-example__row">
                <?= $form->field($model, 'status_id')->dropDownList([
                    Sales::STATUS_ACTIVE => Yii::t('app', 'Активная'),
                    Sales::STATUS_PAUSED => Yii::t('app', 'Приостановлена'),
                    Sales::STATUS_DELETED => Yii::t('app', 'Удалена'),
                    Sales::STATUS_ARCHIVED => Yii::t('app', 'В архиве'),
                ]) ?>
            </div>
            <div class="admin-goods-sales-example__textarea">
                <?= $form->field($model, 'comment')->textarea() ?>
            </div>
        </div>

        <?php if (!$model->is_new): ?>
            <div class="admin-goods-sales-example__content">
                <div class="admin-goods-sales-example__title-table active">
                    <div class="admin-goods-sales-example__name-table">
                        <div class="admin-goods-sales-example__table-name">
                            <p><?= Yii::t('app', 'Учасники') ?></p>
                            <span>(<?= count($model->_sale->participants) ?>)</span>
                        </div>
                        <i class="fas fa-chevron-up"></i>
                    </div>
                    <div class="admin-goods-sales-example__table">
                        <ul class="admin-seller__menu">
                            <li class="admin-seller__item">
                                <div class="admin-seller__value"><?= Yii::t('app', 'Продавец') ?></div>
                                <div class="admin-seller__value">
                                    <?= Yii::t('app', 'Рейтинг') ?>
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <div class="admin-seller__value">
                                    <?= Yii::t('app', 'К-во заказов') ?>
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <div class="admin-seller__value">
                                    <?= Yii::t('app', 'К-во товаров') ?>
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <div class="admin-seller__value">
                                    <?= Yii::t('app', 'Баланс') ?>
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <div class="admin-seller__value"></div>
                            </li>
                            <?php foreach ($model->_sale->participants as $participant): ?>
                                <li class="admin-seller__item">
                                    <a class="admin-seller"
                                       href="<?= Url::to(['/admin/sellers/view', 'id' => $participant->brand_id]) ?>">
                                        <div class="admin-seller__value">
                                            <div class="admin-seller__text"><?= $participant->brand->name ?></div>
                                        </div>
                                        <div class="admin-seller__value">
                                            <div class="admin-seller__text">
                                                <?php for ($i = 1; $i <= 5; $i++): ?>
                                                    <i class="fas fa-star <?= $i < $participant->brand->averageScore ? 'active' : '' ?>"></i>
                                                <?php endfor; ?>
                                                <span><?= $participant->brand->averageScore ?></span>
                                            </div>
                                        </div>
                                        <div class="admin-seller__value">
                                            <div class="admin-seller__text"><?= $participant->brand->commonOrdersAmount ?></div>
                                        </div>
                                        <div class="admin-seller__value">
                                            <div class="admin-seller__text"><?= $participant->brand->commonProductsAmount ?></div>
                                        </div>
                                        <div class="admin-seller__value">
                                            <div class="admin-seller__text green">
                                                <span><?= $participant->brand->balance->balance ?> <span>грн</span></span>
                                            </div>
                                        </div>
                                        <div class="admin-seller__value">
                                            <div class="admin-seller__remove">
                                                <div class="admin-seller__img">
                                                    <svg fill="none" height="12" viewBox="0 0 9 12"
                                                         width="9" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M3.53126 0.689852H5.46881V1.01124H6.16853V0.644861C6.16862 0.289298 5.87532 0 5.51481 0H3.48526C3.12475 0 2.83145 0.289298 2.83145 0.644861V1.01124H3.53126V0.689852Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M7.78179 3.79297H1.21788C1.03803 3.79297 0.896439 3.94536 0.910934 4.12346L1.45969 10.8629C1.49027 11.2392 1.80624 11.5289 2.18584 11.5289H6.81375C7.19335 11.5289 7.50931 11.2392 7.53989 10.8628L8.08865 4.12346C8.10324 3.94536 7.96165 3.79297 7.78179 3.79297ZM2.80725 10.8101C2.79991 10.8105 2.79257 10.8108 2.78533 10.8108C2.60202 10.8108 2.44815 10.669 2.43675 10.4849L2.09286 4.95212C2.08102 4.76067 2.22764 4.5959 2.42031 4.58414C2.61236 4.57255 2.77896 4.71783 2.79081 4.90937L3.13461 10.4421C3.14654 10.6336 2.99992 10.7982 2.80725 10.8101ZM4.85336 10.4635C4.85336 10.6552 4.69684 10.8107 4.50373 10.8107C4.31061 10.8107 4.15409 10.6552 4.15409 10.4635V4.9307C4.15409 4.7389 4.31061 4.58344 4.50373 4.58344C4.69675 4.58344 4.85336 4.7389 4.85336 4.9307V10.4635ZM6.90682 4.95115L6.57848 10.4839C6.56761 10.6684 6.41347 10.8107 6.22982 10.8107C6.22292 10.8107 6.21594 10.8105 6.20896 10.8102C6.0162 10.7988 5.86913 10.6345 5.88053 10.4431L6.20878 4.91024C6.22009 4.71879 6.38501 4.57273 6.5783 4.58405C6.77106 4.59529 6.91813 4.7597 6.90682 4.95115Z"
                                                              fill="#A4A4A4"/>
                                                        <path d="M8.98501 2.70608L8.7557 2.02112C8.69524 1.84056 8.5256 1.71875 8.33451 1.71875H0.665404C0.474403 1.71875 0.304673 1.84056 0.244301 2.02112L0.0149929 2.70608C-0.029227 2.83818 0.0283206 2.97292 0.135737 3.04012C0.179515 3.06747 0.231326 3.08392 0.288256 3.08392H8.71174C8.76867 3.08392 8.82057 3.06747 8.86426 3.04003C8.97168 2.97284 9.02923 2.8381 8.98501 2.70608Z"
                                                              fill="#A4A4A4"/>
                                                    </svg>
                                                </div>
                                                <?= Yii::t('app', 'Удалить') ?>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="admin-goods-sales-example__content">
                <div class="admin-goods-sales-example__title-table active">
                    <div class="admin-goods-sales-example__name-table">
                        <div class="admin-goods-sales-example__table-name">
                            <p><?= Yii::t('app', 'Товары') ?></p><span>(<?= count($model->_sale->products) ?>)</span>
                        </div>
                        <div class="admin-goods-sales-example__btn hover-button">
                            <div class="hover-button__front"><?= Yii::t('app', 'Добавить товар') ?></div>
                            <div class="hover-button__back"><?= Yii::t('app', 'Добавить товар') ?></div>
                        </div>
                        <i class="fas fa-chevron-up"></i>
                    </div>
                    <div class="admin-goods-sales-example__table">
                        <ul class="seller-cab-order-goods__table">
                            <li class="seller-cab-order-goods__table_item">
                                <div class="seller-cab-order-goods__table_value"><span
                                            class="square"></span>
                                    <p><?= Yii::t('app', 'Код товара') ?></p>
                                </div>
                                <div class="seller-cab-order-goods__table_value"><?= Yii::t('app', 'Название товара') ?>
                                </div>
                                <div class="seller-cab-order-goods__table_value"><?= Yii::t('app', 'Категория') ?></div>
                                <div class="seller-cab-order-goods__table_value"><?= Yii::t('app', 'Цена') ?></div>
                                <div class="seller-cab-order-goods__table_value"><?= Yii::t('app', 'Продано') ?></div>
                                <div class="seller-cab-order-goods__table_value"></div>
                            </li>
                            <?php foreach ($model->_sale->products as $product): ?>
                                <li class="seller-cab-order-goods__table_item">
                                    <div class="product-seller-cab-order-goods">
                                        <label class="product-seller-cab-order-goods__lable">
                                            <div class="seller-cab-order-goods__table_value">
                                                <input class="product-seller-cab-order-goods__input"
                                                       type="checkbox">
                                                <div class="product-seller-cab-order-goods__fake">
                                                    <svg fill="none" height="8" viewBox="0 0 9 8"
                                                         width="9" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                              fill="#FFF" stroke-width="0.2"/>
                                                    </svg>
                                                </div>
                                                <div class="product-seller-cab-order-goods__cod">
                                                    №<?= $product->product_id ?>
                                                </div>
                                            </div>
                                            <a class="seller-cab-order-goods__table_value"
                                               href="<?= Url::to(['/admin/products/update', 'id' => $product->product_id]) ?>">
                                                <div class="product-seller-cab-order-goods__img">
                                                    <img alt="" src="<?= $product->product->image->imageUrl ?>">
                                                </div>
                                                <div class="product-seller-cab-order-goods__info">
                                                    <div class="product-seller-cab-order-goods__name">
                                                        <?= $product->product->info->name ?>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="seller-cab-order-goods__table_value">
                                                <div class="product-seller-cab-order-goods__categori">
                                                    <?= $product->product->category->info->name ?>
                                                </div>
                                            </div>
                                            <div class="seller-cab-order-goods__table_value">
                                                <div class="product-seller-cab-order-goods__cost">
                                                    <p><?= $product->product->discountPrice ?> грн</p>
                                                </div>
                                            </div>
                                            <div class="seller-cab-order-goods__table_value">
                                                <div class="product-seller-cab-order-goods__sold">
                                                    2
                                                </div>
                                            </div>
                                            <div class="seller-cab-order-goods__table_value">
                                                <div class="product-seller-cab-order-goods__remove remove-product">
                                                    <div class="product-seller-cab-order-goods__icon">
                                                        <svg fill="none" height="12" viewBox="0 0 9 12"
                                                             width="9"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M3.53126 0.689852H5.46881V1.01124H6.16853V0.644861C6.16862 0.289298 5.87532 0 5.51481 0H3.48526C3.12475 0 2.83145 0.289298 2.83145 0.644861V1.01124H3.53126V0.689852Z"
                                                                  fill="#A4A4A4"/>
                                                            <path d="M7.78179 3.79297H1.21788C1.03803 3.79297 0.896439 3.94536 0.910934 4.12346L1.45969 10.8629C1.49027 11.2392 1.80624 11.5289 2.18584 11.5289H6.81375C7.19335 11.5289 7.50931 11.2392 7.53989 10.8628L8.08865 4.12346C8.10324 3.94536 7.96165 3.79297 7.78179 3.79297ZM2.80725 10.8101C2.79991 10.8105 2.79257 10.8108 2.78533 10.8108C2.60202 10.8108 2.44815 10.669 2.43675 10.4849L2.09286 4.95212C2.08102 4.76067 2.22764 4.5959 2.42031 4.58414C2.61236 4.57255 2.77896 4.71783 2.79081 4.90937L3.13461 10.4421C3.14654 10.6336 2.99992 10.7982 2.80725 10.8101ZM4.85336 10.4635C4.85336 10.6552 4.69684 10.8107 4.50373 10.8107C4.31061 10.8107 4.15409 10.6552 4.15409 10.4635V4.9307C4.15409 4.7389 4.31061 4.58344 4.50373 4.58344C4.69675 4.58344 4.85336 4.7389 4.85336 4.9307V10.4635ZM6.90682 4.95115L6.57848 10.4839C6.56761 10.6684 6.41347 10.8107 6.22982 10.8107C6.22292 10.8107 6.21594 10.8105 6.20896 10.8102C6.0162 10.7988 5.86913 10.6345 5.88053 10.4431L6.20878 4.91024C6.22009 4.71879 6.38501 4.57273 6.5783 4.58405C6.77106 4.59529 6.91813 4.7597 6.90682 4.95115Z"
                                                                  fill="#A4A4A4"/>
                                                            <path d="M8.98501 2.70608L8.7557 2.02112C8.69524 1.84056 8.5256 1.71875 8.33451 1.71875H0.665404C0.474403 1.71875 0.304673 1.84056 0.244301 2.02112L0.0149929 2.70608C-0.029227 2.83818 0.0283206 2.97292 0.135737 3.04012C0.179515 3.06747 0.231326 3.08392 0.288256 3.08392H8.71174C8.76867 3.08392 8.82057 3.06747 8.86426 3.04003C8.97168 2.97284 9.02923 2.8381 8.98501 2.70608Z"
                                                                  fill="#A4A4A4"/>
                                                        </svg>
                                                    </div>
                                                    <?= Yii::t('app', 'Удалить') ?>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <button type="submit" class="admin-goods-sales-example__btn center hover-button">
            <div class="hover-button__front"><?= Yii::t('app', 'Сохранить изменения') ?></div>
            <div class="hover-button__back"><?= Yii::t('app', 'Сохранить изменения') ?></div>
        </button>
    </div>
    <div class="admin-goods-sales-example-cart">
        <div class="admin-goods-sales-example-cart__editing">
            <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                 xmlns="http://www.w3.org/2000/svg">
                <path d="M10.9816 0.539161C10.2626 -0.17972 9.09706 -0.17972 8.37811 0.539161L1.07968 7.83694C1.02966 7.88696 0.993549 7.94895 0.974693 8.01702L0.0149281 11.4817C-0.0245427 11.6237 0.0155673 11.7758 0.119758 11.8802C0.224108 11.9844 0.376238 12.0245 0.518301 11.9852L3.98327 11.0253C4.05134 11.0065 4.11334 10.9704 4.16336 10.9203L11.4616 3.62241C12.1795 2.90305 12.1795 1.73852 11.4616 1.01916L10.9816 0.539161ZM1.58625 8.87492L3.12529 10.414L0.996426 11.0038L1.58625 8.87492ZM10.8832 3.04398L10.4493 3.4778L8.52273 1.55141L8.95675 1.11759C9.35609 0.718281 10.0036 0.718281 10.403 1.11759L10.8832 1.59759C11.2819 1.99737 11.2819 2.64435 10.8832 3.04398Z"
                      fill="#6D6D6D"/>
            </svg>
        </div>
        <div class="admin-goods-sales-example-cart__body">
            <div class="admin-goods-sales-example-cart__name"><?= Html::encode($this->title) ?></div>
            <div class="admin-goods-sales-example-cart__text">
                <?php if (!$model->is_new): ?>
                    <p><?= Yii::t('app', 'Создано') ?>:</p>
                    <span><?= date('d.m.Y', $model->_sale->created_at) ?></span>
                    <span><?= date('H:i', $model->_sale->created_at) ?></span>
                <?php endif; ?>
            </div>
            <label class="admin-goods-sales-example-cart__img">
                <input type="file" class="image-product-thumb"
                       name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>">

                <?php if ($model->_sale->image): ?>
                    <img alt="sale image" src="<?= $model->_sale->image->imageUrl ?>"
                         data-key="<?= $model->_sale->image->id ?>">
                <?php endif; ?>

                <?php if ($model->label_name && $model->_sale->image): ?>
                    <?php
                    $label_data = explode(' ', $model->label_name);
                    $main_text = array_shift($label_data);
                    ?>
                    <div class="admin-goods-sales-example-cart__img_text">
                        <span><?= Html::encode($main_text) ?></span>
                        <?= Html::encode(implode(' ', $label_data)) ?>
                    </div>
                <?php endif; ?>
                <div class="admin-goods-sales-example-cart__cross" <?= $model->_sale->image ? 'style="z-index: -1;"' : '' ?>></div>
                <div class="admin-goods-sales-example-cart__close"></div>
            </label>
            <!--<div class="admin-goods-sales-example-cart__btn hover-button">
                <div class="hover-button__front"><? /*= Yii::t('app', 'Добавить баннер') */ ?></div>
                <div class="hover-button__back"><? /*= Yii::t('app', 'Добавить баннер') */ ?></div>
            </div>-->
            <ul class="admin-goods-sales-example-cart__menu">
                <li class="admin-goods-sales-example-cart__item">
                    - <?= Yii::t('app', 'Размеры') ?>: 1070 х 640, 530 х 315, 1620 х 180, 530 х 750, 530 х 640
                </li>
                <li class="admin-goods-sales-example-cart__item">- <?= Yii::t('app', 'Форматы') ?> JPG, GIF, PNG, PDF
                </li>
                <li class="admin-goods-sales-example-cart__item">- <?= Yii::t('app', 'Максимальный размер') ?>: 10 Мб.
                </li>
            </ul>
            <div class="admin-goods-sales-example-cart__more baner-select"><?= Yii::t('app', 'Размещение баннеров') ?></div>
        </div>
    </div>
</div>
<?php CustomActiveForm::end() ?>
