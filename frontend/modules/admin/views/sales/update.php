<?php

/* @var $this View */

/* @var $model SalesForm */

/* @var $model_upload \common\models\UploadForm */

use frontend\modules\admin\models\SalesForm;
use frontend\widgets\Breadcrumbs;
use yii\web\View;

$this->title = $model->_info->name;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Акции и промокоды'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <?= $this->render('_form', [
                        'model' => $model,
                        'model_upload' => $model_upload,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</main>
