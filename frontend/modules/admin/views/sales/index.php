<?php

/* @var $this \yii\web\View */

/* @var $sales \common\models\Sales[] */

use common\models\Sales;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomHtml;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Акции и промокоды');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="admin-goods-sales-page">
                            <div class="admin-goods-sales-page__title">
                                <div class="title">
                                    <div class="title__line"></div>
                                    <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
                                </div>
                                <a href="<?= Url::to(['/admin/sales/create']) ?>"
                                   class="admin-goods-sales-page__btn hover-button">
                                    <div class="hover-button__front">
                                        <span></span>
                                        <?= Yii::t('app', 'Добавить акцию') ?>
                                    </div>
                                    <div class="hover-button__back">
                                        <span></span>
                                        <?= Yii::t('app', 'Добавить акцию') ?>
                                    </div>
                                </a>
                            </div>
                            <div class="admin-goods-sales-page__body">
                                <div class="admin-goods-sales-page__options active">
                                    <?= Yii::t('app', 'Действия') ?>
                                    <i class="fas fa-chevron-up"></i>
                                </div>
                                <ul class="admin-goods-sales__table active">
                                    <li class="admin-goods-sales__item">
                                        <div class="admin-goods-sales__value">
                                            <?= Yii::t('app', 'Название') ?></div>
                                        <div class="admin-goods-sales__value">
                                            <?= Yii::t('app', 'Категория') ?></div>
                                        <div class="admin-goods-sales__value">
                                            <?= Yii::t('app', 'К-во учасников') ?>
                                            <i class="fas fa-sort-down"></i>
                                        </div>
                                        <div class="admin-goods-sales__value">
                                            <?= Yii::t('app', 'К-во товаров') ?>
                                            <i class="fas fa-sort-down"></i></div>
                                        <div class="admin-goods-sales__value">
                                            <?= Yii::t('app', 'Статус акции') ?>
                                        </div>
                                        <div class="admin-goods-sales__value">
                                            <?= Yii::t('app', 'Срок действия') ?>
                                            <i class="fas fa-sort-down"></i>
                                        </div>
                                        <div class="admin-goods-sales__value"></div>
                                    </li>
                                    <?php foreach ($sales as $sale): ?>
                                        <li class="admin-goods-sales__item">
                                            <a class="admin-goods-sales"
                                               href="<?= Url::to(['/admin/sales/update', 'id' => $sale->id]) ?>">
                                                <div class="admin-goods-sales__body">
                                                    <div class="admin-goods-sales__value">
                                                        <div class="admin-goods-sales__text"><?= $sale->info->name ?></div>
                                                    </div>
                                                    <div class="admin-goods-sales__value">
                                                        <div class="admin-goods-sales__text"><?= $sale->category->info->name ?></div>
                                                    </div>
                                                    <div class="admin-goods-sales__value">
                                                        <div class="admin-goods-sales__text"><?= count($sale->participants) ?></div>
                                                        <div class="admin-goods-sales__add"></div>
                                                    </div>
                                                    <div class="admin-goods-sales__value">
                                                        <div class="admin-goods-sales__text"><?= count($sale->products) ?></div>
                                                        <div class="admin-goods-sales__add"></div>
                                                    </div>
                                                    <div class="admin-goods-sales__value">
                                                        <?= CustomHtml::dropDownList('status', $sale->status_id, [
                                                            Sales::STATUS_ACTIVE => Yii::t('app', 'Активная'),
                                                            Sales::STATUS_PAUSED => Yii::t('app', 'Приостановлена'),
                                                            Sales::STATUS_DELETED => Yii::t('app', 'Удалена'),
                                                            Sales::STATUS_ARCHIVED => Yii::t('app', 'В архиве'),
                                                        ]) ?>
                                                    </div>
                                                    <div class="admin-goods-sales__value">
                                                        <div class="admin-goods-sales__text font">
                                                            до <?= date('d.m.Y', $sale->due_to) ?></div>
                                                    </div>
                                                    <div class="admin-goods-sales__value">
                                                        <div class="admin-goods-sales__text btn"><?= Yii::t('app', 'Приостановить') ?></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
