<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $brand Brands */

use common\models\Brands;
use frontend\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Отзывы о продавце');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Продавцы'), 'url' => ['/admin/sellers/index']];
$breadcrumbs[] = ['label' => $brand->name, 'url' => ['/admin/sellers/view', 'id' => $brand->id]];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <?php Pjax::begin(['id' => 'products_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'seller-cab-reviews pjax-more-container']]); ?>
                        <div class="title">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Yii::t('app', 'Отзывы о продавце') ?><span>(<?= $dataProvider->totalCount ?>)</span></div>
                        </div>
                        <div class="seller-cab-reviews__body"></div>
                        <?php $widget = ListView::begin([
                            'dataProvider' => $dataProvider,
                            'itemView' => '_seller',
                            'options' => [
                                'tag' => false,
                            ],
                            'itemOptions' => [
                                'tag' => false,
                            ],
                            'layout' => '{items}'
                        ]) ?>

                        <?php $widget->end() ?>
                        <div class="paginations">
                            <div class="paginations__page"><?= $widget->renderSummary() ?></div>
                            <?php if ($dataProvider->totalCount > 12): ?>
                                <a class="more-link hover-button more-products-button" href="#">
                                    <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                    <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                </a>
                            <?php endif; ?>
                            <ul class="paginations__menu">
                                <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                            </ul>
                        </div>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
