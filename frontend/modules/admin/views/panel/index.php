<?php

/* @var $this \yii\web\View */
/* @var $payments_info array */

/* @var $brands \common\models\Brands[] */
/* @var $last_orders \common\models\Orders[] */
/* @var $last_payments \common\models\Payments[] */

use frontend\components\Helper;
use frontend\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Панель управления');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="admin">
                            <div class="admin__title">
                                <div class="title">
                                    <div class="title__line"></div>
                                    <div class="title__text"><?= Yii::t('app', 'Панель управления') ?><span></span>
                                    </div>
                                </div>
                                <div class="admin__options">
                                    <div class="options__conteiner">
                                        <div class="options">
                                            <div class="options__body">
                                                <div class="options__value">
                                                    <div class="options__img"><img alt="" src="/assets/img/time.png">
                                                    </div>
                                                    <span class="value">
                                 За месяц</span><i class="fas fa-chevron-up"></i>
                                                </div>
                                                <div class="options__content">
                                                    <div class="options__opt">За неделю</div>
                                                    <div class="options__opt">За месяц</div>
                                                    <div class="options__opt">За пол года</div>
                                                    <div class="options__opt">За год</div>
                                                </div>
                                                <input type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="admin__body">
                                <div class="admin__container">
                                    <div class="seller-cab-title green">
                                        <div class="seller-cab-title__body">
                                            <div class="seller-cab-title__time">
                                                <img alt="" src="/assets/img/time.png">
                                                <span><?= Yii::t('app', 'Показатели за месяц') ?></span>
                                            </div>
                                            <div class="seller-cab-title__content">
                                                <div class="seller-cab-title__text">
                                                    <p><?= Yii::t('app', 'К-во заказов') ?></p>
                                                    <span><?= $payments_info['orders'] ?></span>
                                                </div>
                                                <div class="seller-cab-title__text">
                                                    <p><?= Yii::t('app', 'Общие начисления') ?>:</p>
                                                    <span><?= Helper::formatPrice($payments_info['balance_add']) ?> грн</span>
                                                </div>
                                                <div class="seller-cab-title__text">
                                                    <p><?= Yii::t('app', 'Общее списание') ?>:</p>
                                                    <span><?= Helper::formatPrice($payments_info['balance_charge']) ?> грн</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="admin__container">
                                    <div class="title-seller-cab"><?= Yii::t('app', 'Продавцы') ?></div>
                                    <div class="seller-cab-main__name"><?= Yii::t('app', 'Последние 3 зарегестрированные продавцы') ?></div>
                                    <ul class="admin-seller__menu admin-page">
                                        <li class="admin-seller__item">
                                            <div class="admin-seller__value"><?= Yii::t('app', 'Продавец') ?></div>
                                            <div class="admin-seller__value"><?= Yii::t('app', 'Рейтинг') ?></div>
                                            <div class="admin-seller__value"><?= Yii::t('app', 'К-во товаров') ?></div>
                                            <div class="admin-seller__value"><?= Yii::t('app', 'К-во заказов') ?></div>
                                            <div class="admin-seller__value"><?= Yii::t('app', 'Баланс') ?></div>
                                            <div class="admin-seller__value"></div>
                                        </li>
                                        <?php foreach ($brands as $brand): ?>
                                            <li class="admin-seller__item">
                                                <a class="admin-seller" href="<?= Url::to(['/admin/sellers/view', 'id' => $brand->id]) ?>">
                                                    <div class="admin-seller__value">
                                                        <div class="admin-seller__text"><?= $brand->name ?></div>
                                                    </div>
                                                    <div class="admin-seller__value">
                                                        <div class="admin-seller__text">
                                                            <?php for ($i = 1; $i <= 5; $i++): ?>
                                                                <i class="fas fa-star <?= $i < $brand->averageScore ? 'active' : '' ?>"></i>
                                                            <?php endfor; ?>
                                                            <span><?= $brand->averageScore ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="admin-seller__value">
                                                        <div class="admin-seller__text"><?= $brand->commonProductsAmount ?></div>
                                                    </div>
                                                    <div class="admin-seller__value">
                                                        <div class="admin-seller__text"><?= $brand->commonOrdersAmount ?></div>
                                                    </div>
                                                    <div class="admin-seller__value">
                                                        <div class="admin-seller__text green">
                                                            <span>
                                                                <?= $brand->balance->balance ?>
                                                                <span>грн</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="admin-seller__value">
                                                        <div class="admin-seller__arrow"><i
                                                                    class="fas fa-chevron-right"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <a class="more-link hover-button" href="<?= Url::to(['/admin/sellers/index']) ?>">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Смотреть все') ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Смотреть все') ?></span></div>
                                    </a>
                                </div>
                                <div class="admin__container">
                                    <div class="title-seller-cab"><?= Yii::t('app', 'Заказы') ?></div>
                                    <div class="seller-cab-main__name"><?= Yii::t('app', 'Последние {0} заказа', 3) ?></div>
                                    <div class="seller-cab-main__content">
                                        <?php foreach ($last_orders as $last_order): ?>
                                            <?= $this->render('/includes/_order', ['model' => $last_order]) ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <a class="more-link hover-button" href="<?= Url::to(['/admin/orders/index']) ?>">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Смотреть все') ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Смотреть все') ?></span></div>
                                    </a>
                                </div>
                                <div class="admin__container">
                                    <div class="title-seller-cab">Товары</div>
                                    <div class="seller-cab-title">
                                        <div class="seller-cab-title__body">
                                            <div class="seller-cab-title__time"><img alt=""
                                                                                     src="/assets/img/time.png"><span>добавлено товаров за месяц </span>
                                            </div>
                                            <div class="seller-cab-title__content">
                                                <div class="seller-cab-title__text">
                                                    <p>К-во импорта:</p><span>20 </span>
                                                </div>
                                                <div class="seller-cab-title__text">
                                                    <p>К-во новых товаров:</p><span>22</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="seller-cab-main__name">последние 3 импорта</div>
                                    <div class="seller-cab-main__content">
                                        <div class="cab-import"><a class="cab-import__body" href="#">
                                                <div class="cab-import__title">
                                                    <div class="cab-import__name">Импорт №1324</div>
                                                    <div class="cab-import__time">
                                                        <p>21.05.2020</p><span>21:34</span>
                                                    </div>
                                                </div>
                                                <div class="cab-import__value">
                                                    <div class="cab-import__img">
                                                        <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.99583 11.0592 6.16505 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91228C11.1131 5.72729 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                                  fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                                  stroke="#35CB44" stroke-linecap="round"
                                                                  stroke-linejoin="bevel"
                                                                  stroke-width="1.1"/>
                                                        </svg>
                                                    </div>
                                                    Обработано 13 позиций
                                                </div>
                                                <div class="cab-import__arrow"><i class="fas fa-chevron-up"></i></div>
                                            </a></div>
                                        <div class="cab-import"><a class="cab-import__body" href="#">
                                                <div class="cab-import__title">
                                                    <div class="cab-import__name">Импорт №1324</div>
                                                    <div class="cab-import__time">
                                                        <p>21.05.2020</p><span>21:34</span>
                                                    </div>
                                                </div>
                                                <div class="cab-import__value">
                                                    <div class="cab-import__img">
                                                        <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.99583 11.0592 6.16505 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91228C11.1131 5.72729 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                                  fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                                  stroke="#35CB44" stroke-linecap="round"
                                                                  stroke-linejoin="bevel"
                                                                  stroke-width="1.1"/>
                                                        </svg>
                                                    </div>
                                                    Обработано 13 позиций
                                                </div>
                                                <div class="cab-import__arrow"><i class="fas fa-chevron-up"></i></div>
                                            </a></div>
                                        <div class="cab-import"><a class="cab-import__body" href="#">
                                                <div class="cab-import__title">
                                                    <div class="cab-import__name">Импорт №1324</div>
                                                    <div class="cab-import__time">
                                                        <p>21.05.2020</p><span>21:34</span>
                                                    </div>
                                                </div>
                                                <div class="cab-import__value">
                                                    <div class="cab-import__img">
                                                        <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.99583 11.0592 6.16505 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91228C11.1131 5.72729 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                                  fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                                  stroke="#35CB44" stroke-linecap="round"
                                                                  stroke-linejoin="bevel"
                                                                  stroke-width="1.1"/>
                                                        </svg>
                                                    </div>
                                                    Обработано 13 позиций
                                                </div>
                                                <div class="cab-import__arrow"><i class="fas fa-chevron-up"></i></div>
                                            </a></div>
                                    </div>
                                    <a class="more-link hover-button" href="#">
                                        <div class="hover-button__front"><span>Смотреть все</span></div>
                                        <div class="hover-button__back"><span>Смотреть все</span></div>
                                    </a>
                                </div>
                                <div class="admin__container">
                                    <div class="title-seller-cab"><?= Yii::t('app', 'Последние транзакции') ?></div>
                                    <div class="seller-cab-main__name"><?= Yii::t('app', 'Последние {0} записи', 3) ?></div>
                                    <div class="seller-cab-main__content"></div>
                                    <ul class="seller-cab-table-transactions">
                                        <li class="seller-cab-table-transactions__item">
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Номер транзакции') ?></div>
                                            <div class="seller-cab-table-transactions__value">Дата</div>
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Тип операции') ?></div>
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'ID Заказа') ?></div>
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Стоимость заказа') ?></div>
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Начисление') ?></div>
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Списание') ?></div>
                                            <div class="seller-cab-table-transactions__value"><?= Yii::t('app', 'Баланс') ?></div>
                                            <div class="seller-cab-table-transactions__value"></div>
                                        </li>
                                        <?php foreach ($last_payments as $last_payment): ?>
                                            <?= $this->render('//../modules/seller/views/balance/_item', ['model' => $last_payment]) ?>
                                        <?php endforeach; ?>
                                    </ul>
                                    <a class="more-link hover-button" href="#">
                                        <div class="hover-button__front"><span>Смотреть все</span></div>
                                        <div class="hover-button__back"><span>Смотреть все</span></div>
                                    </a>
                                </div>
                                <div class="seller-cab__slider">
                                    <div class="title">
                                        <div class="title__line"></div>
                                        <div class="title__text">популярные товары<span></span></div>
                                    </div>
                                    <div class="slider-product">
                                        <div class="slider-product__body">
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product product-slider">
                                                <div class="product__size-container"><img
                                                            alt="" src="/assets/img/product/product_img_1.png"></div>
                                                <a class="product__body" href="#">
                                                    <div class="product__img">
                                                        <div class="product__like"><img alt=""
                                                                                        src="/assets/img/icon/like.svg">
                                                        </div>
                                                        <div class="product__more-info__body">
                                                            <div class="product__more-info red">-20 % по промокоду</div>
                                                            <div class="product__more-info blue">Новинка</div>
                                                        </div>
                                                        <div class="product__slider-img"><img
                                                                    alt="" src="/assets/img/product/product_img_1.png">
                                                        </div>
                                                    </div>
                                                    <div class="product__data">
                                                        <div class="product__row">
                                                            <div class="product__cost">799 грн</div>
                                                            <div class="product__color__body">
                                                                <div class="product__color red"></div>
                                                                <div class="product__color white-red"></div>
                                                                <div class="product__color white"></div>
                                                            </div>
                                                        </div>
                                                        <div class="product__type">
                                                            <p>Savage</p>/<span>Платье</span>
                                                        </div>
                                                        <div class="product__size">
                                                            <p>Размеры (UKR):</p>
                                                            <span>54</span><span>56</span><span>58</span><span>60</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
