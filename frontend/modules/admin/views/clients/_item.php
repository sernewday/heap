<?php

/* @var $this View */
/* @var $model User */

use common\models\User;
use yii\helpers\Url;
use yii\web\View;

?>

<li class="admin-buyers__item">
    <a href="<?= Url::to(['/admin/clients/view', 'id' => $model->id]) ?>" class="admin-buyers">
        <div class="admin-buyers__body">
            <div class="admin-buyers__value">
                <div class="admin-buyers__text"><?= $model->fullName ?></div>
            </div>
            <div class="admin-buyers__value">
                <div class="admin-buyers__text"><?= $model->amountOfOrders ?></div>
            </div>
            <div class="admin-buyers__value">
                <div class="admin-buyers__text">
                    <div class="admin-buyers__img">
                        <svg fill="none" height="9" viewBox="0 0 8 9" width="8"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.0146618 4.01081C0.044405 4.0809 0.113812 4.12664 0.190834 4.12664H2.28011V8.81256C2.28011 8.91602 2.36553 9 2.47078 9H5.52138C5.62663 9 5.71205 8.91602 5.71205 8.81256V4.12663H7.80933C7.88635 4.12663 7.95576 4.08088 7.98551 4.01116C8.01487 3.94106 7.99886 3.86046 7.94432 3.80685L4.1406 0.0551062C4.10477 0.0198736 4.05633 0 4.00562 0C3.95491 0 3.90647 0.0198736 3.87063 0.0547371L0.0558443 3.80648C0.00130892 3.86011 -0.0150814 3.94069 0.0146618 4.01081Z"
                                  fill="#35CB44"
                                  opacity="0.35"/>
                        </svg>
                    </div>
                    85 %
                </div>
            </div>
            <div class="admin-buyers__value">
                <div class="admin-buyers__text"><?= $model->amountOfReviews ?></div>
            </div>
            <div class="admin-buyers__value">
                <div class="admin-buyers__text arrow"><i class="fas fa-chevron-up"></i>
                </div>
            </div>
        </div>
    </a>
</li>
