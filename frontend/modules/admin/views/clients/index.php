<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use frontend\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Покупатели');

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$js = <<< JS
$('#products_container').on('pjax:complete', function () {
    moreItemsEvent();
});
JS;
$this->registerJs($js);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="admin-seller-page__title">
                            <div class="admin__title">
                                <div class="title">
                                    <div class="title__line"></div>
                                    <div class="title__text"><?= Yii::t('app', 'Покупатели') ?><span>(<?= $dataProvider->totalCount ?>)</span></div>
                                </div>
                                <div class="admin__options">
                                    <div class="options__conteiner">
                                        <div class="options">
                                            <div class="options__body">
                                                <div class="options__value">
                                                    <div class="options__img"><img alt="" src="/assets/img/time.png">
                                                    </div>
                                                    <span class="value">
                                 За месяц</span><i class="fas fa-chevron-up"></i>
                                                </div>
                                                <div class="options__content">
                                                    <div class="options__opt">За неделю</div>
                                                    <div class="options__opt">За месяц</div>
                                                    <div class="options__opt">За пол года</div>
                                                    <div class="options__opt">За год</div>
                                                </div>
                                                <input type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="admin-order-goods__search big">
                                <div class="input-block input-search">
                                    <input placeholder="Поиск по покупателям" type="text"><img
                                        alt="" src="/assets/img/header/search.png">
                                </div>
                            </div>
                        </div>
                        <?php Pjax::begin(['id' => 'products_container', 'timeout' => 10000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'admin-clients pjax-more-container']]); ?>
                        <div class="seller-cab-filter-container">
                            <div class="seller-cab-filter-body">
                                <div class="seller-cab-filter__filter">
                                    <div class="seller-cab-filter">
                                        <div class="seller-cab-filter-input">
                                            <div class="seller-cab-filter__img">
                                                <svg fill="none" height="9" viewBox="0 0 8 9" width="8"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M0.0146618 4.01081C0.044405 4.0809 0.113812 4.12664 0.190834 4.12664H2.28011V8.81256C2.28011 8.91602 2.36553 9 2.47078 9H5.52138C5.62663 9 5.71205 8.91602 5.71205 8.81256V4.12663H7.80933C7.88635 4.12663 7.95576 4.08088 7.98551 4.01116C8.01487 3.94106 7.99886 3.86046 7.94432 3.80685L4.1406 0.0551062C4.10477 0.0198736 4.05633 0 4.00562 0C3.95491 0 3.90647 0.0198736 3.87063 0.0547371L0.0558443 3.80648C0.00130892 3.86011 -0.0150814 3.94069 0.0146618 4.01081Z"
                                                          fill="#353535"/>
                                                </svg>
                                            </div>
                                            <div class="seller-cab-filter__text">статистика</div>
                                            <div class="input-block">
                                                <label class="input-block__lable"><span
                                                            class="input-block__title"></span>
                                                    <input class="input-block__input" placeholder="От" type="text">
                                                    <div class="help-block"></div>
                                                </label>
                                            </div>
                                            <div class="input-block">
                                                <label class="input-block__lable"><span
                                                            class="input-block__title"></span>
                                                    <input class="input-block__input" placeholder="До" type="text">
                                                    <div class="help-block"></div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="admin-buyers__menu">
                            <li class="admin-buyers__item">
                                <div class="admin-buyers__value"><?= Yii::t('app', 'Покупатель') ?></div>
                                <div class="admin-buyers__value"><?= Yii::t('app', 'К-во заказов') ?><i class="fas fa-sort-down"></i></div>
                                <div class="admin-buyers__value"><?= Yii::t('app', 'Статистика заказов') ?><i class="fas fa-sort-down"></i>
                                </div>
                                <div class="admin-buyers__value"><?= Yii::t('app', 'К-во отзывов') ?></div>
                                <div class="admin-buyers__value"></div>
                            </li>
                            <?php $widget = ListView::begin([
                                'dataProvider' => $dataProvider,
                                'itemView' => '_item',
                                'options' => [
                                    'tag' => false,
                                ],
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'layout' => '{items}'
                            ]) ?>

                            <?php $widget->end() ?>
                        </ul>
                        <div class="paginations">
                            <div class="paginations__page"><?= $widget->renderSummary() ?></div>
                            <?php if ($dataProvider->totalCount > 12): ?>
                                <a class="more-link hover-button more-products-button" href="#">
                                    <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                    <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                </a>
                            <?php endif; ?>
                            <ul class="paginations__menu">
                                <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                            </ul>
                        </div>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
