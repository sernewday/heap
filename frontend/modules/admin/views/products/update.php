<?php

/* @var $this \yii\web\View */

/* @var $model \frontend\models\ModuleProductsForm */
/* @var $model_upload \common\models\UploadForm */

use frontend\widgets\Breadcrumbs;

$this->title = Yii::t('app', 'Редактировать товар');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Товары'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <?= $this->render('_form', [
                            'model' => $model,
                            'model_upload' => $model_upload,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
