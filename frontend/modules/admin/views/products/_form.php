<?php

/* @var $this \yii\web\View */

use common\models\Brands;
use common\models\Categories;
use common\models\ProductParams;
use frontend\widgets\CustomActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model \frontend\models\ModuleProductsForm */
/* @var $model_upload \common\models\UploadForm */

$this->registerJsFile('/js/image_product_seller.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

?>

<?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab-product-editing', 'enctype' => 'multipart/form-data']]) ?>
<div class="seller-cab-product-editing__title">
    <a class="seller-cab-product-editing__link hover-button" href="#">
        <div class="hover-button__front">
            <svg fill="none" height="8" viewBox="0 0 17 8" width="17"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                        d="M4.79117 6.77032C5.10028 7.08607 5.05287 7.55525 4.68512 7.82148C4.31736 8.08795 3.76628 8.05199 3.45031 7.74115L0.207926 4.47766L0.181892 4.45524C0.173122 4.44754 0.16408 4.43259 0.155584 4.42535C0.144623 4.41811 0.138321 4.4069 0.138321 4.39522C0.129276 4.38798 0.120506 4.3728 0.111738 4.36556C0.102968 4.35809 0.102968 4.34314 0.0944748 4.33544C0.0854301 4.3282 0.0854301 4.31325 0.0766621 4.30578C0.0678921 4.29831 0.0678921 4.28336 0.059124 4.27612C0.0506287 4.26094 0.0506287 4.25347 0.0415859 4.23875C0.0415859 4.23105 0.0328159 4.21633 0.0328159 4.20863C0.0328159 4.19368 0.0243206 4.18621 0.0243206 4.17126C0.0243206 4.16402 0.0152779 4.14884 0.0152779 4.1416C0.0152779 4.12665 0.00678253 4.11147 0.00678253 4.09653V4.07411C-0.00226212 4.02717 -0.00226212 3.97929 0.00678253 3.93235V3.90993C0.00678253 3.89498 0.0152779 3.88027 0.0152779 3.86532C0.0152779 3.85762 0.0243206 3.8429 0.0243206 3.8352C0.0243206 3.82025 0.0328159 3.81278 0.0328159 3.79783C0.0328159 3.79059 0.0415859 3.77541 0.0415859 3.76817C0.0506287 3.75322 0.0506287 3.74575 0.059124 3.7308C0.0678921 3.72333 0.0678921 3.70838 0.0766621 3.70068C0.0854301 3.68596 0.0854301 3.67849 0.0944748 3.67102C0.102968 3.66331 0.102968 3.6486 0.111738 3.64136C0.120506 3.63365 0.129276 3.61871 0.138321 3.61123C0.146814 3.60399 0.146814 3.59629 0.155584 3.58134C0.16408 3.57387 0.173122 3.55892 0.181892 3.55145L0.207926 3.52926L3.45031 0.265543C3.76244 -0.0497352 4.31572 -0.0901376 4.68594 0.175864C5.05644 0.442099 5.1033 0.913382 4.79117 1.22913L2.76714 3.26793H16.1236C16.6073 3.26793 17 3.60212 17 4.01455C17 4.42675 16.6073 4.76141 16.1236 4.76141H2.76714L4.79117 6.77032Z"
                        fill="#353535"/>
            </svg>
            <span><?= Yii::t('app', 'Назад к товарам') ?></span>
        </div>
        <div class="hover-button__back">
            <svg fill="none" height="8" viewBox="0 0 17 8" width="17"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                        d="M4.79117 6.77032C5.10028 7.08607 5.05287 7.55525 4.68512 7.82148C4.31736 8.08795 3.76628 8.05199 3.45031 7.74115L0.207926 4.47766L0.181892 4.45524C0.173122 4.44754 0.16408 4.43259 0.155584 4.42535C0.144623 4.41811 0.138321 4.4069 0.138321 4.39522C0.129276 4.38798 0.120506 4.3728 0.111738 4.36556C0.102968 4.35809 0.102968 4.34314 0.0944748 4.33544C0.0854301 4.3282 0.0854301 4.31325 0.0766621 4.30578C0.0678921 4.29831 0.0678921 4.28336 0.059124 4.27612C0.0506287 4.26094 0.0506287 4.25347 0.0415859 4.23875C0.0415859 4.23105 0.0328159 4.21633 0.0328159 4.20863C0.0328159 4.19368 0.0243206 4.18621 0.0243206 4.17126C0.0243206 4.16402 0.0152779 4.14884 0.0152779 4.1416C0.0152779 4.12665 0.00678253 4.11147 0.00678253 4.09653V4.07411C-0.00226212 4.02717 -0.00226212 3.97929 0.00678253 3.93235V3.90993C0.00678253 3.89498 0.0152779 3.88027 0.0152779 3.86532C0.0152779 3.85762 0.0243206 3.8429 0.0243206 3.8352C0.0243206 3.82025 0.0328159 3.81278 0.0328159 3.79783C0.0328159 3.79059 0.0415859 3.77541 0.0415859 3.76817C0.0506287 3.75322 0.0506287 3.74575 0.059124 3.7308C0.0678921 3.72333 0.0678921 3.70838 0.0766621 3.70068C0.0854301 3.68596 0.0854301 3.67849 0.0944748 3.67102C0.102968 3.66331 0.102968 3.6486 0.111738 3.64136C0.120506 3.63365 0.129276 3.61871 0.138321 3.61123C0.146814 3.60399 0.146814 3.59629 0.155584 3.58134C0.16408 3.57387 0.173122 3.55892 0.181892 3.55145L0.207926 3.52926L3.45031 0.265543C3.76244 -0.0497352 4.31572 -0.0901376 4.68594 0.175864C5.05644 0.442099 5.1033 0.913382 4.79117 1.22913L2.76714 3.26793H16.1236C16.6073 3.26793 17 3.60212 17 4.01455C17 4.42675 16.6073 4.76141 16.1236 4.76141H2.76714L4.79117 6.77032Z"
                        fill="#353535"/>
            </svg>
            <span><?= Yii::t('app', 'Назад к товарам') ?></span>
        </div>
    </a>
    <div class="seller-cab-product-editing__name"><?= $this->title ?></div>
    <div class="seller-cab-product-editing__progres">
        <div class="personal-cart-progres__container">
            <div class="personal-cart-progres__text">Прогресс заполнения аккаунта</div>
            <div class="personal-cart-progres__bar_value">
                <div class="personal-cart-progres__bar" data-value="98"><span></span></div>
                <div class="personal-cart-progres__value"></div>
            </div>
        </div>
    </div>
</div>
<div class="seller-cab-product-editing__body">
    <div class="seller-cab-product-editing__content">
        <div class="seller-cab-product-editing__container info">
            <div class="title-seller-cab">
                <p><?= Yii::t('app', 'Общая информация') ?></p>
                <div class="title-seller-cab__dop">
                    <span>
                        <?php if ($model->isNew): ?>
                            При создании товара информация...
                        <?php endif; ?>
                    </span>
                    <div class="title-seller-cab__leng">
                        <a class="active" href="#">UKR</a>/
                        <a href="#">RUS</a></div>
                </div>
            </div>
            <div class="seller-cab-product-editing__row">
                <?= $form->field($model, 'name')->textInput() ?>
                <?= $form->field($model, 'vendor_code')->textInput() ?>

                <?= $form->field($model, 'description', [
                    'options' => ['class' => 'seller-cab-product-editing__textarea']
                ])->textarea() ?>
            </div>
        </div>
        <div class="seller-cab-product-editing__container small">
            <div class="title-seller-cab"><?= Yii::t('app', 'Цена') ?></div>
            <div class="seller-cab-product-editing__row">

                <?= $form->field($model, 'price')->textInput() ?>

                <?= $form->field($model, 'price_discount')->textInput() ?>

            </div>
        </div>
        <div class="seller-cab-product-editing__container small">
            <div class="title-seller-cab"><?= Yii::t('app', 'Наличие') ?></div>

            <div class="seller-cab-product-editing__row">
                <?= $form->field($model, 'available')->dropDownList([
                    1 => Yii::t('app', 'В наличии'),
                    0 => Yii::t('app', 'Нет в наличии'),
                ]) ?>

                <?= $form->field($model, 'amount_left')->textInput() ?>
            </div>
        </div>
        <div class="seller-cab-product-editing__container categori">
            <div class="title-seller-cab">категория</div>

            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::find()->where(['depth' => 3])->joinWith(['info'])->cache(3600 * 24)->all(), 'id', 'info.name'))->label(false) ?>

            <div class="add-button categori-add">
                <div class="add-button__body">
                    <div class="add-button__cross"></div>
                    <div class="add-button__text">Предложить категорию</div>
                </div>
            </div>
        </div>
        <div class="seller-cab-product-editing__container">
            <div class="title-seller-cab"><?= Yii::t('app', 'Характеристики') ?></div>
            <div class="seller-cab-product-editing__row">

                <?= $form->field($model, 'producer_char')->dropDownList(ArrayHelper::map(Brands::find()->orderBy('name ASC')->cache(3600 * 24)->all(), 'id', 'name')) ?>

                <?= $form->field($model, 'country_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_COUNTRY)) ?>

                <?= $form->field($model, 'style_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_STYLE)) ?>

                <?= $form->field($model, 'season_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_SEASON)) ?>

                <?= $form->field($model, 'gender_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_GENDER)) ?>

            </div>
            <div class="seller-cab-product-editing__row">
                <?= $form->field($model, 'material_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_MATERIAL)) ?>

                <?= $form->field($model, 'color_char[0]')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_COLOR)) ?>

            </div>
            <div class="seller-cab-product-editing__row">

                <?= $form->field($model, 'length_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_LENGTH)) ?>

                <?= $form->field($model, 'clasp_char')->dropDownList(ProductParams::getValuesMap(ProductParams::PARAM_CLASP)) ?>
            </div>
            <div class="seller-cab-product-editing__row">
                <div class="add-button">
                    <div class="add-button__body">
                        <div class="add-button__cross"></div>
                        <div class="add-button__text">Добавить характеристику</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="seller-cab-product-editing__container">
            <div class="title-seller-cab">продвижение</div>
            <div class="seller-cab-product-editing__row">
                <div class="add-button">
                    <div class="add-button__body">
                        <div class="add-button__cross"></div>
                        <div class="add-button__text">Добавить скидку</div>
                    </div>
                </div>
                <div class="add-button">
                    <div class="add-button__body">
                        <div class="add-button__cross"></div>
                        <div class="add-button__text">Добавить подарок</div>
                    </div>
                </div>
                <div class="add-button">
                    <div class="add-button__body">
                        <div class="add-button__cross"></div>
                        <div class="add-button__text">Добавить ярлык</div>
                    </div>
                </div>
                <div class="add-button">
                    <div class="add-button__body">
                        <div class="add-button__cross"></div>
                        <div class="add-button__text">Добавить сопутствующие</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="seller-cab-product-editing__container">
            <div class="title-seller-cab"><?= Yii::t('app', 'Размеры') ?> +</div>
            <div class="seller-cab-product-editing__row">

                <?php for ($i = 0; $i < 2; $i++): ?>
                    <?= $form->field($model, "size[{$i}]")->textInput() ?>
                    <?= $form->field($model, "size_international[{$i}]")->textInput() ?>
                <?php endfor; ?>

            </div>
        </div>
        <div class="seller-cab-product-editing__container">
            <div class="title-seller-cab">Разновидности</div>
            <ul class="seller-cab-order-goods__table">
                <li class="seller-cab-order-goods__table_item">
                    <div class="seller-cab-order-goods__table_value"><span
                                class="square"></span>
                        <p>Код товара</p>
                    </div>
                    <div class="seller-cab-order-goods__table_value">фото</div>
                    <div class="seller-cab-order-goods__table_value">размер</div>
                    <div class="seller-cab-order-goods__table_value">цена</div>
                    <div class="seller-cab-order-goods__table_value">Статус товара</div>
                    <div class="seller-cab-order-goods__table_value">продано</div>
                    <div class="seller-cab-order-goods__table_value">Остатки</div>
                    <div class="seller-cab-order-goods__table_value"></div>
                </li>
            </ul>
            <li class="seller-cab-order-goods__table_item">
                <div class="product-seller-cab-order-goods"><label
                            class="product-seller-cab-order-goods__lable">
                        <div class="seller-cab-order-goods__table_value"><input
                                    class="product-seller-cab-order-goods__input" type="checkbox">
                            <div class="product-seller-cab-order-goods__fake">
                                <svg fill="none" height="8"
                                     viewBox="0 0 9 8" width="9"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                            fill="#FFF" stroke-width="0.2"/>
                                </svg>
                            </div>
                            <div class="product-seller-cab-order-goods__cod">№164278243235</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value"><a
                                    class="product-seller-cab-order-goods__foto" href="#"><img
                                        alt="" src="/assets/img/product/product_img_1.png"></a></div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__size">44</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__cost">
                                <p>245 грн</p>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__statys">
                                <svg fill="none" height="14"
                                     viewBox="0 0 16 16" width="14"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                            fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                    <path
                                            d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                            stroke="#35CB44" stroke-linecap="round"
                                            stroke-linejoin="bevel"
                                            stroke-width="1.1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__sold">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__leftovers">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__actions"><span>Действия<i
                                            class="fas fa-chevron-up"></i></span>
                                <div class="product-seller-cab-order-goods__actions_menu">
                                    <div class="product-seller-cab-order-goods__actions_body">
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Редактировать
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Поднять в начало группы
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Удалить
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label></div>
            </li>
            <li class="seller-cab-order-goods__table_item">
                <div class="product-seller-cab-order-goods"><label
                            class="product-seller-cab-order-goods__lable">
                        <div class="seller-cab-order-goods__table_value"><input
                                    class="product-seller-cab-order-goods__input" type="checkbox">
                            <div class="product-seller-cab-order-goods__fake">
                                <svg fill="none" height="8"
                                     viewBox="0 0 9 8" width="9"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                            fill="#FFF" stroke-width="0.2"/>
                                </svg>
                            </div>
                            <div class="product-seller-cab-order-goods__cod">№164278243235</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value"><a
                                    class="product-seller-cab-order-goods__foto" href="#"><img
                                        alt="" src="/assets/img/product/product_img_1.png"></a></div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__size">44</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__cost">
                                <p>245 грн</p>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__statys">
                                <svg fill="none" height="14"
                                     viewBox="0 0 16 16" width="14"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                            fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                    <path
                                            d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                            stroke="#35CB44" stroke-linecap="round"
                                            stroke-linejoin="bevel"
                                            stroke-width="1.1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__sold">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__leftovers">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__actions"><span>Действия<i
                                            class="fas fa-chevron-up"></i></span>
                                <div class="product-seller-cab-order-goods__actions_menu">
                                    <div class="product-seller-cab-order-goods__actions_body">
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Редактировать
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Поднять в начало группы
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Удалить
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label></div>
            </li>
            <li class="seller-cab-order-goods__table_item">
                <div class="product-seller-cab-order-goods"><label
                            class="product-seller-cab-order-goods__lable">
                        <div class="seller-cab-order-goods__table_value"><input
                                    class="product-seller-cab-order-goods__input" type="checkbox">
                            <div class="product-seller-cab-order-goods__fake">
                                <svg fill="none" height="8"
                                     viewBox="0 0 9 8" width="9"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                            fill="#FFF" stroke-width="0.2"/>
                                </svg>
                            </div>
                            <div class="product-seller-cab-order-goods__cod">№164278243235</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value"><a
                                    class="product-seller-cab-order-goods__foto" href="#"><img
                                        alt="" src="/assets/img/product/product_img_1.png"></a></div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__size">44</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__cost">
                                <p>245 грн</p>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__statys">
                                <svg fill="none" height="14"
                                     viewBox="0 0 16 16" width="14"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                            fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                    <path
                                            d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                            stroke="#35CB44" stroke-linecap="round"
                                            stroke-linejoin="bevel"
                                            stroke-width="1.1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__sold">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__leftovers">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__actions"><span>Действия<i
                                            class="fas fa-chevron-up"></i></span>
                                <div class="product-seller-cab-order-goods__actions_menu">
                                    <div class="product-seller-cab-order-goods__actions_body">
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Редактировать
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Поднять в начало группы
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Удалить
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label></div>
            </li>
            <li class="seller-cab-order-goods__table_item">
                <div class="product-seller-cab-order-goods"><label
                            class="product-seller-cab-order-goods__lable">
                        <div class="seller-cab-order-goods__table_value"><input
                                    class="product-seller-cab-order-goods__input" type="checkbox">
                            <div class="product-seller-cab-order-goods__fake">
                                <svg fill="none" height="8"
                                     viewBox="0 0 9 8" width="9"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                            fill="#FFF" stroke-width="0.2"/>
                                </svg>
                            </div>
                            <div class="product-seller-cab-order-goods__cod">№164278243235</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value"><a
                                    class="product-seller-cab-order-goods__foto" href="#"><img
                                        alt="" src="/assets/img/product/product_img_1.png"></a></div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__size">44</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__cost">
                                <p>245 грн</p>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__statys">
                                <svg fill="none" height="14"
                                     viewBox="0 0 16 16" width="14"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                            fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                    <path
                                            d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                            stroke="#35CB44" stroke-linecap="round"
                                            stroke-linejoin="bevel"
                                            stroke-width="1.1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__sold">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__leftovers">2</div>
                        </div>
                        <div class="seller-cab-order-goods__table_value">
                            <div class="product-seller-cab-order-goods__actions"><span>Действия<i
                                            class="fas fa-chevron-up"></i></span>
                                <div class="product-seller-cab-order-goods__actions_menu">
                                    <div class="product-seller-cab-order-goods__actions_body">
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Редактировать
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Поднять в начало группы
                                        </div>
                                        <div class="product-seller-cab-order-goods__actions_item">
                                            Удалить
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label></div>
            </li>
        </div>
    </div>
    <div class="seller-cab-product-editing__cart">
        <div class="seller-cab-product-editing-cart">
            <div class="seller-cab-product-editing-cart__body">
                <?php if (!$model->isNew): ?>
                    <div class="seller-cab-product-editing-cart__title">
                        <div class="seller-cab-product-editing-cart__name">
                            <?= $model->_product->info->name ?>
                        </div>
                        <div class="seller-cab-product-editing-cart__order">
                            <p><?= Yii::t('app', 'Товар создан') ?></p>
                            <span><?= date('d.m.Y', $model->_product->created_at) ?></span>
                            <span><?= date('H:i', $model->_product->created_at) ?></span>
                        </div>
                        <?php if ($model->_product->updated_at != $model->_product->created_at): ?>
                            <div class="seller-cab-product-editing-cart__order">
                                <p><?= Yii::t('app', 'Товар изменён') ?></p>
                                <span><?= date('d.m.Y', $model->_product->updated_at) ?></span>
                                <span><?= date('H:i', $model->_product->updated_at) ?></span>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="seller-cab-product-editing-cart__images">
                    <div class="seller-cab-product-editing-cart__images_title">
                        <p><?= Yii::t('app', 'Изображения') ?></p>
                        <?php if (!$model->isNew): ?>
                            <span>(<?= count($model->_product->images) ?> <?= Yii::t('app', 'из') ?> 10)</span>
                        <?php endif; ?>
                    </div>
                    <div class="seller-cab-product-editing-cart__container">
                        <label class="seller-cab-product-editing-cart__img big">
                            <input type="file" name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>"
                                   class="image-product-thumb">
                            <div class="seller-cab-product-editing-cart__close"></div>
                            <div class="seller-cab-product-editing-cart__cross"></div>
                            <?php if (!$model->isNew && isset($model->_product->images[0])): ?>
                                <img alt="image 0" src="<?= $model->_product->images[0]->imageUrl ?>"
                                     data-key="<?= $model->_product->images[0]->id ?>">
                            <?php endif; ?>
                        </label>
                        <?php $image_index = 1; ?>
                        <?php for ($i = 0; $i < 3; $i++): ?>
                            <div class="seller-cab-product-editing-cart__row">

                                <?php for ($j = 0; $j < 3; $j++): ?>
                                    <label class="seller-cab-product-editing-cart__img">
                                        <input type="file"
                                               name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>"
                                               class="image-product-thumb">
                                        <div class="seller-cab-product-editing-cart__close"></div>
                                        <div class="seller-cab-product-editing-cart__cross"></div>

                                        <?php if (isset($model->_product->images[$image_index])): ?>
                                            <img alt="image <?= $image_index ?>"
                                                 src="<?= $model->_product->images[$image_index]->imageUrl ?>"
                                                 data-key="<?= $model->_product->images[$image_index]->id ?>">
                                        <?php endif; ?>
                                    </label>
                                    <?php $image_index++; endfor; ?>

                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
                <div class="seller-cab-product-editing-cart__btn">
                    <div class="seller-cab-product-editing-cart__remove hover-button">
                        <div class="hover-button__front"><span>Удалить позицию</span></div>
                        <div class="hover-button__back"><span>Удалить позицию</span></div>
                    </div>
                    <div class="seller-cab-product-editing-cart__save-close hover-button">
                        <div class="hover-button__front"><span>Сохранить и закрыть</span></div>
                        <div class="hover-button__back"><span>Сохранить и закрыть</span></div>
                    </div>
                    <button type="submit" class="seller-cab-product-editing-cart__save hover-button">
                        <div class="hover-button__front"><span>Сохранить изменения</span></div>
                        <div class="hover-button__back"><span>Сохранить изменения</span></div>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php CustomActiveForm::end() ?>
