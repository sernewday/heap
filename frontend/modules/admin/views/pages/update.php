<?php

/* @var $this View */

/* @var $model PagesForm */

/* @var $lang int */

use frontend\modules\admin\models\PagesForm;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = $model->_info->title;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Страницы сайта'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$this->registerJsFile('/js/admin__pages.js', ['depends' => ['frontend\assets\AppAsset']]);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <?php $form = CustomActiveForm::begin(['options' => ['class' => 'admin-editing']]) ?>
                        <div class="admin-editing__title">
                            <div class="title">
                                <div class="title__line"></div>
                                <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
                            </div>
                            <button type="submit" class="admin-editing__btn top hover-button">
                                <div class="hover-button__front"><?= Yii::t('app', 'Сохранить изменения') ?></div>
                                <div class="hover-button__back"><?= Yii::t('app', 'Сохранить изменения') ?></div>
                            </button>
                        </div>
                        <div class="admin-editing__body">
                            <ul class="admin-editing__menu">
                                <li class="admin-editing__item">
                                    <div class="admin-editing__number"><span>1.</span></div>
                                    <div class="admin-editing__value">
                                        <?= $form->field($model, 'title')->textInput() ?>
                                    </div>
                                </li>
                            </ul>
                            <?php if ($model->_content): ?>
                                <?php for ($i = 0; $i < count($model->_content); $i++): ?>
                                    <ul class="admin-editing__menu">
                                        <li class="admin-editing__item">
                                            <div class="admin-editing__number"><span><?= $i + 2 ?>.</span></div>
                                            <div class="admin-editing__value">
                                                <?= $form->field($model, 'content[' . $i . '][title]')->textInput(['value' => $model->_content[$i]->getInfoByLang($lang)->title])->label(Yii::t('app', 'Название')) ?>
                                                <?= $form->field($model, 'content[' . $i . '][text]')->textarea(['placeholder' => '', 'value' => $model->_content[$i]->getInfoByLang($lang)->text])->label(Yii::t('app', 'Описание')) ?>
                                                <?= $form->field($model, 'content[' . $i . '][id]')->hiddenInput(['value' => $model->_content[$i]->id]) ?>
                                            </div>
                                        </li>
                                    </ul>
                                <?php endfor; ?>
                            <?php else: ?>
                                <ul class="admin-editing__menu">
                                    <li class="admin-editing__item">
                                        <div class="admin-editing__number"><span>2.</span></div>
                                        <div class="admin-editing__value">
                                            <?= $form->field($model, 'content[0][title]')->textInput()->label(Yii::t('app', 'Название')) ?>
                                            <?= $form->field($model, 'content[0][text]')->textarea(['placeholder' => ''])->label(Yii::t('app', 'Описание')) ?>
                                            <?= $form->field($model, 'content[0][id]')->hiddenInput(['value' => 0]) ?>
                                        </div>
                                    </li>
                                </ul>
                            <?php endif; ?>
                            <a href="#" class="admin-editing__add">
                                <span></span>
                                <p><?= Yii::t('app', 'Добавить пункт') ?></p>
                            </a>
                            <button type="submit" class="admin-editing__btn hover-button">
                                <div class="hover-button__front"><?= Yii::t('app', 'Сохранить изменения') ?></div>
                                <div class="hover-button__back"><?= Yii::t('app', 'Сохранить изменения') ?></div>
                            </button>
                        </div>
                        <?php CustomActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
