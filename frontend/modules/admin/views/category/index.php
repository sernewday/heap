<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Категории');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/admin/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Категории'), 'url' => '#'];

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

$js = <<< JS
$('#products_container').on('pjax:complete', function () {
    moreItemsEvent();
});
JS;
$this->registerJs($js);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="seller-cab-order-goods">
                            <div class="seller-cab-order-goods__title">
                                <div class="seller-cab-order-goods__name">
                                    <div class="title">
                                        <div class="title__line"></div>
                                        <div class="title__text"><?= Yii::t('app', 'Категории') ?><span>(<?= $dataProvider->totalCount ?>)</span></div>
                                    </div>
                                    <div class="seller-cab-order-goods__search"><img
                                            alt="" src="/assets/img/header/search.png"></div>
                                </div>
                                <a href="<?= Url::to(['/admin/products/create']) ?>" class="seller-cab-order-goods__add hover-button">
                                    <div class="hover-button__front">
                                        <span></span>
                                        <?= Yii::t('app', 'Добавить категорию') ?>
                                    </div>
                                    <div class="hover-button__back">
                                        <span></span>
                                        <?= Yii::t('app', 'Добавить категорию') ?>
                                    </div>
                                </a>
                            </div>
                            <div class="seller-cab-filter-container">
                                <div class="seller-cab-filter-body">
                                    <div class="seller-cab-filter__filter">
                                        <div class="seller-cab-filter__filter-container">
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="9" viewBox="0 0 12 9"
                                                             width="12" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M0.9375 0.418605V8.5814C0.9375 8.81261 0.727661 9 0.46875 9C0.209839 9 0 8.81261 0 8.5814V0.418605C0 0.187391 0.209839 0 0.46875 0C0.727661 0 0.9375 0.187391 0.9375 0.418605ZM11.5312 0C11.2723 0 11.0625 0.187391 11.0625 0.418605V7.33866C11.0625 7.56988 11.2723 7.75727 11.5312 7.75727C11.7902 7.75727 12 7.56988 12 7.33866V0.418605C12 0.187391 11.7902 0 11.5312 0ZM9.70312 7.8907C9.96204 7.8907 10.1719 7.70331 10.1719 7.47209V0.418605C10.1719 0.187391 9.96204 0 9.70312 0C9.44421 0 9.23438 0.187391 9.23438 0.418605V7.47209C9.23438 7.70331 9.44421 7.8907 9.70312 7.8907ZM7.85156 7.94564C8.11047 7.94564 8.32031 7.75825 8.32031 7.52703V0.418605C8.32031 0.187391 8.11047 0 7.85156 0C7.59265 0 7.38281 0.187391 7.38281 0.418605V7.52703C7.38281 7.75825 7.59265 7.94564 7.85156 7.94564ZM4.17188 8.16279C3.91296 8.16279 3.70312 8.35018 3.70312 8.5814C3.70312 8.81261 3.91296 9 4.17188 9C4.43079 9 4.64062 8.81261 4.64062 8.5814C4.64062 8.35018 4.43079 8.16279 4.17188 8.16279ZM4.17188 0C3.91296 0 3.70312 0.187391 3.70312 0.418605V7.47209C3.70312 7.70331 3.91296 7.8907 4.17188 7.8907C4.43079 7.8907 4.64062 7.70331 4.64062 7.47209V0.418605C4.64062 0.187391 4.43079 0 4.17188 0ZM2.36719 8.16279C2.10828 8.16279 1.89844 8.35018 1.89844 8.5814C1.89844 8.81261 2.10828 9 2.36719 9C2.6261 9 2.83594 8.81261 2.83594 8.5814C2.83594 8.35018 2.6261 8.16279 2.36719 8.16279ZM2.36719 0C2.10828 0 1.89844 0.187391 1.89844 0.418605V7.47209C1.89844 7.70331 2.10828 7.8907 2.36719 7.8907C2.6261 7.8907 2.83594 7.70331 2.83594 7.47209V0.418605C2.83594 0.187391 2.6261 0 2.36719 0ZM5.97656 0C5.71765 0 5.50781 0.187391 5.50781 0.418605V8.5814C5.50781 8.81261 5.71765 9 5.97656 9C6.23547 9 6.44531 8.81261 6.44531 8.5814V0.418605C6.44531 0.187391 6.23547 0 5.97656 0Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M7.38281 8.5814C7.38281 8.35018 7.59265 8.16279 7.85156 8.16279C8.11047 8.16279 8.32031 8.35018 8.32031 8.5814C8.32031 8.81261 8.11047 9 7.85156 9C7.59265 9 7.38281 8.81261 7.38281 8.5814Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M11.0625 8.5814C11.0625 8.35018 11.2723 8.16279 11.5312 8.16279C11.7902 8.16279 12 8.35018 12 8.5814C12 8.81261 11.7902 9 11.5312 9C11.2723 9 11.0625 8.81261 11.0625 8.5814Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M9.23438 8.5814C9.23438 8.35018 9.44421 8.16279 9.70312 8.16279C9.96204 8.16279 10.1719 8.35018 10.1719 8.5814C10.1719 8.81261 9.96204 9 9.70312 9C9.44421 9 9.23438 8.81261 9.23438 8.5814Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">Код товара</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__value">
                                                                <div class="input-block-small"><label
                                                                        class="input-block-small__lable"><input
                                                                            class="input-block-small__input" placeholder="Код товара"
                                                                            type="text">
                                                                        <div class="input-block-small__search"><img
                                                                                alt=""
                                                                                src="/assets/img/header/search.png"></div>
                                                                    </label></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="11" viewBox="0 0 8 11"
                                                             width="8" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M0.571429 11H7.42857C7.74405 11 8 10.7274 8 10.3911V3.04428C8 2.70798 7.74405 2.43542 7.42857 2.43542H5.71429V1.82657C5.71429 0.819419 4.94524 0 4 0C3.05476 0 2.28571 0.819419 2.28571 1.82657V2.43542H0.571429C0.255804 2.43542 0 2.70798 0 3.04428V10.3911C0 10.7274 0.255804 11 0.571429 11V11ZM3.42857 1.82657C3.42857 1.4909 3.68482 1.21771 4 1.21771C4.31503 1.21771 4.57143 1.4909 4.57143 1.82657V2.43542H3.42857V1.82657ZM1.14286 3.65314H2.28571V4.26199C2.28571 4.59829 2.54152 4.87085 2.85714 4.87085C3.17262 4.87085 3.42857 4.59829 3.42857 4.26199V3.65314H4.57143V4.26199C4.57143 4.59829 4.82723 4.87085 5.14286 4.87085C5.45833 4.87085 5.71429 4.59829 5.71429 4.26199V3.65314H6.85714V9.78229H1.14286V3.65314Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">название товара</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__value">
                                                                <div class="input-block-small"><label
                                                                        class="input-block-small__lable"><input
                                                                            class="input-block-small__input" placeholder="Поиск"
                                                                            type="text">
                                                                        <div class="input-block-small__search"><img
                                                                                alt=""
                                                                                src="/assets/img/header/search.png"></div>
                                                                    </label></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="11" viewBox="0 0 11 11"
                                                             width="11" xmlns="http://www.w3.org/2000/svg">
                                                            <rect height="4" rx="0.5" stroke="#353535" width="4" x="0.5"
                                                                  y="0.5"/>
                                                            <rect height="4" rx="0.5" stroke="#353535" width="4" x="0.5"
                                                                  y="6.5"/>
                                                            <rect height="4" rx="0.5" stroke="#353535" width="4" x="6.5"
                                                                  y="0.5"/>
                                                            <rect height="4" rx="0.5" stroke="#353535" width="4" x="6.5"
                                                                  y="6.5"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">категория</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__column">
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                            </div>
                                                            <div class="seller-cab-filter__column">
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                            </div>
                                                            <div class="seller-cab-filter__column">
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                                <div class="seller-cab-filter__value">
                                                                    <div class="checkbox"><label class="checkbox__body"><input
                                                                                class="checkbox__input"
                                                                                type="checkbox"><span
                                                                                class="checkbox__fake"><svg fill="none"
                                                                                                            height="8"
                                                                                                            viewBox="0 0 9 8"
                                                                                                            width="9"
                                                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                                fill="#FFF" stroke-width="0.2"/>
                                          </svg></span><span class="checkbox__text">Платье</span></label></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter__content">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="12" viewBox="0 0 12 12"
                                                             width="12" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M3.54617 6.00053C3.54617 5.6993 3.30195 5.45508 3.00071 5.45508H0.818892C0.517656 5.45508 0.273438 5.6993 0.273438 6.00053C0.273438 6.30177 0.517656 6.54599 0.818892 6.54599H3.00071C3.30195 6.54599 3.54617 6.30177 3.54617 6.00053Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M11.1832 5.45508H10.0923C9.79109 5.45508 9.54688 5.6993 9.54688 6.00053C9.54688 6.30177 9.79109 6.54599 10.0923 6.54599H11.1832C11.4845 6.54599 11.7287 6.30177 11.7287 6.00053C11.7287 5.6993 11.4845 5.45508 11.1832 5.45508Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M6.27592 3.27273C6.57716 3.27273 6.82138 3.02851 6.82138 2.72727V0.545455C6.82138 0.244218 6.57716 0 6.27592 0C5.97469 0 5.73047 0.244218 5.73047 0.545455V2.72727C5.73047 3.02851 5.97469 3.27273 6.27592 3.27273Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M6.27202 8.72656C5.97078 8.72656 5.72656 8.97078 5.72656 9.27202V11.4538C5.72656 11.7551 5.97078 11.9993 6.27202 11.9993C6.57325 11.9993 6.81747 11.7551 6.81747 11.4538V9.27202C6.81747 8.97078 6.57325 8.72656 6.27202 8.72656Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M2.80224 1.75741C2.58929 1.54439 2.24391 1.54442 2.03086 1.75741C1.81784 1.97042 1.81784 2.31577 2.03086 2.52879L3.57366 4.07162C3.68017 4.17813 3.81977 4.23141 3.95933 4.23141C4.09889 4.23141 4.23853 4.17813 4.345 4.07166C4.55802 3.85864 4.55802 3.5133 4.345 3.30028L2.80224 1.75741Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M8.9741 7.92927C8.76112 7.71629 8.41574 7.71629 8.20272 7.92927C7.9897 8.14229 7.98974 8.48763 8.20272 8.70065L9.74556 10.2434C9.85207 10.3499 9.99167 10.4032 10.1313 10.4032C10.2709 10.4032 10.4105 10.3499 10.5169 10.2434C10.73 10.0304 10.73 9.68505 10.5169 9.47203L8.9741 7.92927Z"
                                                                fill="#353535"/>
                                                            <path
                                                                d="M3.57362 7.9293L2.03086 9.47206C1.81784 9.68508 1.81784 10.0304 2.03086 10.2434C2.13737 10.35 2.27697 10.4032 2.41657 10.4032C2.55617 10.4032 2.69577 10.35 2.80224 10.2434L4.345 8.70068C4.55802 8.48766 4.55802 8.14231 4.345 7.9293C4.13198 7.71628 3.7866 7.71628 3.57362 7.9293Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">Статус</div>
                                                    <i class="fas fa-chevron-up"></i>
                                                    <div class="seller-cab-filter__body">
                                                        <div class="seller-cab-filter__options">
                                                            <div class="seller-cab-filter__column">
                                                                <div class="seller-cab-filter__value statys">
                                                                    <div class="seller-cab-filter__img">
                                                                        <svg fill="none" height="11" viewBox="0 0 12 11"
                                                                             width="12"
                                                                             xmlns="http://www.w3.org/2000/svg">
                                                                            <path
                                                                                d="M7.17718 3.4282L7.1772 3.42821C7.23462 3.36263 7.32609 3.29768 7.4369 3.28228C7.55467 3.26592 7.67792 3.30789 7.78325 3.42822C7.95333 3.62251 7.90168 3.88421 7.78396 4.01948L4.91024 7.44014L4.90893 7.4417L4.90892 7.44169C4.83484 7.5263 4.70845 7.55476 4.60631 7.55477C4.50417 7.55478 4.37775 7.52635 4.30365 7.44168M7.17718 3.4282L7.25243 3.49408C7.34464 3.38877 7.537 3.29872 7.70801 3.49408C7.83888 3.64359 7.80021 3.84913 7.70801 3.95443L4.83368 7.37582C4.74155 7.48105 4.47106 7.48112 4.3789 7.37582L3.12481 5.94333C3.03261 5.83801 2.98549 5.63339 3.12481 5.47443C3.26407 5.31555 3.48808 5.36911 3.58027 5.47443M7.17718 3.4282L7.17597 3.42964L4.62592 6.45512M7.17718 3.4282L4.62592 6.45512M4.30365 7.44168L4.37866 7.37603L4.30366 7.44169L4.30365 7.44168ZM4.30365 7.44168L3.04957 6.0092L3.04957 6.0092C2.93286 5.87588 2.86938 5.61415 3.04961 5.40852C3.14283 5.30216 3.26612 5.26684 3.37805 5.27566C3.48584 5.28416 3.58949 5.33387 3.65448 5.40739M3.65448 5.40739L3.6536 5.40644L3.58027 5.47443M3.65448 5.40739C3.65483 5.40778 3.65517 5.40817 3.65552 5.40856L3.58027 5.47443M3.65448 5.40739L4.62592 6.45512M3.58027 5.47443L4.56099 6.53216L4.62592 6.45512"
                                                                                fill="#35CB44" stroke="#35CB44"
                                                                                stroke-width="0.2"/>
                                                                            <path
                                                                                d="M1 7.20047C1 8.84185 2.3306 10.1725 3.97198 10.1725H7.11379C8.98966 10.1725 10.5103 8.65177 10.5103 6.7759V4.05866C10.5103 2.1828 8.98966 0.662109 7.11379 0.662109H3.97198C2.3306 0.662109 1 1.99271 1 3.63409"
                                                                                stroke="#35CB44"
                                                                                stroke-linecap="round"
                                                                                stroke-linejoin="bevel"/>
                                                                        </svg>
                                                                    </div>
                                                                    <span>Заказ доставлен</span>
                                                                </div>
                                                                <div class="seller-cab-filter__value statys">
                                                                    <div class="seller-cab-filter__img">
                                                                        <svg fill="none" height="11" viewBox="0 0 12 11"
                                                                             width="12"
                                                                             xmlns="http://www.w3.org/2000/svg">
                                                                            <path
                                                                                d="M1 7.20047C1 8.84185 2.3306 10.1725 3.97198 10.1725H7.11379C8.98966 10.1725 10.5103 8.65177 10.5103 6.7759V4.05866C10.5103 2.1828 8.98966 0.662109 7.11379 0.662109H3.97198C2.3306 0.662109 1 1.99271 1 3.63409"
                                                                                stroke="#A4A4A4"
                                                                                stroke-linecap="round"
                                                                                stroke-linejoin="bevel"/>
                                                                        </svg>
                                                                    </div>
                                                                    <span>Заказ в процессе доставки</span>
                                                                </div>
                                                                <div class="seller-cab-filter__value statys">
                                                                    <div class="seller-cab-filter__img">
                                                                        <svg fill="none" height="11" viewBox="0 0 12 11"
                                                                             width="12"
                                                                             xmlns="http://www.w3.org/2000/svg">
                                                                            <path
                                                                                d="M1 7.20047C1 8.84185 2.3306 10.1725 3.97198 10.1725H7.11379C8.98966 10.1725 10.5103 8.65177 10.5103 6.7759V4.05866C10.5103 2.1828 8.98966 0.662109 7.11379 0.662109H3.97198C2.3306 0.662109 1 1.99271 1 3.63409"
                                                                                stroke="#EE4A2E"
                                                                                stroke-linecap="round"
                                                                                stroke-linejoin="bevel"/>
                                                                            <path d="M8 3.00004L5.5 5.5M5.5 5.5L3 3M5.5 5.5L8 7.99995M5.5 5.5L3 8"
                                                                                  stroke="#EE4A2E"
                                                                                  stroke-linecap="round"/>
                                                                        </svg>
                                                                    </div>
                                                                    <span>Заказ не доставлен</span>
                                                                </div>
                                                                <div class="seller-cab-filter__value statys">
                                                                    <div class="seller-cab-filter__img">
                                                                        <svg fill="none" height="11" viewBox="0 0 12 11"
                                                                             width="12"
                                                                             xmlns="http://www.w3.org/2000/svg">
                                                                            <path
                                                                                d="M1 7.20047C1 8.84185 2.3306 10.1725 3.97198 10.1725H7.11379C8.98966 10.1725 10.5103 8.65177 10.5103 6.7759V4.05866C10.5103 2.1828 8.98966 0.662109 7.11379 0.662109H3.97198C2.3306 0.662109 1 1.99271 1 3.63409"
                                                                                stroke="#EE4A2E"
                                                                                stroke-linecap="round"
                                                                                stroke-linejoin="bevel"/>
                                                                        </svg>
                                                                    </div>
                                                                    <span>Заказ отменен</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="seller-cab-filter">
                                                <div class="seller-cab-filter-input">
                                                    <div class="seller-cab-filter__img">
                                                        <svg fill="none" height="12" viewBox="0 0 5 12"
                                                             width="5" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M4.75838 6.42159C4.59723 6.11377 4.39792 5.88668 4.16078 5.74034C3.9236 5.5942 3.66617 5.46648 3.38871 5.35678C3.11134 5.24714 2.85146 5.16113 2.60959 5.09851C2.36746 5.03589 2.16439 4.94711 1.99998 4.83215C1.83562 4.71749 1.75342 4.57135 1.75342 4.39377C1.75342 4.21624 1.83562 4.05451 1.99998 3.90841C2.16434 3.76227 2.40174 3.68913 2.71229 3.68913C2.99524 3.68913 3.29899 3.75626 3.62332 3.89034C3.94739 4.02461 4.14599 4.09155 4.21919 4.09155C4.40176 4.09155 4.55484 3.97083 4.67812 3.7294C4.80145 3.48791 4.86311 3.27617 4.86311 3.09358C4.86311 2.80388 4.64152 2.55713 4.19869 2.35318C3.91848 2.22437 3.61351 2.13643 3.28411 2.08895V0.74298C3.28411 0.332657 3.00099 0 2.65177 0C2.30255 0 2.01942 0.332657 2.01942 0.74298V2.10579C1.50309 2.20332 1.10341 2.4237 0.821936 2.76812C0.429141 3.24851 0.23287 3.82166 0.23287 4.48762C0.23287 4.96801 0.344572 5.3621 0.568483 5.66992C0.792183 5.97775 1.06377 6.19704 1.38357 6.32759C1.70317 6.45809 2.02057 6.57032 2.33564 6.66407C2.65071 6.75812 2.91997 6.8781 3.14388 7.02424C3.36758 7.17063 3.47949 7.36877 3.47949 7.61929C3.47949 8.0682 3.14215 8.29255 2.46746 8.29255C2.12538 8.29255 1.82983 8.23649 1.58022 8.12406C1.33062 8.01164 1.12966 7.90174 0.977254 7.79414C0.824641 7.68648 0.697513 7.63265 0.596081 7.63265C0.438946 7.63265 0.30028 7.73728 0.18021 7.94649C0.0598868 8.15574 2.21467e-08 8.35691 2.21467e-08 8.55008C-8.45044e-05 8.91501 0.241788 9.23694 0.725956 9.51587C1.0977 9.73009 1.529 9.86179 2.01934 9.91154V11.257C2.01934 11.6673 2.30246 12 2.65168 12C3.0009 12 3.28403 11.6673 3.28403 11.257V9.84768C3.70476 9.74862 4.05542 9.56458 4.33562 9.29499C4.7785 8.86898 5 8.28421 5 7.54103C5.00004 7.1027 4.91932 6.72947 4.75838 6.42159Z"
                                                                fill="#353535"/>
                                                        </svg>
                                                    </div>
                                                    <div class="seller-cab-filter__text">Сумма</div>
                                                    <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="От"
                                                                type="text">
                                                            <div class="help-block"></div>
                                                        </label></div>
                                                    <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="До"
                                                                type="text">
                                                            <div class="help-block"></div>
                                                        </label></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="seller-cab-filter__result">
                                        <div class="seller-cab-filter__result-container">
                                            <div class="seller-cab-filter__result-example">Заказ доставлен<span> </span>
                                            </div>
                                        </div>
                                        <div class="seller-cab-filter__remove-all">Сбросить фильтры</div>
                                    </div>
                                </div>
                            </div>
                            <?php Pjax::begin(['id' => 'products_container', 'timeout' => 10000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'seller-cab-order-goods__body pjax-more-container']]); ?>
                            <div class="seller-cab-order-goods__filter">
                                <div class="seller-cab-order-goods__link">поменять категорию товара</div>
                                <div class="seller-cab-order-goods__link">привязать акцию</div>
                            </div>
                            <ul class="seller-cab-order-goods__table">
                                <li class="seller-cab-order-goods__table_item">
                                    <div class="seller-cab-order-goods__table_value"><?= Yii::t('app', 'Название категории') ?></div>
                                    <div class="seller-cab-order-goods__table_value"><?= Yii::t('app', 'Дата создания') ?></div>
                                    <div class="seller-cab-order-goods__table_value"></div>
                                </li>
                                <?php $widget = ListView::begin([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => '_category',
                                    'options' => [
                                        'tag' => false,
                                    ],
                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'layout' => '{items}'
                                ]) ?>

                                <?php $widget->end() ?>
                            </ul>
                            <div class="paginations">
                                <div class="paginations__page"><?= $widget->renderSummary() ?></div>

                                <?php if ($dataProvider->totalCount > 12): ?>
                                    <a class="more-link hover-button more-products-button" href="#">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} записей', 12) ?></span></div>
                                    </a>
                                <?php endif; ?>

                                <ul class="paginations__menu">
                                    <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                    <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                                </ul>
                            </div>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
