<?php

/* @var $this \yii\web\View */

use frontend\components\Helper;
use yii\helpers\Url;

/* @var $model \common\models\Categories */

?>

<li class="seller-cab-order-goods__table_item">
    <div class="product-seller-cab-order-goods">
        <label class="product-seller-cab-order-goods__lable">
            <div class="seller-cab-order-goods__table_value">
                <input class="product-seller-cab-order-goods__input" type="checkbox">
                <div class="product-seller-cab-order-goods__fake">
                    <svg fill="none" height="8"
                         viewBox="0 0 9 8" width="9"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                            fill="#FFF" stroke-width="0.2"/>
                    </svg>
                </div>
                <div class="product-seller-cab-order-goods__cod"><?= $model->info->name ?></div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__sold"><?= date('d.m.Y H:i:s', $model->created_at) ?></div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__actions">
                    <span><?= Yii::t('app', 'Действия') ?>
                        <i class="fas fa-chevron-up"></i>
                    </span>
                    <div class="product-seller-cab-order-goods__actions_menu">
                        <div class="product-seller-cab-order-goods__actions_body">
                            <a href="<?= Url::to(['/admin/category/update', 'id' => $model->id]) ?>" class="product-seller-cab-order-goods__actions_item">
                                <?= Yii::t('app', 'Редактировать') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </label>
    </div>
</li>
