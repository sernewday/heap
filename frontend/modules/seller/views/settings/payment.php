<?php

/* @var $this \yii\web\View */

/* @var $model \frontend\modules\seller\models\SettingsPaymentForm */

use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;

$this->title = Yii::t('app', 'Оплата');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/seller/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Настройки'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="seller-cab-payment">
                            <div class="title">
                                <div class="title__line"></div>
                                <div class="title__text"><?= Yii::t('app', 'Оплата') ?><span></span></div>
                            </div>
                            <?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab-payment__body']]) ?>
                            <div class="seller-cab-payment__content">
                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input checked class="radio-button__input" name="" type="radio"><span
                                                class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text">
                                                <?= Yii::t('app', 'Наложенный платеж') ?>
                                                <div class="more-information">
                                                    <span>?</span>
                                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora distinctio! Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam omnis dolorum voluptates. Distinctio doloremque veniam optio aut.
                                                    </p>
                                                </div>
                                            </span>
                                    </label>
                                </div>
                                <div class="seller-cab-payment__text">Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                    ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                    ut aliquip ex ea commodo consequat.
                                </div>
                                <div class="seller-cab-payment__row">
                                    <?= $form->field($model, 'np_name')->textInput() ?>
                                </div>
                                <div class="seller-cab-payment__row">
                                    <div class="seller-cab-payment__textarea">
                                        <?= $form->field($model, 'np_description')->textarea() ?>
                                    </div>
                                </div>
                            </div>
                            <div class="seller-cab-payment__content">
                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input checked class="radio-button__input" name="" type="radio"><span
                                                class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text">
                                            <?= Yii::t('app', 'Перевод с карты на карту') ?>
                                            <div class="more-information">
                                                <span>?</span>
                                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora distinctio! Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam omnis dolorum voluptates. Distinctio doloremque veniam optio aut.</p>
                                            </div>
                                        </span>
                                    </label>
                                </div>
                                <div class="seller-cab-payment__text">Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                    ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                    ut aliquip ex ea commodo consequat. <a href="#">Подробнее</a></div>
                                <div class="seller-cab-payment__row">
                                    <?= $form->field($model, 'transfer_name')->textInput() ?>
                                </div>
                                <div class="seller-cab-payment__row full">
                                    <?= $form->field($model, 'transfer_card')->textInput() ?>
                                    <div class="seller-cab-payment__description">
                                        <?= Yii::t('app', 'Укажите номер карты, которую получит клиент после оформления заказа.') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="seller-cab-payment__content">
                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input checked class="radio-button__input" name="" type="radio">
                                        <span class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text">Wayforpay</span>
                                    </label>
                                </div>
                                <div class="seller-cab-payment__text">Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                    ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                    ut aliquip ex ea commodo consequat, nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea commodo consequat. nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea co...
                                </div>
                                <div class="seller-cab-payment__row">
                                    <?= $form->field($model, 'wayforpay_name')->textInput() ?>
                                </div>
                                <div class="seller-cab-payment__row">
                                    <?= $form->field($model, 'merchant_name')->textInput() ?>
                                    <?= $form->field($model, 'merchant_secret')->textInput() ?>
                                </div>
                                <div class="seller-cab-payment__row">
                                    <div class="seller-cab-payment__textarea">
                                        <?= $form->field($model, 'wayforpay_description')->textarea() ?>
                                    </div>
                                </div>
                                <div class="seller-cab-payment__row">
                                    <button type="submit" class="checkout-page-btn__button hover-button">
                                        <div class="hover-button__front"><span><?= Yii::t('app', 'Сохранить') ?></span></div>
                                        <div class="hover-button__back"><span><?= Yii::t('app', 'Сохранить') ?></span></div>
                                    </button>
                                </div>
                            </div>
                            <?php CustomActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
