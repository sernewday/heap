<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\seller\models\SettingsDocumentsForm */
/* @var $model_upload \common\models\UploadForm */

use common\models\CompanyDocuments;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Регистрационные документы');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/seller/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Настройки'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$this->registerJsFile('/js/image_thumb.js', ['depends' => ['frontend\assets\AppAsset']]);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab-documents', 'enctype' => 'multipart/form-data']]) ?>
                        <div class="title">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Yii::t('app', 'Регистрационные документы') ?><span></span></div>
                        </div>
                        <div class="seller-cab-documents__body">
                            <div class="seller-cab-documents__content">
                                <div class="title-seller-cab"><?= Yii::t('app', 'Общая информация') ?></div>
                                <div class="seller-cab-documents__row">
                                    <div class="radio-button" id="jur">
                                        <label class="radio-button__body">
                                            <input class="radio-button__input" name="<?= Html::getInputName($model, 'company_type') ?>" type="radio" value="<?= CompanyDocuments::TYPE_ENTITY ?>" <?= $model->company_type == CompanyDocuments::TYPE_ENTITY ? 'checked' : '' ?>>
                                            <span class="radio-button__fake"><span></span></span>
                                            <span class="radio-button__text"><?= Yii::t('app', 'Юридическое лицо') ?></span>
                                        </label>
                                    </div>
                                    <div class="radio-button" id="phy">
                                        <label class="radio-button__body">
                                            <input class="radio-button__input" name="<?= Html::getInputName($model, 'company_type') ?>" type="radio" value="<?= CompanyDocuments::TYPE_INDIVIDUAL ?>" <?= $model->company_type == CompanyDocuments::TYPE_INDIVIDUAL ? 'checked' : '' ?>>
                                            <span class="radio-button__fake"><span></span></span>
                                            <span class="radio-button__text"><?= Yii::t('app', 'Физическое лицо-предприниматель') ?></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="seller-cab-documents__phy <?= $model->company_type == CompanyDocuments::TYPE_INDIVIDUAL ? 'active' : '' ?>">
                                    <div class="seller-cab-documents__statys">
                                        <p>Статус:</p>
                                        <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.0924 5.07074L10.0924 5.07072L10.0906 5.07288L6.33876 9.52422L4.91089 7.98421C4.8147 7.87541 4.66142 7.80196 4.50211 7.7894C4.33659 7.77635 4.15419 7.82859 4.0163 7.98591C3.74984 8.28992 3.84375 8.6768 4.01624 8.87384L4.01624 8.87384L5.86235 10.9826C5.86236 10.9826 5.86236 10.9826 5.86237 10.9826C5.97223 11.1081 6.15932 11.15 6.30999 11.15C6.46067 11.15 6.64773 11.108 6.75755 10.9826L6.75758 10.9826L6.75954 10.9803L10.9899 5.94479C11.1639 5.74483 11.2403 5.35801 10.9888 5.07075C10.8332 4.89299 10.6509 4.83076 10.4765 4.855C10.3125 4.87779 10.1773 4.97384 10.0924 5.07074Z"
                                                  fill="#35CB44" stroke="#35CB44" stroke-width="0.3"/>
                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                  stroke="#35CB44" stroke-linecap="round" stroke-linejoin="bevel"
                                                  stroke-width="1.4"/>
                                        </svg>
                                        <span>Документы успешно прошли проверку.</span>
                                    </div>
                                    <div class="seller-cab-documents__row">
                                        <?= $form->field($model, 'full_name')->textInput() ?>
                                        <?= $form->field($model, 'card_num')->textInput() ?>
                                    </div>
                                    <label class="seller-cab-documents__foto">
                                        <input type="file" name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>" class="image-product-thumb">
                                        <div class="seller-cab-documents__text upp">
                                            <?= Yii::t('app', 'Скан/фото выписки из ЕГР или другого документа') ?>
                                        </div>
                                        <div class="seller-cab-documents__add-foto">
                                            <div class="seller-cab-documents__cross"></div>
                                        </div>
                                        <div class="seller-cab-documents__text">
                                            - <?= Yii::t('app', 'Сканы документов должны быть цветными копиями оригиналов документов') ?>
                                        </div>
                                        <div class="seller-cab-documents__text">- <?= Yii::t('app', 'Форматы') ?> JPG, GIF, PNG</div>
                                        <div class="seller-cab-documents__text">- <?= Yii::t('app', 'Максимальный размер') ?>: 10 Мб.</div>
                                    </label>
                                </div>
                                <div class="seller-cab-documents__jur <?= $model->company_type == CompanyDocuments::TYPE_ENTITY ? 'active' : '' ?>">
                                    <div class="seller-cab-documents__statys">
                                        <p>Статус:</p>
                                        <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.0924 5.07074L10.0924 5.07072L10.0906 5.07288L6.33876 9.52422L4.91089 7.98421C4.8147 7.87541 4.66142 7.80196 4.50211 7.7894C4.33659 7.77635 4.15419 7.82859 4.0163 7.98591C3.74984 8.28992 3.84375 8.6768 4.01624 8.87384L4.01624 8.87384L5.86235 10.9826C5.86236 10.9826 5.86236 10.9826 5.86237 10.9826C5.97223 11.1081 6.15932 11.15 6.30999 11.15C6.46067 11.15 6.64773 11.108 6.75755 10.9826L6.75758 10.9826L6.75954 10.9803L10.9899 5.94479C11.1639 5.74483 11.2403 5.35801 10.9888 5.07075C10.8332 4.89299 10.6509 4.83076 10.4765 4.855C10.3125 4.87779 10.1773 4.97384 10.0924 5.07074Z"
                                                  fill="#35CB44" stroke="#35CB44" stroke-width="0.3"/>
                                            <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                  stroke="#35CB44" stroke-linecap="round" stroke-linejoin="bevel"
                                                  stroke-width="1.4"/>
                                        </svg>
                                        <span>Документы успешно прошли проверку.</span>
                                    </div>
                                    <div class="seller-cab-documents__row">
                                        <?= $form->field($model, 'organization_form')->dropDownList(CompanyDocuments::$organization_forms) ?>
                                    </div>
                                    <div class="seller-cab-documents__row">
                                        <?= $form->field($model, 'lawyer_name')->textInput() ?>
                                        <?= $form->field($model, 'lawyer_id')->textInput() ?>
                                    </div>
                                    <label class="seller-cab-documents__foto">
                                        <input type="file" name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>" class="image-product-thumb">
                                        <div class="seller-cab-documents__text upp">
                                            <?= Yii::t('app', 'Скан/фото выписки из ЕГР или другого документа') ?>
                                        </div>
                                        <div class="seller-cab-documents__add-foto">
                                            <div class="seller-cab-documents__cross"></div>
                                        </div>
                                        <div class="seller-cab-documents__text">
                                            - <?= Yii::t('app', 'Сканы документов должны быть цветными копиями оригиналов документов') ?>
                                        </div>
                                        <div class="seller-cab-documents__text">- <?= Yii::t('app', 'Форматы') ?> JPG, GIF, PNG</div>
                                        <div class="seller-cab-documents__text">- <?= Yii::t('app', 'Максимальный размер') ?>: 10 Мб.</div>
                                    </label>
                                </div>
                                <div class="seller-cab-documents__row">
                                    <div class="radio-button">
                                        <label class="radio-button__body">
                                            <input checked class="radio-button__input" name="<?= Html::getInputName($model, 'pay_type') ?>" type="radio" value="<?= CompanyDocuments::TYPE_ENTITY ?>" <?= $model->pay_type == CompanyDocuments::PAY_ENTITY ? 'checked' : '' ?>>
                                            <span class="radio-button__fake"><span></span></span>
                                            <span class="radio-button__text"><?= Yii::t('app', 'Плательщик налога на прибыль') ?></span>
                                        </label>
                                    </div>
                                    <div class="radio-button">
                                        <label class="radio-button__body">
                                            <input class="radio-button__input" name="<?= Html::getInputName($model, 'pay_type') ?>" type="radio" value="<?= CompanyDocuments::PAY_INDIVIDUAL ?>" <?= $model->pay_type == CompanyDocuments::PAY_INDIVIDUAL ? 'checked' : '' ?>>
                                            <span class="radio-button__fake"><span></span></span>
                                            <span class="radio-button__text"><?= Yii::t('app', 'Физическое лицо-предприниматель') ?></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="seller-cab-documents__row">
                                    <?= $form->field($model, 'address')->textInput() ?>
                                    <?= $form->field($model, 'phone')->textInput() ?>
                                </div>
                                <div class="seller-cab-documents__row">
                                    <div class="checkbox">
                                        <label class="checkbox__body">
                                            <input class="checkbox__input" type="checkbox" name="<?= Html::getInputName($model, 'send_bill') ?>" <?= $model->send_bill == 1 ? 'checked' : '' ?>>
                                            <span class="checkbox__fake">
                                                <svg fill="none" height="8" viewBox="0 0 9 8"
                                                                                width="9"
                                                                                xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg>
                                            </span>
                                            <span class="checkbox__text"><?= Yii::t('app', 'Отсылать счёт на оплату') ?></span>
                                        </label>
                                    </div>
                                    <div class="seller-cab-documents__descriptions">
                                        <?= Yii::t('app', 'При совершении заказа покупатели получат на свой e-mail счёт на оплату в формате PDF.') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="seller-cab-documents__content">
                                <div class="title-seller-cab"><?= Yii::t('app', 'Реквизиты компании') ?></div>
                                <div class="seller-cab-documents__row">
                                    <?= $form->field($model, 'company_name')->textInput() ?>

                                    <?= $form->field($model, 'account_number')->textInput() ?>

                                    <?= $form->field($model, 'bank_name')->textInput() ?>

                                    <?= $form->field($model, 'bank_mfo')->textInput() ?>
                                </div>
                            </div>
                            <div class="seller-cab-documents__content">
                                <div class="title-seller-cab"><?= Yii::t('app', 'Адрес доставки документов') ?></div>
                                <div class="seller-cab-documents__row">

                                    <?= $form->field($model, 'documents_region')->textInput() ?>

                                    <?= $form->field($model, 'documents_index')->textInput() ?>

                                    <?= $form->field($model, 'documents_city')->textInput() ?>

                                    <?= $form->field($model, 'documents_address')->textInput() ?>
                                </div>

                                <button type="submit" class="seller-cab-documents__btn hover-button">
                                    <div class="hover-button__front">
                                        <span><?= Yii::t('app', 'Сохранить изменения') ?></span>
                                    </div>
                                    <div class="hover-button__back">
                                        <span><?= Yii::t('app', 'Сохранить изменения') ?></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <?php CustomActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
