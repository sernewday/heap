<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\seller\models\SettingsCompanyForm */
/* @var $model_upload \common\models\UploadForm */

use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Компания');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/seller/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Настройки'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$this->registerJsFile('/js/settings_company.js', ['depends' => ['frontend\assets\AppAsset']]);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab__content', 'enctype' => 'multipart/form-data']]) ?>
                    <div class="seller-cab-company">
                        <div class="title">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Yii::t('app', 'Компания') ?><span></span></div>
                        </div>
                        <div class="seller-cab-company__body">
                            <div class="seller-cab-company__content">
                                <div class="seller-cab-company__container">
                                    <div class="title-seller-cab">Аккаунт</div>
                                    <div class="seller-cab-company__row">

                                        <?= $form->field($model, 'login')->textInput() ?>

                                        <?= $form->field($model, 'new_pass')->textInput() ?>

                                    </div>
                                </div>
                                <div class="seller-cab-company__container">
                                    <div class="title-seller-cab"><?= Yii::t('app', 'Информация о компании') ?></div>
                                    <div class="seller-cab-company__row">
                                        <?= $form->field($model, 'name')->textInput() ?>

                                        <?= $form->field($model, 'contact_person')->textInput() ?>

                                        <div class="seller-cab-company__textarea">
                                            <?= $form->field($model, 'description')->textarea() ?>
                                        </div>
                                    </div>
                                    <div class="seller-cab-company__row">
                                        <?= $form->field($model, 'email')->textInput() ?>
                                    </div>
                                    <div class="seller-cab-company__row">
                                        <?= $form->field($model, 'phones[]')->textInput() ?>

                                        <?= $form->field($model, 'contact_persons[]')->textInput() ?>

                                        <div class="add-button">
                                            <div class="add-button__body">
                                                <div class="add-button__cross"></div>
                                                <div class="add-button__text"><?= Yii::t('app', 'Добавить еще') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="seller-cab-company__row">
                                        <?= $form->field($model, 'contact_types[]')->dropDownList([
                                                'site' => Yii::t('app', 'Сайт компании'),
                                        ]) ?>

                                        <?= $form->field($model, 'links[]')->textInput() ?>

                                        <div class="add-button">
                                            <div class="add-button__body">
                                                <div class="add-button__cross"></div>
                                                <div class="add-button__text"><?= Yii::t('app', 'Добавить еще вид связи') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="seller-cab-company__container">
                                    <div class="title-seller-cab"><?= Yii::t('app', 'Место расположения') ?></div>
                                    <div class="seller-cab-company__row full">
                                        <?= $form->field($model, 'country')->textInput() ?>

                                        <?= $form->field($model, 'city')->textInput() ?>

                                        <?= $form->field($model, 'address')->textInput() ?>
                                    </div>
                                </div>
                            </div>
                            <div class="seller-cab-company-cart">
                                <div class="seller-cab-company-cart__body">
                                    <div class="seller-cab-company-cart__title"><?= Yii::t('app', 'Компания') ?></div>
                                    <div class="seller-cab-company-cart__info">
                                        <p><?= Yii::t('app', 'Создано') ?>:</p>
                                        <span><?= date('d.m.Y', $model->_brand->creation_time) ?></span>
                                        <span><?= date('H:i', $model->_brand->creation_time) ?></span>
                                    </div>

                                    <?php if ($model->_brand->creation_time != $model->_brand->update_time): ?>
                                        <div class="seller-cab-company-cart__info">
                                            <p><?= Yii::t('app', 'Изменено') ?>:</p>
                                            <span><?= date('d.m.Y', $model->_brand->update_time) ?></span>
                                            <span><?= date('H:i', $model->_brand->update_time) ?></span>
                                        </div>
                                    <?php endif; ?>

                                    <div class="seller-cab-company-cart__img">
                                        <div class="seller-cab-company-cart__title">Логотип</div>
                                        <label class="seller-cab-company-cart__add-img">
                                            <input type="file" name="<?= Html::getInputName($model_upload, 'imageFiles[]') ?>" class="image-product-thumb">
                                            <?php if ($model->_brand->image): ?>
                                                <img alt="brand_image" src="<?= $model->_brand->image->imageUrl ?>" data-key="<?= $model->_brand->image->id ?>">
                                            <?php endif; ?>
                                            <div class="seller-cab-product-editing-cart__close"></div>
                                            <div class="seller-cab-company-cart__cross" style="<?= $model->_brand->image ? 'z-index: -1;' : '' ?>"></div>
                                        </label>
                                        <div class="seller-cab-company-cart__text">
                                            <p>Формат: JPG, GIF, PNG. </p>
                                            <p><?= Yii::t('app', 'Рекомендуемый размер') ?>: 100 х 100 <?= Yii::t('app', 'пикселей') ?>, до 2 МБ</p>
                                        </div>
                                    </div>
                                    <div class="seller-cab-company-cart__btn">
                                        <a href="<?= Url::current() ?>" class="seller-cab-company-cart__cancel hover-button">
                                            <div class="hover-button__front"><?= Yii::t('app', 'Отменить все изменения') ?></div>
                                            <div class="hover-button__back"><?= Yii::t('app', 'Отменить все изменения') ?></div>
                                        </a>
                                        <button type="submit" class="seller-cab-company-cart__save hover-button">
                                            <div class="hover-button__front"><?= Yii::t('app', 'Сохранить изменения') ?></div>
                                            <div class="hover-button__back"><?= Yii::t('app', 'Сохранить изменения') ?></div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php CustomActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</main>
