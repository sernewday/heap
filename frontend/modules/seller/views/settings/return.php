<?php

/* @var $this \yii\web\View */

use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;

/* @var $model \frontend\modules\seller\models\SettingsReturnForm */

$this->title = Yii::t('app', 'Данные для возврата');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/seller/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Настройки'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab-return']]) ?>
                        <div class="title">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Yii::t('app', 'Данные для возврата') ?><span></span></div>
                        </div>
                        <div class="seller-cab-return__body">
                            <div class="seller-cab-return__warning">
                                <div>i</div>
                                <span><?= Yii::t('app', 'Укажите данные, на которые будут возвращать товар.') ?></span>
                            </div>
                            <div class="seller-cab-return__container">
                                <div class="title-seller-cab"><?= Yii::t('app', 'Личные данные') ?></div>
                                <div class="seller-cab-return__row">
                                    <?= $form->field($model, 'return_name')->textInput() ?>

                                    <?= $form->field($model, 'return_phone')->textInput() ?>

                                    <?= $form->field($model, 'return_email')->textInput() ?>
                                </div>
                            </div>
                            <div class="seller-cab-return__container">
                                <div class="title-seller-cab"><?= Yii::t('app', 'Доставка в отделение Новой почты') ?></div>
                                <div class="seller-cab-return__row">
                                    <?= $form->field($model, 'return_city')->textInput() ?>

                                    <?= $form->field($model, 'return_np')->textInput() ?>
                                </div>
                                <div class="seller-cab-return__row">
                                    <div class="seller-cab-return__textarea">
                                        <?= $form->field($model, 'return_comment')->textarea() ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="seller-cab-return__btn hover-button">
                                <div class="hover-button__front"><?= Yii::t('app', 'Сохранить') ?></div>
                                <div class="hover-button__back"><?= Yii::t('app', 'Сохранить') ?></div>
                            </button>
                        </div>
                        <?php CustomActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
