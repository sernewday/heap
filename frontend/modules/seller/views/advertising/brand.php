<?php

/* @var $this View */

/* @var $model AdvertisingBrandForm */

use common\models\TopBrands;
use frontend\modules\seller\models\AdvertisingBrandForm;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Добавить в топ брендов');

$this->registerJsFile('/js/top_brands_price.js', ['depends' => ['frontend\assets\AppAsset']]);
$this->registerJsVar('price_for_days', TopBrands::$pricesOneDay);

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/seller/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Реклама'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$days = [];
for ($i = 5; $i <= 30; $i += 5) {
    $days[$i] = $i . ' дней';
}
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab seller-cab-main">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <?php $form = CustomActiveForm::begin(['options' => ['class' => 'seller-cab-adv-logo']]) ?>
                        <div class="title">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Yii::t('app', 'Добавить в топ брендов') ?><span></span></div>
                        </div>
                        <div class="seller-cab-adv-logo__body">

                            <?= $form->field($model, 'days')->dropDownList($days) ?>

                            <?= $form->field($model, 'type')->dropDownList([
                                TopBrands::TYPE_MAIN => 'На главной странице',
                                TopBrands::TYPE_TOP30 => 'Топ 30 брендов',
                            ]) ?>

                            <div class="seller-cab-adv-logo__cost">
                                <p><?= Yii::t('app', 'Стоимость') ?>:</p>
                                <span>
                                    2 397
                                    <span>грн</span>
                                </span>
                            </div>

                            <button type="submit" class="seller-cab-adv-logo__btn hover-button">
                                <div class="hover-button__front"><?= Yii::t('app', 'Разместить') ?></div>
                                <div class="hover-button__back"><?= Yii::t('app', 'Разместить') ?></div>
                            </button>

                        </div>
                        <?php CustomActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
