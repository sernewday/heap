<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\Products */
/* @var $input_name string */

?>

<li class="seller-cab-order-goods__table_item">
    <div class="product-seller-cab-order-goods">
        <label class="product-seller-cab-order-goods__lable">
            <div class="seller-cab-order-goods__table_value">
                <input class="product-seller-cab-order-goods__input checkbox__product"
                       type="checkbox" name="<?= $input_name ?>" value="<?= $model->id ?>">
                <div class="product-seller-cab-order-goods__fake">
                    <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                              fill="#FFF" stroke-width="0.2"/>
                    </svg>
                </div>
                <div class="product-seller-cab-order-goods__cod">
                    №<?= $model->id ?>
                </div>
            </div>
            <a class="seller-cab-order-goods__table_value" href="#">
                <div class="product-seller-cab-order-goods__img">
                    <img alt="prod image" src="<?= $model->image->imageUrl ?>">
                </div>
                <div class="product-seller-cab-order-goods__info">
                    <div class="product-seller-cab-order-goods__name">
                        <?= $model->info->name ?>
                    </div>
                    <div class="product-seller-cab-order-goods__more">Еще 5
                        разновидностей<i class="fas fa-chevron-right"></i></div>
                </div>
            </a>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__categori">
                    <?= $model->category->info->name ?>
                </div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__cost">
                    <p><?= $model->price ?> грн</p>
                </div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__statys">
                    <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                              fill="#35CB44" stroke="#35CB44"
                              stroke-width="0.2"/>
                        <path d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                              stroke="#35CB44" stroke-linecap="round"
                              stroke-linejoin="bevel" stroke-width="1.1"/>
                    </svg>
                </div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__sold">2</div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__add"><?= Yii::t('app', 'Добавить') ?></div>
            </div>
        </label>
    </div>
</li>
