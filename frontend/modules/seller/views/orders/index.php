<?php

/* @var $this \yii\web\View */

use frontend\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Заказы');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Заказы'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Все заказы'), 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <?php Pjax::begin(['id' => 'orders_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'seller-cab__content']]); ?>

                    <div class="seller-cab__content">
                        <div class="seller-cab__title">
                            <div class="title">
                                <div class="title__line"></div>
                                <div class="title__text">Все заказы<span>(60)</span></div>
                            </div>
                            <div class="header-search">
                                <div class="input-block input-search"><input
                                            placeholder="Поиск по номеру заказа или номеру телефона покупателя"
                                            type="text"><img
                                            alt="" src="./assets/img/header/search.png"></div>
                                <div class="header-search__result"></div>
                            </div>
                        </div>
                        <div class="seller-cab-title">
                            <div class="seller-cab-title__body">
                                <div class="seller-cab-title__time"><img alt="" src="./assets/img/time.png">
                                    <span>Заказы за неделю</span></div>
                                <div class="seller-cab-title__content">
                                    <div class="seller-cab-title__text">
                                        <p>К-во заказов</p><span>20</span>
                                    </div>
                                    <div class="seller-cab-title__text">
                                        <p>Общие начисления:</p><span>799 грн</span>
                                    </div>
                                    <div class="seller-cab-title__text">
                                        <p>Общее списание:</p><span>20 грн</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="seller-cab-filter-container">
                            <div class="seller-cab-filter-body">
                                <div class="seller-cab-filter__filter">
                                    <div class="seller-cab-filter__filter-container">
                                        <div class="seller-cab-filter">
                                            <div class="seller-cab-filter__content">
                                                <div class="seller-cab-filter__img">
                                                    <svg fill="none" height="12" viewBox="0 0 12 12"
                                                         width="12" xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                                d="M3.54617 6.00053C3.54617 5.6993 3.30195 5.45508 3.00071 5.45508H0.818892C0.517656 5.45508 0.273438 5.6993 0.273438 6.00053C0.273438 6.30177 0.517656 6.54599 0.818892 6.54599H3.00071C3.30195 6.54599 3.54617 6.30177 3.54617 6.00053Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M11.1832 5.45508H10.0923C9.79109 5.45508 9.54688 5.6993 9.54688 6.00053C9.54688 6.30177 9.79109 6.54599 10.0923 6.54599H11.1832C11.4845 6.54599 11.7287 6.30177 11.7287 6.00053C11.7287 5.6993 11.4845 5.45508 11.1832 5.45508Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M6.27592 3.27273C6.57716 3.27273 6.82138 3.02851 6.82138 2.72727V0.545455C6.82138 0.244218 6.57716 0 6.27592 0C5.97469 0 5.73047 0.244218 5.73047 0.545455V2.72727C5.73047 3.02851 5.97469 3.27273 6.27592 3.27273Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M6.27202 8.72656C5.97078 8.72656 5.72656 8.97078 5.72656 9.27202V11.4538C5.72656 11.7551 5.97078 11.9993 6.27202 11.9993C6.57325 11.9993 6.81747 11.7551 6.81747 11.4538V9.27202C6.81747 8.97078 6.57325 8.72656 6.27202 8.72656Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M2.80224 1.75741C2.58929 1.54439 2.24391 1.54442 2.03086 1.75741C1.81784 1.97042 1.81784 2.31577 2.03086 2.52879L3.57366 4.07162C3.68017 4.17813 3.81977 4.23141 3.95933 4.23141C4.09889 4.23141 4.23853 4.17813 4.345 4.07166C4.55802 3.85864 4.55802 3.5133 4.345 3.30028L2.80224 1.75741Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M8.9741 7.92927C8.76112 7.71629 8.41574 7.71629 8.20272 7.92927C7.9897 8.14229 7.98974 8.48763 8.20272 8.70065L9.74556 10.2434C9.85207 10.3499 9.99167 10.4032 10.1313 10.4032C10.2709 10.4032 10.4105 10.3499 10.5169 10.2434C10.73 10.0304 10.73 9.68505 10.5169 9.47203L8.9741 7.92927Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M3.57362 7.9293L2.03086 9.47206C1.81784 9.68508 1.81784 10.0304 2.03086 10.2434C2.13737 10.35 2.27697 10.4032 2.41657 10.4032C2.55617 10.4032 2.69577 10.35 2.80224 10.2434L4.345 8.70068C4.55802 8.48766 4.55802 8.14231 4.345 7.9293C4.13198 7.71628 3.7866 7.71628 3.57362 7.9293Z"
                                                                fill="#353535"/>
                                                    </svg>
                                                </div>
                                                <div class="seller-cab-filter__text">Статус</div>
                                                <i class="fas fa-chevron-up"></i>
                                                <div class="seller-cab-filter__body"></div>
                                            </div>
                                        </div>
                                        <div class="seller-cab-filter">
                                            <div class="seller-cab-filter__content">
                                                <div class="seller-cab-filter__img">
                                                    <svg fill="none" height="11" viewBox="0 0 8 11" width="8"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                                d="M0.571429 11H7.42857C7.74405 11 8 10.7274 8 10.3911V3.04428C8 2.70798 7.74405 2.43542 7.42857 2.43542H5.71429V1.82657C5.71429 0.819419 4.94524 0 4 0C3.05476 0 2.28571 0.819419 2.28571 1.82657V2.43542H0.571429C0.255804 2.43542 0 2.70798 0 3.04428V10.3911C0 10.7274 0.255804 11 0.571429 11V11ZM3.42857 1.82657C3.42857 1.4909 3.68482 1.21771 4 1.21771C4.31503 1.21771 4.57143 1.4909 4.57143 1.82657V2.43542H3.42857V1.82657ZM1.14286 3.65314H2.28571V4.26199C2.28571 4.59829 2.54152 4.87085 2.85714 4.87085C3.17262 4.87085 3.42857 4.59829 3.42857 4.26199V3.65314H4.57143V4.26199C4.57143 4.59829 4.82723 4.87085 5.14286 4.87085C5.45833 4.87085 5.71429 4.59829 5.71429 4.26199V3.65314H6.85714V9.78229H1.14286V3.65314Z"
                                                                fill="#353535"/>
                                                    </svg>
                                                </div>
                                                <div class="seller-cab-filter__text">категория Товара</div>
                                                <i class="fas fa-chevron-up"></i>
                                            </div>
                                            <div class="seller-cab-filter__body"></div>
                                        </div>
                                        <div class="seller-cab-filter">
                                            <div class="seller-cab-filter-input">
                                                <div class="seller-cab-filter__img">
                                                    <svg fill="none" height="12" viewBox="0 0 12 12"
                                                         width="12" xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                                d="M10.125 0.9375H9.51562V0.46875C9.51562 0.209859 9.30577 0 9.04688 0C8.78798 0 8.57812 0.209859 8.57812 0.46875V0.9375H6.44531V0.46875C6.44531 0.209859 6.23545 0 5.97656 0C5.71767 0 5.50781 0.209859 5.50781 0.46875V0.9375H3.39844V0.46875C3.39844 0.209859 3.18858 0 2.92969 0C2.6708 0 2.46094 0.209859 2.46094 0.46875V0.9375H1.875C0.841125 0.9375 0 1.77863 0 2.8125V10.125C0 11.1589 0.841125 12 1.875 12H9.67969C9.93858 12 10.1484 11.7901 10.1484 11.5312C10.1484 11.2724 9.93858 11.0625 9.67969 11.0625H1.875C1.35806 11.0625 0.9375 10.6419 0.9375 10.125V2.8125C0.9375 2.29556 1.35806 1.875 1.875 1.875H2.46094V2.34375C2.46094 2.60264 2.6708 2.8125 2.92969 2.8125C3.18858 2.8125 3.39844 2.60264 3.39844 2.34375V1.875H5.50781V2.34375C5.50781 2.60264 5.71767 2.8125 5.97656 2.8125C6.23545 2.8125 6.44531 2.60264 6.44531 2.34375V1.875H8.57812V2.34375C8.57812 2.60264 8.78798 2.8125 9.04688 2.8125C9.30577 2.8125 9.51562 2.60264 9.51562 2.34375V1.875H10.125C10.6419 1.875 11.0625 2.29556 11.0625 2.8125V9.23438C11.0625 9.49327 11.2724 9.70312 11.5312 9.70312C11.7901 9.70312 12 9.49327 12 9.23438V2.8125C12 1.77863 11.1589 0.9375 10.125 0.9375Z"
                                                                fill="#353535"/>
                                                        <path
                                                                d="M9.05078 5.44063C9.33728 5.44063 9.56953 5.20837 9.56953 4.92188C9.56953 4.63538 9.33728 4.40312 9.05078 4.40312C8.76428 4.40312 8.53203 4.63538 8.53203 4.92188C8.53203 5.20837 8.76428 5.44063 9.05078 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M7.01172 5.44063C7.29822 5.44063 7.53047 5.20837 7.53047 4.92188C7.53047 4.63538 7.29822 4.40312 7.01172 4.40312C6.72522 4.40312 6.49297 4.63538 6.49297 4.92188C6.49297 5.20837 6.72522 5.44063 7.01172 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M4.97266 7.47969C5.25915 7.47969 5.49141 7.24744 5.49141 6.96094C5.49141 6.67444 5.25915 6.44219 4.97266 6.44219C4.68616 6.44219 4.45391 6.67444 4.45391 6.96094C4.45391 7.24744 4.68616 7.47969 4.97266 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M9.05078 7.47969C9.33728 7.47969 9.56953 7.24744 9.56953 6.96094C9.56953 6.67444 9.33728 6.44219 9.05078 6.44219C8.76428 6.44219 8.53203 6.67444 8.53203 6.96094C8.53203 7.24744 8.76428 7.47969 9.05078 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M2.93359 5.44063C3.22009 5.44063 3.45234 5.20837 3.45234 4.92188C3.45234 4.63538 3.22009 4.40312 2.93359 4.40312C2.6471 4.40312 2.41484 4.63538 2.41484 4.92188C2.41484 5.20837 2.6471 5.44063 2.93359 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M2.93359 7.47969C3.22009 7.47969 3.45234 7.24744 3.45234 6.96094C3.45234 6.67444 3.22009 6.44219 2.93359 6.44219C2.6471 6.44219 2.41484 6.67444 2.41484 6.96094C2.41484 7.24744 2.6471 7.47969 2.93359 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M7.01172 7.47969C7.29822 7.47969 7.53047 7.24744 7.53047 6.96094C7.53047 6.67444 7.29822 6.44219 7.01172 6.44219C6.72522 6.44219 6.49297 6.67444 6.49297 6.96094C6.49297 7.24744 6.72522 7.47969 7.01172 7.47969Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M2.93359 9.51875C3.22009 9.51875 3.45234 9.2865 3.45234 9C3.45234 8.7135 3.22009 8.48125 2.93359 8.48125C2.6471 8.48125 2.41484 8.7135 2.41484 9C2.41484 9.2865 2.6471 9.51875 2.93359 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M7.01172 9.51875C7.29822 9.51875 7.53047 9.2865 7.53047 9C7.53047 8.7135 7.29822 8.48125 7.01172 8.48125C6.72522 8.48125 6.49297 8.7135 6.49297 9C6.49297 9.2865 6.72522 9.51875 7.01172 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M4.97266 9.51875C5.25915 9.51875 5.49141 9.2865 5.49141 9C5.49141 8.7135 5.25915 8.48125 4.97266 8.48125C4.68616 8.48125 4.45391 8.7135 4.45391 9C4.45391 9.2865 4.68616 9.51875 4.97266 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M9.05078 9.51875C9.33728 9.51875 9.56953 9.2865 9.56953 9C9.56953 8.7135 9.33728 8.48125 9.05078 8.48125C8.76428 8.48125 8.53203 8.7135 8.53203 9C8.53203 9.2865 8.76428 9.51875 9.05078 9.51875Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                        <path
                                                                d="M4.97266 5.44063C5.25915 5.44063 5.49141 5.20837 5.49141 4.92188C5.49141 4.63538 5.25915 4.40312 4.97266 4.40312C4.68616 4.40312 4.45391 4.63538 4.45391 4.92188C4.45391 5.20837 4.68616 5.44063 4.97266 5.44063Z"
                                                                fill="#353535" stroke="#6D6D6D" stroke-width="0.1"/>
                                                    </svg>
                                                </div>
                                                <div class="seller-cab-filter__text">Дата</div>
                                                <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="От"
                                                                type="text">
                                                        <div class="help-block"></div>
                                                    </label></div>
                                                <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="До"
                                                                type="text">
                                                        <div class="help-block"></div>
                                                    </label></div>
                                            </div>
                                        </div>
                                        <div class="seller-cab-filter">
                                            <div class="seller-cab-filter-input">
                                                <div class="seller-cab-filter__img">
                                                    <svg fill="none" height="12" viewBox="0 0 5 12" width="5"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                                d="M4.75838 6.42159C4.59723 6.11377 4.39792 5.88668 4.16078 5.74034C3.9236 5.5942 3.66617 5.46648 3.38871 5.35678C3.11134 5.24714 2.85146 5.16113 2.60959 5.09851C2.36746 5.03589 2.16439 4.94711 1.99998 4.83215C1.83562 4.71749 1.75342 4.57135 1.75342 4.39377C1.75342 4.21624 1.83562 4.05451 1.99998 3.90841C2.16434 3.76227 2.40174 3.68913 2.71229 3.68913C2.99524 3.68913 3.29899 3.75626 3.62332 3.89034C3.94739 4.02461 4.14599 4.09155 4.21919 4.09155C4.40176 4.09155 4.55484 3.97083 4.67812 3.7294C4.80145 3.48791 4.86311 3.27617 4.86311 3.09358C4.86311 2.80388 4.64152 2.55713 4.19869 2.35318C3.91848 2.22437 3.61351 2.13643 3.28411 2.08895V0.74298C3.28411 0.332657 3.00099 0 2.65177 0C2.30255 0 2.01942 0.332657 2.01942 0.74298V2.10579C1.50309 2.20332 1.10341 2.4237 0.821936 2.76812C0.429141 3.24851 0.23287 3.82166 0.23287 4.48762C0.23287 4.96801 0.344572 5.3621 0.568483 5.66992C0.792183 5.97775 1.06377 6.19704 1.38357 6.32759C1.70317 6.45809 2.02057 6.57032 2.33564 6.66407C2.65071 6.75812 2.91997 6.8781 3.14388 7.02424C3.36758 7.17063 3.47949 7.36877 3.47949 7.61929C3.47949 8.0682 3.14215 8.29255 2.46746 8.29255C2.12538 8.29255 1.82983 8.23649 1.58022 8.12406C1.33062 8.01164 1.12966 7.90174 0.977254 7.79414C0.824641 7.68648 0.697513 7.63265 0.596081 7.63265C0.438946 7.63265 0.30028 7.73728 0.18021 7.94649C0.0598868 8.15574 2.21467e-08 8.35691 2.21467e-08 8.55008C-8.45044e-05 8.91501 0.241788 9.23694 0.725956 9.51587C1.0977 9.73009 1.529 9.86179 2.01934 9.91154V11.257C2.01934 11.6673 2.30246 12 2.65168 12C3.0009 12 3.28403 11.6673 3.28403 11.257V9.84768C3.70476 9.74862 4.05542 9.56458 4.33562 9.29499C4.7785 8.86898 5 8.28421 5 7.54103C5.00004 7.1027 4.91932 6.72947 4.75838 6.42159Z"
                                                                fill="#353535"/>
                                                    </svg>
                                                </div>
                                                <div class="seller-cab-filter__text">Сумма</div>
                                                <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="От"
                                                                type="text">
                                                        <div class="help-block"></div>
                                                    </label></div>
                                                <div class="input-block"><label class="input-block__lable"><span
                                                                class="input-block__title"></span><input
                                                                class="input-block__input" placeholder="До"
                                                                type="text">
                                                        <div class="help-block"></div>
                                                    </label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="seller-cab-filter__result">
                                    <div class="seller-cab-filter__result-container">
                                        <div class="seller-cab-filter__result-example">Заказ доставлен<span> </span>
                                        </div>
                                    </div>
                                    <div class="seller-cab-filter__remove-all">Сбросить фильтры</div>
                                </div>
                            </div>
                        </div>
                        <ul class="seller-cab-table-product">
                            <li class="seller-cab-table-product__item">
                                <div class="seller-cab-table-product__value">номер заказа</div>
                                <div class="seller-cab-table-product__value">дата заказа</div>
                                <div class="seller-cab-table-product__value">товары</div>
                                <div class="seller-cab-table-product__value">сумма заказа</div>
                                <div class="seller-cab-table-product__value">покупатель</div>
                            </li>
                            <?php $widget = ListView::begin([
                                'dataProvider' => $dataProvider,
                                'itemView' => '_order',
                                'options' => [
                                    'tag' => false,
                                ],
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'layout' => '{items}'
                            ]) ?>

                            <?php $widget->end() ?>
                        </ul>
                        <div class="paginations">
                            <div class="paginations__page"><?= $widget->renderSummary() ?></div>
                            <a
                                    class="more-link hover-button" href="#">
                                <div class="hover-button__front"><span>Еще 30 заказов</span></div>
                                <div class="hover-button__back"><span>Еще 30 заказов</span></div>
                            </a>
                            <ul class="paginations__menu">
                                <li class="paginations__item"><a class="paginations__link active" href="#">1</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                <li class="paginations__item more"><a class="paginations__link" href="#">...</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                            </ul>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</main>
