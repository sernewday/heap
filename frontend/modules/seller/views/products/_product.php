<?php

/* @var $this \yii\web\View */

use frontend\components\Helper;
use yii\helpers\Url;

/* @var $model \common\models\Products */

?>

<li class="seller-cab-order-goods__table_item">
    <div class="product-seller-cab-order-goods">
        <label class="product-seller-cab-order-goods__lable">
            <div class="seller-cab-order-goods__table_value">
                <input class="product-seller-cab-order-goods__input" type="checkbox">
                <div class="product-seller-cab-order-goods__fake">
                    <svg fill="none" height="8"
                         viewBox="0 0 9 8" width="9"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                            fill="#FFF" stroke-width="0.2"/>
                    </svg>
                </div>
                <div class="product-seller-cab-order-goods__cod">№ <?= Helper::formatNumber($model->id) ?></div>
            </div>
            <a class="seller-cab-order-goods__table_value" href="<?= Url::to(['/products/view', 'id' => $model->id]) ?>">
                <div class="product-seller-cab-order-goods__img">
                    <img alt="<?= $model->image->imageUrl ?>" src="<?= $model->image->imageUrl ?>"></div>
                <div class="product-seller-cab-order-goods__info">
                    <div class="product-seller-cab-order-goods__name">
                        <?= $model->info->name ?>
                    </div>
                    <div class="product-seller-cab-order-goods__more">
                        Еще 5 разновидностей
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </div>
            </a>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__categori">
                    <?= $model->category->info->name ?>
                </div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__cost">
                    <p><?= $model->price ?> грн</p>
                    <?php if ($model->discount): ?>
                        <div class="product-seller-cab-order-goods__promo">
                            <svg fill="none" height="11"
                                 viewBox="0 0 14 11" width="14"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M1.09375 0.509554V10.4459C1.09375 10.7273 0.848938 10.9554 0.546875 10.9554C0.244812 10.9554 0 10.7273 0 10.4459V0.509554C0 0.228105 0.244812 0 0.546875 0C0.848938 0 1.09375 0.228105 1.09375 0.509554ZM13.4531 0C13.1511 0 12.9062 0.228105 12.9062 0.509554V9.09554C12.9062 9.37699 13.1511 9.6051 13.4531 9.6051C13.7552 9.6051 14 9.37699 14 9.09554V0.509554C14 0.228105 13.7552 0 13.4531 0ZM11.3203 9.6051C11.6224 9.6051 11.8672 9.37699 11.8672 9.09554V0.509554C11.8672 0.228105 11.6224 0 11.3203 0C11.0182 0 10.7734 0.228105 10.7734 0.509554V9.09554C10.7734 9.37699 11.0182 9.6051 11.3203 9.6051ZM9.16016 9.6051C9.46222 9.6051 9.70703 9.37699 9.70703 9.09554V0.509554C9.70703 0.228105 9.46222 0 9.16016 0C8.85809 0 8.61328 0.228105 8.61328 0.509554V9.09554C8.61328 9.37699 8.85809 9.6051 9.16016 9.6051ZM4.86719 9.93631C4.56512 9.93631 4.32031 10.1644 4.32031 10.4459C4.32031 10.7273 4.56512 10.9554 4.86719 10.9554C5.16925 10.9554 5.41406 10.7273 5.41406 10.4459C5.41406 10.1644 5.16925 9.93631 4.86719 9.93631ZM4.86719 0C4.56512 0 4.32031 0.228105 4.32031 0.509554V9.09554C4.32031 9.37699 4.56512 9.6051 4.86719 9.6051C5.16925 9.6051 5.41406 9.37699 5.41406 9.09554V0.509554C5.41406 0.228105 5.16925 0 4.86719 0ZM2.76172 9.93631C2.45966 9.93631 2.21484 10.1644 2.21484 10.4459C2.21484 10.7273 2.45966 10.9554 2.76172 10.9554C3.06378 10.9554 3.30859 10.7273 3.30859 10.4459C3.30859 10.1644 3.06378 9.93631 2.76172 9.93631ZM2.76172 0C2.45966 0 2.21484 0.228105 2.21484 0.509554V9.09554C2.21484 9.37699 2.45966 9.6051 2.76172 9.6051C3.06378 9.6051 3.30859 9.37699 3.30859 9.09554V0.509554C3.30859 0.228105 3.06378 0 2.76172 0ZM6.97266 0C6.67059 0 6.42578 0.228105 6.42578 0.509554V10.4459C6.42578 10.7273 6.67059 10.9554 6.97266 10.9554C7.27472 10.9554 7.51953 10.7273 7.51953 10.4459V0.509554C7.51953 0.228105 7.27472 0 6.97266 0Z"
                                    fill="#6D6D6D"/>
                                <path
                                    d="M8.61328 10.4459C8.61328 10.1644 8.85809 9.93631 9.16016 9.93631C9.46222 9.93631 9.70703 10.1644 9.70703 10.4459C9.70703 10.7273 9.46222 10.9554 9.16016 10.9554C8.85809 10.9554 8.61328 10.7273 8.61328 10.4459Z"
                                    fill="#6D6D6D"/>
                                <path
                                    d="M10.7734 10.4459C10.7734 10.1644 11.0182 9.93631 11.3203 9.93631C11.6224 9.93631 11.8672 10.1644 11.8672 10.4459C11.8672 10.7273 11.6224 10.9554 11.3203 10.9554C11.0182 10.9554 10.7734 10.7273 10.7734 10.4459Z"
                                    fill="#6D6D6D"/>
                                <path
                                    d="M12.9062 10.4459C12.9062 10.1644 13.1511 9.93631 13.4531 9.93631C13.7552 9.93631 14 10.1644 14 10.4459C14 10.7273 13.7552 10.9554 13.4531 10.9554C13.1511 10.9554 12.9062 10.7273 12.9062 10.4459Z"
                                    fill="#6D6D6D"/>
                            </svg>
                            <span><?= $model->discount ?>%</span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__statys">
                    <svg fill="none" height="16"
                         viewBox="0 0 16 16" width="16"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                            fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                        <path
                            d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                            stroke="#35CB44" stroke-linecap="round"
                            stroke-linejoin="bevel" stroke-width="1.1"/>
                    </svg>
                </div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__sold">2</div>
            </div>
            <div class="seller-cab-order-goods__table_value">
                <div class="product-seller-cab-order-goods__actions">
                    <span><?= Yii::t('app', 'Действия') ?>
                        <i class="fas fa-chevron-up"></i>
                    </span>
                    <div class="product-seller-cab-order-goods__actions_menu">
                        <div class="product-seller-cab-order-goods__actions_body">
                            <a href="<?= Url::to(['/seller/products/update', 'id' => $model->id]) ?>" class="product-seller-cab-order-goods__actions_item">
                                <?= Yii::t('app', 'Редактировать') ?>
                            </a>
                            <div class="product-seller-cab-order-goods__actions_item">
                                <?= Yii::t('app', 'Поднять в начало группы') ?>
                            </div>
                            <div class="product-seller-cab-order-goods__actions_item">
                                <?= Yii::t('app', 'Удалить') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </label></div>
</li>
