<?php

/* @var $this \yii\web\View */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Импорт');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Панель управления'), 'url' => ['/seller/panel/index']];
$breadcrumbs[] = ['label' => Yii::t('app', 'Товары'), 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Импорт'), 'url' => '#'];
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
        <div class="container">
            <div class="seller-cab">
                <div class="seller-cab__body">
                    <?= $this->render('/includes/_menu') ?>
                    <div class="seller-cab__content">
                        <div class="seller-cab-import">
                            <div class="seller-cab-import__title">
                                <div class="title">
                                    <div class="title__line"></div>
                                    <div class="title__text"><?= Yii::t('app', 'Импорт товаров') ?><span></span></div>
                                </div>
                                <a class="seller-cab-import__btn hover-button" href="<?= Url::to(['/seller/products/create']) ?>">
                                    <div class="hover-button__front">
                                        <span></span>
                                        <?= Yii::t('app', 'Добавить товар вручную') ?>
                                    </div>
                                    <div class="hover-button__back">
                                        <span></span>
                                        <?= Yii::t('app', 'Добавить товар вручную') ?>
                                    </div>
                                </a>
                            </div>
                            <div class="seller-cab-import__body">
                                <div class="seller-cab-import__file">
                                    <div class="radio-button" id=""><label class="radio-button__body"><input
                                                checked
                                                class="radio-button__input" name="seller-cab-import-file" type="radio"><span
                                                class="radio-button__fake"><span></span></span><span
                                                class="radio-button__text">Загрузить файл
                          с сервера</span></label></div>
                                    <div class="seller-cab-import__input">
                                        <div class="input-block"><label class="input-block__lable"><span
                                                    class="input-block__title"></span><input class="input-block__input"
                                                                                             placeholder="Вставьте ссылку"
                                                                                             type="text">
                                                <div class="help-block"></div>
                                            </label></div>
                                        <div class="seller-cab-import__input_rem"></div>
                                    </div>
                                    <div class="radio-button" id=""><label class="radio-button__body"><input
                                                class="radio-button__input"
                                                name="seller-cab-import-file" type="radio"><span
                                                class="radio-button__fake"><span></span></span><span
                                                class="radio-button__text">Загрузить файл
                          с компьютера</span></label></div>
                                </div>
                            </div>
                            <div class="seller-cab-import__body">
                                <div class="title-seller-cab"> Настройки импорта</div>
                                <div class="seller-cab-import__setting">
                                    <div class="seller-cab-import__column">
                                        <div class="seller-cab-import__title">Информация для обновления</div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Название</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Код товара</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Цена</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Фото</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Наличие</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Количество</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Описание</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Группа</span></label></div>
                                        <div class="checkbox"><label class="checkbox__body"><input
                                                    class="checkbox__input"
                                                    type="checkbox"><span class="checkbox__fake"><svg fill="none" height="8"
                                                                                                      viewBox="0 0 9 8"
                                                                                                      width="9"
                                                                                                      xmlns="http://www.w3.org/2000/svg">
                              <path
                                  d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
                                  fill="#FFF" stroke-width="0.2"/>
                            </svg></span><span class="checkbox__text">Характеристики</span></label></div>
                                    </div>
                                    <div class="seller-cab-import__column">
                                        <div class="seller-cab-import__title">автоматическое обновление</div>
                                        <div class="radio-button" id=""><label class="radio-button__body"><input
                                                    class="radio-button__input" name="seller-cab-import-setting"
                                                    type="radio"><span
                                                    class="radio-button__fake"><span></span></span><span
                                                    class="radio-button__text">Никогда</span></label></div>
                                        <div class="radio-button" id=""><label class="radio-button__body"><input
                                                    checked class="radio-button__input"
                                                    name="seller-cab-import-setting" type="radio"><span
                                                    class="radio-button__fake"><span></span></span><span
                                                    class="radio-button__text">Раз в
                            день</span></label></div>
                                        <div class="radio-button" id=""><label class="radio-button__body"><input
                                                    class="radio-button__input" name="seller-cab-import-setting"
                                                    type="radio"><span
                                                    class="radio-button__fake"><span></span></span><span
                                                    class="radio-button__text">Раз в
                            месяц</span></label></div>
                                        <div class="radio-button" id=""><label class="radio-button__body"><input
                                                    class="radio-button__input" name="seller-cab-import-setting"
                                                    type="radio"><span
                                                    class="radio-button__fake"><span></span></span><span
                                                    class="radio-button__text">Раз в
                            месяц</span></label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="seller-cab-import__body">
                                <div class="title-seller-cab"> История импорта</div>
                                <div class="seller-cab-import__histori">
                                    <div class="seller-cab-import-histori">
                                        <div class="seller-cab-import-histori__body">
                                            <div class="seller-cab-import-histori__info">
                                                <div class="seller-cab-import-histori__name">Импорт №1324</div>
                                                <div class="seller-cab-import-histori__data">
                                                    <p>21.05.2020</p><span>21:34</span>
                                                </div>
                                            </div>
                                            <div class="seller-cab-import-histori__statys">
                                                <svg fill="none" height="16" viewBox="0 0 16 16"
                                                     width="16" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                        fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                                    <path
                                                        d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                        stroke="#35CB44" stroke-linecap="round" stroke-linejoin="bevel"
                                                        stroke-width="1.1"/>
                                                </svg>
                                                <span>Обработано 13 позиций</span></div>
                                            <div class="seller-cab-import-histori__arrow"><i
                                                    class="fas fa-chevron-up"></i></div>
                                        </div>
                                        <div class="seller-cab-import-histori__more">
                                            <div class="seller-cab-import-histori__info"></div>
                                            <ul class="seller-cab-import-histori__menu">
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Всего позиций:</div>
                                                    <div class="seller-cab-import-histori__value">13</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Позиций с ошибками:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">3</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Импортировано
                                                        позиций:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">13</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Обновлено товаров:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">3</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Не изменилось:</div>
                                                    <div class="seller-cab-import-histori__value">42</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Не найдено в файле:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">0</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Импорт фото:</div>
                                                    <div class="seller-cab-import-histori__value">8</div>
                                                </li>
                                            </ul>
                                            <div class="seller-cab-import-histori__arrow"></div>
                                        </div>
                                    </div>
                                    <div class="seller-cab-import-histori">
                                        <div class="seller-cab-import-histori__body">
                                            <div class="seller-cab-import-histori__info">
                                                <div class="seller-cab-import-histori__name">Импорт №1324</div>
                                                <div class="seller-cab-import-histori__data">
                                                    <p>21.05.2020</p><span>21:34</span>
                                                </div>
                                            </div>
                                            <div class="seller-cab-import-histori__statys">
                                                <svg fill="none" height="16" viewBox="0 0 16 16"
                                                     width="16" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                        fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                                    <path
                                                        d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                        stroke="#35CB44" stroke-linecap="round" stroke-linejoin="bevel"
                                                        stroke-width="1.1"/>
                                                </svg>
                                                <span>Обработано 13 позиций</span></div>
                                            <div class="seller-cab-import-histori__arrow"><i
                                                    class="fas fa-chevron-up"></i></div>
                                        </div>
                                        <div class="seller-cab-import-histori__more">
                                            <div class="seller-cab-import-histori__info"></div>
                                            <ul class="seller-cab-import-histori__menu">
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Всего позиций:</div>
                                                    <div class="seller-cab-import-histori__value">13</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Позиций с ошибками:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">3</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Импортировано
                                                        позиций:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">13</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Обновлено товаров:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">3</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Не изменилось:</div>
                                                    <div class="seller-cab-import-histori__value">42</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Не найдено в файле:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">0</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Импорт фото:</div>
                                                    <div class="seller-cab-import-histori__value">8</div>
                                                </li>
                                            </ul>
                                            <div class="seller-cab-import-histori__arrow"></div>
                                        </div>
                                    </div>
                                    <div class="seller-cab-import-histori active">
                                        <div class="seller-cab-import-histori__body">
                                            <div class="seller-cab-import-histori__info">
                                                <div class="seller-cab-import-histori__name">Импорт №1324</div>
                                                <div class="seller-cab-import-histori__data">
                                                    <p>21.05.2020</p><span>21:34</span>
                                                </div>
                                            </div>
                                            <div class="seller-cab-import-histori__statys">
                                                <svg fill="none" height="16" viewBox="0 0 16 16"
                                                     width="16" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M10.1301 5.10368L10.1301 5.10367L10.1288 5.10511L6.34055 9.59969L4.87382 8.01777C4.78705 7.91936 4.64607 7.8509 4.49818 7.83924C4.34616 7.82726 4.17999 7.875 4.0539 8.01887C3.8079 8.29954 3.89362 8.65787 4.05386 8.84091L4.1291 8.77504L4.05386 8.84091L5.89998 10.9496C5.89998 10.9496 5.89998 10.9496 5.89999 10.9496C5.99584 11.0592 6.16506 11.1 6.30999 11.1C6.45493 11.1 6.62411 11.0591 6.71993 10.9496L6.71995 10.9497L6.72126 10.9481L10.9519 5.91229C11.1131 5.72731 11.1831 5.36856 10.9512 5.10368C10.8055 4.93725 10.6394 4.88284 10.4834 4.90452C10.3343 4.92524 10.2092 5.01324 10.1301 5.10368Z"
                                                        fill="#35CB44" stroke="#35CB44" stroke-width="0.2"/>
                                                    <path
                                                        d="M1 5.375V5.375C1 2.95875 2.95875 1 5.375 1H10C12.7614 1 15 3.23858 15 6V10C15 12.7614 12.7614 15 10 15H5.375C2.95875 15 1 13.0412 1 10.625V10.625"
                                                        stroke="#35CB44" stroke-linecap="round" stroke-linejoin="bevel"
                                                        stroke-width="1.1"/>
                                                </svg>
                                                <span>Обработано 13 позиций</span></div>
                                            <div class="seller-cab-import-histori__arrow"><i
                                                    class="fas fa-chevron-up"></i></div>
                                        </div>
                                        <div class="seller-cab-import-histori__more">
                                            <div class="seller-cab-import-histori__info"></div>
                                            <ul class="seller-cab-import-histori__menu">
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Всего позиций:</div>
                                                    <div class="seller-cab-import-histori__value">13</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Позиций с ошибками:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">3</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Импортировано
                                                        позиций:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">13</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Обновлено товаров:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">3</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Не изменилось:</div>
                                                    <div class="seller-cab-import-histori__value">42</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Не найдено в файле:
                                                    </div>
                                                    <div class="seller-cab-import-histori__value">0</div>
                                                </li>
                                                <li class="seller-cab-import-histori__item">
                                                    <div class="seller-cab-import-histori__value">Импорт фото:</div>
                                                    <div class="seller-cab-import-histori__value">8</div>
                                                </li>
                                            </ul>
                                            <div class="seller-cab-import-histori__arrow"></div>
                                        </div>
                                    </div>
                                </div>
                                <a class="more-link hover-button" href="#">
                                    <div class="hover-button__front"><span>Загрузить еще</span></div>
                                    <div class="hover-button__back"><span>Загрузить еще</span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
