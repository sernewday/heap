<?php


namespace frontend\modules\seller\models;


use common\models\Brands;
use common\models\PaymentSettings;
use frontend\components\Crypt;
use Yii;
use yii\base\Model;

/**
 * Class SettingsPaymentForm
 * @package frontend\modules\seller\models
 */
class SettingsPaymentForm extends Model
{
    public $np_name;
    public $np_description;

    public $transfer_name;
    public $transfer_card;

    public $wayforpay_name;
    public $merchant_name;
    public $merchant_secret;
    public $wayforpay_description;

    /**
     * @var Brands
     */
    private $_brand;

    /**
     * @var int
     */
    private $_uid;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['np_name', 'transfer_name', 'wayforpay_name'], 'required'],
            [['np_name', 'np_description', 'transfer_name', 'transfer_card', 'wayforpay_name', 'merchant_name', 'merchant_secret', 'wayforpay_description'], 'string', 'max' => 255],
            [['transfer_card'], 'match', 'pattern' => '/^\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d$/', 'message' => Yii::t('app', 'Номер карты указан неправильно')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'np_name' => \Yii::t('app', 'Название'),
            'np_description' => \Yii::t('app', 'Детали оплаты (если есть)'),
            'transfer_name' => \Yii::t('app', 'Название'),
            'transfer_card' => \Yii::t('app', 'Номер карты'),
            'wayforpay_name' => \Yii::t('app', 'Название'),
            'merchant_name' => 'Merchant Name',
            'merchant_secret' => 'Merchant Secret',
            'wayforpay_description' => \Yii::t('app', 'Детали оплаты (если есть)'),
        ];
    }

    public function loadData($user_id)
    {
        $this->_uid = $user_id;

        $brand = Brands::findOne(['user_id' => $user_id]);
        if ($brand) {
            $this->_brand = $brand;

            $settings = PaymentSettings::findOne(['seller_id' => $brand->id]);
            if ($settings) {
                $this->setAttributes($settings->getAttributes());

                if ($this->transfer_card)
                    $this->transfer_card = Crypt::decrypt($this->transfer_card);

                if ($this->merchant_name)
                    $this->merchant_name = Crypt::decrypt($this->merchant_name);

                if ($this->merchant_secret)
                    $this->merchant_secret = Crypt::decrypt($this->merchant_secret);

            }
        }

        if (!$this->np_name)
            $this->np_name = 'Наложенный платеж “Новая Почта”';

        if (!$this->transfer_name)
            $this->transfer_name = 'Перевод с карты на карту';

        if (!$this->wayforpay_name)
            $this->wayforpay_name = 'Wayforpay';
    }

    /**
     * @return bool
     */
    public function save()
    {
        $seller_id = null;
        if ($this->_brand)
            $seller_id = $this->_brand->id;
        else {
            $brand = Brands::findOne(['user_id' => $this->_uid]);

            if (!$brand)
                return false;

            $seller_id = $brand->id;
        }

        $payment = PaymentSettings::findOne(['seller_id' => $seller_id]);
        if (!$payment)
            $payment = new PaymentSettings(['seller_id' => $seller_id]);

        $payment->setAttributes($this->getAttributes());

        if ($this->transfer_card)
            $payment->transfer_card = Crypt::encrypt($this->transfer_card);

        if ($this->merchant_name)
            $payment->merchant_name = Crypt::encrypt($this->merchant_name);

        if ($this->merchant_secret)
            $payment->merchant_secret = Crypt::encrypt($this->merchant_secret);

        return $payment->save();
    }
}