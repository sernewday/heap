<?php


namespace frontend\modules\seller\models;


use common\models\Brands;
use common\models\Payments;
use common\models\TopBrands;
use common\models\TopProducts;
use yii\base\Model;

/**
 * Class AdvertisingProductsForm
 * @package frontend\modules\seller\models
 */
class AdvertisingProductsForm extends Model
{
    public $days;
    public $products = [];

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['days', 'products'], 'required'],
            [['days'], 'integer'],
            [['products'], 'each', 'rule' => ['integer']],

            ['days', 'check_days'],

            ['days', 'check_balance'],
        ];
    }

    public function check_days($attribute, $params)
    {
        $days = [];
        for ($i = 5; $i <= 30; $i += 5) {
            $days[] = $i;
        }

        if (!in_array($this->days, $days)) {
            $this->addError($attribute, \Yii::t('app', 'Неверное значение даты'));

            return false;
        }

        return true;
    }

    public function check_balance($attribute, $params)
    {
        $price = count($this->products) * $this->days * TopProducts::$priceForOne;
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            return false;

        if ($brand->balance->balance < $price) {
            $this->addError($attribute, \Yii::t('app', 'Недостаточно средств на счету'));

            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'days' => \Yii::t('app', 'Длительность акции'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            return false;

        $price = count($this->products) * $this->days * TopProducts::$priceForOne;

        foreach ($this->products as $product) {
            $top_product = new TopProducts();
            $top_product->product_id = $product;
            $top_product->due_to = time() + (3600 * 24 * $this->days);
            $top_product->cost = TopProducts::$priceForOne * $this->days;

            $top_product->save();
        }

        $brand->addPayment([
            'payment_type' => Payments::TYPE_ADVERTISING,
            'balance_charge' => $price,
        ]);

        return true;
    }
}