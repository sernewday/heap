<?php


namespace frontend\modules\seller\models;


use common\models\Brands;
use common\models\BrandsInfo;
use common\models\CompanyInfo;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Class SettingsCompanyForm
 * @package frontend\modules\seller\models
 */
class SettingsCompanyForm extends Model
{
    public $login;
    public $new_pass;

    public $name;
    public $contact_person;
    public $description;

    public $email;

    public $phones = [];
    public $contact_persons = [];

    public $contact_types = [];
    public $links = [];

    public $country;
    public $city;
    public $address;

    /**
     * @var Brands
     */
    public $_brand;

    /**
     * @var User
     */
    public $_user;

    /**
     * @var BrandsInfo
     */
    public $_info;

    /**
     * @var CompanyInfo
     */
    public $_company;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['login', 'name', 'contact_person', 'description', 'email', 'country', 'city', 'address'], 'required'],

            [['login', 'name', 'contact_person', 'email', 'country', 'city', 'address', 'new_pass'], 'string', 'max' => 255],

            [['description'], 'string'],

            [['email'], 'email'],

            [['phones'], 'each', 'rule' => ['match', 'pattern' => '/\+380\d\d\d\d\d\d\d\d\d/'], 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],
            [['contact_persons'], 'each', 'rule' => ['string', 'max' => 255]],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Логин'),
            'name' => Yii::t('app', 'Название'),
            'contact_person' => Yii::t('app', 'Контактное лицо'),
            'description' => Yii::t('app', 'О нас'),
            'email' => Yii::t('app', 'E-Mail'),
            'country' => Yii::t('app', 'Страна'),
            'city' => Yii::t('app', 'Город'),
            'address' => Yii::t('app', 'Адрес'),
            'new_pass' => Yii::t('app', 'Пароль'),
            'phones' => Yii::t('app', 'Телефон'),
            'contact_persons' => Yii::t('app', 'ФИО'),
            'contact_types' => Yii::t('app', 'Другие виды связи'),
            'links' => Yii::t('app', 'Ссылка'),
        ];
    }

    public function loadData($user_id, $lang)
    {
        $this->_brand = Brands::findOne(['user_id' => $user_id]);
        $this->_user = User::findOne($user_id);
        $this->_info = BrandsInfo::findOne(['record_id' => $this->_brand->id, 'lang' => $lang]);
        $this->_company = CompanyInfo::findOne(['brand_id' => $this->_brand->id]);

        $this->login = $this->_user->email;

        $this->name = $this->_brand->name;
        $this->description = $this->_info->description;

        if ($this->_company)
            $this->setAttributes($this->_company->getAttributes());
    }

    /**
     * @param int $lang
     * @return bool
     * @throws \yii\base\Exception
     */
    public function save($lang)
    {
        $this->_user->email = $this->login;
        $this->_user->username = $this->login;

        if ($this->new_pass)
            $this->_user->password_hash = Yii::$app->security->generatePasswordHash($this->new_pass);

        $this->_brand->name = $this->name;

        if (!$this->_company)
            $this->_company = new CompanyInfo(['brand_id' => $this->_brand->id]);

        $this->_company->setAttributes($this->getAttributes());

        if (!$this->_info)
            $this->_info = new BrandsInfo(['record_id' => $this->_brand->id, 'lang' => $lang]);

        $this->_info->description = $this->description;

        return $this->_user->save() && $this->_brand->save() && $this->_company->save() && $this->_info->save();
    }
}