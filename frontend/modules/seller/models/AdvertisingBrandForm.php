<?php


namespace frontend\modules\seller\models;


use common\models\Brands;
use common\models\Payments;
use common\models\TopBrands;
use yii\base\Model;

/**
 * Class AdvertisingBrandForm
 * @package frontend\modules\seller\models
 */
class AdvertisingBrandForm extends Model
{
    public $days;
    public $type;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['days', 'type'], 'required'],

            [['days', 'type'], 'integer'],

            ['days', 'check_days'],

            ['type', 'check_balance'],
        ];
    }

    public function check_days($attribute, $params)
    {
        $days = [];
        for ($i = 5; $i <= 30; $i += 5) {
            $days[] = $i;
        }

        if (!in_array($this->days, $days)) {
            $this->addError($attribute, \Yii::t('app', 'Неверное значение даты'));

            return false;
        }

        return true;
    }

    public function check_balance($attribute, $params)
    {
        $price = TopBrands::$pricesOneDay[$this->type] * $this->days;
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            return false;

        if ($brand->balance->balance < $price) {
            $this->addError($attribute, \Yii::t('app', 'Недостаточно средств на счету'));

            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'days' => \Yii::t('app', 'Длительность акции'),
            'type' => \Yii::t('app', 'Тип размещения'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            return false;

        $top_brand = new TopBrands(['brand_id' => $brand->id]);
        $top_brand->type = $this->type;
        $top_brand->due_to = time() + (3600 * 24 * $this->days);
        $top_brand->cost = TopBrands::$pricesOneDay[$this->type] * $this->days;

        if ($top_brand->save()) {
            $brand->addPayment([
                'payment_type' => Payments::TYPE_ADVERTISING,
                'balance_charge' => $top_brand->cost,
            ]);

            return true;
        }

        return false;
    }
}