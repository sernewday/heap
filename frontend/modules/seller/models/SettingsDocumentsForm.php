<?php


namespace frontend\modules\seller\models;


use common\models\Brands;
use common\models\CompanyDocuments;
use Yii;

class SettingsDocumentsForm extends \yii\base\Model
{
    public $company_type;
    public $organization_form;
    public $lawyer_name;
    public $lawyer_id;
    public $full_name;
    public $card_num;
    public $pay_type;
    public $company_name;
    public $account_number;
    public $bank_name;
    public $bank_mfo;
    public $documents_region;
    public $documents_index;
    public $documents_city;
    public $documents_address;
    public $address;
    public $phone;
    public $send_bill;

    /**
     * @var CompanyDocuments|null
     */
    public $_documents;

    /**
     * @var Brands|null
     */
    private $_brand;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_type', 'pay_type', 'company_name', 'account_number', 'bank_name', 'bank_mfo', 'documents_region', 'documents_index', 'documents_city', 'documents_address', 'address', 'phone'], 'required'],
            [['company_type', 'pay_type', 'send_bill'], 'integer'],
            [['lawyer_name', 'lawyer_id', 'full_name', 'card_num', 'company_name', 'account_number', 'bank_name', 'bank_mfo', 'documents_region', 'documents_index', 'documents_city', 'documents_address', 'address', 'phone', 'organization_form'], 'string', 'max' => 255],

            [['phone'], 'match', 'pattern' => '/\+380\d\d\d\d\d\d\d\d\d/', 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_type' => Yii::t('app', 'Company Type'),
            'organization_form' => Yii::t('app', 'Организационно-правовая форма'),
            'lawyer_name' => Yii::t('app', 'Полное наименование юридического лица'),
            'lawyer_id' => Yii::t('app', 'ИДЕНТИФИКАЦИОННЫЙ КОД ЮРИДИЧЕСКОГО ЛИЦА (ЕГРПОУ)'),
            'full_name' => Yii::t('app', 'ФИО'),
            'card_num' => Yii::t('app', 'НОМЕР КАРТОЧКИ ПЛАТЕЛЬЩИКА НАЛОГОВ ЛИБО СЕРИЯ И НОМЕР ПАСПОРТА'),
            'pay_type' => Yii::t('app', 'Pay Type'),
            'company_name' => Yii::t('app', 'ОФИЦИАЛЬНОЕ НАЗВАНИЕ КОМПАНИИ'),
            'account_number' => Yii::t('app', 'НОМЕР РАСЧЕТНОГО СЧЕТА'),
            'bank_name' => Yii::t('app', 'НАЗВАНИЕ БАНКА ПОЛУЧАТЕЛЯ'),
            'bank_mfo' => Yii::t('app', 'МФО БАНКА ПОЛУЧАТЕЛЯ'),
            'documents_region' => Yii::t('app', 'ОБЛАСТЬ'),
            'documents_index' => Yii::t('app', 'ИНДЕКС'),
            'documents_city' => Yii::t('app', 'ГОРОД'),
            'documents_address' => Yii::t('app', 'Адрес'),
            'address' => Yii::t('app', 'Адрес'),
            'phone' => Yii::t('app', 'Телефон'),
            'send_bill' => Yii::t('app', 'Отсылать счёт на оплату'),
        ];
    }

    /**
     * @param int $user_id
     * @return bool
     */
    public function loadData($user_id)
    {
        $brand = Brands::findOne(['user_id' => $user_id]);
        if (!$brand)
            return false;

        $this->_brand = $brand;

        $documents = CompanyDocuments::findOne(['brand_id' => $brand->id]);
        if (!$documents)
            return false;

        $this->_documents = $documents;

        $this->setAttributes($this->_documents->getAttributes());

        return true;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->_documents)
            $this->_documents = new CompanyDocuments(['brand_id' => $this->_brand->id]);

        $this->_documents->setAttributes($this->getAttributes());

        return $this->_documents->save();
    }
}