<?php


namespace frontend\modules\seller\models;


use common\models\Brands;
use common\models\CompanyInfo;
use Yii;
use yii\base\Model;

/**
 * Class SettingsReturnForm
 * @package frontend\modules\seller\models
 */
class SettingsReturnForm extends Model
{
    public $return_name;
    public $return_phone;
    public $return_email;
    public $return_city;
    public $return_np;
    public $return_comment;

    /**
     * @var CompanyInfo
     */
    private $_company;

    /**
     * @var Brands
     */
    private $_brand;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['return_name', 'return_phone', 'return_email', 'return_city', 'return_np'], 'required'],

            [['return_phone'], 'match', 'pattern' => '/\+380\d\d\d\d\d\d\d\d\d/', 'message' => Yii::t('app', 'Просим номер телефона указать в формате') . ' +380...'],

            [['return_name', 'return_phone', 'return_email', 'return_city', 'return_comment'], 'string', 'max' => 255],

            [['return_email'], 'email'],

            [['return_np'], 'integer', 'min' => 1],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'return_name' => \Yii::t('app', 'ФИО'),
            'return_phone' => \Yii::t('app', 'Телефонный номер'),
            'return_email' => \Yii::t('app', 'E-Mail'),
            'return_city' => \Yii::t('app', 'Город'),
            'return_np' => \Yii::t('app', 'Отделение Новой почты'),
            'return_comment' => \Yii::t('app', 'Дополнительная информация'),
        ];
    }

    public function loadData($user_id)
    {
        $this->_brand = Brands::findOne(['user_id' => $user_id]);

        $this->_company = CompanyInfo::findOne(['brand_id' => $this->_brand->id]);

        if ($this->_company)
            $this->setAttributes($this->_company->getAttributes());
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->_company)
            $this->_company = new CompanyInfo(['brand_id' => $this->_brand->id]);

        $this->_company->setAttributes($this->getAttributes());

        return $this->_company->save();
    }
}