<?php


namespace frontend\modules\seller\controllers;


use common\models\Brands;
use common\models\ImageManager;
use common\models\Orders;
use common\models\Products;
use common\models\UploadForm;
use frontend\models\ModuleProductsForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProductsController extends ModuleController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['seller'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-image' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['delete-image'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);

        $params = \Yii::$app->request->queryParams;
        if (!isset($params['status_id']))
            $params['status_id'] = Products::STATUS_ACTIVE;

        unset($params['limit']);
        unset($params['_pjax']);
        $products = Products::byBrand($brand->id, 0, $params, \Yii::$app->request->queryParams['limit'] ?: 12, 'created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $products,
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new ModuleProductsForm();
        $model_upload = new UploadForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save(1) &&
            $model_upload->save(Products::tableName(), $model->_product->id))
            return $this->refresh();

        return $this->render('create', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    /**
     * @param $id
     * @param int $lang
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id, $lang = 1)
    {
        $model = new ModuleProductsForm();
        $model_upload = new UploadForm();
        if (!$model->loadProduct($id, $lang))
            throw new NotFoundHttpException('Товар не найден');

        if (!$model->_product->hasAdminPermissions(\Yii::$app->user->getId()))
            throw new ForbiddenHttpException('Доступ запрещён');

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save($lang) &&
            $model_upload->save(Products::tableName(), $model->_product->id))
            return $this->redirect(['/seller/products/update', 'id' => $model->_product->id]);

        return $this->render('update', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (($model = ImageManager::findOne(Yii::$app->request->post('key')))) {
            return $model->removeSelf();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}