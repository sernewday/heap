<?php


namespace frontend\modules\seller\controllers;

use common\models\Brands;
use common\models\Products;
use common\models\Questions;
use common\models\Reviews;
use common\models\UploadForm;
use frontend\models\QuestionAnswerForm;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

/**
 * Class ReviewsController
 * @package frontend\modules\seller\controllers
 */
class ReviewsController extends ModuleController
{
    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['answer-question']))
            $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            throw new ForbiddenHttpException();

        $params = \Yii::$app->request->queryParams;
        unset($params['limit']);

        $query = Reviews::get($params);
        $query->joinWith(['product'])->andWhere([Products::tableName() . '.brand_id' => $brand->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSeller()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            throw new ForbiddenHttpException();

        $dataProvider = new ActiveDataProvider([
            'query' => Brands::getReviews($brand->id),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('seller', [
            'dataProvider' => $dataProvider,
            'brand' => $brand,
        ]);
    }

    public function actionQuestions()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);
        if (!$brand)
            throw new ForbiddenHttpException();

        $dataProvider = new ActiveDataProvider([
            'query' => Questions::find()->joinWith(['product'])->andWhere([Products::tableName() . '.brand_id' => $brand->id])->not_answered()->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('questions', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAnswerQuestion()
    {
        $model = new QuestionAnswerForm();
        $model_upload = new UploadForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()
            && $model_upload->save(Questions::tableName(), $model->id))
            $this->redirect(\Yii::$app->request->referrer ?: ['/']);

        $this->redirect(\Yii::$app->request->referrer ?: ['/']);
    }
}