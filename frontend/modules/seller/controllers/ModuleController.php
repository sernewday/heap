<?php


namespace frontend\modules\seller\controllers;


use frontend\controllers\BaseController;
use yii\filters\AccessControl;

class ModuleController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['seller'],
                    ],
                ],
            ],
        ];
    }
}