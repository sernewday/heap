<?php


namespace frontend\modules\seller\controllers;

use common\models\Brands;
use common\models\Products;
use frontend\modules\seller\models\AdvertisingBrandForm;
use frontend\modules\seller\models\AdvertisingProductsForm;
use yii\data\ActiveDataProvider;

/**
 * Class AdvertisingController
 * @package frontend\modules\seller\controllers
 */
class AdvertisingController extends ModuleController
{
    public function actionBrand()
    {
        $model = new AdvertisingBrandForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save())
            $this->refresh();

        return $this->render('brand', [
            'model' => $model,
        ]);
    }

    public function actionProducts()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);

        $model = new AdvertisingProductsForm();

        $params = \Yii::$app->request->queryParams;
        if (!isset($params['status_id']))
            $params['status_id'] = Products::STATUS_ACTIVE;

        unset($params['limit']);
        unset($params['_pjax']);
        $products = Products::byBrand($brand->id, 0, $params, 0, 'created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $products,
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save())
            $this->refresh();

        return $this->render('products', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}