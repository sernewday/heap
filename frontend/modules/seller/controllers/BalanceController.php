<?php


namespace frontend\modules\seller\controllers;


use common\models\Brands;
use common\models\Payments;
use common\models\SellersBalance;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

class BalanceController extends ModuleController
{
    public function actionIndex()
    {
        $brand = Brands::getByUser(\Yii::$app->user->getId());
        if (!$brand)
            throw new ForbiddenHttpException();

        $paymentsDataProvider = new ActiveDataProvider([
            'query' => Payments::find()->where(['brand_id' => $brand->id])->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'balance' => SellersBalance::getByBrand($brand->id),
            'paymentsDataProvider' => $paymentsDataProvider,
        ]);
    }
}