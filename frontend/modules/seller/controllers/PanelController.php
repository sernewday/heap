<?php


namespace frontend\modules\seller\controllers;


use common\models\Brands;
use common\models\Orders;
use common\models\Payments;
use common\models\SellersBalance;
use yii\web\ForbiddenHttpException;

/**
 * Class PanelController
 * @package frontend\modules\seller\controllers
 */
class PanelController extends ModuleController
{
    public function actionIndex()
    {
        $brand = Brands::getByUser(\Yii::$app->user->getId());
        if (!$brand)
            throw new ForbiddenHttpException();

        return $this->render('index', [
            'seller' => $brand,
            'balance' => SellersBalance::getByBrand($brand->id),
            'last_orders' => Orders::getLastBySeller($brand->id),
            'last_payments' => Payments::getLastByBrand($brand->id),
            'payments_info' => Payments::getDataByPeriod(3600 * 24 * 30, $brand->id),
        ]);
    }
}