<?php


namespace frontend\modules\seller\controllers;


use common\models\Brands;
use common\models\CompanyDocuments;
use common\models\UploadForm;
use frontend\modules\seller\models\SettingsCompanyForm;
use frontend\modules\seller\models\SettingsDocumentsForm;
use frontend\modules\seller\models\SettingsPaymentForm;
use frontend\modules\seller\models\SettingsReturnForm;

/**
 * Class SettingsController
 * @package frontend\modules\seller\controllers
 */
class SettingsController extends ModuleController
{
    public function actionCompany($lang = 1)
    {
        $model = new SettingsCompanyForm();
        $model_upload = new UploadForm();

        $model->loadData(\Yii::$app->user->getId(), $lang);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()
            && $model->save($lang) && $model_upload->save(Brands::tableName(), $model->_brand->id))
            $this->refresh();

        return $this->render('company', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }

    public function actionPayment()
    {
        $model = new SettingsPaymentForm();
        $model->loadData(\Yii::$app->user->getId());

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save())
            $this->refresh();

        return $this->render('payment', [
            'model' => $model,
        ]);
    }

    public function actionReturn()
    {
        $model = new SettingsReturnForm();
        $model->loadData(\Yii::$app->user->getId());

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save())
            $this->refresh();

        return $this->render('return', [
            'model' => $model,
        ]);
    }

    public function actionDocuments()
    {
        $model = new SettingsDocumentsForm();
        $model->loadData(\Yii::$app->user->getId());

        $model_upload = new UploadForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()
            && $model_upload->save(CompanyDocuments::tableName(), $model->_documents->brand_id))
            $this->refresh();

        return $this->render('documents', [
            'model' => $model,
            'model_upload' => $model_upload,
        ]);
    }
}