<?php


namespace frontend\modules\seller\controllers;

/**
 * Class ImportController
 * @package frontend\modules\seller\controllers
 */
class ImportController extends ModuleController
{
    public function actionIndex()
    {
        return $this->render('import');
    }
}