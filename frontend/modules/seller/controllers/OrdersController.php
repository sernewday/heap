<?php


namespace frontend\modules\seller\controllers;

use common\models\Brands;
use common\models\Orders;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class OrdersController
 * @package frontend\modules\seller\controllers
 */
class OrdersController extends ModuleController
{
    public function actionIndex()
    {
        $brand = Brands::findOne(['user_id' => \Yii::$app->user->getId()]);

        $dataProvider = new ActiveDataProvider([
            'query' => Orders::getBySeller($brand->id, \Yii::$app->request->get()),
            'pagination' => [
                'pageSize' => \Yii::$app->request->queryParams['limit'] ?: 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $order = Orders::getOneForSeller($id);
        if (!$order)
            throw new NotFoundHttpException();

        if (!$order->hasAdminPermissions(\Yii::$app->user->getId()))
            throw new ForbiddenHttpException('Недостаточно прав для просмотра этого материала');

        return $this->render('view', [
            'order' => $order,
        ]);
    }
}