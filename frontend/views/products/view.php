<?php

/* @var $this \yii\web\View */

use frontend\components\Helper;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $product \common\models\Products */
/* @var $recently_viewed \common\models\Products[] */
/* @var $reviewsDataProvider \yii\data\ActiveDataProvider */
/* @var $questionsDataProvider \yii\data\ActiveDataProvider */

$this->title = $product->info->name;
$breadcrumbs = $product->breadcrumbsData;

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);
?>

<div class="product-zoom">
    <div class="product-zoom__body">
        <div class="modal__close"></div>
        <div class="product-zoom__slider">
            <?php foreach ($product->images as $image): ?>
                <div class="product-zoom__img" data-index="<?= $index ?>">
                    <img src="<?= $image->imageUrl ?>" alt="">
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<main>
    <div class="container">
        <div class="prev-link">
            <?= \frontend\widgets\Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="product-img-slider">
        <div class="product-img-slider__body">
            <div class="product-img-slider__button">
                <div class="container">
                    <button class="slider__button product-img-slider__prev slider__prev slick-arrow" style=""><i
                                class="fas fa-chevron-left"></i></button>
                    <button class="slider__button product-img-slider__next slider__next slick-arrow" style=""><i
                                class="fas fa-chevron-right"></i></button>
                </div>
            </div>
            <div class="product-img-slider__container">
                <?php $index = 0; ?>
                <?php foreach ($product->images as $image): ?>
                    <div class="product-img-slider__examlpe zoom" data-index="<?= $index ?>">
                        <img alt="<?= $image->imageUrl ?>" src="<?= $image->imageUrl ?>">
                    </div>
                <?php $index++; ?>
                <?php endforeach; ?>
                <?php if (count($product->images) == 3): ?>
                    <div class="product-img-slider__examlpe zoom" data-index="<?= $index ?>">
                        <img alt="<?= $product->images[0]->imageUrl ?>" src="<?= $product->images[0]->imageUrl ?>">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="cart-product">
            <div class="cart-product__body">
                <div class="cart-product__title">
                    <div class="cart-product__cod">№ <?= Helper::formatNumber($product->id) ?></div>
                    <div class="cart-product_logo_rating">
                        <div class="cart-product__logo">
                            <img alt="" src="<?= $product->brand->image->imageUrl ?>">
                        </div>
                        <div class="cart-product__rating"><?= $product->brand->averageScore ?></div>
                    </div>
                </div>
                <div class="cart-product__name"><?= $product->info->name ?></div>
                <div class="cart-product_stats">
                    <div class="cart-product__stat">
                        <img alt="" src="/assets/img/product-cart/bag.svg">
                        <span><?= $product->ordersAmount ?></span>
                    </div>
                    <div class="cart-product__stat">
                        <img alt="" src="/assets/img/product-cart/star.png">
                        <span><?= $product->averageScore ?></span></div>
                    <div class="cart-product__stat">
                        <img alt="" src="/assets/img/product-cart/coment.svg">
                        <span><?= $product->reviewsAmount ?></span></div>
                    <div class="cart-product__stat">
                        <img alt="" src="/assets/img/product-cart/like.svg">
                        <span><?= $product->likesAmount ?></span>
                    </div>
                </div>
                <!-- <div class="cart-product_options"> -->
                    <div class="cart-product__option">
                        <div class="cart-product__option__title"><?= Yii::t('app', 'Размеры') ?>:</div>
                        <div class="options__conteiner">
                            <div class="options cart-product-size">
                                <div class="options__body">
                                    <div class="options__value">
                                    <span class="value">
                                        <?= $product->sizes[0]->size ?> UKR
                                        (<?= $product->sizes[0]->size_international ?> INT)
                                    </span>
                                        <i class="fas fa-chevron-up"></i>
                                    </div>
                                    <div class="cart-product-size__menu">
                                        <div class="cart-product-size__item">
                                            <div class="cart-product-size__value"><?= Yii::t('app', 'Украинский') ?></div>
                                            <div class="cart-product-size__value"><?= Yii::t('app', 'Производителя') ?></div>
                                        </div>
                                    </div>
                                    <div class="options__content">
                                        <?php foreach ($product->sizes as $size): ?>
                                            <div class="options__opt" data-value="<?= $size->size ?>">
                                                <div class="cart-product-size__value"><?= $size->size ?> UKR</div>
                                                <div class="cart-product-size__value"><?= $size->size_international ?>
                                                    INT
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="cart-product-size__table clothing">
                                        <span><?= Yii::t('app', 'Таблица соответствий размеров') ?></span>
                                    </div>
                                    <input type="text" name="sizes" id="value_size"
                                           value="<?= $product->sizes[0]->size ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <?php if ($product->colors): ?>
                        <div class="cart-product__option">
                            <div class="cart-product__option__title"><?= Yii::t('app', 'Цвет') ?>:</div>
                            <div class="options__conteiner">
                                <div class="options">
                                    <div class="options__body">
                                        <div class="options__value">
                                        <span class="value">
                                            <div class="circle" data-value="<?= $product->colors[0]->value_id ?>"
                                                 style="background-color: <?= $product->colors[0]->value->additional_value ?>;"></div>
                                            <span><?= $product->colors[0]->value->value ?></span>
                                        </span>
                                            <i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="options__content">
                                            <?php foreach ($product->colors as $color): ?>
                                                <div class="options__opt" data-value="<?= $color->value_id ?>">
                                                    <div class="circle"
                                                         style="background-color: <?= $color->value->additional_value ?>;"></div>
                                                    <span><?= $color->value->value ?></span>
                                                </div>
                                            <?php endforeach; ?>

                                        </div>

                                </div>
                            </div>
                        </div>
                            <?php else: ?>
                        <input type="hidden" name="colors" id="value_color"
                               value="0">
                    <?php endif; ?> -->
                <!-- </div> -->
                <div class="cart-product__modal-size clothing"><?= Yii::t('app', 'Таблица соответствий размеров') ?></div>
                <div class="cart-product-cost">
                    <div class="cart-product-cost__cost">
                        <div>
                            <?= Yii::t('app', 'Цена') ?>
                            <span><?= round($product->price) ?></span>
                            <span><?= $product->currencyLabel ?></span>
                        </div>
                        <?php if ($product->discount): ?>
                            <p><?= Yii::t('app', 'c промокодом') ?> <span><?= round($product->discountPrice) ?></span></p>
                        <?php endif; ?>
                    </div>
                    <div class="cart-product-cost_buy_like">
                        <div class="cart-product-cost__buy hover-button buy_product" data-id="<?= $product->id ?>">
                            <div class="hover-button__front">
                                <svg fill="none" height="16" viewBox="0 0 15 16" width="15"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.27552 8.81162C6.96691 8.81162 6.7168 9.06174 6.7168 9.37034V13.0394C6.7168 13.348 6.96691 13.5981 7.27552 13.5981C7.58412 13.5981 7.83424 13.3482 7.83424 13.0394V9.37034C7.83424 9.06174 7.58412 8.81162 7.27552 8.81162Z"/>
                                    <path d="M10.1603 8.81162C9.85168 8.81162 9.60156 9.06174 9.60156 9.37034V13.0394C9.60156 13.348 9.85168 13.5981 10.1603 13.5981C10.4689 13.5981 10.719 13.3482 10.719 13.0394V9.37034C10.719 9.06174 10.4689 8.81162 10.1603 8.81162Z"/>
                                    <path d="M4.38685 8.8116C4.07824 8.8116 3.82812 9.06171 3.82812 9.37032V13.0394C3.82812 13.348 4.07824 13.5981 4.38685 13.5981C4.69545 13.5981 4.94557 13.3482 4.94557 13.0394V9.37032C4.94557 9.06171 4.69545 8.8116 4.38685 8.8116Z"/>
                                    <path d="M9.604 2.24161L12.4225 6.08064C12.5655 6.27535 12.7951 6.3803 13.0317 6.42901C13.9972 6.62778 14.6719 7.56502 14.4988 8.58087L13.4702 14.6191C13.3452 15.3525 12.7097 15.8889 11.9658 15.8889H2.68129C1.94644 15.8889 1.31596 15.3651 1.18117 14.6428L0.0304055 8.47516C-0.170593 7.3979 0.650217 6.40048 1.74603 6.3904H5.50805C5.50805 6.3904 5.90876 6.39039 5.98493 6.78229C6.03956 7.50787 5.50805 7.50787 5.50805 7.50787H2.12962C1.54026 7.50787 1.09196 8.03707 1.18885 8.61842L2.08152 13.9745C2.15817 14.4343 2.55606 14.7714 3.0223 14.7714H11.5229C11.9891 14.7714 12.387 14.4343 12.4636 13.9745L13.3563 8.61842C13.4532 8.03707 13.0049 7.50787 12.4156 7.50787H12.1843C11.7858 7.50787 11.7074 7.25917 11.2637 6.3904L8.7033 2.90295L7.57457 1.41894C7.39067 1.17716 7.02607 1.17987 6.84579 1.42435L4.04149 5.22728C3.88518 5.43926 3.58663 5.48438 3.37466 5.32807C3.16269 5.17176 3.11756 4.87321 3.27387 4.66124L6.26451 0.50497C6.73621 -0.150585 7.70458 -0.171405 8.20402 0.46327L9.57374 2.20386C9.58426 2.21589 9.59436 2.22848 9.604 2.24161Z"/>
                                </svg>
                                <?= Yii::t('app', 'ДОБАВИТЬ В КОРЗИНУ') ?>
                            </div>
                            <div class="hover-button__back">
                                <svg fill="none" height="16" viewBox="0 0 15 16" width="15"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.27552 8.81162C6.96691 8.81162 6.7168 9.06174 6.7168 9.37034V13.0394C6.7168 13.348 6.96691 13.5981 7.27552 13.5981C7.58412 13.5981 7.83424 13.3482 7.83424 13.0394V9.37034C7.83424 9.06174 7.58412 8.81162 7.27552 8.81162Z"/>
                                    <path d="M10.1603 8.81162C9.85168 8.81162 9.60156 9.06174 9.60156 9.37034V13.0394C9.60156 13.348 9.85168 13.5981 10.1603 13.5981C10.4689 13.5981 10.719 13.3482 10.719 13.0394V9.37034C10.719 9.06174 10.4689 8.81162 10.1603 8.81162Z"/>
                                    <path d="M4.38685 8.8116C4.07824 8.8116 3.82812 9.06171 3.82812 9.37032V13.0394C3.82812 13.348 4.07824 13.5981 4.38685 13.5981C4.69545 13.5981 4.94557 13.3482 4.94557 13.0394V9.37032C4.94557 9.06171 4.69545 8.8116 4.38685 8.8116Z"/>
                                    <path d="M9.604 2.24161L12.4225 6.08064C12.5655 6.27535 12.7951 6.3803 13.0317 6.42901C13.9972 6.62778 14.6719 7.56502 14.4988 8.58087L13.4702 14.6191C13.3452 15.3525 12.7097 15.8889 11.9658 15.8889H2.68129C1.94644 15.8889 1.31596 15.3651 1.18117 14.6428L0.0304055 8.47516C-0.170593 7.3979 0.650217 6.40048 1.74603 6.3904H5.50805C5.50805 6.3904 5.90876 6.39039 5.98493 6.78229C6.03956 7.50787 5.50805 7.50787 5.50805 7.50787H2.12962C1.54026 7.50787 1.09196 8.03707 1.18885 8.61842L2.08152 13.9745C2.15817 14.4343 2.55606 14.7714 3.0223 14.7714H11.5229C11.9891 14.7714 12.387 14.4343 12.4636 13.9745L13.3563 8.61842C13.4532 8.03707 13.0049 7.50787 12.4156 7.50787H12.1843C11.7858 7.50787 11.7074 7.25917 11.2637 6.3904L8.7033 2.90295L7.57457 1.41894C7.39067 1.17716 7.02607 1.17987 6.84579 1.42435L4.04149 5.22728C3.88518 5.43926 3.58663 5.48438 3.37466 5.32807C3.16269 5.17176 3.11756 4.87321 3.27387 4.66124L6.26451 0.50497C6.73621 -0.150585 7.70458 -0.171405 8.20402 0.46327L9.57374 2.20386C9.58426 2.21589 9.59436 2.22848 9.604 2.24161Z"/>
                                </svg>
                                <?= Yii::t('app', 'ДОБАВИТЬ В КОРЗИНУ') ?>
                            </div>
                        </div>
                        <div class="cart-product-cost__like hover-button">
                            <div class="hover-button__front">
                                <svg fill="none" height="13" viewBox="0 0 15 13" width="15"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8.66423 1.81097C9.42779 1.19056 10.2909 0.7 11.0526 0.7C12.8601 0.7 14.3 2.19121 14.3 4.20588C14.3 4.29264 14.2766 4.45781 14.2175 4.69919C14.1613 4.92882 14.0831 5.18703 13.9981 5.44271C13.8283 5.95351 13.6424 6.42082 13.5824 6.56878C13.5794 6.57626 13.5763 6.58276 13.5724 6.58969L14.1839 6.93042L13.5724 6.5897C12.7664 8.03619 11.4465 9.49213 10.1735 10.5906C9.5398 11.1374 8.93369 11.5818 8.4292 11.8848C8.17658 12.0366 7.96028 12.1463 7.7867 12.2162C7.70042 12.2509 7.63128 12.2731 7.57856 12.2861C7.52412 12.2995 7.49993 12.3 7.5 12.3C7.36143 12.3 7.1413 12.2504 6.83563 12.1083C6.53789 11.9699 6.20113 11.7635 5.8423 11.5013C5.12459 10.977 4.37058 10.268 3.73515 9.55516C3.67827 9.49136 3.61588 9.42214 3.5489 9.34783C3.10031 8.85014 2.44623 8.12446 1.86765 7.27268C1.1926 6.27889 0.7 5.22458 0.7 4.27182C0.7 2.24243 2.15352 0.7 3.94737 0.7C4.70912 0.7 5.57222 1.19056 6.33577 1.81097C7.01196 2.36038 7.98804 2.36038 8.66423 1.81097Z"
                                          stroke="#000000" stroke-width="1.4"/>
                                </svg>
                            </div>
                            <div class="hover-button__back">
                                <svg fill="none" height="13" viewBox="0 0 15 13" width="15"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8.66423 1.81097C9.42779 1.19056 10.2909 0.7 11.0526 0.7C12.8601 0.7 14.3 2.19121 14.3 4.20588C14.3 4.29264 14.2766 4.45781 14.2175 4.69919C14.1613 4.92882 14.0831 5.18703 13.9981 5.44271C13.8283 5.95351 13.6424 6.42082 13.5824 6.56878C13.5794 6.57626 13.5763 6.58276 13.5724 6.58969L14.1839 6.93042L13.5724 6.5897C12.7664 8.03619 11.4465 9.49213 10.1735 10.5906C9.5398 11.1374 8.93369 11.5818 8.4292 11.8848C8.17658 12.0366 7.96028 12.1463 7.7867 12.2162C7.70042 12.2509 7.63128 12.2731 7.57856 12.2861C7.52412 12.2995 7.49993 12.3 7.5 12.3C7.36143 12.3 7.1413 12.2504 6.83563 12.1083C6.53789 11.9699 6.20113 11.7635 5.8423 11.5013C5.12459 10.977 4.37058 10.268 3.73515 9.55516C3.67827 9.49136 3.61588 9.42214 3.5489 9.34783C3.10031 8.85014 2.44623 8.12446 1.86765 7.27268C1.1926 6.27889 0.7 5.22458 0.7 4.27182C0.7 2.24243 2.15352 0.7 3.94737 0.7C4.70912 0.7 5.57222 1.19056 6.33577 1.81097C7.01196 2.36038 7.98804 2.36038 8.66423 1.81097Z"
                                          stroke="#000000" stroke-width="1.4"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="cart-product-cost-type-puy">
                        <div class="cart-product-cost-type-puy__body"><a class="cart-product-cost__example"
                                                                         href="#"><img
                                        alt="" src="/assets/img/product-cart/img_1.png"></a><a
                                    class="cart-product-cost__example" href="#"><img
                                        alt="" src="/assets/img/product-cart/img_2.png"></a></div>
                        <div class="cart-product__questions"><img alt="" src="/assets/img/product-cart/questions.svg">Задать
                            вопрос о товаре
                        </div>
                    </div>
                </div>
                <div class="cart-product__characteristics characteristics-cart-product">
                    <div class="characteristics-cart-product__ex-cocnt">
                        <div class="characteristics-cart-product__example">
                            <div class="characteristics-cart-product__title">
                                <p><?= Yii::t('app', 'Описание') ?></p><span> </span>
                            </div>
                            <div class="characteristics-cart-product-description">
                                <div class="characteristics-cart-product-description__text">
                                    <?= $product->info->description ?>
                                </div>
                                <div class="characteristics-cart-product-description__menu">
                                    <div class="characteristics-cart-product-description__name">
                                        <?php foreach ($product->characteristics as $characteristic): ?>
                                            <span><?= $characteristic->param->name ?>:</span>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="characteristics-cart-product-description__value">
                                        <?php foreach ($product->characteristics as $characteristic): ?>
                                            <span><?= $characteristic->value->value ?></span>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="characteristics-cart-product__ex-cocnt">
                        <div class="characteristics-cart-product__example">
                            <div class="characteristics-cart-product__title">
                                <p><?= Yii::t('app', 'ДОСТАВКА И ОПЛАТА') ?></p><span> </span></div>
                            <div class="characteristics-cart-product-pay-delivery">
                                <div class="characteristics-cart-product-pay">
                                    <div class="characteristics-cart-product-pay__title"><?= Yii::t('app', 'Доставка') ?></div>
                                    <ul class="characteristics-cart-product-pay__menu">
                                        <li class="characteristics-cart-product-pay__item">
                                            <div class="characteristics-cart-product-pay__name"><img
                                                        alt=""
                                                        src="/assets/img/product-cart/novaposhta.svg"><?= Yii::t('app', 'Новая почта') ?>
                                            </div>
                                            <div class="characteristics-cart-product-pay__day">
                                                2-4 дня
                                                <div class="faq">?</div>
                                            </div>
                                            <div class="characteristics-cart-product-pay__cost green">
                                                <?= Yii::t('app', 'бесплатно') ?>
                                                <div class="faq">?</div>
                                            </div>
                                        </li>
                                        <li class="characteristics-cart-product-pay__item">
                                            <div class="characteristics-cart-product-pay__name"><img
                                                        alt="" src="/assets/img/product-cart/plase.svg">
                                                <?= Yii::t('app', 'Адресная доставка') ?>
                                            </div>
                                            <div class="characteristics-cart-product-pay__day">
                                                2-4 дня
                                                <div class="faq">?</div>
                                            </div>
                                            <div class="characteristics-cart-product-pay__cost green">
                                                <?= Yii::t('app', 'бесплатно') ?>
                                                <div class="faq">?</div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="characteristics-cart-product-delivery">
                                    <div class="characteristics-cart-product-delivery__title">
                                        <?= Yii::t('app', 'Оплата') ?>
                                    </div>
                                    <ul class="characteristics-cart-product-delivery__menu">
                                        <li class="characteristics-cart-product-delivery__item">
                                            <div class="characteristics-cart-product-delivery__name"><img
                                                        alt="" src="/assets/img/product-cart/moneu.svg">
                                                <?= Yii::t('app', 'Наложенный платёж') ?>
                                                <div class="faq">?</div>
                                            </div>
                                            <div class="characteristics-cart-product-delivery__value">
                                                <?= Yii::t('app', 'при доставке новой почтой') ?>
                                            </div>
                                        </li>
                                        <li class="characteristics-cart-product-delivery__item">
                                            <div class="characteristics-cart-product-delivery__name"><img
                                                        alt="" src="/assets/img/product-cart/cart.svg">
                                                <?= Yii::t('app', 'Картой (на сайте)') ?>
                                                <div class="faq">?</div>
                                            </div>
                                            <div class="characteristics-cart-product-delivery__value">
                                                <?= Yii::t('app', 'включительно') ?> WayForPay
                                            </div>
                                        </li>
                                        <li class="characteristics-cart-product-delivery__item">
                                            <div class="characteristics-cart-product-delivery__name"><img
                                                        alt="" src="/assets/img/product-cart/cart-two.svg">
                                                <?= Yii::t('app', 'Перевод с карты на карту') ?>
                                            </div>
                                            <div class="characteristics-cart-product-delivery__value">
                                                <?= Yii::t('app', 'напрямую продавцу') ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="characteristics-cart-product__ex-cocnt">
                        <div class="characteristics-cart-product__example">
                            <div class="characteristics-cart-product__title"><p><?= Yii::t('app', 'условия возврата') ?></p>
                                <span> </span></div>
                            <div class="characteristics-cart-product-return"><p
                                        class="characteristics-cart-product-return__text">Вы всегда можете вернуть его в
                                    течение
                                    14 календарных дней после доставки.</p>
                                <p class="characteristics-cart-product-return__text">Возврату не подлежат товары, перечень
                                    которых определен Постановлением Кабинета Министров Украины №172 от 19.03.1994 г.</p>
                                <p class="characteristics-cart-product-return__text">Полные условия возврата.</p></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="cart-product-button">
            <div class="cart-product-button__example active">
                <p><?= Yii::t('app', 'Отзывы') ?></p>
                <span><?= $reviewsDataProvider->totalCount ?></span>
            </div>
            <div class="cart-product-button__example">
                <p><?= Yii::t('app', 'Вопросы') ?></p>
                <span><?= $questionsDataProvider->totalCount ?></span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="cart-product-stats">
            <div class="container">
                <div class="cart-product-stats__container">
                    <div class="cart-product-stats__text"><?= Yii::t('app', 'Мнение покупателей о товаре') ?>:</div>
                    <div class="cart-product-stats__body">
                        <div class="cart-product-stats__example">
                            <p><?= Yii::t('app', 'Оценка') ?>:</p><span><?= $product->averageScore ?></span>
                        </div>
                        <?php if ($product->isSmallSize): ?>
                            <div class="cart-product-stats__example">
                                <img alt="" src="/assets/img/icon/arrows.svg">
                                <p><?= Yii::t('app', 'Размер не соответствует') ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if ($product->isCorrectColor): ?>
                            <div class="cart-product-stats__example">
                                <img alt="" src="/assets/img/icon/palette.svg">
                                <p><?= Yii::t('app', 'Цвет полностью соответствует') ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if ($product->isPerfectQuality): ?>
                            <div class="cart-product-stats__example">
                                <img alt="" src="/assets/img/icon/diamond.svg">
                                <p><?= Yii::t('app', 'Качество отличное') ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if ($product->isDeliveryInTime): ?>
                            <div class="cart-product-stats__example">
                                <img alt="" src="/assets/img/icon/box.svg">
                                <p><?= Yii::t('app', 'Заказ доставлен вовремя') ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php Pjax::begin(['id' => 'reviews_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'cart-product-review-questions pjax-more-container']]); ?>
        <div class="cart-product-review active">
            <div class="container">
                <div class="cart-product-review__body">

                    <?php if (!Yii::$app->user->isGuest): ?>
                        <div class="modal-button hover-button review-product">
                            <div class="hover-button__front"><span><?= Yii::t('app', 'Оставить отзыв') ?></span></div>
                            <div class="hover-button__back"><span><?= Yii::t('app', 'Оставить отзыв') ?></span></div>
                        </div>
                    <?php endif; ?>

                    <div class="cart-product-review__container">
                        <?php $widget = ListView::begin([
                            'dataProvider' => $reviewsDataProvider,
                            'itemView' => '//includes/_review',
                            'viewParams' => ['main_classes' => 'diamond box arrows palette no-img'],
                            'options' => [
                                'tag' => false,
                            ],
                            'itemOptions' => [
                                'tag' => false,
                            ],
                            'layout' => '{items}'
                        ]) ?>

                        <?php $widget->end() ?>
                    </div>
                    <div class="paginations">
                        <div class="paginations__page"><?= $widget->renderSummary() ?></div>

                        <?php if ($reviewsDataProvider->totalCount > 12): ?>
                            <a class="more-link hover-button more-products-button" href="#">
                                <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} отзывов', 12) ?></span></div>
                                <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} отзывов', 12) ?></span></div>
                            </a>
                        <?php endif; ?>

                        <ul class="paginations__menu">
                            <li class="paginations__item"><a class="paginations__link active" href="#">1</a></li>
                            <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                            <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                            <li class="paginations__item more"><a class="paginations__link" href="#">...</a></li>
                            <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="cart-product-questions">
            <div class="container">
                <div class="cart-product-questions__body">

                    <?php if (!Yii::$app->user->isGuest): ?>
                        <div class="modal-button hover-button question">
                            <div class="hover-button__front"><span><?= Yii::t('app', 'Задать вопрос') ?></span></div>
                            <div class="hover-button__back"><span><?= Yii::t('app', 'Задать вопрос') ?></span></div>
                        </div>
                    <?php endif; ?>

                    <div class="cart-product-questions__container">
                        <?php $widget_questions = ListView::begin([
                            'dataProvider' => $questionsDataProvider,
                            'itemView' => '//includes/_question',
                            'options' => [
                                'tag' => false,
                            ],
                            'itemOptions' => [
                                'tag' => false,
                            ],
                            'layout' => '{items}'
                        ]) ?>

                        <?php $widget_questions->end() ?>
                        <div class="paginations">
                            <div class="paginations__page"><?= $widget_questions->renderSummary() ?></div>

                            <?php if ($questionsDataProvider->totalCount > 12): ?>
                                <a class="more-link hover-button more-products-button" href="#">
                                    <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} вопросов', 12) ?></span></div>
                                    <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} вопросов', 12) ?></span></div>
                                </a>
                            <?php endif; ?>

                            <ul class="paginations__menu">
                                <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php Pjax::end(); ?>
        <div class="container">
            <div class="brands-slider">
                <div class="title">
                    <div class="title__line"></div>
                    <div class="title__text">Вас может заинтересовать<span></span></div>
                </div>
            </div>
            <div class="slider-product">
                <div class="slider-product__body">
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                                <div class="product__size"><p>Размеры (UKR):</p>
                                    <span>54</span><span>56</span><span>58</span><span>60</span></div>
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="brands-slider">
                <div class="title">
                    <div class="title__line"></div>
                    <div class="title__text"><?= Yii::t('app', 'вы недавно просматривали') ?><span></span></div>
                </div>
                <div class="slider-product">
                    <div class="slider-product__body">
                        <?php foreach ($recently_viewed as $recently_viewed_product): ?>
                            <?= $this->render('//includes/_product-slider', ['model' => $recently_viewed_product]) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
