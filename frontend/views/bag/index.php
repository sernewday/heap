<?php

/* @var $this \yii\web\View */

use common\models\Orders;
use frontend\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $bag \common\models\Brands[] */

$count_bag = count($bag);

$this->title = Yii::t('app', 'Корзина');
?>

<div class="container-fluid">
    <div class="container">
        <div class="bag-page">
            <div class="bag-page__title">
                <div class="title">
                    <div class="title__line"></div>
                    <div class="title__text"><?= Yii::t('app', 'Корзина') ?><span></span></div>
                </div>
                <?php if ($count_bag > 1): ?>
                    <div class="bag-page__title__text">
                        <div>i</div>
                        <span><?= Yii::t('app', 'Внимание') ?>!</span>
                        <p><?= Yii::t('app', 'Товары от разных продавцов оформляются отдельно, ваш заказ разделен на {0} части', $count_bag) ?></p>
                    </div>
                <?php endif; ?>
            </div>
            <div class="bag-page__body">
                <?php $order_num = 1; $order_id = Orders::getNewOrderNum(); $order_full_sum = 0; $order_discount_sum = 0; ?>
                <?php foreach ($bag as $bag_item): ?>
                    <div class="bag-page-order <?= $order_num == $count_bag ? 'last' : '' ?>">
                        <div class="bag-page-order__title">
                            <?= Yii::t('app', 'Заказ') . ' ' . $order_num ?>
                            <span>(№<?= $order_id ?>)</span>
                        </div>
                        <div class="bag-page-order__body">
                            <div class="bag-page-order__container">
                                <div class="bag-page-order__menu">
                                    <div class="bag-page-order__value"><?= Yii::t('app', 'Товар') ?></div>
                                    <div class="bag-page-order__value"><?= Yii::t('app', 'Количество') ?></div>
                                    <div class="bag-page-order__value"><?= Yii::t('app', 'Цена') ?></div>
                                </div>
                                <div class="bag-page-order__fake"></div>
                            </div>
                            <div class="bag-page-order__infornation">
                                <div class="bag-page-order__order">
                                    <?php foreach ($bag_item->products as $product): ?>
                                        <?php if (!$product->bag) continue; ?>

                                        <?php foreach ($product->bag as $product_bag): ?>
                                            <div class="product-bag-page">
                                                <div class="product-bag-page__body">
                                                    <?= Html::a('', ['/bag/remove-bag-product', 'product_id' => $product_bag->id, 'quantity' => 1], [
                                                        'class' => 'product-bag-page__close',
                                                        'data' => [
                                                            'confirm' => Yii::t('app', 'Вы уверены что хотите удалить этот товар?'),
                                                            'method' => 'post',
                                                        ],
                                                    ]) ?>
                                                    <div class="product-bag-page__img"><img
                                                                alt="" src="<?= $product->image->imageUrl ?>"></div>
                                                    <div class="product-bag-page__information">
                                                        <div class="product-bag-page__name"><?= $product->info->name ?></div>
                                                        <div class="product-bag-page__number">№ <?= Helper::formatNumber($product->id) ?></div>
                                                        <div class="product-bag-page__size">
                                                            <p><?= Yii::t('app', 'Размер') ?> (UKR): </p><span><?= $product->bag[0]->size ?></span>
                                                        </div>
                                                        <div class="product-bag-page__color blue">
                                                            <p><?= Yii::t('app', 'Цвет') ?></p>
                                                            <div style="background-color: <?= $product_bag->colorData->additional_value ?>;"></div>
                                                            <span><?= $product_bag->colorData->value ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="product-bag-page__counter">
                                                        <div class="product-bag-page__remove">-</div>
                                                        <div class="product-bag-page__value"><?= $product_bag->quantity ?></div>
                                                        <div class="product-bag-page__add">+</div>
                                                    </div>
                                                    <div class="product-bag-page__cost">
                                                        <?= Helper::formatPrice($product->price * $product_bag->quantity) ?> грн
                                                        <?php if ($product->discount): ?>
                                                            <div class="product-bag-page__promo">
                                                                <?= Yii::t('app', 'c промокодом {0} грн', Helper::formatPrice($product->discountPrice * $product_bag->quantity)) ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $order_full_sum += ($product->price * $product_bag->quantity); $order_discount_sum += ($product->discountPrice * $product_bag->quantity); ?>
                                        <?php endforeach; ?>

                                    <?php endforeach; ?>
                                </div>
                                <div class="bag-page-order__cost">
                                    <div class="bag-page-order__text__container">
                                        <div class="bag-page-order__text">
                                            <p><?= Yii::t('app', 'Продавец') ?>:</p><span><?= $bag_item->name ?></span>
                                        </div>
                                        <div class="bag-page-order__text">
                                            <p><?= Yii::t('app', 'Итого к оплате') ?>:</p><span><?= Helper::formatPrice($order_full_sum) ?> грн</span>
                                            <?php if ($order_full_sum > $order_discount_sum): ?>
                                                <div class="bag-page-order__promo"><?= Yii::t('app', 'c промокодом {0} грн', $order_discount_sum) ?></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                $order_discount_sum = 0; $order_full_sum = 0;
                $order_num++;
                $order_id++;
                endforeach;
                ?>
                <div class="bag-page-order__bg">
                    <a href="<?= Url::to(['/order/index']) ?>" class="bag-page-order__btn hover-button">
                        <div class="hover-button__front"><span><?= Yii::t('app', 'Оформить заказ') ?></span></div>
                        <div class="hover-button__back"><span><?= Yii::t('app', 'Оформить заказ') ?></span></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
