<?php

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $sales \common\models\Sales[] */

$this->title = Yii::t('app', 'Акции');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$sale_indexes = [[], [], []];

$data_indexes = [0, 1, 2];
foreach ($sales as $key => $sale) {
    $cur_index = array_shift($data_indexes);
    $sale_indexes[$cur_index][] = $key;

    $data_indexes[] = $cur_index;
}
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
        <div class="blog">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
            </div>
            <div class="blog__body mobail">
                <?php foreach ($sales as $sale): ?>
                    <a class="blog-cart" href="<?= Url::to(['/sales/view', 'id' => $sale->id]) ?>">
                        <div class="blog-cart__fake"><img alt="" src="<?= $sale->image->imageUrl ?>"></div>
                        <div class="blog-cart__body">
                            <div class="blog-cart__img"><img alt="" src="<?= $sale->image->imageUrl ?>">
                                <div class="blog-cart__hashtag">
                                    <div class="blog-cart__hashtag__example">
                                        #<?= Html::encode($sale->info->label_name) ?></div>
                                </div>
                                <div class="blog-cart__watch">
                                    <img alt="" src="/assets/img/product-cart/watch.svg"><span>233</span></div>
                                <div class="blog-cart__text">
                                    <?= Html::encode($sale->info->name) ?>
                                </div>
                            </div>
                            <div class="blog-cart__link">
                                <p><?= Yii::t('app', 'Читать далее') ?></p>
                                <span><img alt="" src="/assets/img/icon/arow_to.svg"></span>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
            <div class="blog__body">
                <?php foreach ($sale_indexes as $column__index): ?>
                    <div class="blog__column">
                        <?php foreach ($column__index as $sale__index): ?>
                            <a class="blog-cart" href="<?= Url::to(['/sales/view', 'id' => $sale->id]) ?>">
                                <div class="blog-cart__fake"><img alt="" src="<?= $sales[$sale__index]->image->imageUrl ?>"></div>
                                <div class="blog-cart__body">
                                    <div class="blog-cart__img"><img alt="" src="<?= $sales[$sale__index]->image->imageUrl ?>">
                                        <div class="blog-cart__hashtag">
                                            <div class="blog-cart__hashtag__example">#<?= Html::encode($sales[$sale__index]->info->label_name) ?></div>
                                        </div>
                                        <div class="blog-cart__watch">
                                            <img alt="" src="/assets/img/product-cart/watch.svg"><span>233</span>
                                        </div>
                                        <div class="blog-cart__text">
                                            <?= Html::encode($sales[$sale__index]->info->name) ?>
                                        </div>
                                    </div>
                                    <div class="blog-cart__link">
                                        <p><?= Yii::t('app', 'Читать далее') ?></p>
                                        <span><img alt="" src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="paginations">
            <div class="paginations__page"><span>30</span>из<span>300</span>статей</div>
            <a class="more-link hover-button" href="#">
                <div class="hover-button__front"><span>Еще 30 статей</span></div>
                <div class="hover-button__back"><span>Еще 30 статей</span></div>
            </a>
            <ul class="paginations__menu">
                <li class="paginations__item"><a class="paginations__link active" href="#">1</a></li>
                <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                <li class="paginations__item more"><a class="paginations__link" href="#">...</a></li>
                <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
            </ul>
        </div>
        <?= $this->render('//site/descriptFemail') ?>
    </div>
</main>
