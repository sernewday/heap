<?php

/* @var $this \yii\web\View */
/* @var $sale \common\models\Sales */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = $sale->info->name;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Акции'), 'url' => ['/sales/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<div class="container">
    <div class="prev-link">
        <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
    </div>
    <div class="blog-example">
        <div class="blog-example__container">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text">
                    <?= Html::encode($this->title) ?>
                    <span></span>
                </div>
            </div>
            <div class="blog-slider-item__img"><img alt="" src="<?= $sale->image->imageUrl ?>">
                <div class="blog-slider-item__hashtag">
<!--                    <div class="blog-slider-item__hashtag__example">#Бренды</div>-->
                </div>
                <div class="blog-slider-item__post-week">пост недели</div>
            </div>
            <div class="blog-example__text">
                <?= Html::encode($sale->info->comment) ?>
            </div>
        </div>
    </div>
</div>
