<?php

/* @var $this */

use yii\helpers\Url;

$order_id = mt_rand(2, 200);

$order = \common\models\Orders::findOne(16);
?>

<!doctype html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="https://secure.wayforpay.com/pay" method="post">
    <input type="text" name="merchantAccount" value="test_merch_n1">
    <input type="text" name="merchantAuthType" value="SimpleSignature">
    <input type="text" name="merchantSignature" value="<?= hash_hmac('md5', 'test_merch_n1;' . Url::home(true) . ';test_o_'.$order_id.';' . time() . ';1;UAH;test product;1;1', 'flk3409refn54t54t*FNJRET') ?>">
    <input type="text" name="merchantDomainName" value="<?= Url::home(true) ?>">
    <input type="text" name="merchantTransactionSecureType" value="AUTO">
    <input type="text" name="language" value="AUTO">
    <input type="text" name="returnUrl" value="<?= Url::canonical() ?>">
    <input type="text" name="serviceUrl" value="<?= Url::canonical() ?>">
    <input type="text" name="orderReference" value="test_o_<?= $order_id ?>">
    <input type="text" name="orderDate" value="<?= time() ?>">
    <input type="text" name="orderNo" value="test<?= $order_id ?>">
    <input type="text" name="amount" value="1">
    <input type="text" name="currency" value="UAH">
    <input type="text" name="orderTimeout" value="3600">
    <input type="text" name="productName[]" value="test product">
    <input type="text" name="productPrice[]" value="1">
    <input type="text" name="productCount[]" value="1">
    <input type="text" name="paymentSystems" value="card;googlePay;applePay;privat24;qrCode;masterPass;visaCheckout">

    <button type="submit">SUBMIT</button>
</form>

<?php

echo "<pre>" . print_r($order->generateSignature(), true) . "</pre>";
echo "<pre>" . print_r(Yii::$app->request->post(), true) . "</pre>";
echo "<pre>" . print_r(Yii::$app->request->get(), true) . "</pre>";

?>

</body>
</html>
