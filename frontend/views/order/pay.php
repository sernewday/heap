<?php

/* @var $this \yii\web\View */
/* @var $order_ids array */

/* @var $order Orders */

use common\models\Orders;
use yii\helpers\Url;

?>

<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pay</title>
</head>
<body>
Подождите секунду, вы будете перенаправлены на сайт платёжной системы...
<form action="https://secure.wayforpay.com/pay" method="post">
    <input type="hidden" name="merchantAccount" value="<?= $order->payAccountName ?>">
    <input type="hidden" name="merchantAuthType" value="SimpleSignature">
    <input type="hidden" name="merchantSignature" value="<?= $order->generateSignature() ?>">
    <input type="hidden" name="merchantDomainName" value="<?= Yii::$app->request->serverName ?>">
    <input type="hidden" name="merchantTransactionSecureType" value="AUTO">
    <input type="hidden" name="language" value="AUTO">
    <input type="hidden" name="returnUrl"
           value="<?= $order_ids ? Url::to(['/order/pay', 'o' => implode(',', $order_ids)], true) : Url::to(['/order/thank'], true) ?>">
    <input type="hidden" name="serviceUrl" value="<?= Url::to(['/callback/order'], true) ?>">
    <input type="hidden" name="orderReference"
           value="<?= (Orders::ENABLE_TEST_MODE ? 'test_' : '') . 'r_' . $order->id ?>">
    <input type="hidden" name="orderDate" value="<?= $order->created_at ?>">
    <input type="hidden" name="orderNo" value="<?= (Orders::ENABLE_TEST_MODE ? 'test_' : '') . $order->id ?>">
    <input type="hidden" name="amount" value="<?= Orders::ENABLE_TEST_MODE ? 1 : $order->orderSum ?>">
    <input type="hidden" name="currency" value="UAH">
    <input type="hidden" name="orderTimeout" value="3600">
    <?php foreach ($order->products as $product): ?>
        <input type="hidden" name="productName[]" value="<?= $product->product->info->name ?>">
        <input type="hidden" name="productPrice[]"
               value="<?= Orders::ENABLE_TEST_MODE ? 1 : $product->price_for_one ?>">
        <input type="hidden" name="productCount[]" value="<?= $product->amount ?>">
    <?php endforeach; ?>
    <input type="hidden" name="paymentSystems" value="card;googlePay;applePay;privat24;qrCode;masterPass;visaCheckout">
    <input type="hidden" name="repayUrl"
           value="<?= $order_ids ? Url::to(['/order/pay', 'o' => $order->id . ',' . implode(',', $order_ids)], true) : Url::to(['/order/pay', 'o' => $order->id], true) ?>">
</form>


<script>
    window.onload = function(){
        document.forms[0].submit();
    }
</script>
</body>
</html>
