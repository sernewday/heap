<?php

/* @var $this \yii\web\View */

use common\models\Products;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Спасибо за заказ');
?>

<main>
    <div class="container">
        <div class="thank">
            <div class="thank__body">
                <div class="title">
                    <div class="title__line"></div>
                    <div class="title__text"><?= Yii::t('app', 'Спасибо за заказ') ?><span></span></div>
                </div>
                <div class="thank__text">
                    <?= Yii::t('app', 'Отслеживайте статус заказа в личном кабинете') ?></div>
                <img alt="" src="/assets/img/thank.png">
                <div class="thank__row"><a class="thank__button" href="<?= Url::to(['/profile/index']) ?>"><?= Yii::t('app', 'В личный кабинет') ?></a>
                    <a class="thank__button__back hover-button" href="<?= Url::home() ?>">
                        <div class="hover-button__front"><span><?= Yii::t('app', 'Продолжить покупки') ?></span></div>
                        <div class="hover-button__back"><span><?= Yii::t('app', 'Продолжить покупки') ?></span></div>
                    </a></div>
            </div>
        </div>
        <div class="brands-slider">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Вас может заинтересовать') ?><span></span></div>
            </div>
            <div class="slider-product">
                <div class="slider-product__body">
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="product product-slider">
                        <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png">
                        </div>
                        <a
                            class="product__body" href="#">
                            <div class="product__img">
                                <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                                <div class="product__more-info__body">
                                    <div class="product__more-info red">-20 % по промокоду</div>
                                    <div class="product__more-info blue">Новинка</div>
                                </div>
                                <div class="product__slider-img"><img alt=""
                                                                      src="/assets/img/product/product_img_1.png"></div>
                            </div>
                            <div class="product__data">
                                <div class="product__row">
                                    <div class="product__cost">799 грн</div>
                                    <div class="product__color__body">
                                        <div class="product__color red"></div>
                                        <div class="product__color white-red"></div>
                                        <div class="product__color white"></div>
                                    </div>
                                </div>
                                <div class="product__type">
                                    <p>Savage</p>/<span>Платье</span>
                                </div>
                                <div class="product__size">
                                    <p>Размеры (UKR):</p><span>54</span><span>56</span><span>58</span><span>60</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="ariticle-slider-article">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text">Читайте наши статьи<span></span></div>
            </div>
            <div class="ariticle-slider-article__body">
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_4.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__watch"><img alt="" src="/assets/img/product-cart/watch.svg">233</div>
                            <div class="article__text">
                                <div class="article__name">Топ-5 лучших брендов с качественными и брендовыми сумками до 1500
                                    грн
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_2.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Топ</div>
                                <div class="article-hashtag__example">#Бренды</div>
                            </div>
                            <div class="article__watch"><img alt="" src="/assets/img/product-cart/watch.svg">233</div>
                            <div class="article__text">
                                <div class="article__name">Топ-5 лучших брендов</div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_3.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_4.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__watch"><img alt="" src="/assets/img/product-cart/watch.svg">233</div>
                            <div class="article__text">
                                <div class="article__name">Топ-5 лучших брендов с качественными и брендовыми сумками до 1500
                                    грн
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_2.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Топ</div>
                                <div class="article-hashtag__example">#Бренды</div>
                            </div>
                            <div class="article__watch"><img alt="" src="/assets/img/product-cart/watch.svg">233</div>
                            <div class="article__text">
                                <div class="article__name">Топ-5 лучших брендов</div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_3.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="brands-slider">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'вы недавно просматривали') ?><span></span></div>
            </div>
            <div class="slider-product">
                <div class="slider-product__body">
                    <?php foreach (Products::getRecentlyViewed() as $recently_viewed_product): ?>
                        <?= $this->render('//includes/_product-slider', ['model' => $recently_viewed_product]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="ariticle-slider-newsletter">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text">интересные рассылки<span></span></div>
            </div>
            <div class="ariticle-slider-newsletter__body">
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_3.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_5.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_3.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_3.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_5.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                <div addClass class="article"><a class="article__body" href="#">
                        <div class="article__img"><img alt="" class="img" src="/assets/img/article/article_3.png">
                            <div class="article__hashtag article-hashtag">
                                <div class="article-hashtag__example">#Рассылка</div>
                            </div>
                            <div class="article__text">
                                <div class="article__name">Топ 10 товаров недели</div>
                                <div class="article_date_more">
                                    <div class="article__date">21.03.2020</div>
                                    <div class="article__more">Подробнее<span><img alt=""
                                                                                   src="/assets/img/icon/arow_to.svg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></div>
            </div>
        </div>
    </div>
</main>
