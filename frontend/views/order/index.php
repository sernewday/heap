<?php

/* @var $this \yii\web\View */

use common\models\Orders;
use frontend\components\Helper;
use frontend\widgets\CustomActiveForm;
use yii\helpers\Html;

/* @var $bag \common\models\Brands[] */
/* @var $model \frontend\models\OrderForm */

$this->title = Yii::t('app', 'Оформление заказа');
?>

<div class="container-fluid">
    <?php $form = CustomActiveForm::begin(['options' => ['class' => 'checkout-page']]) ?>
    <div class="container">
        <div class="title">
            <div class="title__line"></div>
            <div class="title__text"><?= Yii::t('app', 'Оформление заказа') ?><span></span></div>
        </div>
        <div class="checkout-page__body">
            <div class="checkout-page-fio">
                <div class="checkout-page__title"><?= Yii::t('app', 'Общая информация') ?>*</div>
                <div class="checkout-page-fio__body">
                    <?= $form->field($model, 'full_name')->textInput()->label(false) ?>

                    <?= $form->field($model, 'phone')->textInput()->label(false) ?>

                    <?= $form->field($model, 'email')->textInput()->label(false) ?>
                </div>
            </div>
            <div class="checkout-delivery">
                <div class="checkout-page__title">Доставка*</div>
                <div class="checkout-delivery__body">
                    <div class="radio-button" id="nova-pohta">
                        <label class="radio-button__body">
                            <input checked class="radio-button__input" name="<?= Html::getInputName($model, 'delivery_type') ?>" value="<?= Orders::DELIVERY_NEW_POST ?>" type="radio">
                            <span class="radio-button__fake"><span></span></span>
                            <span class="radio-button__text"><?= Yii::t('app', 'Доставка в отделение Новой почты') ?>
                                <div class="more-information">
                                    <span>?</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora distinctio!
                                        Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam omnis dolorum
                                        voluptates. Distinctio doloremque veniam optio aut.
                                    </p>
                                </div>
                            </span>
                        </label>
                    </div>
                    <div class="checkout-delivery__nova-pohta">
                        <div class="delivery-nova-pohta">

                            <?= $form->field($model, 'city')->textInput()->label(false) ?>

                            <div class="input-nova-pohta">
                                <div class="input-block-menu">
                                    <?= $form->field($model, 'post_number')->searchInput()->label(false) ?>
                                    <div class="input-block-menu__result">
                                        <div class="input-block-menu__item">
                                            <p>Отделение №1., ул. Пироговский путь, 135.</p>
                                        </div>
                                        <div class="input-block-menu__item">
                                            <p>Отделение №1., ул. Пироговский путь, 135.</p>
                                        </div>
                                        <div class="input-block-menu__item">
                                            <p>Отделение №1., ул. Пироговский путь, 135.</p>
                                        </div>
                                        <div class="input-block-menu__item">
                                            <p>Отделение №1., ул. Пироговский путь, 135.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="delivery-nova-pohta__result">
                            <span data-name="Киев">Киев</span>
                            <span data-name="Одесса">Одесса</span>
                            <span data-name="Днепр">Днепр</span>
                            <span data-name="Харьков">Харьков</span>
                            <span data-name="Львов">Львов</span>
                        </div>
                    </div>
                    <div class="radio-button" id="adres">
                        <label class="radio-button__body">
                            <input class="radio-button__input" name="<?= Html::getInputName($model, 'delivery_type') ?>" value="<?= Orders::DELIVERY_ADDRESS ?>" type="radio">
                            <span class="radio-button__fake"><span></span></span>
                            <span class="radio-button__text"><?= Yii::t('app', 'Адресная доставка') ?>
                                <div class="more-information">
                                    <span>?</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora distinctio!
                                        Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam omnis dolorum
                                        voluptates. Distinctio doloremque veniam optio aut.
                                    </p>
                                </div>
                            </span>
                        </label>
                    </div>
                    <div class="checkout-delivery__adres">

                        <?= $form->field($model, 'city')->textInput()->label(false) ?>

                        <?= $form->field($model, 'street')->textInput()->label(false) ?>

                        <div class="checkout-delivery__adres-number">

                            <?= $form->field($model, 'house')->textInput()->label(false) ?>

                            <?= $form->field($model, 'flat')->textInput()->label(false) ?>
                        </div>

                    </div>
                </div>
            </div>

            <?php $order_index = 0; $order_num = 1; $order_id = Orders::getNewOrderNum(); $order_full_sum = 0; $order_discount_sum = 0; $full_sum = 0; ?>
            <?php foreach ($bag as $bag_item): ?>
                <div class="checkout-order">
                    <div class="checkout-order__body">
                        <div class="checkout-order__information">
                            <div class="checkout-order__pay">
                                <div class="checkout-page__title">Оплата*</div>
                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input checked class="radio-button__input" name="<?= Html::getInputName($model, "payment_type[{$order_index}]") ?>" value="<?= Orders::PAY_AFTER ?>" type="radio">
                                        <span class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text"><?= Yii::t('app', 'Наложенный платёж') ?>
                                            <div class="more-information"><span>?</span>
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora
                                                distinctio! Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam
                                                omnis dolorum voluptates. Distinctio doloremque veniam optio aut.
                                                </p>
                                            </div>
                                        </span>
                                    </label>
                                </div>
                                <?php if ($bag_item->decryptedMerchantName && $bag_item->decryptedMerchantSecret): ?>
                                    <div class="radio-button">
                                        <label class="radio-button__body">
                                            <input class="radio-button__input" name="<?= Html::getInputName($model, "payment_type[{$order_index}]") ?>" value="<?= Orders::PAY_ONLINE ?>" type="radio">
                                            <span class="radio-button__fake"><span></span></span>
                                            <span class="radio-button__text"><?= Yii::t('app', 'Картой на сайте') ?>
                                            <div class="more-information"><span>?</span>
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora
                                                distinctio! Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam
                                                omnis dolorum voluptates. Distinctio doloremque veniam optio aut.
                                                </p>
                                            </div>
                                        </span>
                                        </label>
                                    </div>
                                <?php endif; ?>
                                <?php if ($bag_item->decryptedCardNum): ?>
                                    <div class="radio-button">
                                        <label class="radio-button__body">
                                            <input class="radio-button__input" name="<?= Html::getInputName($model, "payment_type[{$order_index}]") ?>" value="<?= Orders::PAY_TRANSFER ?>" type="radio">
                                            <span class="radio-button__fake"><span></span></span>
                                            <span class="radio-button__text"><?= Yii::t('app', 'Перевод с карты на карту') ?>
                                            <div class="more-information"><span>?</span>
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia aspernatur tempora
                                                distinctio! Ipsa distinctio natus laboriosam, earum possimus neque reiciendis ex, aperiam
                                                omnis dolorum voluptates. Distinctio doloremque veniam optio aut.
                                                </p>
                                            </div>
                                        </span>
                                        </label>
                                    </div>
                                <?php endif; ?>
                                <div class="checkout-order__promo">
                                    <p><?= Yii::t('app', 'Промокод или код подарочного сертификата') ?></p>
                                </div>
                            </div>
                            <div class="checkout-order__data">
                                <div class="checkout-page__title"><?= Yii::t('app', 'Дополнительные данные') ?></div>
                                <textarea class="checkout-order__textarea" cols="30" name="<?= Html::getInputName($model, "comment[{$order_index}]") ?>" placeholder="<?= Yii::t('app', 'Комментарий к заказу') ?>" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="order-checkout">
                            <div class="order-checkout__body">
                                <div class="order-checkout__title">
                                    <p><?= Yii::t('app', 'Заказ') . ' ' . $order_num ?></p><span>(№<?= $order_id ?>)</span>
                                </div>
                                <div class="order-checkout__container">
                                    <?php foreach ($bag_item->products as $product): ?>
                                        <?php if (!$product->bag) continue; ?>

                                        <?php foreach ($product->bag as $product_bag): ?>
                                            <div class="product-order-checkout">
                                                <div class="product-order-checkout__body">
                                                    <div class="product-order-checkout__close"><span></span></div>
                                                    <div class="product-order-checkout__img">
                                                        <img alt="" src="<?= $product->image->imageUrl ?>">
                                                    </div>
                                                    <div class="product-order-checkout__name">
                                                        <?= $product->info->name ?>
                                                    </div>
                                                    <div class="product-order-checkout__cost"><?= $product->price * $product_bag->quantity ?> грн</div>
                                                </div>
                                            </div>
                                            <?php $order_full_sum += ($product->price * $product_bag->quantity); $order_discount_sum += ($product->discountPrice  * $product_bag->quantity); $full_sum += ($product->discountPrice  * $product_bag->quantity) ?>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class="order-checkout__cost">
                                    <div class="order-checkout__text"><span><?= Yii::t('app', 'Итого') ?>:</span>
                                        <p><?= $order_full_sum ?> грн</p>
                                    </div>
                                    <div class="order-checkout__text"><span><?= Yii::t('app', 'Доставка') ?>:</span>
                                        <p><?= Yii::t('app', 'бесплатно') ?></p>
                                    </div>
                                    <div class="order-checkout__text"><span><?= Yii::t('app', 'Скидка') ?>:</span>
                                        <p><?= $order_full_sum - $order_discount_sum ?> грн</p>
                                    </div>
                                    <div class="order-checkout__text sell"><span><?= Yii::t('app', 'Всего') ?>:</span>
                                        <p><?= Helper::formatPrice($order_discount_sum) ?> <span>грн</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php $order_num++; $order_id++; $order_full_sum = 0; $order_discount_sum = 0; $order_index++; ?>
            <?php endforeach; ?>
        </div>
        <div class="checkout-page-btn">
            <div class="checkout-page-btn__body">
                <button type="submit" class="checkout-page-btn__button hover-button">
                    <div class="hover-button__front">
                        <svg fill="none" height="10" viewBox="0 0 15 10" width="15"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                    d="M0.4 3.84119C0.179086 3.84119 0 4.02027 0 4.24119V9.25729C0 9.4782 0.179086 9.65729 0.4 9.65729H14.6C14.8209 9.65729 15 9.4782 15 9.25729V4.24119C15 4.02027 14.8209 3.84119 14.6 3.84119H0.4ZM4.62053 7.0483C4.62053 7.26922 4.44144 7.4483 4.22053 7.4483H2.31997C2.09906 7.4483 1.91997 7.26922 1.91997 7.0483V6.9694C1.91997 6.74848 2.09906 6.5694 2.31997 6.5694H4.22053C4.44144 6.5694 4.62053 6.74848 4.62053 6.9694V7.0483ZM11.854 7.9714C11.7295 7.9714 11.6106 7.94776 11.5015 7.90475C11.286 7.81981 11.015 7.81981 10.7996 7.90475C10.6904 7.94776 10.5715 7.9714 10.4471 7.9714C9.91547 7.9714 9.48448 7.54041 9.48448 7.00879C9.48448 6.47717 9.91547 6.04619 10.4471 6.04619C10.5716 6.04619 10.6904 6.06983 10.7996 6.11285C11.0151 6.19779 11.286 6.1978 11.5015 6.11286C11.6106 6.06983 11.7295 6.04619 11.854 6.04619C12.3856 6.04619 12.8166 6.47717 12.8166 7.00879C12.8166 7.54041 12.3856 7.9714 11.854 7.9714Z"
                                    fill="black"/>
                            <path
                                    d="M14.6 0H0.4C0.179086 0 0 0.179086 0 0.4V1.5957C0 1.81662 0.179086 1.9957 0.4 1.9957H14.6C14.8209 1.9957 15 1.81662 15 1.5957V0.4C15 0.179086 14.8209 0 14.6 0Z"
                                    fill="black"/>
                        </svg>
                        <?= Yii::t('app', 'ПРОДОЛЖИТЬ') ?>
                    </div>
                    <div class="hover-button__back">
                        <svg fill="none" height="10" viewBox="0 0 15 10" width="15"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                    d="M0.4 3.84119C0.179086 3.84119 0 4.02027 0 4.24119V9.25729C0 9.4782 0.179086 9.65729 0.4 9.65729H14.6C14.8209 9.65729 15 9.4782 15 9.25729V4.24119C15 4.02027 14.8209 3.84119 14.6 3.84119H0.4ZM4.62053 7.0483C4.62053 7.26922 4.44144 7.4483 4.22053 7.4483H2.31997C2.09906 7.4483 1.91997 7.26922 1.91997 7.0483V6.9694C1.91997 6.74848 2.09906 6.5694 2.31997 6.5694H4.22053C4.44144 6.5694 4.62053 6.74848 4.62053 6.9694V7.0483ZM11.854 7.9714C11.7295 7.9714 11.6106 7.94776 11.5015 7.90475C11.286 7.81981 11.015 7.81981 10.7996 7.90475C10.6904 7.94776 10.5715 7.9714 10.4471 7.9714C9.91547 7.9714 9.48448 7.54041 9.48448 7.00879C9.48448 6.47717 9.91547 6.04619 10.4471 6.04619C10.5716 6.04619 10.6904 6.06983 10.7996 6.11285C11.0151 6.19779 11.286 6.1978 11.5015 6.11286C11.6106 6.06983 11.7295 6.04619 11.854 6.04619C12.3856 6.04619 12.8166 6.47717 12.8166 7.00879C12.8166 7.54041 12.3856 7.9714 11.854 7.9714Z"
                                    fill="black"/>
                            <path
                                    d="M14.6 0H0.4C0.179086 0 0 0.179086 0 0.4V1.5957C0 1.81662 0.179086 1.9957 0.4 1.9957H14.6C14.8209 1.9957 15 1.81662 15 1.5957V0.4C15 0.179086 14.8209 0 14.6 0Z"
                                    fill="black"/>
                        </svg>
                        <?= Yii::t('app', 'ПРОДОЛЖИТЬ') ?>
                    </div>
                </button>
                <div class="checkout-page-btn__cost"><span><?= Yii::t('app', 'Итого') ?>:</span>
                    <p><?= Helper::formatPrice($full_sum) ?><span>грн</span></p>
                </div>
            </div>
        </div>
    </div>
    <?php CustomActiveForm::end() ?>
</div>
