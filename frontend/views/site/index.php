<?php

/* @var $this yii\web\View */
/* @var $sale \common\models\Products[] */
/* @var $last_brands \common\models\Brands[] */
/* @var $top_categories \common\models\Categories[] */
/* @var $new_products \common\models\Products[] */

use common\models\Banners;
use yii\helpers\Url;

$this->title = 'Marketplace';

$main_banners = Banners::getBanners(1);
$banner_2 = Banners::getBanner(2);
$banner_3 = Banners::getBanner(3);
$banner_4 = Banners::getBanner(4);
$banner_5 = Banners::getBanner(5);
$banner_6 = Banners::getBanner(6);
$banner_7 = Banners::getBanner(7);
$banner_8 = Banners::getBanner(8);
$banner_9 = Banners::getBanner(9);

?>
<main class="main-page">
    <div class="container">
        <div class="main-slider-promo">
            <div class="main-slider-baner">
                <div class="main-slider-baner__img">
                    <img alt="" src="<?= $main_banners[0]->image->imageUrl ?>">
                </div>
                <div class="main-slider-container-baner">
                    <div class="main-slider-baner__body">
                        <?php foreach ($main_banners as $main_banner): ?>
                            <a href="<?= $main_banner->url ?>" class="main-slider-baner__example">
                                <img alt="" src="<?= $main_banner->image->imageUrl ?>">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="main-promo">
                <a href="<?= $banner_2->url ?>" class="main-promo__example">
                    <div class="main-promo__example__type"><?= $banner_2->label_edge ?></div>
                    <div class="main-promo__example__text">
                        <p><?= $banner_2->label_main ?></p>
                        <span><?= $banner_2->label_top ?></span>
                    </div>
                    <img alt="" src="<?= $banner_2->image->imageUrl ?>">
                </a>
                <a href="<?= $banner_3->url ?>" class="main-promo__new">
                    <div class="main-promo__new__type"><?= $banner_3->label_edge ?></div>
                    <div class="main-promo__new__text">
                        <p><?= $banner_3->label_main ?></p>
                        <span><?= $banner_3->label_top ?></span>
                    </div>
                    <img alt="" src="<?= $banner_3->image->imageUrl ?>">
                </a>
            </div>
        </div>
        <?php if ($banner_4): ?>
            <div class="main-counter__bg">
                <div class="main-counter__text">
                    <span><?= $banner_4->label_top ?></span>
                    <p><?= $banner_4->label_main ?></p>
                </div>
                <div class="main-counter__img">
                    <img alt="" src="<?= $banner_4->image->imageUrl ?>">
                </div>
                <div class="main-counter" data-counter="<?= $banner_4->due_to ?>">
                    <div class="main-counter__example">
                        <div class="main-counter__value">31</div>
                        <div class="main-counter__type">день</div>
                    </div>
                    <div class="main-counter__example">
                        <div class="main-counter__value">13</div>
                        <div class="main-counter__type">часов</div>
                    </div>
                    <div class="main-counter__example">
                        <div class="main-counter__value">50</div>
                        <div class="main-counter__type">минут</div>
                    </div>
                    <div class="main-counter__example">
                        <div class="main-counter__value">59</div>
                        <div class="main-counter__type">секунд</div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="offer-block">
            <a href="<?= $banner_5->url ?>" class="offer">
                <div class="offer__img"><img alt="" src="<?= $banner_5->image->imageUrl ?>"></div>
                <div class="offer__type"><?= $banner_5->label_edge ?></div>
                <div class="offer__name">
                    <p><?= $banner_5->label_main ?></p>
                    <span><?= $banner_5->label_top ?></span>
                </div>
            </a>
            <a href="<?= $banner_6->url ?>" class="offer">
                <div class="offer__img"><img alt="" src="<?= $banner_6->image->imageUrl ?>"></div>
                <div class="offer__type orange"><?= $banner_6->label_edge ?></div>
                <div class="offer__name reverse">
                    <p><?= $banner_6->label_main ?></p>
                    <span><?= $banner_6->label_top ?></span>
                </div>
            </a>
            <a href="<?= $banner_7->url ?>" class="offer">
                <div class="offer__img"><img alt="" src="<?= $banner_7->image->imageUrl ?>"></div>
                <div class="offer__type"><?= $banner_7->label_edge ?></div>
                <div class="offer__name reverse">
                    <p><?= $banner_7->label_main ?></p>
                    <span><?= $banner_7->label_top ?></span>
                </div>
            </a>
        </div>
        <!-- <div class="main-brend">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Бренды') ?><span></span></div>
            </div>
            <div class="main-brend__body">
                <div class="main-brend__container">
                    <div class="main-brend__row">
                        <?php for ($i = 0; $i < 9; $i++): ?>
                            <a class="main-brend__example" href="<?= Url::to(['/brands/view', 'id' => $last_brands[$i]->id]) ?>">
                                <div class="main-brend__img_content">
                                    <div class="main-brend__img">
                                        <img alt="" src="<?= $last_brands[$i]->image->imageUrl ?>">
                                    </div>
                                </div>
                                <div class="main-brend__name"><?= $last_brands[$i]->name ?> <span>(<?= $last_brands[$i]->commonProductsAmount ?>)</span></div>
                            </a>
                        <?php endfor; ?>
                    </div>
                    <div class="main-brend__row">
                        <?php for ($i = 9; $i < 18; $i++): ?>
                            <a class="main-brend__example" href="<?= Url::to(['/brands/view', 'id' => $last_brands[$i]->id]) ?>">
                                <div class="main-brend__img_content">
                                    <div class="main-brend__img">
                                        <img alt="" src="<?= $last_brands[$i]->image->imageUrl ?>">
                                    </div>
                                </div>
                                <div class="main-brend__name"><?= $last_brands[$i]->name ?> <span>(<?= $last_brands[$i]->commonProductsAmount ?>)</span></div>
                            </a>
                        <?php endfor; ?>
                    </div>
                </div>
                <a class="main-brend__all" href="<?= Url::to(['/brands/index']) ?>">
                    <p><?= Yii::t('app', 'Смотреть все') ?></p>
                    <span><i class="fas fa-chevron-right"></i></span>
                </a>
                <a class="main-brend__all-mob more-link hover-button" href="#">
                    <div class="hover-button__front">
                        <span><?= Yii::t('app', 'Смотреть все') ?></span>
                    </div>
                    <div class="hover-button__back">
                        <span><?= Yii::t('app', 'Смотреть все') ?></span>
                    </div>
                </a>
            </div>
        </div> -->
        <div class="sell-out-block">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Распродажа') ?></div>
            </div>
            <div class="slider-product">
                <div class="slider-product__body">
                    <?php foreach ($sale as $sale_item): ?>
                        <?= $this->render('//includes/_product-slider', ['model' => $sale_item]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="main-stocks-block">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Актуальные акции') ?></div>
            </div>
            <div class="main-stocks">
                <div class="main-stocks__content">
                    <a class="main-stocks__body small" href="<?= $banner_8->url ?>">
                        <div class="main-stocks__name">
                            <span><?= $banner_8->label_top ?></span>
                            <p><?= $banner_8->label_main ?></p>
                        </div>
                        <div class="main-stocks__proposition red"><?= $banner_8->label_edge ?></div>
                        <div class="main-stocks__data">до <?= date('d.m.Y', $banner_8->due_to) ?></div>
                        <img alt="" src="<?= $banner_8->image->imageUrl ?>">
                    </a>

                    <a class="main-stocks__body big" href="<?= $banner_9->url ?>">
                        <div class="main-stocks__name">
                            <span><?= $banner_9->label_top ?></span>
                            <p><?= $banner_9->label_main ?></p>
                        </div>
                        <div class="main-stocks__proposition orange"><?= $banner_9->label_edge ?></div>
                        <div class="main-stocks__data">до <?= date('d.m.Y', $banner_9->due_to) ?></div>
                        <img alt="" src="<?= $banner_9->image->imageUrl ?>"></a>
                </div>

                <a class="more-link hover-button" href="<?= Url::to(['/sales/index']) ?>">
                    <div class="hover-button__front"><span><?= Yii::t('app', 'Cмотреть все акции') ?></span></div>
                    <div class="hover-button__back"><span><?= Yii::t('app', 'Cмотреть все акции') ?></span></div>
                </a>
            </div>
        </div>
        <div class="main-news-block">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'НОВИНКИ') ?></div>
            </div>
            <div class="slider-product">
                <div class="slider-product__body">
                    <?php foreach ($new_products as $new_product): ?>
                        <?= $this->render('//includes/_product-slider', ['model' => $new_product]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <!-- <div class="main-blog-video-block">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'видео и блог') ?></div>
            </div>
            <div class="main-blog-video-block__body">
                <div class="main-video">
                    <div class="main-video__body"><img alt="" src="/assets/img/video-main.png">
                        <div class="main-video__name"><p>Как обыграть асиметрию в одежде: советы от нашего модного
                                эксперта</p><span>Видеообзор</span></div>
                    </div>
                    <a class="more-link hover-button" href="#">
                        <div class="hover-button__front"><span>Больше видео</span></div>
                        <div class="hover-button__back"><span>Больше видео</span></div>
                    </a></div>
                <div class="main-blog">
                    <div class="main-blog__body"><img alt="" src="/assets/img/article/article_1.png">
                        <div class="main-blog__name"><p>Топ 5 тканей для костюма</p><span>Статья</span></div>
                    </div>
                    <div class="main-blog__body"><img alt="" src="/assets/img/article/article_1.png">
                        <div class="main-blog__name"><p>Топ 5 тканей для костюма</p><span>Статья</span></div>
                    </div>
                    <a class="more-link hover-button" href="#">
                        <div class="hover-button__front"><span>Больше статей</span></div>
                        <div class="hover-button__back"><span>Больше статей</span></div>
                    </a></div>
            </div>
        </div> -->

        <div class="main-categori-block">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Актуальные категории') ?></div>
            </div>
            <div class="product-categori-slider">
                <div class="product-categori-slider__body">
                    <!-- <?php foreach ($top_categories as $top_category): ?>
                        <div class="product-categori">
                            <a class="product-categori__body" href="<?= Url::to(['/category/index', 'alias' => $top_category->name_alt]) ?>">
                                <img alt="<?= $top_category->image ?>" src="<?= $top_category->image ?>">
                                <div class="product-categori__name"><p><?= $top_category->info->name ?></p></div>
                            </a>
                        </div>
                    <?php endforeach; ?> -->
                    <!-- временный хардкод -->
                    <div class="product-categori">
                            <a class="product-categori__body" href="/category/Bryuki">
                                <img alt="/uploads/products/br/bruki-7126-89229302758666.jpg" src="/uploads/products/br/bruki-7126-89229302758666.jpg">
                                <div class="product-categori__name"><p>ШТАНИ</p></div>
                            </a>
                        </div>
                                            <div class="product-categori">
                            <a class="product-categori__body" href="/category/Bluzi_i_rubashki">
                                <img alt="/uploads/products/bl/bluza-1260-81403708829917.jpg" src="/uploads/products/bl/bluza-1260-81403708829917.jpg">
                                <div class="product-categori__name"><p>БЛУЗИ ТА СОРОЧКИ</p></div>
                            </a>
                        </div>
                                            <div class="product-categori">
                            <a class="product-categori__body" href="/category/Domashnyaya_odezhda">
                                <img alt="/uploads/products/su/suknya-56115096-17381720252345.jpg" src="/uploads/products/su/suknya-56115096-17381720252345.jpg">
                                <div class="product-categori__name"><p>ДОМАШНІЙ ОДЯГ</p></div>
                            </a>
                        </div>
                                            <div class="product-categori">
                            <a class="product-categori__body" href="/category/Aksessuari">
                                <img alt="/uploads/products/re/remn-7688-23913889957798.jpg" src="/uploads/products/re/remn-7688-23913889957798.jpg">
                                <div class="product-categori__name"><p>АКСЕСУАРИ</p></div>
                            </a>
                        </div>
                </div>
            </div>
        </div>
        <?= $this->render('//site/descriptFemail') ?>
    </div>
</main>
