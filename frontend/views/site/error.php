<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>

<div class="container-fluid">
    <div class="error">
        <div class="title">
            <div class="title__line"></div>
            <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
        </div>
        <div class="error__body">
            <div class="error__img"><img alt="error" src="/assets/img/error.png"></div>
            <div class="error__btn-container">
                <a class="error__btn gray hover-button" href="<?= Url::home() ?>">
                    <div class="hover-button__front"><span><?= Yii::t('app', 'На главную') ?></span></div>
                    <div class="hover-button__back"><span><?= Yii::t('app', 'На главную') ?></span></div>
                </a>
                <a class="error__btn hover-button" href="<?= Yii::$app->request->referrer ?: ['/'] ?>">
                    <div class="hover-button__front"><span><?= Yii::t('app', 'Назад') ?></span></div>
                    <div class="hover-button__back"><span><?= Yii::t('app', 'Назад') ?></span></div>
                </a>
            </div>
        </div>
    </div>
</div>
