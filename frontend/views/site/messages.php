<?php

use yii\helpers\Url;

?>
<form action="<?= Url::current() ?>" method="post">
    <button type="submit">OK</button>

    <?php $i = 0; ?>
    <?php foreach ($data as $key => $value): ?>
        <div>
            <input type="hidden" name="messages[<?= $i ?>][key]" value="<?= $key ?>">
            <?= $key ?> => <input type="text" name="messages[<?= $i ?>][value]" value="<?= $value ?>" style="width: 500px;">
        </div><br><br>

    <?php $i++; ?>
    <?php endforeach; ?>

    <button type="submit">OK</button>
</form>