<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\models\Bag;
use common\models\LoginForm;
use frontend\assets\AppAsset;
use frontend\models\EmailVerifyForm;
use frontend\models\SignupForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

AppAsset::register($this);

$signup = new SignupForm();
$email_verify = new EmailVerifyForm();
$login_model = new LoginForm();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container-fluid">
    <header>
        <div class="header-min">
            <div class="container">
                <div class="header-min__body">
                    <a class="header-min__back" href="<?= Yii::$app->request->referrer ?: Url::home() ?>">
                        <i class="fas fa-chevron-left"></i>
                        <?= Yii::t('app', 'ВЕРНУТСЯ НАЗАД') ?>
                    </a>
                    <a class="header-min__logo" href="<?= Url::home() ?>">
                        <img alt="" src="/assets/img/logo-whith.png">
                    </a>
                    <div class="header-min__leng">
                        <span><?= Yii::t('app', 'Язык') ?>: <?= Yii::$app->language ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
<?= $content ?>
<div class="container-fluid">
    <footer>
        <div class="footer-min">
            <div class="container">
                <div class="footer-min__body">
                    <div class="footer-min__text">2011-2020. Все права защищены</div>
                    <a class="footer-min__logo" href="#"><img
                                alt="" src="/assets/img/logo.png"></a>
                    <div class="footer-min__text">Сайт разработан в компании sysale.ua</div>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
