<?php

/* @var $this \yii\web\View */

use common\models\Likes;
use common\models\Orders;
use common\models\User;
use frontend\components\Helper;
use yii\helpers\Url;

$amount_orders = Orders::getAmountActiveByUser(Yii::$app->user->getId());

$common_value = $amount_orders;

$likes_amount = Likes::getAmountByUser(Yii::$app->user->getId());

$user = User::find()->where([User::tableName() . '.id' => Yii::$app->user->getId()])->joinWith(['family'])->one();
?>

<div class="header-button">
    <div class="header-button__example promocod value">
        <img alt="" src="/assets/img/header/promocod.png">
        <p>Промокоды</p>
        <span>1</span>
        <div class="header-button-more">
            <div class="header-button-more__body">
                <div class="header-button-more__text">
                    Вам доступен новый промокод на товары от Savage со скидкой 30%
                </div>
                <a class="header-button-more__link hover-button" href="#">
                    <div class="hover-button__front">
                        Подробнее
                        <img alt="" src="/assets/img/icon/arow_to.svg">
                    </div>
                    <div class="hover-button__back">
                        Подробнее
                        <img alt="" src="/assets/img/icon/arow_to.svg">
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="header-button__example <?= Yii::$app->user->isGuest ? 'login' : '' ?> <?= $common_value > 0 ? 'value' : '' ?>">
        <img alt="" src="/assets/img/header/profile.png">
        <p><?= Yii::t('app', 'Профиль') ?></p>
        <span><?= $common_value ?></span>
        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="personal-area-sing-in">
                <div class="personal-area-sing-in__menu">
                    <div class="personal-area-sing-in__item">
                        <a class="personal-area-sing-in__link"
                           href="<?= Url::to(['/profile/index']) ?>">
                            <p><?= Yii::t('app', 'Мои данные') ?></p>
                        </a>
                    </div>
                    <div class="personal-area-sing-in__item <?= $amount_orders > 0 ? 'value' : '' ?>">
                        <a class="personal-area-sing-in__link"
                           href="<?= Url::to(['/profile/orders']) ?>">
                            <p><?= Yii::t('app', 'Мои заказы') ?></p>
                            <span><?= $amount_orders ?></span>
                        </a>
                    </div>
                    <div class="personal-area-sing-in__item">
                        <a class="personal-area-sing-in__link" href="<?= Url::to(['/profile/reviews']) ?>">
                            <p><?= Yii::t('app', 'Мои отзывы и вопросы') ?></p>
                            <span>1</span>
                        </a>
                    </div>
                    <div class="personal-area-sing-in__item">
                        <a class="personal-area-sing-in__link" href="#">
                            <p>Промокоды и сертификаты</p>
                            <span>1</span>
                        </a>
                    </div>
                </div>
                <?php if ($user->family): ?>
                    <div class="personal-area-sing-in__buy-container">
                        <div class="personal-area-sing-in__buy">
                            <?php foreach ($user->family as $family_item): ?>
                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input class="radio-button__input" name="" type="radio">
                                        <span class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text"><?= Yii::t('app', 'Покупаю для {0}', Helper::getNameByFIO($family_item->full_name)) ?></span>
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <a class="personal-area-sing-in__btn"
                   href="<?= Url::to(['/site/logout']) ?>"><?= Yii::t('app', 'Выйти') ?></a>
            </div>
        <?php endif; ?>
    </div>
    <?php if (!Yii::$app->user->isGuest): ?>
        <div class="header-button__example like <?= $likes_amount > 0 ? 'value' : '' ?>">
            <img alt="" src="/assets/img/header/like.png">
            <p><?= Yii::t('app', 'Избранное') ?></p>
            <span id="counter-likes"><?= Likes::getAmountByUser(Yii::$app->user->getId()) ?></span>
            <div class="header-button-more">
                <div class="header-button-more__body">
                    <div class="header-button-more__text">
                        <?php if ($likes_amount > 0): ?>
                            <?= Yii::t('app', 'В избранном уже {0} товаров', $likes_amount) ?>
                        <?php else: ?>
                            <?= Yii::t('app', 'В избранном пока нет товаров') ?>
                        <?php endif; ?>
                    </div>
                    <a class="header-button-more__link hover-button" href="<?= Url::to(['/profile/likes']) ?>">
                        <div class="hover-button__front">
                            <?= Yii::t('app', 'Подробнее') ?>
                            <img alt="" src="/assets/img/icon/arow_to.svg">
                        </div>
                        <div class="hover-button__back">
                            <?= Yii::t('app', 'Подробнее') ?>
                            <img alt="" src="/assets/img/icon/arow_to.svg">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="header-button__example bag value"><img alt="" src="/assets/img/header/bag.png">
        <p>Корзина</p><span>1</span>
    </div>
</div>
