<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;

?>

<div class="header-button">
    <div class="header-button__example cab">
        <img alt="" src="/assets/img/header/bag.png">
        <p><?= Yii::t('app', 'Мой кабинет') ?></p>
        <span>1</span>
        <div class="header-button-more">
            <ul class="header-button-more__menu">
                <li class="header-button-more__item">
                    <a href="<?= Url::to(['/seller/panel/index']) ?>"><?= Yii::t('app', 'Панель управления') ?></a>
                </li>
                <li class="header-button-more__item">
                    <a href="<?= Url::to(['/seller/orders/index']) ?>"><?= Yii::t('app', 'Заказы') ?></a>
                </li>
                <li class="header-button-more__item"><a href="#">Моя переписка<span>1</span></a>
                </li>
                <li class="header-button-more__item"><a href="#">Товары</a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/seller/balance/index']) ?>"><?= Yii::t('app', 'Мой баланс') ?></a></li>
                <li class="header-button-more__item"><a href="#">Реклама</a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/seller/settings/company']) ?>"><?= Yii::t('app', 'Настройки') ?></a></li>
                <li class="header-button-more__item"><a href="#">Контакты</a></li>
            </ul>
            <a class="header-button-more__exit" href="<?= Url::to(['/site/logout']) ?>"><?= Yii::t('app', 'Выйти') ?></a>
        </div>
    </div>
</div>
