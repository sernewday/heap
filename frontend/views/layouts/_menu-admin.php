<?php

/* @var $this \yii\web\View */

use common\models\Sales;
use yii\helpers\Url;

?>

<div class="header-button">
    <div class="header-button__example admin-head"><img alt="" src="./assets/img/header/admin.png">
        <p><?= Yii::t('app', 'Администратор') ?></p>
        <span>1</span>
        <div class="header-button-more">
            <ul class="header-button-more__menu">
                <li class="header-button-more__item">
                    <a href="<?= Url::to(['/admin/panel/index']) ?>"><?= Yii::t('app', 'Панель управления') ?></a>
                </li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/admin/sellers/index']) ?>"><?= Yii::t('app', 'Продавцы') ?></a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/admin/orders/index']) ?>"><?= Yii::t('app', 'Заказы') ?><span>1</span></a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/admin/products/index']) ?>"><?= Yii::t('app', 'Товары') ?></a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/admin/sales/index', 'status_id' => Sales::STATUS_ACTIVE]) ?>"><?= Yii::t('app', 'Акции и промокоды') ?></a></li>
                <li class="header-button-more__item"><a href="#">Мои переписки</a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/admin/balance/index']) ?>"><?= Yii::t('app', 'Финансовые операции') ?></a></li>
                <li class="header-button-more__item"><a href="#">Реклама</a></li>
                <li class="header-button-more__item"><a href="#">Настройки</a></li>
                <li><a class="header-button-more__exit" href="<?= Url::to(['/site/logout']) ?>"><?= Yii::t('app', 'Выйти') ?></a></li>
                <li class="header-button-more__item"><a href="#">Баннеры</a></li>
                <li class="header-button-more__item"><a href="#">Блог</a></li>
                <li class="header-button-more__item"><a href="#">Категории</a></li>
                <li class="header-button-more__item"><a href="<?= Url::to(['/admin/clients/index']) ?>"><?= Yii::t('app', 'Покупатели') ?> </a></li>
            </ul>
            <a class="header-button-more__exit" href="<?= Url::to(['/site/logout']) ?>"><?= Yii::t('app', 'Выйти') ?></a>
        </div>
    </div>
</div>
