<?php

/* @var $this \yii\web\View */

/* @var $brands \common\models\Brands[] */

/* @var $top_brands \common\models\Brands[] */

/* @var $recently_viewed \common\models\Products[] */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Бренды');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Бренды'), 'url' => '#'];

$count_brands = count($brands);
$current_letter = false;

$letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'G', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
?>
<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
        <div class="brand-top">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Бренды') ?><span></span></div>
            </div>
            <div class="brand-top__title"><?= Yii::t('app', 'Топ {0} брендов', 30) ?></div>
            <div class="brand-top__body">
                <?php foreach ($top_brands as $top_brand): ?>
                    <a class="brand-top__example" href="<?= Url::to(['/brands/view', 'id' => $top_brand->id]) ?>">
                        <div class="brand-top__img">
                            <img src="<?= $top_brand->image->imageUrl ?>" alt="">
                        </div>
                        <div class="brand-top__name"><?= $top_brand->name ?> <span>(<?= $top_brand->commonProductsAmount ?>)</span></div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="brand-top-filter">

            <?php for ($i = 0; $i < count($letters); $i++): ?>
                <?php
                if ($i != 0 && $i % 14 == 0) echo '</div>';
                if ($i % 14 == 0) echo '<div class="brand-top-filter__body">';
                if ($i == 0) echo '<a class="brand-top-filter__example ' . (Yii::$app->request->get('l') ? '' : 'active') . '" href="' . Url::to(['/brands/index']) . '">Все</a>';
                ?>
                <a class="brand-top-filter__example <?= Yii::$app->request->get('l') == $letters[$i] ? 'active' : '' ?>"
                   href="<?= Url::to(['/brands/index', 'l' => $letters[$i]]) ?>"><?= $letters[$i] ?></a>
                <?php if ($i == (count($letters) - 1)) echo '</div>'; ?>
            <?php endfor; ?>

        </div>
        <div class="brand-top-list-container">
            <?php for ($i = 0; $i < $count_brands; $i++): ?>
                <?php
                if (mb_strtoupper(mb_substr($brands[$i]->name, 0, 1)) != $current_letter) {
                    if ($i != 0)
                        echo '</div></div>' . "\n\n";

                    $current_letter = mb_strtoupper(mb_substr($brands[$i]->name, 0, 1));
                    echo "<div class=\"brand-top-list\"><div class=\"brand-top-list__name\">{$current_letter}</div><div class=\"brand-top-list__body\">\n\n";
                }
                ?>

                <a class="brand-top-list__example"
                   href="<?= Url::to(['/brands/view', 'id' => $brands[$i]->id]) ?>"><?= $brands[$i]->name ?>
                    <span>(<?= $brands[$i]->commonProductsAmount ?>) </span></a>

            <?php endfor; ?>
            <?php echo '</div></div>' . "\n\n"; ?>
        </div>
        <div class="brands-slider">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text">Вас может заинтересовать<span></span></div>
            </div>
        </div>
        <div class="slider-product">
            <div class="slider-product__body">
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
            </div>
        </div>
        <div class="brands-slider">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'вы недавно просматривали') ?><span></span></div>
            </div>
            <div class="slider-product">
                <div class="slider-product__body">
                    <?php foreach ($recently_viewed as $recently_viewed_product): ?>
                        <?= $this->render('//includes/_product-slider', ['model' => $recently_viewed_product]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="slider-product">
            <div class="slider-product__body">
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
                <div class="product product-slider">
                    <div class="product__size-container"><img alt="" src="/assets/img/product/product_img_1.png"></div>
                    <a class="product__body" href="#">
                        <div class="product__img">
                            <div class="product__like"><img alt="" src="/assets/img/icon/like.svg"></div>
                            <div class="product__more-info__body">
                                <div class="product__more-info red">-20 % по промокоду</div>
                                <div class="product__more-info blue">Новинка</div>
                            </div>
                            <div class="product__slider-img"><img alt="" src="/assets/img/product/product_img_1.png">
                            </div>
                        </div>
                        <div class="product__data">
                            <div class="product__row">
                                <div class="product__cost">799 грн</div>
                                <div class="product__color__body">
                                    <div class="product__color red"></div>
                                    <div class="product__color white-red"></div>
                                    <div class="product__color white"></div>
                                </div>
                            </div>
                            <div class="product__type"><p>Savage</p>/<span>Платье</span></div>
                            <div class="product__size"><p>Размеры (UKR):</p>
                                <span>54</span><span>56</span><span>58</span><span>60</span></div>
                        </div>
                    </a></div>
            </div>
        </div>
        <div class="main-information-block">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text">женская одежда<span></span></div>
            </div>
        </div>
        <div class="main-information">
            <div class="main-information__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class="main-information__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class="main-information__link"><p>Популярные категории:</p><a href="#">Джинсы-мом</a><a href="#">Джинсы-мом</a><a
                        href="#">Джинсы-мом</a><a href="#">Джинсы-мом</a><a href="#">Джинсы-мом</a></div>
            <div class="main-information__link"><p>Популярные теги:</p><a href="#">Белые</a><a href="#">Распродажа</a><a
                        href="#">Из хлопка</a><a href="#">Хаки</a><a href="#">Милитари</a></div>
        </div>
    </div>
</main>
