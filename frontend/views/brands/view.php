<?php

/* @var $this \yii\web\View */

use common\models\Categories;
use frontend\components\Helper;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CategoriesWidget;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $brand \common\models\Brands */
/* @var $reviews \common\models\Reviews[] */
/* @var $brand_products \yii\data\ActiveDataProvider */
/* @var $category Categories|false */

$this->title = $brand->name;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', 'Бренды'), 'url' => Url::to(['/brands/index'])];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);
$this->registerJsFile('/js/filter_params.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

$js = <<< JS
$('#products_container').on('pjax:complete', function () {
    moreItemsEvent();
});
JS;
$this->registerJs($js);
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="containet-fluid">
        <div class="brands-categori-stats">
            <div class="container">
                <div class="brands-categori-stats__body">
                    <div class="brands-categori-stats__logo">
                        <img alt="" src="<?= $brand->image->imageUrl ?>">
                    </div>
                    <div class="brands-categori-stats__rating">
                        <p><?= Yii::t('app', 'Средняя оценка') ?>:</p>
                        <span>
                            <div class="stars">
                                <?= \frontend\widgets\Stars::widget(['score' => $brand->averageScore]) ?>
                            </div>
                        </span>
                        <div class="brands-categori-stats__rating_value"><?= $brand->averageScore ?></div>
                    </div>
                    <div class="brands-categori-stats__id"><p><?= Yii::t('app', 'ID продавца') ?>:
                            <span><?= Helper::formatNumber($brand->id) ?></span></p></div>
                </div>
            </div>
        </div>
    </div>
    <div class="containet-fluid">
        <div class="brands-categori-info">
            <div class="container">
                <div class="title small">
                    <div class="title__line"></div>
                    <div class="title__text"><?= Yii::t('app', 'О бренде') ?><span></span></div>
                </div>
                <div class="brands-categori-info__body">
                    <p><?= $brand->info->description ?></p>
                </div>
            </div>
        </div>
        <div class="brands-categori-review">
            <div class="container">
                <div class="brands-categori-review__body">
                    <div class="brands-categori-review__title">
                        <div class="title small">
                            <div class="title__line"></div>
                            <div class="title__text"><?= Yii::t('app', 'Отзывы') ?><span></span></div>
                        </div>
                        <div class="brands-categori-review__btn hover-button">
                            <div class="hover-button__front"><span><?= Yii::t('app', 'Оставить отзыв') ?></span></div>
                            <div class="hover-button__back"><span><?= Yii::t('app', 'Оставить отзыв') ?></span></div>
                        </div>
                    </div>
                    <div class="brands-categori-review__slider">
                        <div class="review-slider">
                            <div class="review-slider__body">
                                <?php foreach ($reviews as $review): ?>
                                    <?= $this->render('//includes/_review', ['model' => $review, 'main_classes' => 'review-slider-style user diamond box']) ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <a class="more-link hover-button" href="#">
                        <div class="hover-button__front"><span><?= Yii::t('app', 'Все отзывы') ?></span></div>
                        <div class="hover-button__back"><span><?= Yii::t('app', 'Все отзывы') ?></span></div>
                    </a></div>
            </div>
        </div>
        <div class="product-catalog-container">
            <div class="container">
                <div class="product-catalog-container__title">
                    <div class="title">
                        <div class="title__line"></div>
                        <div class="title__text"><?= $brand->name ?><span>(<?= $brand_products->totalCount ?> <?= Yii::t('app', 'товаров') ?>)</span></div>
                    </div>
                </div>
                <div class="product-catalog-container__catalog">
                    <div class="product-catalog">
                        <div class="product-catalog__fix">
                            <div class="product-catalog__categiris">
                                <?= CategoriesWidget::widget(['category' => $category]) ?>
                            </div>
                        </div>
                        <div class="product-catalog__product">
                            <?= $this->render('//includes/_catalog-filter') ?>
                            <?php Pjax::begin(['id' => 'products_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'catalog-product pjax-more-container']]); ?>
                            <?php $widget = ListView::begin([
                                'dataProvider' => $brand_products,
                                'itemView' => '//includes/_product',
                                'options' => [
                                    'tag' => false,
                                ],
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'layout' => '{items}'
                            ]) ?>

                            <?php $widget->end() ?>

                            <div class="paginations">
                                <div class="paginations__page"><?= $widget->renderSummary() ?></div>
                                <a class="more-link hover-button more-products-button" href="#">
                                    <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} товаров', 12) ?></span></div>
                                    <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} товаров', 12) ?></span></div>
                                </a>
                                <ul class="paginations__menu">
                                    <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                    <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                    </li>
                                    <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                                </ul>
                            </div>

                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
