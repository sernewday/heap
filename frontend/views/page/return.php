<?php

/* @var $this \yii\web\View */
/* @var $page \common\models\Pages */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = $page->info->title;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
        <div class="return-page">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
            </div>
            <div class="return-page__body">
                <div class="return-page__title back">
                    <span><?= Html::encode($page->content[0]->info->title) ?></span>
                    <img alt="" src="/assets/img/plash/back.png">
                    <img alt="" src="/assets/img/plash/back_mob.png">
                </div>
                <div class="return-page__content">
                    <div class="return-page__text">
                        <?= nl2br(Html::encode($page->content[0]->info->text)) ?>
                    </div>
                </div>
            </div>
            <div class="return-page__body">
                <div class="return-page__title">
                    <span><?= Html::encode($page->content[1]->info->title) ?></span>
                    <img alt="" src="/assets/img/plash/close.png">
                    <img alt="" src="/assets/img/plash/close_mob.png">
                </div>
                <div class="return-page__content">
                    <div class="return-page__text">
                        <?= nl2br(Html::encode($page->content[1]->info->text)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
