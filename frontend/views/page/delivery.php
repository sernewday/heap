<?php

/* @var $this \yii\web\View */
/* @var $page \common\models\Pages */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = $page->info->title;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
        <div class="delievery">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Html::encode($this->title) ?></div>
            </div>
            <div class="delievery__body">
                <div class="delievery__title">
                    <span><?= Html::encode($page->content[0]->info->title) ?></span>
                    <img alt="" src="/assets/img/plash/car.png">
                    <img alt="" src="/assets/img/plash/car_mob.png">
                </div>
                <div class="delievery__content">
                    <div class="delievery__text">
                        <?= nl2br(Html::encode($page->content[0]->info->text)) ?>
                    </div>
                    <ul class="delievery__menu">
                        <li class="delievery__item">
                            <div class="delievery__value"><?= Yii::t('app', 'Тип доставки') ?></div>
                            <div class="delievery__value"><?= Yii::t('app', 'Срок доставки') ?></div>
                            <div class="delievery__value"><?= Yii::t('app', 'Цена доставки') ?></div>
                            <div class="delievery__value"><?= Yii::t('app', 'Наложенный платёж') ?></div>
                            <div class="delievery__value"><?= Yii::t('app', 'Оплата товара') ?></div>
                        </li>
                        <li class="delievery__item">
                            <div class="delievery__value"><?= Yii::t('app', 'Новой почтой') ?></div>
                            <div class="delievery__value">2-4 дня</div>
                            <div class="delievery__value green"><?= Yii::t('app', 'Бесплатно') ?></div>
                            <div class="delievery__value"><img alt="" src="/assets/img/icon/done.svg"></div>
                            <div class="delievery__value">
                                <span><?= Yii::t('app', 'Картой') ?></span>
                                <span><?= Yii::t('app', 'Наличными') ?></span>
                            </div>
                        </li>
                        <li class="delievery__item">
                            <div class="delievery__value"><?= Yii::t('app', 'Адресная доставка') ?></div>
                            <div class="delievery__value">2-4 дня</div>
                            <div class="delievery__value green"><?= Yii::t('app', 'Бесплатно') ?></div>
                            <div class="delievery__value"><img alt="" src="/assets/img/icon/no-done.svg"></div>
                            <div class="delievery__value">
                                <span><?= Yii::t('app', 'Картой') ?></span>
                                <span><?= Yii::t('app', 'Наличными') ?></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="delievery__body">
                <div class="delievery__title">
                    <span><?= Html::encode($page->content[1]->info->title) ?></span>
                    <img alt="" src="/assets/img/plash/box.png"><img
                        alt="" src="/assets/img/plash/box_mob.png">
                </div>
                <div class="delievery__content">
                    <div class="delievery__text">
                        <?= nl2br(Html::encode($page->content[1]->info->text)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
