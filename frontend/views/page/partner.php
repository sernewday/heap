<?php

/* @var $this \yii\web\View */
/* @var $page \common\models\Pages */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = $page->info->title;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
        <div class="cooperation">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Html::encode($this->title) ?></div>
                <img alt="" src="/assets/img/plash/shopping.png"><img alt="" src="/assets/img/plash/shopping_mob.png">
            </div>
            <div class="cooperation__body">
                <div class="cooperation__text">
                    <?= nl2br(Html::encode($page->content[0]->info->text)) ?>
                </div>
            </div>
        </div>
    </div>
</main>
