<?php

/* @var $this \yii\web\View */
/* @var $page \common\models\Pages */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = $page->info->title;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<div class="container-fluid">
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
        <div class="faq-page">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Html::encode($this->title) ?><span></span></div>
            </div>
            <div class="faq-page__body">
                <div class="faq-page__title"><span>возврат</span><img alt="" src="/assets/img/plash/back.png"><img
                        alt="" src="/assets/img/plash/back_mob.png"></div>
                <div class="faq-page__content">
                    <?php foreach ($page->content as $key => $content): ?>
                        <div class="faq-page__example <?= $key == 0 ? 'active' : '' ?>">
                            <div class="faq-page__title-content">
                                <p><?= Html::encode($content->info->title) ?></p><span></span>
                            </div>
                            <div class="faq-page__text">
                                <p><?= nl2br(Html::encode($content->info->text)) ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
