<?php

/* @var $this \yii\web\View */
/* @var $reviews \common\models\Reviews[] */

use frontend\widgets\Breadcrumbs;

$this->title = Yii::t('app', 'Мои отзывы и вопросы');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="personal-cart">
        <div class="container">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Личный кабинет') ?><span></span></div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart__button-container">
                    <?= $this->render('_tabs') ?>
                </div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart-reviews">
                    <div class="personal-cart-reviews__answer">
                        <div class="personal-cart-reviews__answer-text">
                            <div>i</div>
                            <span>Продавец ответил на ваш вопрос</span>
                        </div>
                        <div class="review review-personal-cart">
                            <div class="review__content">
                                <div class="review__body">
                                    <div class="review__title">
                                        <div class="review__name">ВЫ
                                            <div class="review__date">20 марта</div>
                                        </div>
                                        <div class="review__name-brand">Джинсы-мом с завышеной талией</div>
                                    </div>
                                    <div class="review__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex eпууa commodo consequat. Duis
                                        aute irure dolor in
                                        reprehenderit in voluptate velit.
                                    </div>
                                </div>
                                <div class="review-answer">
                                    <div class="review-answer__body">
                                        <div class="review-answer__title">
                                            <div class="review-answer_name_data">
                                                <div class="review-answer__name">Savage</div>
                                                <div class="review-answer__data">20 марта</div>
                                            </div>
                                            <div class="review-answer__seller"><img
                                                    alt="" src="/assets/img/icon/confirmed.svg">Продавец
                                            </div>
                                        </div>
                                        <div class="review-answer__text">Анна, Lorem ipsum dolor sit amet, consectetur
                                            adipiscing elit,
                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                            ad
                                            mауфуцауцауцуацуацацуinim veniam, quis nауru!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="review__adit link" href="#"><i class="fas fa-long-arrow-alt-right"></i>
                                <p>Перейти к товару</p>
                            </a>
                        </div>
                    </div>
                    <div class="personal-cart-reviews__button-container">
                        <?= $this->render('_reviews-buttons') ?>
                    </div>
                    <div class="personal-cart-reviews__reviews">
                        <?php foreach ($reviews as $review): ?>
                            <?= $this->render('//includes/_review', ['model' => $review, 'can_edit' => true]) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
