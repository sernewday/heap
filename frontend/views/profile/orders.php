<?php

/* @var $this \yii\web\View */

/* @var $orders Orders[] */

use common\models\Orders;
use frontend\components\Helper;
use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Мои заказы');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Мои заказы'), 'url' => '#'];

$js = <<< JS
$('div.product-personal-cart-order__btn.review-product').on('click', function(event) {
    $('#form-review-product').find('input[data-target="record_id"]').val($(this).attr('data-id'));
});

$('div.personal-cart-order__button.review-seller').on('click', function(event) {
    $('#form-seller-review').find('input[data-target="record_id"]').val($(this).attr('data-id'));
    $('#form-seller-review').find('input[data-target="order_id"]').val($(this).attr('data-order'));
});
JS;

$this->registerJs($js);
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="personal-cart">
        <div class="container">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Личный кабинет') ?><span></span></div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart__button-container">
                    <?= $this->render('_tabs') ?>
                </div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart-order-container">
                    <div class="personal-cart__title"><?= Yii::t('app', 'Общая информация') ?> (<?= count($orders) ?>)
                    </div>
                    <div class="personal-cart-order-container__body">
                        <?php foreach ($orders as $order): ?>
                            <div class="personal-cart-order">
                                <div class="personal-cart-order__body">
                                    <div class="personal-cart-order__title">
                                        <div class="personal-cart-order__name"><?= Yii::t('app', 'Заказ') ?>
                                            №<?= $order->id ?></div>
                                        <div class="personal-cart-order__statys">
                                            <img src="<?= $order->statusIcon ?>" alt="<?= $order->statusIcon ?>">
                                            <?= Yii::t('app', $order->statusLabel) ?>
                                        </div>
                                        <div class="personal-cart-order__seller">
                                            <p><?= Yii::t('app', 'Продавец') ?>:</p>
                                            <span><?= $order->seller->name ?></span>
                                        </div>
                                    </div>
                                    <div class="personal-cart-order__cost">
                                        <p><?= Yii::t('app', 'Сумма заказа') ?>:</p>
                                        <span><?= Helper::formatPrice($order->orderSum) ?> грн</span>
                                    </div>
                                    <div class="personal-cart-order__product">

                                        <?php foreach ($order->products as $product): ?>
                                            <div class="product-personal-cart-order">
                                                <a class="product-personal-cart-order__body"
                                                   href="<?= Url::to(['/products/view', 'id' => $product->product_id]) ?>">
                                                    <div class="product-personal-cart-order__img">
                                                        <img alt="" src="<?= $product->product->image->imageUrl ?>">
                                                    </div>
                                                    <div class="product-personal-cart-order__name">
                                                        <?= $product->product->info->name ?>
                                                    </div>
                                                </a>
                                                <div class="product-personal-cart-order__btn review-product"
                                                     data-id="<?= $product->product_id ?>">
                                                    <img alt="" src="/assets/img/product-cart/coment.svg">
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                    <div class="personal-cart-order__button-container">
                                        <?php if ($order->status_id != Orders::STATUS_CANCELED): ?>
                                            <?php if ($order->canBeCancelled()): ?>
                                                <?= Html::a('
                                                <div>
                                                    <svg fill="none" height="11" viewBox="0 0 11 11" width="11"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10 1.00008L5.5 5.5M5.5 5.5L1 1M5.5 5.5L10 9.99991M5.5 5.5L1 10"
                                                              stroke="#6D6D6D"
                                                              stroke-linecap="round" stroke-width="1.2"/>
                                                    </svg>
                                                </div>
                                                <p>' . Yii::t('app', 'Отменить заказ') . '</p>',
                                                    ['/order/cancel', 'id' => $order->id], [
                                                        'class' => 'personal-cart-order__button',
                                                        'data' => [
                                                            'confirm' => Yii::t('app', 'Вы уверены что хотите отменить заказ?'),
                                                            'method' => 'post',
                                                        ],
                                                    ]) ?>
                                            <?php endif; ?>
                                            <div class="personal-cart-order__button review-seller" data-id="<?= $order->seller->id ?>" data-order="<?= $order->id ?>">
                                                <div>
                                                    <svg fill="none" height="15" viewBox="0 0 15 15" width="15"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path clip-rule="evenodd"
                                                              d="M0.498655 14.0182L4.26876 14.0516C5.90411 14.0624 6.6984 14.0547 6.9463 14.0523C6.98732 14.0519 7.01337 14.0516 7.02581 14.0516C11.5451 14.0516 14.9196 9.81246 13.8577 5.38644L13.2724 3.81272C12.104 1.55032 9.74264 0 7.02581 0C3.15197 0 0.000407124 3.15177 0.000407124 7.0258C0.000407124 8.73132 0.604126 10.3443 1.71053 11.6205L0.149367 13.1612C-0.167044 13.4735 0.052141 14.0161 0.498655 14.0182ZM12.3569 4.22009C11.344 2.30397 9.33229 1 7.02581 1C3.70431 1 1.00041 3.70399 1.00041 7.0258C1.00041 8.49117 1.51767 9.8715 2.46611 10.9655L3.0802 11.6738L2.41295 12.3323L1.7071 13.0289L4.27534 13.0516C4.27577 13.0516 4.2762 13.0516 4.27663 13.0516C5.88988 13.0623 6.66877 13.0548 6.932 13.0523C6.97931 13.0519 7.00996 13.0516 7.02581 13.0516C10.8824 13.0516 13.7668 9.44985 12.8986 5.67633L12.3569 4.22009Z"
                                                              fill="#6D6D6D"
                                                              fill-rule="evenodd"/>
                                                        <circle cx="4.43971" cy="7.0425" fill="#6D6D6D" r="0.801042"/>
                                                        <circle cx="7.04323" cy="7.0425" fill="#6D6D6D" r="0.801042"/>
                                                        <circle cx="9.64674" cy="7.0425" fill="#6D6D6D" r="0.801042"/>
                                                    </svg>
                                                </div>
                                                <p><?= Yii::t('app', 'Оставить отзыв о продавце') ?></p>
                                            </div>
                                            <div class="personal-cart-order__button value chat">
                                                <div>
                                                    <svg fill="none" height="13" viewBox="0 0 12 13" width="12"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <rect height="11" rx="5.5" stroke="#353535" width="11" x="0.5"
                                                              y="0.501953"/>
                                                        <path
                                                                d="M5.45312 2.95605H6.54199L6.46387 7.66797H5.53125L5.45312 2.95605ZM5.99512 10.0752C5.81608 10.0752 5.66471 10.015 5.54102 9.89453C5.41732 9.77409 5.35547 9.62435 5.35547 9.44531C5.35547 9.26628 5.41732 9.11654 5.54102 8.99609C5.66471 8.87565 5.81608 8.81543 5.99512 8.81543C6.17741 8.81543 6.3304 8.87565 6.4541 8.99609C6.5778 9.11654 6.63965 9.26628 6.63965 9.44531C6.63965 9.62435 6.5778 9.77409 6.4541 9.89453C6.3304 10.015 6.17741 10.0752 5.99512 10.0752Z"
                                                                fill="#353535"/>
                                                    </svg>
                                                </div>
                                                <p><?= Yii::t('app', 'Чат с продавцом') ?></p>
                                                <span>1</span>
                                            </div>
                                            <?php if ($order->canBeReturned()): ?>
                                                <div class="personal-cart-order__button return">
                                                    <div>
                                                        <svg fill="none" height="10" viewBox="0 0 13 10" width="13"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                    d="M12.3561 7.62247C12.3388 7.6071 12.3213 7.59198 12.3037 7.57709C11.9399 7.27046 11.436 7.00265 10.8366 6.7796C9.6334 6.33184 7.95024 6.02932 5.95998 6.00351L5.45349 5.99694V6.50347V8.2517C5.45349 8.35253 5.35123 8.50167 5.14165 8.50167C5.0576 8.50167 4.97924 8.47179 4.92094 8.42171L4.91999 8.4209L4.59514 8.801L4.91995 8.42087L4.91984 8.42077L4.91938 8.42038L4.91754 8.41881L4.91021 8.41254L4.88145 8.38795L4.77128 8.29368C4.67587 8.21199 4.53843 8.09426 4.37042 7.95012C4.03437 7.66182 3.57611 7.26798 3.08719 6.84573C2.10677 5.99901 1.01322 5.04679 0.528067 4.59751C0.520944 4.59071 0.5 4.56563 0.5 4.50181C0.5 4.43689 0.521658 4.41232 0.528006 4.4064L0.187104 4.04063M12.3561 7.62247L0.187104 4.96323C0.0665996 4.85068 0 4.68711 0 4.50181C0 4.31652 0.0665996 4.15295 0.187104 4.04063M12.3561 7.62247C11.7539 5.1016 9.17457 3.10709 5.93785 3.00577L5.45349 2.99061V2.50602V0.75193C5.45349 0.651086 5.35124 0.501953 5.14165 0.501953C5.05764 0.501953 4.97925 0.531834 4.92091 0.581939L4.91999 0.582728L4.59514 0.202625M12.3561 7.62247L4.59514 0.202625M0.187104 4.04063C1.17178 3.1283 4.59514 0.202625 4.59514 0.202625M0.187104 4.04063L0.526927 4.4074C1.01145 3.95848 2.10597 3.00536 3.08719 2.15791C3.57612 1.73565 4.03437 1.3418 4.37042 1.0535C4.53843 0.909362 4.67587 0.79163 4.77128 0.70995L4.88145 0.615678L4.91021 0.591085L4.91754 0.58482L4.91938 0.583245L4.91984 0.582853L4.91995 0.582756L4.59514 0.202625"
                                                                    stroke="#6D6D6D"/>
                                                        </svg>
                                                    </div>
                                                    <p><?= Yii::t('app', 'Возврат товара') ?></p>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
