<?php

/* @var $this \yii\web\View */
/* @var $questions \common\models\Questions[] */

use frontend\widgets\Breadcrumbs;

$this->title = Yii::t('app', 'Ваши вопросы');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => '#'];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="personal-cart">
        <div class="container">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Личный кабинет') ?><span></span></div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart__button-container">
                    <?= $this->render('_tabs') ?>
                </div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart-reviews">
                    <div class="personal-cart-reviews__button-container">
                        <?= $this->render('_reviews-buttons') ?>
                    </div>
                    <div class="personal-cart-reviews__reviews">
                        <?php foreach ($questions as $question): ?>
                            <?= $this->render('//includes/_question', ['model' => $question, 'can_go' => true]) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
