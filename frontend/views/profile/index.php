<?php

/* @var $this \yii\web\View */

use common\models\User;
use frontend\components\Helper;
use frontend\components\Size;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CustomActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $user User */
/* @var $model \frontend\models\ProfileForm */
/* @var $model_family \frontend\models\FamilyForm */
/* @var $profileProgress int */
/* @var $family \common\models\UserFamily[] */

$this->title = Yii::t('app', 'Личный кабинет');

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Мои данные'), 'url' => '#'];
?>

<main>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="personal-cart">
        <div class="container">
            <div class="title">
                <div class="title__line"></div>
                <div class="title__text"><?= Yii::t('app', 'Личный кабинет') ?><span></span></div>
            </div>
            <div class="personal-cart__body">
                <div class="personal-cart__button-container">
                    <?= $this->render('_tabs') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="personal-cart-progres">
        <div class="container">
            <div class="personal-cart-progres__body mes">
                <div class="personal-cart-progres__mes">
                    <?php if ($user->confirmed_email == 0 || $user->confirmed_phone == 0): ?>
                        <div>i</div>
                        <span><?= Yii::t('app', 'Аккаунт не подтвержден') ?>.</span>
                        <p>
                            Подтвердите свой
                            <?php if ($user->confirmed_email == 0): ?>
                                <a href="#" class="confirm-mail-done" style="text-decoration: underline;">e-mail</a>
                            <?php endif; ?>
                            <?php if ($user->confirmed_phone == 0 && $user->confirmed_email == 0 ): ?>
                                и
                            <?php endif; ?>
                            <?php if ($user->confirmed_phone == 0): ?>
                                <a href="#" class="open-modal" data-target="confirm-phone-done" style="text-decoration: underline;">телефон</a>
                            <?php endif; ?>
                            для возможности оформить заказ.
                        </p>
                    <?php endif; ?>
                </div>
                <div class="personal-cart-progres__container">
                    <div class="personal-cart-progres__text"><?= Yii::t('app', 'Прогресс заполнения аккаунта') ?></div>
                    <div class="personal-cart-progres__bar" data-value="<?= $profileProgress ?>">
                        <span></span>
                    </div>
                    <div class="personal-cart-progres__value">
                        <p><?= $profileProgress ?></p>
                        <span>%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="personal-cart">

        <div class="container">
            <div class="personal-cart__body">
                <?php $form = CustomActiveForm::begin(['options' => ['class' => 'form-personal-cart__info-data']]) ?>
                <div class="personal-cart__content">
                    <div class="personal-cart-main-info">
                        <div class="personal-cart-main-info__body">
                            <div class="personal-cart__title">
                                <?= Yii::t('app', 'Общая информация') ?>
                                <div class="personal-cart__corect">
                                    <img alt="" src="/assets/img/personal-cart/pen.svg">
                                </div>
                            </div>

                            <?= $form->field($model, 'name')->textInput() ?>

                            <?= $form->field($model, 'surname')->textInput() ?>

                            <?= $form->field($model, 'phone')->textInput() ?>

                            <?= $form->field($model, 'email')->textInput() ?>
                        </div>
                        <div class="personal-cart-main-info__body">
                            <div class="personal-cart__title no-line">ПАРОЛЬ
                                <div class="personal-cart__corect"><img
                                            alt="" src="/assets/img/personal-cart/pen.svg"></div>
                            </div>
                            <?= $form->field($model, 'new_password')->passwordInput() ?>
                        </div>
                        <div class="personal-cart-main-info__body">
                            <div class="personal-cart__title">
                                <?= Yii::t('app', 'Адрес доставки') ?>
                                <div class="personal-cart__corect">
                                    <img alt="" src="/assets/img/personal-cart/pen.svg">
                                </div>
                            </div>
                            <?= $form->field($model, 'city')->textInput() ?>

                            <?= $form->field($model, 'street')->textInput() ?>

                            <div class="personal-cart-main-info__row">
                                <?= $form->field($model, 'house')->textInput() ?>

                                <?= $form->field($model, 'flat')->textInput() ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit"><?= Yii::t('app', 'Сохранить') ?></button>
                </div>
                <div class="personal-cart__content">
                    <div class="personal-cart-date">
                        <div class="personal-cart-date__body">
                            <div class="personal-cart__title"><?= Yii::t('app', 'Дата рождения') ?></div>
                            <div class="personal-cart__text"><img alt="" src="/assets/img/personal-cart/present.svg">
                                <?= Yii::t('app', 'Укажите полную дату рождения для получания дополнительных скидок') ?>
                            </div>
                            <div class="personal-cart__row-three">
                                <?= $form->field($model, 'birthday_year')->textInput() ?>

                                <?= $form->field($model, 'birthday_month')->dropDownList(Helper::getMonthsList()) ?>

                                <?= $form->field($model, 'birthday_day')->textInput() ?>
                            </div>
                        </div>
                        <div class="personal-cart-date__body">
                            <div class="personal-cart__title"><?= Yii::t('app', 'Пол') ?></div>
                            <div class="personal-cart__row">
                                <div class="radio-button" id="">
                                    <label class="radio-button__body">
                                        <input <?= $model->gender == User::GENDER_FEMALE ? 'checked' : '' ?>
                                                class="radio-button__input"
                                                name="<?= Html::getInputName($model, 'gender') ?>" type="radio"
                                                value="<?= User::GENDER_FEMALE ?>">
                                        <span class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text"><?= Yii::t('app', 'Женский') ?></span>
                                    </label>
                                </div>
                                <div class="radio-button" id="">
                                    <label class="radio-button__body">
                                        <input <?= $model->gender == User::GENDER_MALE ? 'checked' : '' ?>
                                                class="radio-button__input"
                                                name="<?= Html::getInputName($model, 'gender') ?>" type="radio"
                                                value="<?= User::GENDER_MALE ?>">
                                        <span class="radio-button__fake"><span></span></span>
                                        <span class="radio-button__text"><?= Yii::t('app', 'Мужской') ?></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="personal-cart-date__body">
                            <div class="personal-cart__title">
                                <?= Yii::t('app', 'Размеры') ?>
                                <div class="personal-cart__corect"><img
                                            alt="" src="/assets/img/personal-cart/pen.svg"></div>
                            </div>
                            <div class="personal-cart__row-two">
                                <?= $form->field($model, 'size_clothes')->dropDownList(Size::getClothesSize()) ?>
                                <?= $form->field($model, 'size_shoes')->dropDownList(Size::getShoesSize()) ?>
                            </div>
                            <div class="personal-cart__row-three">
                                <?= $form->field($model, 'size_chest')->dropDownList(Size::getChestSize()) ?>
                                <?= $form->field($model, 'size_waist')->dropDownList(Size::getWaistSize()) ?>
                                <?= $form->field($model, 'size_hips')->dropDownList(Size::getHipsSize()) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php CustomActiveForm::end() ?>

                <?php $form_family = CustomActiveForm::begin(['options' => ['class' => 'personal-cart__content']]) ?>
                <div class="personal-cart-dop">
                    <div class="personal-cart-dop__body">
                        <div class="personal-cart__title"><?= Yii::t('app', 'Дополнительная информация') ?></div>
                        <div class="personal-cart__text">
                            <img alt="" src="/assets/img/personal-cart/famile.svg">
                            <?= Yii::t('app', 'Укажите информацию о своих близких, что б покупать товары для них напрямую') ?>
                        </div>
                        <div class="personal-cart-person__content">
                            <?php foreach ($family as $family_item): ?>
                                <div class="personal-cart-person">
                                    <div class="personal-cart-person__body">
                                        <div class="personal-cart-person__title">
                                            <div class="personal-cart-person__name">
                                                <img alt=""
                                                     src="/assets/img/personal-cart/<?= $family_item->personIcon ?>">
                                                <span>
                                                    <?= Helper::getNameByFIO($family_item->full_name) ?>,
                                                    <?= Helper::getFormattedDateFromString($family_item->birthday) ?>
                                                </span>
                                            </div>
                                            <div class="personal-cart-person__corect">
                                                <img alt="" src="/assets/img/personal-cart/pen.svg">
                                            </div>
                                        </div>
                                        <div class="personal-cart-person__row">
                                            <div class="personal-cart-person__text">
                                                <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M6.37509 0H5.43769C5.43769 0.551495 5.0169 1.00002 4.5 1.00002C3.9831 1.00002 3.56259 0.551495 3.56259 0H2.62505L0 1.6L0.750094 3.79998L1.87509 3.39994V8H7.12519V3.39994L8.25019 3.79998L9 1.6L6.37509 0Z"
                                                            fill="#A4A4A4"/>
                                                </svg>
                                                <p><?= Yii::t('app', 'Размер одежды') ?>: </p>
                                                <span><?= $family_item->size_clothes ?></span>
                                            </div>
                                            <div class="personal-cart-person__text">
                                                <svg fill="none" height="12" viewBox="0 0 8 12"
                                                     width="8" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M5.43347 11.0483C5.54051 11.0556 5.64793 11.0417 5.7496 11.0075C5.85126 10.9732 5.94519 10.9193 6.026 10.8487C6.10682 10.7781 6.17295 10.6924 6.22061 10.5962C6.26827 10.5001 6.29653 10.3956 6.30378 10.2885L6.33964 9.75945L4.75022 9.04883L4.67368 10.1781C4.65931 10.3942 4.73123 10.6072 4.87366 10.7703C5.0161 10.9335 5.21742 11.0334 5.43347 11.0483Z"
                                                            fill="#A4A4A4"/>
                                                    <path
                                                            d="M4.85668 7.79988L6.54009 8.59221L7.08509 6.64085C7.47518 4.8456 7.23399 3.50424 6.4232 2.96027C5.94768 2.64115 5.43299 2.70617 5.13387 2.91223C5.13049 2.91459 5.127 2.91674 5.12343 2.91877C5.12107 2.92009 4.85257 3.07725 4.61708 3.42536C4.30308 3.88927 4.19198 4.45745 4.28656 5.11427L4.85668 7.79988Z"
                                                            fill="#A4A4A4"/>
                                                    <path
                                                            d="M2.51267 6.31152L0.921875 7.01928L0.956651 7.54849C0.963704 7.65554 0.991773 7.76016 1.03925 7.85636C1.08674 7.95256 1.1527 8.03847 1.23339 8.10917C1.31407 8.17988 1.40789 8.23401 1.50949 8.26846C1.61109 8.30291 1.71848 8.31701 1.82553 8.30995C1.93258 8.3029 2.03719 8.27483 2.13339 8.22735C2.22959 8.17987 2.3155 8.1139 2.38621 8.03322C2.45692 7.95253 2.51104 7.85871 2.54549 7.75711C2.57994 7.65551 2.59404 7.54812 2.58699 7.44108L2.51267 6.31152Z"
                                                            fill="#A4A4A4"/>
                                                    <path
                                                            d="M0.721081 5.85151L2.40598 5.06233L2.98122 2.37768C3.07696 1.72105 2.96682 1.15268 2.65382 0.688178C2.41896 0.339621 2.15067 0.18194 2.14801 0.180405C2.1447 0.178493 2.14109 0.176203 2.13795 0.174054C1.83925 -0.0325206 1.3247 -0.0985302 0.848543 0.21969C0.0367156 0.762191 -0.206948 2.10313 0.17976 3.89911L0.721081 5.85151Z"
                                                            fill="#A4A4A4"/>
                                                </svg>
                                                <p><?= Yii::t('app', 'Размер обуви') ?>: </p>
                                                <span><?= $family_item->size_shoes ?></span>
                                            </div>
                                        </div>
                                        <div class="personal-cart-person__row">
                                            <div class="personal-cart-person__text">
                                                <svg fill="none" height="10" viewBox="0 0 10 10"
                                                     width="10" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M8.84314 5.43869C8.84314 5.78531 8.74279 6.10937 8.56915 6.38462C8.39552 6.65988 8.32946 6.98858 8.10088 7.08834C7.87231 7.1881 7.61938 7.24366 7.35342 7.24366H3.86852C3.60256 7.24366 3.34963 7.1881 3.12106 7.08834C2.89249 6.98858 2.47338 6.65988 2.29977 6.38462C2.12615 6.10937 2.01953 5.78735 2.01953 5.44071L2.02055 5.26661C2.02344 4.77255 2.42489 4.37363 2.91896 4.37386L3.08631 4.37393L3.12106 2.75293H3.86852V4.3531H7.09388V2.77377H7.88681V4.37393L7.94467 4.3739C8.44081 4.37365 8.84314 4.77578 8.84314 5.27192V5.43869Z"
                                                            fill="#6D6D6D"/>
                                                    <path
                                                            d="M1.48604 3.84019C1.91275 2.98677 2.73062 2.75275 3.08621 2.75275L3.84137 2.75264C4.20058 2.75264 4.11078 2.02997 4.11078 1.22176C4.11078 0.952529 4.64959 0.890782 4.64959 1.22179V2.5C4.64959 2.9016 4.39124 3.20164 4.02098 3.20164C3.76416 3.20164 3.37344 3.23856 3.10411 3.24201C3.09831 3.24221 3.09235 3.24231 3.08621 3.24231C2.65692 3.24231 2.39672 3.42217 2.16742 3.65076C1.89695 3.92038 1.80927 4.09956 1.62967 5.44657C1.48598 6.52419 1.4779 8.69413 1.48989 9.48763C1.48989 9.62157 1.36412 9.71137 1.27408 9.71137C1.00491 9.71137 1 9.57562 1 9.39782C1 7.97545 1.05933 4.69361 1.48604 3.84019Z"
                                                            fill="#6D6D6D"/>
                                                    <path
                                                            d="M2.78385 9.72372C2.51468 9.72372 2.50977 9.58797 2.50977 9.41017C2.50977 8.46483 2.53597 6.6981 2.67883 5.36885L3.13943 5.45892C2.99575 6.53654 2.98767 8.70648 2.99966 9.49997C2.99966 9.63392 2.87388 9.72372 2.78385 9.72372Z"
                                                            fill="#6D6D6D"/>
                                                    <path
                                                            d="M8.35483 9.72372C8.624 9.72372 8.62891 9.58797 8.62891 9.41017C8.62891 8.46483 8.6027 6.6981 8.45985 5.36885L7.99924 5.45892C8.14292 6.53654 8.15101 8.70648 8.13901 9.49997C8.13901 9.63392 8.26479 9.72372 8.35483 9.72372Z"
                                                            fill="#6D6D6D"/>
                                                    <path
                                                            d="M9.49443 3.83997C9.06772 2.98654 8.24985 2.75253 7.89426 2.75253H7.06931C6.71011 2.75253 6.71011 2.03388 6.71011 1.22567C6.71011 0.956435 6.1713 0.894688 6.1713 1.22569C6.1713 1.7645 6.1713 2.12366 6.2611 2.57267C6.33986 2.96647 6.51945 3.20153 6.88971 3.20153C7.14676 3.20153 7.60147 3.23841 7.87665 3.24179C7.88235 3.24199 7.88822 3.24208 7.89426 3.24208C8.32354 3.24208 8.58375 3.42195 8.81305 3.65054C9.08352 3.92015 9.1712 4.09933 9.3508 5.44635C9.49448 6.52396 9.50257 8.6939 9.49058 9.4874C9.49058 9.62134 9.61635 9.71115 9.70639 9.71115C9.97556 9.71115 9.98047 9.5754 9.98047 9.3976C9.98047 7.97523 9.92114 4.69339 9.49443 3.83997Z"
                                                            fill="#6D6D6D"/>
                                                    <path
                                                            d="M8.32851 7.70428C8.12568 8.18282 8.02285 8.6889 8.02285 9.20849C8.02285 9.20849 8.07681 9.69668 7.51544 9.69668C6.62612 9.69668 3.7352 9.67373 3.7352 9.67373C3.38935 9.64097 3.18063 9.37489 3.22779 9.20851C3.22779 8.68892 3.12496 8.18282 2.92215 7.70433C2.54651 6.8176 2.01953 6.29687 2.01953 5.3339C2.01953 5.19378 2.13314 5.08019 2.27324 5.08019C2.41334 5.08019 2.52695 5.19378 2.52695 5.3339C2.52695 6.02251 2.96873 6.28004 3.17618 6.92994L3.34883 7.40727C3.3622 7.44032 3.37542 7.47343 3.38935 7.50634C3.61883 8.04777 3.7352 8.6205 3.7352 9.20849C4.1863 9.21172 6.82003 9.20851 7.51544 9.20846C7.51544 8.62047 7.63181 8.04777 7.86131 7.50629C7.87522 7.47343 7.88841 7.44037 7.90178 7.40737L8.07448 6.93004C8.28191 6.28015 8.32947 6.02245 8.32947 5.33379C8.32947 5.19366 8.44308 5.08008 8.58318 5.08008C8.72328 5.08008 8.83689 5.19366 8.83689 5.33379C8.83689 6.29681 8.70413 6.81763 8.32851 7.70428Z"
                                                            fill="#6D6D6D"/>
                                                </svg>
                                                <p><?= Yii::t('app', 'Обхват груди') ?>:</p>
                                                <span><?= $family_item->size_chest ?></span>
                                            </div>
                                            <div class="personal-cart-person__text">
                                                <svg fill="none" height="13" viewBox="0 0 9 13"
                                                     width="9" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M7.90545 7.80056C7.90372 7.7977 7.90196 7.79487 7.90015 7.79205C7.4252 7.05844 7.17416 6.20842 7.17416 5.33393C7.17416 4.7242 7.29483 4.13034 7.53285 3.56879C7.97363 2.52832 8.19712 1.4278 8.19712 0.297719C8.19712 0.133289 8.0638 0 7.8994 0C7.735 0 7.60169 0.133289 7.60169 0.297719C7.60169 1.10585 7.47816 1.8976 7.23475 2.66024C5.36998 2.04572 3.35148 2.04569 1.48672 2.66012C1.24327 1.89748 1.11975 1.10579 1.11975 0.297719C1.11975 0.133289 0.986432 0 0.822031 0C0.657631 0 0.524313 0.133289 0.524313 0.297719C0.524313 1.42774 0.74781 2.52829 1.18861 3.56885C1.42661 4.13034 1.54727 4.72423 1.54727 5.33396C1.54727 6.20842 1.29621 7.05844 0.821287 7.79211C0.819471 7.7949 0.817715 7.7977 0.816018 7.80056C0.282089 8.62902 0 9.58774 0 10.5738V10.8415V12.4838C0 12.6482 0.133319 12.7815 0.297719 12.7815C0.462119 12.7815 3.7626 12.7815 3.7626 12.7815C3.927 12.7815 4.06032 12.6482 4.06032 12.4838V11.5C4.26027 11.5048 4.46087 11.5048 4.66082 11.5V12.4838C4.66082 12.6482 4.79414 12.7815 4.95854 12.7815C4.95854 12.7815 8.25929 12.7815 8.42369 12.7815C8.58809 12.7815 8.72141 12.6483 8.72141 12.4838V10.8414V10.5738C8.72141 9.58774 8.43926 8.62896 7.90545 7.80056ZM2.14271 5.33393C2.14271 4.64394 2.00615 3.97187 1.73686 3.33651C1.72052 3.29789 1.70501 3.25904 1.68932 3.22025C3.4232 2.65279 5.2982 2.65279 7.03209 3.22037C7.0164 3.2591 7.00092 3.29789 6.9846 3.33645C6.71529 3.97187 6.57872 4.64391 6.57872 5.3339C6.57872 6.32021 6.86099 7.27911 7.39504 8.10748C6.42328 8.47519 5.40428 8.66228 4.36066 8.66228C3.31706 8.66228 2.298 8.47543 1.32625 8.10772C1.86039 7.27931 2.14271 6.32027 2.14271 5.33393Z"
                                                            fill="#A4A4A4"/>
                                                    <path
                                                            d="M4.36059 6.96825C4.5209 6.96825 4.65086 6.83829 4.65086 6.67797C4.65086 6.51766 4.5209 6.3877 4.36059 6.3877C4.20027 6.3877 4.07031 6.51766 4.07031 6.67797C4.07031 6.83829 4.20027 6.96825 4.36059 6.96825Z"
                                                            fill="#A4A4A4"/>
                                                </svg>
                                                <p><?= Yii::t('app', 'Обхват талии') ?>:</p>
                                                <span><?= $family_item->size_waist ?></span>
                                            </div>
                                            <div class="personal-cart-person__text">
                                                <svg fill="none" height="12" viewBox="0 0 9 12"
                                                     width="9" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M7.90015 2.80578L7.90545 2.8143C8.43926 3.6427 8.72141 4.60147 8.72141 5.58752V5.85517L8.29772 11.0137C8.29772 11.1781 8.1644 11.3114 8 11.3114H4.79772C4.63332 11.3114 4.5 11.1781 4.5 11.0137L4.66082 6.51374C4.46087 6.51853 4.26027 6.51853 4.06032 6.51374L4.36066 11.0137C4.36066 11.1781 4.22734 11.3114 4.06294 11.3114L1 11.3114C0.8356 11.3114 0.702281 11.1781 0.702281 11.0137L0 5.8552V5.58755C0 4.60147 0.282089 3.64276 0.816018 2.8143L0.821287 2.80584C1.29621 2.07217 1.54727 1.22216 1.54727 0.347696C1.54727 0.347696 1.5 0.0136719 2 0.0136518C2.17578 0.0136448 2.14271 0.347666 2.14271 0.347666C2.14271 1.33401 1.86039 2.29305 1.32625 3.12145C2.298 3.48917 3.31706 3.67601 4.36066 3.67601C5.40428 3.67601 6.42328 3.48893 7.39504 3.12122C6.86099 2.29284 6.57872 1.33395 6.57872 0.347636C6.57872 0.347636 6.50904 -0.0810106 7 0.0136518C7.17416 0.0472321 7.17416 0.347666 7.17416 0.347666C7.17416 1.22216 7.4252 2.07217 7.90015 2.80578Z"
                                                            fill="#A4A4A4"/>
                                                    <path
                                                            d="M4.36059 1.98192C4.5209 1.98192 4.65086 1.85196 4.65086 1.69164C4.65086 1.53133 4.5209 1.40137 4.36059 1.40137C4.20027 1.40137 4.07031 1.53133 4.07031 1.69164C4.07031 1.85196 4.20027 1.98192 4.36059 1.98192Z"
                                                            fill="#A4A4A4"/>
                                                </svg>
                                                <p><?= Yii::t('app', 'Обхват бедер') ?>:</p>
                                                <span><?= $family_item->size_hips ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="personal-cart__add-person"><span></span>
                            <p><?= Yii::t('app', 'Добавить человека') ?></p>
                        </div>
                        <div class="personal-cart-add-person">

                            <?= $form_family->field($model_family, 'full_name')->textInput() ?>

                            <div class="personal-cart__row center">
                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input checked class="radio-button__input"
                                               name="<?= Html::getInputName($model_family, 'gender') ?>"
                                               type="radio" value="<?= User::GENDER_FEMALE ?>">
                                        <span class="radio-button__fake">
                                            <span></span>
                                        </span>
                                        <span class="radio-button__text"><?= Yii::t('app', 'Женский') ?></span>
                                    </label>
                                </div>

                                <div class="radio-button">
                                    <label class="radio-button__body">
                                        <input class="radio-button__input"
                                               name="<?= Html::getInputName($model_family, 'gender') ?>"
                                               type="radio"
                                               value="<?= User::GENDER_MALE ?>">
                                        <span class="radio-button__fake">
                                            <span></span>
                                        </span>
                                        <span class="radio-button__text"><?= Yii::t('app', 'Мужской') ?></span>
                                    </label>
                                </div>

                            </div>
                            <div class="personal-cart__row-three">

                                <?= $form_family->field($model_family, 'birthday_year')->textInput() ?>

                                <?= $form_family->field($model_family, 'birthday_month')->dropDownList(Helper::getMonthsList()) ?>

                                <?= $form_family->field($model_family, 'birthday_day')->textInput() ?>

                            </div>
                            <div class="personal-cart__row-two">
                                <?= $form_family->field($model_family, 'size_clothes')->dropDownList(Size::getClothesSize()) ?>

                                <?= $form_family->field($model_family, 'size_shoes')->dropDownList(Size::getShoesSize()) ?>
                            </div>
                            <div class="personal-cart__row-three">
                                <?= $form_family->field($model_family, 'size_chest')->dropDownList(Size::getChestSize()) ?>

                                <?= $form_family->field($model_family, 'size_waist')->dropDownList(Size::getWaistSize()) ?>

                                <?= $form_family->field($model_family, 'size_hips')->dropDownList(Size::getHipsSize()) ?>
                            </div>

                            <button type="submit" class="more-link hover-button">
                                <div class="hover-button__front"><span><?= Yii::t('app', 'Сохранить') ?></span></div>
                                <div class="hover-button__back"><span><?= Yii::t('app', 'Сохранить') ?></span></div>
                            </button>
                        </div>
                    </div>
                </div>
                <?php CustomActiveForm::end() ?>

            </div>
        </div>


    </div>
</main>
