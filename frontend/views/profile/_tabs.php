<?php

/* @var $this \yii\web\View */

use common\models\Orders;
use yii\helpers\Url;

$action = Yii::$app->controller->action->id;
?>

<a href="<?= Url::to(['/profile/index']) ?>" class="personal-cart__button <?= $action == 'index' ? 'active' : '' ?>">
    <?= Yii::t('app', 'Мои данные') ?>
</a>

<a href="<?= Url::to(['/profile/orders']) ?>" class="personal-cart__button value <?= $action == 'orders' ? 'active' : '' ?>">
    <?= Yii::t('app', 'Мои заказы') ?>
    <span><?= Orders::getAmountActiveByUser(Yii::$app->user->getId()) ?></span>
</a>

<a href="<?= Url::to(['/profile/reviews']) ?>" class="personal-cart__button value <?= $action == 'reviews' ? 'active' : '' ?>">
    <?= Yii::t('app', 'Мои отзывы и вопросы') ?>
    <span>1</span>
</a>

<a href="#" class="personal-cart__button value">
    <?= Yii::t('app', 'Промокоды и сертификаты') ?>
    <span>1</span>
</a>
