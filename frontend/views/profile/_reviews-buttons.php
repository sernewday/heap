<?php

/* @var $this \yii\web\View */

use common\models\Questions;
use common\models\Reviews;
use yii\helpers\Url;

$action = Yii::$app->controller->action->id;
?>

<a class="personal-cart-reviews__button <?= $action == 'questions' ? 'active' : '' ?>" href="<?= Url::to(['/profile/questions']) ?>">
    <?= Yii::t('app', 'Ваши вопросы') ?>
    <div class="personal-cart-reviews__button__value">
        <?= Questions::getCountByUser(Yii::$app->user->getId()) ?>
    </div>
</a>
<a class="personal-cart-reviews__button <?= $action == 'reviews' ? 'active' : '' ?>" href="<?= Url::to(['/profile/reviews']) ?>">
    <?= Yii::t('app', 'Отзывы о товарах') ?>
    <div class="personal-cart-reviews__button__value">
        <?= Reviews::getCountByUser(Yii::$app->user->getId()) ?>
    </div>
</a>
<a class="personal-cart-reviews__button" href="#">
    <?= Yii::t('app', 'Отзывы о продавцах') ?>
    <div class="personal-cart-reviews__button__value">5</div>
</a>
