<?php

/* @var $this \yii\web\View */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use frontend\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Избранное');

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

$breadcrumbs = [];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
$breadcrumbs[] = ['label' => Yii::t('app', 'Личный кабинет'), 'url' => ['/profile/index']];
$breadcrumbs[] = ['label' => $this->title, 'url' => '#'];
?>

<main>
    <div class="container-fluid"></div>
    <div class="container">
        <div class="prev-link">
            <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
        </div>
    </div>
    <div class="personal-cart-like">
        <div class="container">
            <div class="product-catalog-container__title">
                <div class="title">
                    <div class="title__line"></div>
                    <div class="title__text">
                        <?= Html::encode($this->title) ?>
                        <span>(<?= Yii::t('app', '{0} товаров', $dataProvider->totalCount) ?>)</span>
                    </div>
                </div>
                <div class="product-catalog-container-filter">
                    <div class="options__conteiner">
                        <div class="options">
                            <div class="options__body">
                                <div class="options__value">
                                    <span class="value">
                                        <div class="options__img">
                                            <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                 xmlns="http://www.w3.org/2000/svg">
<path d="M13.2512 0H9.75122C9.25122 0 8.75181 0.158509 8.75144 0.991307C8.75126 1.40471 9.27761 1.5 9.75122 1.5L11.7512 1.5L7.75122 5.5L6.25122 3.5C5.75122 3 4.75122 3 4.2512 3.5L0.251261 7.5C-0.0838397 7.85976 -0.083655 8.14025 0.251223 8.5C0.586324 8.85976 0.91612 8.85952 1.25122 8.5L5.25122 4.5L7.25122 7C7.41207 7.17268 7.52358 7.23019 7.75122 7.23019C7.97864 7.23019 8.09037 7.17268 8.25122 7L12.7512 2.49999V4.36677C12.7512 4.87523 12.9196 5.28035 13.3934 5.28035C13.8673 5.28035 14.0664 4.87523 14.0664 4.36677V0.991316C14.0664 0.482622 13.7251 0 13.2512 0Z"
      fill="#EE4A2E"/>
</svg>
                                        </div>По популярности
                                    </span>
                                    <i class="fas fa-chevron-up"></i>
                                </div>
                                <div class="options__content">
                                    <div class="options__opt" data-value="0">
                                        <div class="options__img">
                                            <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13.2512 0H9.75122C9.25122 0 8.75181 0.158509 8.75144 0.991307C8.75126 1.40471 9.27761 1.5 9.75122 1.5L11.7512 1.5L7.75122 5.5L6.25122 3.5C5.75122 3 4.75122 3 4.2512 3.5L0.251261 7.5C-0.0838397 7.85976 -0.083655 8.14025 0.251223 8.5C0.586324 8.85976 0.91612 8.85952 1.25122 8.5L5.25122 4.5L7.25122 7C7.41207 7.17268 7.52358 7.23019 7.75122 7.23019C7.97864 7.23019 8.09037 7.17268 8.25122 7L12.7512 2.49999V4.36677C12.7512 4.87523 12.9196 5.28035 13.3934 5.28035C13.8673 5.28035 14.0664 4.87523 14.0664 4.36677V0.991316C14.0664 0.482622 13.7251 0 13.2512 0Z"
                                                      fill="#EE4A2E"/>
                                            </svg>
                                        </div>
                                        По популярности
                                    </div>
                                    <div class="options__opt" data-value="0">
                                        <div class="options__img">
                                            <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13.2512 0H9.75122C9.25122 0 8.75181 0.158509 8.75144 0.991307C8.75126 1.40471 9.27761 1.5 9.75122 1.5L11.7512 1.5L7.75122 5.5L6.25122 3.5C5.75122 3 4.75122 3 4.2512 3.5L0.251261 7.5C-0.0838397 7.85976 -0.083655 8.14025 0.251223 8.5C0.586324 8.85976 0.91612 8.85952 1.25122 8.5L5.25122 4.5L7.25122 7C7.41207 7.17268 7.52358 7.23019 7.75122 7.23019C7.97864 7.23019 8.09037 7.17268 8.25122 7L12.7512 2.49999V4.36677C12.7512 4.87523 12.9196 5.28035 13.3934 5.28035C13.8673 5.28035 14.0664 4.87523 14.0664 4.36677V0.991316C14.0664 0.482622 13.7251 0 13.2512 0Z"
                                                      fill="#EE4A2E"/>
                                            </svg>
                                        </div>
                                        По возрастанию цены
                                    </div>
                                    <div class="options__opt" data-value="1">
                                        <div class="options__img">
                                            <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0.815186 8.76978H4.31519C4.81519 8.76978 5.3146 8.61127 5.31496 7.77847C5.31514 7.36507 4.7888 7.26978 4.31519 7.26978L2.31519 7.26977L6.31519 3.26978L7.81519 5.26977C8.31519 5.76978 9.31519 5.76978 9.8152 5.26977L13.8151 1.26977C14.1502 0.910018 14.1501 0.62953 13.8152 0.269774C13.4801 -0.0899811 13.1503 -0.0897408 12.8152 0.269774L8.81519 4.26977L6.81519 1.76977C6.65434 1.59709 6.54283 1.53959 6.31519 1.53959C6.08776 1.53959 5.97603 1.59709 5.81519 1.76977L1.31519 6.26978V4.403C1.31519 3.89455 1.1468 3.48942 0.672962 3.48942C0.199129 3.48942 4.19617e-05 3.89455 4.19617e-05 4.403V7.77846C4.19617e-05 8.28715 0.341352 8.76978 0.815186 8.76978Z"
                                                      fill="#EE4A2E"/>
                                            </svg>
                                        </div>
                                        По убыванию цены
                                    </div>
                                    <div class="options__opt" data-value="2">
                                        <div class="options__img">
                                            <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12 6L11.1796 4.61198L11.197 2.99913L9.79095 2.20905L9.00087 0.803039L7.38802 0.820359L6 0L4.61198 0.820383L2.99913 0.803039L2.20905 2.20908L0.803039 2.99916L0.820359 4.61201L0 6L0.820359 7.38802L0.803016 9.00087L2.20903 9.79095L2.99911 11.197L4.61198 11.1796L6 12L7.38802 11.1796L9.00087 11.197L9.79095 9.79097L11.197 9.00089L11.1796 7.38802L12 6ZM4.27453 2.61619C4.81205 2.34159 5.39255 2.20235 6 2.20235C6.60743 2.20235 7.18795 2.34157 7.72547 2.61619C8.23781 2.87794 8.69173 3.26004 9.03811 3.7212L8.47467 4.14445C7.88395 3.3581 6.98198 2.90709 6 2.90709C5.01802 2.90709 4.11602 3.35808 3.52535 4.14445L2.96191 3.7212C3.30832 3.26004 3.76221 2.87794 4.27453 2.61619ZM6.13523 5.73996V6.23114H5.28V6.49697H6.29126V7.01126H4.60969V4.98874H6.25371V5.50303H5.28V5.73996H6.13523ZM2.3445 4.98877H2.90503L3.67359 5.90756V4.98877H4.33814V7.01128H3.77761L3.00905 6.09248V7.01128H2.3445V4.98877ZM7.72544 9.38381C7.18795 9.65841 6.60743 9.79765 5.99998 9.79765C5.39255 9.79765 4.812 9.65843 4.27451 9.38381C3.76216 9.12206 3.30827 8.73996 2.96187 8.2788L3.52535 7.85555C4.11602 8.64192 5.01797 9.09293 6 9.09293C6.98198 9.09293 7.88395 8.64192 8.47465 7.85555L9.03809 8.2788C8.69168 8.73996 8.23779 9.12206 7.72544 9.38381ZM9.11123 7.01128H8.38024L8.05376 5.93067L7.70993 7.01128H6.97894L6.33173 4.98877H7.03383L7.38633 6.1387L7.75906 4.98877H8.38603L8.73853 6.15316L9.11126 4.98877H9.75846L9.11123 7.01128Z"
                                                      fill="#EE4A2E"/>
                                            </svg>
                                        </div>
                                        По новинкам
                                    </div>
                                </div>
                                <input type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-catalog-container__catalog">
                <div class="product-catalog">
                    <div class="product-catalog__fix">
                        <div class="product-catalog__categiris">
                            <ul class="catalog-menu active">
                                <li class="catalog-menu__item title__menu">
                                    <p>Одежда</p><span>245</span>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Блузки и
                                        рубашки<span>55</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Боди<span>17</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Брюки<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Верхняя
                                        одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Джемперы и свитеры<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Джинсы<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Домашняя
                                        одежда<span>37</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Комбинезоны<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Купальники и
                                        пляжная одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Нижнее белье<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="catalog-menu">
                                <li class="catalog-menu__item title__menu">
                                    <p>Обувь</p><span>223</span>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Блузки и
                                        рубашки<span>55</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Боди<span>17</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Брюки<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Верхняя
                                        одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Джемперы и свитеры<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Джинсы<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Домашняя
                                        одежда<span>37</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Комбинезоны<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Купальники и
                                        пляжная одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Нижнее белье<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="catalog-menu">
                                <li class="catalog-menu__item title__menu">
                                    <p>Аксессуары</p><span>352</span>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Блузки и
                                        рубашки<span>55</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Боди<span>17</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Брюки<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Верхняя
                                        одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Джемперы и свитеры<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Джинсы<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Домашняя
                                        одежда<span>37</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Комбинезоны<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Купальники и
                                        пляжная одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Нижнее белье<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="catalog-menu">
                                <li class="catalog-menu__item title__menu">
                                    <p>Спорт</p><span>35</span>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Блузки и
                                        рубашки<span>55</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Боди<span>17</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Брюки<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Верхняя
                                        одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Джемперы и свитеры<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Джинсы<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Домашняя
                                        одежда<span>37</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Комбинезоны<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Купальники и
                                        пляжная одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Нижнее белье<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="catalog-menu">
                                <li class="catalog-menu__item title__menu">
                                    <p>Красота</p><span>233</span>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Блузки и
                                        рубашки<span>55</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Боди<span>17</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Брюки<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Верхняя
                                        одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Джемперы и свитеры<span>34</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link"
                                                                  href="#">Джинсы<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Домашняя
                                        одежда<span>37</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Комбинезоны<span>12</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"><a class="catalog-menu__link" href="#">Купальники и
                                        пляжная одежда<span>19</span></a>
                                    <ul class="subcatalog-menu">
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                        </li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                                джинсы<span>3</span></a></li>
                                        <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="catalog-menu__item"></li>
                                <a class="catalog-menu__link" href="#">Нижнее белье<span>34</span></a>
                                <ul class="subcatalog-menu">
                                    <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-мом<span>9</span></a>
                                    </li>
                                    <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Зауженные
                                            джинсы<span>3</span></a></li>
                                    <li class="subcatalog-menu__item"><a class="subcatalog-menu__link" href="#">Джинсы-клеш<span>7</span></a>
                                    </li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                    <div class="product-catalog__product">
                        <div class="catalog-filter">
                            <div class="catalog-filter__body">
                                <div class="filter-catalog-filter-container">
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Материал<i
                                                    class="fas fa-chevron-up"></i></div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">цВЕТ<i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Атлас</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Бенгалин</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Бренд<i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Stradivarius</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Цена<i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__row">
                                                    <input class="filter-catalog-filter__range" placeholder="От"
                                                           type="number">
                                                    <input class="filter-catalog-filter__range" placeholder="До"
                                                           type="number">
                                                </div>
                                                <div class="filter-catalog-filter__btn hover-button">
                                                    <div class="hover-button__front">Применить</div>
                                                    <div class="hover-button__back">Применить</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Длина<i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Мини</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Миди</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Макси</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Застежка<i
                                                    class="fas fa-chevron-up"></i></div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Пуговицы</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Молния</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Магниты</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Стиль<i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Повседневный</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Вечерний</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Спортивный</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="catalog-filter__filter filter-catalog-filter">
                                        <div class="filter-catalog-filter__name">Сезон<i class="fas fa-chevron-up"></i>
                                        </div>
                                        <div class="filter-catalog-filter__body">
                                            <div class="filter-catalog-filter__column">
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Лето</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Осень</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Зима</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Весна</span>
                                                    </label>
                                                </div>
                                                <div class="filter-catalog-filter__example">
                                                    <label class="checkbox-filter">
                                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                                class="checkbox-filter__fake">
                                                   <svg fill="none" height="8" viewBox="0 0 9 8" width="9"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Демисезон</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="catalog-filter__pay">
                                    <label class="checkbox-filter">
                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                class="checkbox-filter__fake">
                           <svg fill="none" height="8" viewBox="0 0 9 8" width="9" xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Оплата частями</span>
                                    </label>
                                    <label class="checkbox-filter">
                                        <input class="checkbox-filter__input" type="checkbox"><span
                                                class="checkbox-filter__fake">
                           <svg fill="none" height="8" viewBox="0 0 9 8" width="9" xmlns="http://www.w3.org/2000/svg">
<path d="M7.13006 1.10368L7.13005 1.10367L7.12884 1.10511L3.34055 5.59969L1.87382 4.01777C1.78705 3.91936 1.64607 3.8509 1.49818 3.83924C1.34616 3.82726 1.17999 3.875 1.0539 4.01887C0.807898 4.29954 0.893624 4.65787 1.05386 4.84091L1.1291 4.77504L1.05386 4.84091L2.89998 6.94964C2.89998 6.94964 2.89998 6.94964 2.89999 6.94965C2.99584 7.05917 3.16506 7.10001 3.30999 7.1C3.45493 7.09999 3.62411 7.0591 3.71993 6.94965L3.71995 6.94966L3.72126 6.9481L7.95188 1.91229C8.11313 1.72731 8.18306 1.36856 7.95119 1.10368C7.80549 0.937246 7.63935 0.882842 7.48335 0.904522C7.3343 0.925236 7.20925 1.01324 7.13006 1.10368Z"
      fill="#FFF" stroke-width="0.2"/>
</svg></span><span class="checkbox-filter__text">Одежда Plus Size</span>
                                    </label>
                                </div>
                            </div>
                            <div class="catalog-filter__container">
                                <div class="catalog-filter__filter-select">
                                    <div class="filter-catalog-filter__select">Синий
                                        <div class="filter-catalog-filter__remove"></div>
                                    </div>
                                </div>
                                <div class="catalog-filter__remove_all">Сбросить фильтры</div>
                            </div>
                        </div>
                        <?php Pjax::begin(['id' => 'products_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'catalog-product pjax-more-container']]); ?>
                        <?php $widget = ListView::begin([
                            'dataProvider' => $dataProvider,
                            'itemView' => '//includes/_product',
                            'viewParams' => ['getVar' => true],
                            'options' => [
                                'tag' => false,
                            ],
                            'itemOptions' => [
                                'tag' => false,
                            ],
                            'layout' => '{items}'
                        ]) ?>

                        <?php $widget->end() ?>

                        <div class="paginations">
                            <div class="paginations__page"><?= $widget->renderSummary() ?></div>
                            <?php if ($dataProvider->totalCount > 12): ?>
                                <a class="more-link hover-button more-products-button" href="#">
                                    <div class="hover-button__front"><span><?= Yii::t('app', 'Еще {0} товаров', 12) ?></span></div>
                                    <div class="hover-button__back"><span><?= Yii::t('app', 'Еще {0} товаров', 12) ?></span></div>
                                </a>
                            <?php endif; ?>
                            <ul class="paginations__menu">
                                <li class="paginations__item"><a class="paginations__link active" href="#">1</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">2</a></li>
                                <li class="paginations__item"><a class="paginations__link" href="#">3</a></li>
                                <li class="paginations__item more"><a class="paginations__link" href="#">...</a>
                                </li>
                                <li class="paginations__item"><a class="paginations__link" href="#">10</a></li>
                            </ul>
                        </div>

                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
