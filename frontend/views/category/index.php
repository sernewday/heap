<?php

/* @var $this View */

use common\models\Brands;
use common\models\Categories;
use common\models\ProductParams;
use frontend\components\Target;
use frontend\widgets\Breadcrumbs;
use frontend\widgets\CategoriesWidget;
use frontend\widgets\FilterParamWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $category Categories */
/* @var $products \yii\data\ActiveDataProvider */


$this->title = (string) $category->info->name;

$breadcrumbs = [];
$breadcrumbs[] = ['label' => Yii::t('app', Target::getLabel().' '.$this->title), 'url' => '#'];

$this->registerJsFile('/js/product_filter.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);
$this->registerJsFile('/js/filter_params.js', ['depends' => ['yii\widgets\PjaxAsset', 'frontend\assets\AppAsset']]);

$js = <<< JS
$('#products_container').on('pjax:complete', function () {
    moreItemsEvent();
});
JS;
$this->registerJs($js);
?>

<main>
    <div class="container-fluid">
        <div class="container">
            <div class="prev-link">
                <?= Breadcrumbs::widget(['links' => $breadcrumbs]) ?>
            </div>
        </div>
    </div>
    <!-- <div class="container-fluid">
        <?php if ($category->childs): ?>
            <div class="categoris-slider">
                <div class="container">
                    <div class="product-categori-slider">
                        <div class="product-categori-slider__body">
                            <?php $img_num = 1; ?>
                            <?php foreach ($category->childs as $cat_child): ?>
                                <div class="product-categori">
                                    <a class="product-categori__body"
                                       href="<?= Url::to(['/category/index', 'alias' => $cat_child->name_alt]) ?>">
                                        <img alt="" src="<?= $cat_child->image ?: '/assets/img/product/product_img_1.png' ?>">
                                        <div class="product-categori__name">
                                            <p><?= Html::encode($cat_child->info->name) ?></p>
                                        </div>
                                    </a>
                                </div>

                                <?php $img_num += 1;
                                if ($img_num > 5) $img_num = 1; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?> -->
        <div class="product-catalog-container">
            <div class="container">
                <div class="product-catalog-container__title">
                    <div class="title">
                        <div class="title__line"></div>
                        <div class="title__text">
                            <?= Yii::t('app', Target::getLabel().' '.Html::encode($category->info->name))  ?>
                            <span>(<?= $products->totalCount ?> <?= Yii::t('app', 'товаров') ?>)</span>
                        </div>
                    </div>
                    <div class="product-catalog-container-filter">
                        <div class="options__conteiner">
                            <div class="options">
                                <div class="options__body">
                                    <div class="options__value"><span class="value"> <div class="options__img">
                                                <svg
                                                        fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                        xmlns="http://www.w3.org/2000/svg">
<path d="M13.2512 0H9.75122C9.25122 0 8.75181 0.158509 8.75144 0.991307C8.75126 1.40471 9.27761 1.5 9.75122 1.5L11.7512 1.5L7.75122 5.5L6.25122 3.5C5.75122 3 4.75122 3 4.2512 3.5L0.251261 7.5C-0.0838397 7.85976 -0.083655 8.14025 0.251223 8.5C0.586324 8.85976 0.91612 8.85952 1.25122 8.5L5.25122 4.5L7.25122 7C7.41207 7.17268 7.52358 7.23019 7.75122 7.23019C7.97864 7.23019 8.09037 7.17268 8.25122 7L12.7512 2.49999V4.36677C12.7512 4.87523 12.9196 5.28035 13.3934 5.28035C13.8673 5.28035 14.0664 4.87523 14.0664 4.36677V0.991316C14.0664 0.482622 13.7251 0 13.2512 0Z"
      fill="#EE4A2E"/>
</svg></div>По популярности</span><i class="fas fa-chevron-up"></i></div>
                                    <div class="options__content">
                                        <div class="options__opt" data-value="0">
                                            <div class="options__img">
                                                <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M13.2512 0H9.75122C9.25122 0 8.75181 0.158509 8.75144 0.991307C8.75126 1.40471 9.27761 1.5 9.75122 1.5L11.7512 1.5L7.75122 5.5L6.25122 3.5C5.75122 3 4.75122 3 4.2512 3.5L0.251261 7.5C-0.0838397 7.85976 -0.083655 8.14025 0.251223 8.5C0.586324 8.85976 0.91612 8.85952 1.25122 8.5L5.25122 4.5L7.25122 7C7.41207 7.17268 7.52358 7.23019 7.75122 7.23019C7.97864 7.23019 8.09037 7.17268 8.25122 7L12.7512 2.49999V4.36677C12.7512 4.87523 12.9196 5.28035 13.3934 5.28035C13.8673 5.28035 14.0664 4.87523 14.0664 4.36677V0.991316C14.0664 0.482622 13.7251 0 13.2512 0Z"
                                                          fill="#EE4A2E"/>
                                                </svg>
                                            </div>
                                            По популярности
                                        </div>
                                        <div class="options__opt" data-value="0">
                                            <div class="options__img">
                                                <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M13.2512 0H9.75122C9.25122 0 8.75181 0.158509 8.75144 0.991307C8.75126 1.40471 9.27761 1.5 9.75122 1.5L11.7512 1.5L7.75122 5.5L6.25122 3.5C5.75122 3 4.75122 3 4.2512 3.5L0.251261 7.5C-0.0838397 7.85976 -0.083655 8.14025 0.251223 8.5C0.586324 8.85976 0.91612 8.85952 1.25122 8.5L5.25122 4.5L7.25122 7C7.41207 7.17268 7.52358 7.23019 7.75122 7.23019C7.97864 7.23019 8.09037 7.17268 8.25122 7L12.7512 2.49999V4.36677C12.7512 4.87523 12.9196 5.28035 13.3934 5.28035C13.8673 5.28035 14.0664 4.87523 14.0664 4.36677V0.991316C14.0664 0.482622 13.7251 0 13.2512 0Z"
                                                          fill="#EE4A2E"/>
                                                </svg>
                                            </div>
                                            По возрастанию цены
                                        </div>
                                        <div class="options__opt" data-value="1">
                                            <div class="options__img">
                                                <svg fill="none" height="9" viewBox="0 0 15 9" width="15"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M0.815186 8.76978H4.31519C4.81519 8.76978 5.3146 8.61127 5.31496 7.77847C5.31514 7.36507 4.7888 7.26978 4.31519 7.26978L2.31519 7.26977L6.31519 3.26978L7.81519 5.26977C8.31519 5.76978 9.31519 5.76978 9.8152 5.26977L13.8151 1.26977C14.1502 0.910018 14.1501 0.62953 13.8152 0.269774C13.4801 -0.0899811 13.1503 -0.0897408 12.8152 0.269774L8.81519 4.26977L6.81519 1.76977C6.65434 1.59709 6.54283 1.53959 6.31519 1.53959C6.08776 1.53959 5.97603 1.59709 5.81519 1.76977L1.31519 6.26978V4.403C1.31519 3.89455 1.1468 3.48942 0.672962 3.48942C0.199129 3.48942 4.19617e-05 3.89455 4.19617e-05 4.403V7.77846C4.19617e-05 8.28715 0.341352 8.76978 0.815186 8.76978Z"
                                                          fill="#EE4A2E"/>
                                                </svg>
                                            </div>
                                            По убыванию цены
                                        </div>
                                        <div class="options__opt" data-value="2">
                                            <div class="options__img">
                                                <svg fill="none" height="12" viewBox="0 0 12 12" width="12"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12 6L11.1796 4.61198L11.197 2.99913L9.79095 2.20905L9.00087 0.803039L7.38802 0.820359L6 0L4.61198 0.820383L2.99913 0.803039L2.20905 2.20908L0.803039 2.99916L0.820359 4.61201L0 6L0.820359 7.38802L0.803016 9.00087L2.20903 9.79095L2.99911 11.197L4.61198 11.1796L6 12L7.38802 11.1796L9.00087 11.197L9.79095 9.79097L11.197 9.00089L11.1796 7.38802L12 6ZM4.27453 2.61619C4.81205 2.34159 5.39255 2.20235 6 2.20235C6.60743 2.20235 7.18795 2.34157 7.72547 2.61619C8.23781 2.87794 8.69173 3.26004 9.03811 3.7212L8.47467 4.14445C7.88395 3.3581 6.98198 2.90709 6 2.90709C5.01802 2.90709 4.11602 3.35808 3.52535 4.14445L2.96191 3.7212C3.30832 3.26004 3.76221 2.87794 4.27453 2.61619ZM6.13523 5.73996V6.23114H5.28V6.49697H6.29126V7.01126H4.60969V4.98874H6.25371V5.50303H5.28V5.73996H6.13523ZM2.3445 4.98877H2.90503L3.67359 5.90756V4.98877H4.33814V7.01128H3.77761L3.00905 6.09248V7.01128H2.3445V4.98877ZM7.72544 9.38381C7.18795 9.65841 6.60743 9.79765 5.99998 9.79765C5.39255 9.79765 4.812 9.65843 4.27451 9.38381C3.76216 9.12206 3.30827 8.73996 2.96187 8.2788L3.52535 7.85555C4.11602 8.64192 5.01797 9.09293 6 9.09293C6.98198 9.09293 7.88395 8.64192 8.47465 7.85555L9.03809 8.2788C8.69168 8.73996 8.23779 9.12206 7.72544 9.38381ZM9.11123 7.01128H8.38024L8.05376 5.93067L7.70993 7.01128H6.97894L6.33173 4.98877H7.03383L7.38633 6.1387L7.75906 4.98877H8.38603L8.73853 6.15316L9.11126 4.98877H9.75846L9.11123 7.01128Z"
                                                          fill="#EE4A2E"/>
                                                </svg>
                                            </div>
                                            По новинкам
                                        </div>
                                    </div>
                                    <input type="text"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-catalog-container__catalog">
                    <div class="product-catalog">
                        <div class="product-catalog__fix">
                            <div class="product-catalog__categiris">

                                <?= CategoriesWidget::widget(['category' => $category]) ?>

                            </div>
                        </div>
                        <div class="product-catalog__product">
                            <?= $this->render('//includes/_catalog-filter') ?>
                            <?php Pjax::begin(['id' => 'products_container', 'timeout' => 5000, 'clientOptions' => ['replace' => true], 'options' => ['class' => 'catalog-product pjax-more-container']]); ?>
                            <?php $widget = ListView::begin([
                                'dataProvider' => $products,
                                'itemView' => '//includes/_product',
                                'options' => [
                                    'tag' => false,
                                ],
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'layout' => '{items}'
                            ]) ?>

                            <?php $widget->end() ?>

                            <div class="paginations">
                                <?php echo LinkPager::widget([
                        'pagination' => $products->pagination,
                        'options' => ['class' => 'paginations'],
                        'maxButtonCount' => 1,
                        'firstPageCssClass' => 'first',
                        'activePageCssClass' => 'current',
                        'prevPageLabel' => 'Попередня',
                        'nextPageLabel' => 'Наступна',
                    ]); ?>  </div>

                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
        <?= $this->render('//site/descriptFemail') ?>
        </div>
    </div>
</main>
