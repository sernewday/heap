<?php

/* @var $this \yii\web\View */

use common\models\Orders;
use yii\helpers\Url;

/* @var $bag_products \common\models\Bag[] */

?>

<?php if ($bag_products): ?>
    <?php $order_num = 1; $order_id = Orders::getNewOrderNum(); $current_brand = 0; $order_sum = 0; ?>
    <?php foreach ($bag_products as $bag_product): ?>
        <?php if ($current_brand != $bag_product->product->brand_id): ?>
            <?php if ($current_brand != 0): ?>
                <?php echo '</div>'; ?>
                <div class="order-model-bag__sum">
                    <span><?= Yii::t('app', 'Сумма заказа') ?></span>
                    <p><?= $order_sum ?> грн</p>
                </div>
            <?php endif; ?>

            <div class="order-model-bag__title">
                <div class="order-model-bag__name"><?= Yii::t('app', 'Заказ') ?> <?= $order_num ?></div>
                <div class="order-model-bag__number">(№<?= $order_id ?>)</div>
            </div>

            <?php echo '<div class="order-model-bag__body">'; ?>
            <?php
            $order_num++;
            $order_id++;
            $order_sum = 0;
            $current_brand = $bag_product->product->brand_id;
        endif;
        ?>

        <a class="product-bag" href="<?= Url::to(['/products/view', 'id' => $bag_product->product_id]) ?>">
            <div class="product-bag__container">
                <div class="product-bag__img"><img alt="" src="<?= $bag_product->product->image->imageUrl ?>"></div>
                <div class="product-bag__body">
                    <div class="product-bag__info">
                        <div class="product-bag__name"><?= $bag_product->product->info->name ?></div>
                        <div class="product-bag__size"><span><?= Yii::t('app', 'Размер') ?> (UKR):</span>
                            <p><?= $bag_product->size ?></p>
                        </div>
                        <div class="product-bag__color"><span><?= Yii::t('app', 'Цвет') ?>:</span>
                            <div class="product-bag__color__value blue" style="background-color: <?= $bag_product->colorData->additional_value ?>;"></div>
                            <p><?= $bag_product->colorData->value ?></p>
                        </div>
                    </div>
                    <div class="product-bag__cost"><?= $bag_product->product->discountPrice * $bag_product->quantity ?> грн</div>
                    <div class="product-bag__remove" data-id="<?= $bag_product->id ?>"></div>
                </div>
            </div>
        </a>

        <?php
        $order_sum += ($bag_product->product->discountPrice * $bag_product->quantity);
    endforeach;

    echo '</div>';
    ?>

    <div class="order-model-bag__sum">
        <span><?= Yii::t('app', 'Сумма заказа') ?></span>
        <p><?= $order_sum ?> грн</p>
    </div>
<?php endif; ?>
