<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model \common\models\Questions */
/* @var $can_go bool */

?>

<div class="questions">
    <div class="questions__body">
        <div class="questions__title">
            <div class="questions__name">
                <?= $model->user->info->name ?>
                <?php if ($model->user->info->city): ?>
                    , <?= $model->user->info->city ?>
                <?php endif; ?>
            </div>
            <div class="questions__data"><?= $model->date ?></div>
        </div>
        <div class="questions__text">
            <?= Html::encode($model->question) ?>
        </div>
    </div>
    <div class="questions-answer">
        <div class="questions-answer__body">
            <div class="questions-answer__title">
                <div class="questions-answer_name_data">
                    <div class="questions-answer__name"><?= $model->product->brand->name ?></div>
                    <div class="questions-answer__data"><?= $model->answerDate ?></div>
                </div>
                <div class="questions-answer__who">
                    <img alt="" src="/assets/img/icon/confirmed.svg"> <?= Yii::t('app', 'Продавец') ?>
                </div>
            </div>
            <div class="questions-answer__text">
                <?= Html::encode($model->answer) ?>
            </div>
        </div>
    </div>
    
    <?php if (isset($can_go) && $can_go === true): ?>
        <a class="questions__link" href="<?= Url::to(['/products/view', 'id' => $model->record_id]) ?>">
            <i class="fas fa-long-arrow-alt-right"></i>
            <p><?= Yii::t('app', 'Перейти к товару') ?></p>
        </a>
    <?php endif; ?>
</div>
