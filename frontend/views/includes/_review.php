<?php

/* @var $this \yii\web\View */

use common\models\Brands;
use yii\helpers\Html;

/* @var $model \common\models\Reviews */
/* @var $main_classes string */
/* @var $can_edit bool */

?>

<div class="review <?= $main_classes ?>">
    <div class="review__content">
        <div class="review__body">
            <div class="review__title">
                <div class="review__name">
                    <?= $model->user->info->name ?>
                    <?php if ($model->user->info->city): ?>
                        , <?= $model->user->info->city ?>
                    <?php endif; ?>
                    <div class="review__date"><?= $model->date ?></div>
                </div>
                <div class="review__name-brand"><?= $model->module == Brands::tableName() ? $model->brand->name : $model->product->brand->name ?></div>
                <div class="review__confirmed">
                    <?php if ($model->confirmed == 1): ?>
                        <img alt="confirmed" src="/assets/img/icon/confirmed.svg">
                        <span><?= Yii::t('app', 'Покупка подтверждена') ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="review__stats stats-review">
                <div class="stats-review__body">
                    <?php if ($model->small_size == 1): ?>
                        <div class="stats-review__example arrows">
                            <img alt="small size" src="/assets/img/icon/arrows.svg">
                            <span><?= Yii::t('app', 'Размер не соответствует') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->correct_color == 1): ?>
                        <div class="stats-review__example palette">
                            <img alt="correct color" src="/assets/img/icon/palette.svg">
                            <span><?= Yii::t('app', 'Цвет полностью соответствует') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->perfect_quality == 1): ?>
                        <div class="stats-review__example diamond">
                            <img alt="perfect quality" src="/assets/img/icon/diamond.svg">
                            <span><?= Yii::t('app', 'Качество отличное') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->delivery_in_time == 1): ?>
                        <div class="stats-review__example box">
                            <img alt="delivery in time" src="/assets/img/icon/box.svg">
                            <span><?= Yii::t('app', 'Заказ доставлен вовремя') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->is_polite == 1): ?>
                        <div class="stats-review__example">
                            <img src="/assets/img/icon/coment.svg" alt="">
                            <span><?= Yii::t('app', 'Вежливый и профессионалный') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->is_online == 1): ?>
                        <div class="stats-review__example">
                            <img src="/assets/img/icon/time.svg" alt="">
                            <span><?= Yii::t('app', 'Всегда на связи') ?></span>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="stats-review__reting">
                    <div class="stars">
                        <?php for ($s = 0; $s < 5; $s++): ?>
                            <div class="stars__example">
                                <i class="fas fa-star-half <?= ($s < $model->score) ? 'active' : '' ?>"></i>
                                <i class="fas fa-star-half <?= ($s < $model->score) ? 'active' : '' ?>"></i>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="stats-review__reting_value"><?= $model->score ?></div>
                </div>
            </div>
            <div class="review__text">
                <?= Html::encode($model->comment) ?>
            </div>

            <?php if ($model->image): ?>
                <div class="review__img-body">
                    <div class="review__img">
                        <img alt="review image" src="<?= $model->image->imageUrl ?>">
                    </div>
                </div>
            <?php endif; ?>

            <div class="review__footer">

                <?php if (!Yii::$app->user->isGuest): ?>
                    <div class="review__answer">
                        <img alt="" src="/assets/img/icon/arow_back.svg"><span><?= Yii::t('app', 'Ответить') ?></span>
                    </div>

                    <div class="review_like_dizlike">
                        <div class="review__like">
                            <svg fill="none" height="20" viewBox="0 0 20 20" width="20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_f)">
                                    <path d="M3.16671 18H4.83344C5.47679 18 6.00014 17.4767 6.00014 16.8333V16.5893C7.00818 17.1027 9.2976 18 13.0211 18H14.0878C15.6332 18 16.9452 16.8947 17.2072 15.372L17.9539 11.0387C18.1126 10.114 17.8572 9.174 17.2546 8.458C16.6525 7.74333 15.7698 7.33333 14.8345 7.33333H11.8104C11.999 6.82667 12.1877 6.088 12.1877 5.16667C12.1877 2.662 10.7703 2 10.021 2C8.63691 2 8.5209 3.29067 8.5209 4.5C8.5209 5.976 6.86551 7.20267 5.99148 7.748C5.94681 7.14533 5.44746 6.66667 4.83344 6.66667H3.16671C2.52335 6.66667 2 7.19 2 7.83333V16.8333C2 17.4767 2.52335 18 3.16671 18V18ZM9.52094 4.5C9.52094 3 9.77495 3 10.021 3C10.4903 3 11.1877 3.57733 11.1877 5.16667C11.1877 6.644 10.609 7.54933 10.605 7.556C10.503 7.70933 10.493 7.90667 10.5803 8.06933C10.667 8.232 10.837 8.33333 11.021 8.33333H14.8345C15.4745 8.33333 16.0778 8.61333 16.4899 9.102C16.9025 9.59133 17.0772 10.2353 16.9685 10.868L16.2218 15.2013C16.0425 16.2427 15.1451 16.9993 14.0878 16.9993H13.0211C8.48557 17 6.21349 15.5813 6.00014 15.4407V8.90067C6.5555 8.606 9.52094 6.91933 9.52094 4.5V4.5ZM3.00004 7.83333C3.00004 7.74133 3.07471 7.66667 3.16671 7.66667H4.83344C4.92544 7.66667 5.00011 7.74133 5.00011 7.83333V16.8333C5.00011 16.924 4.92411 17 4.83344 17H3.16671C3.07471 17 3.00004 16.9253 3.00004 16.8333V7.83333Z"
                                          fill="#84EB8E"/>
                                </g>
                                <defs>
                                    <filter color-interpolation-filters="sRGB" filterUnits="userSpaceOnUse" height="20"
                                            id="filter0_f" width="20"
                                            x="0"
                                            y="0">
                                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                        <feBlend in="SourceGraphic" in2="BackgroundImageFix"
                                                 mode="normal" result="shape"/>
                                        <feGaussianBlur result="effect1_foregroundBlur"
                                                        stdDeviation="1"/>
                                    </filter>
                                </defs>
                            </svg>
                            <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.16671 16H2.83344C3.47679 16 4.00014 15.4767 4.00014 14.8333V14.5893C5.00818 15.1027 7.2976 16 11.0211 16H12.0878C13.6332 16 14.9452 14.8947 15.2072 13.372L15.9539 9.03867C16.1126 8.114 15.8572 7.174 15.2546 6.458C14.6525 5.74333 13.7698 5.33333 12.8345 5.33333H9.81036C9.99903 4.82667 10.1877 4.088 10.1877 3.16667C10.1877 0.662 8.77032 0 8.02096 0C6.63691 0 6.5209 1.29067 6.5209 2.5C6.5209 3.976 4.86551 5.20267 3.99148 5.748C3.94681 5.14533 3.44746 4.66667 2.83344 4.66667H1.16671C0.523352 4.66667 0 5.19 0 5.83333V14.8333C0 15.4767 0.523352 16 1.16671 16ZM7.52094 2.5C7.52094 1 7.77495 1 8.02096 1C8.49031 1 9.18767 1.57733 9.18767 3.16667C9.18767 4.644 8.60898 5.54933 8.60498 5.556C8.50297 5.70933 8.49297 5.90667 8.58031 6.06933C8.66698 6.232 8.83699 6.33333 9.02099 6.33333H12.8345C13.4745 6.33333 14.0778 6.61333 14.4899 7.102C14.9025 7.59133 15.0772 8.23533 14.9685 8.868L14.2218 13.2013C14.0425 14.2427 13.1451 14.9993 12.0878 14.9993H11.0211C6.48557 15 4.21349 13.5813 4.00014 13.4407V6.90067C4.5555 6.606 7.52094 4.91933 7.52094 2.5ZM1.00004 5.83333C1.00004 5.74133 1.07471 5.66667 1.16671 5.66667H2.83344C2.92544 5.66667 3.00011 5.74133 3.00011 5.83333V14.8333C3.00011 14.924 2.92411 15 2.83344 15H1.16671C1.07471 15 1.00004 14.9253 1.00004 14.8333V5.83333Z"
                                      fill="#353535"/>
                            </svg>
                            <span> 1</span></div>
                        <div class="review__dizlike">
                            <svg fill="none" height="20" viewBox="0 0 20 20" width="20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_f)">
                                    <path d="M3.16671 2H4.83344C5.47679 2 6.00014 2.52333 6.00014 3.16667V3.41067C7.00818 2.89733 9.2976 2 13.0211 2H14.0878C15.6332 2 16.9452 3.10533 17.2072 4.628L17.9539 8.96133C18.1126 9.886 17.8572 10.826 17.2546 11.542C16.6525 12.2567 15.7698 12.6667 14.8345 12.6667H11.8104C11.999 13.1733 12.1877 13.912 12.1877 14.8333C12.1877 17.338 10.7703 18 10.021 18C8.63691 18 8.5209 16.7093 8.5209 15.5C8.5209 14.024 6.86551 12.7973 5.99148 12.252C5.94681 12.8547 5.44746 13.3333 4.83344 13.3333H3.16671C2.52335 13.3333 2 12.81 2 12.1667V3.16667C2 2.52333 2.52335 2 3.16671 2V2ZM9.52094 15.5C9.52094 17 9.77495 17 10.021 17C10.4903 17 11.1877 16.4227 11.1877 14.8333C11.1877 13.356 10.609 12.4507 10.605 12.444C10.503 12.2907 10.493 12.0933 10.5803 11.9307C10.667 11.768 10.837 11.6667 11.021 11.6667H14.8345C15.4745 11.6667 16.0778 11.3867 16.4899 10.898C16.9025 10.4087 17.0772 9.76467 16.9685 9.132L16.2218 4.79867C16.0425 3.75733 15.1451 3.00067 14.0878 3.00067H13.0211C8.48557 3 6.21349 4.41867 6.00014 4.55933V11.0993C6.5555 11.394 9.52094 13.0807 9.52094 15.5V15.5ZM3.00004 12.1667C3.00004 12.2587 3.07471 12.3333 3.16671 12.3333H4.83344C4.92544 12.3333 5.00011 12.2587 5.00011 12.1667V3.16667C5.00011 3.076 4.92411 3 4.83344 3H3.16671C3.07471 3 3.00004 3.07467 3.00004 3.16667V12.1667Z"
                                          fill="#F98C79"/>
                                </g>
                                <defs>
                                    <filter color-interpolation-filters="sRGB" filterUnits="userSpaceOnUse" height="20"
                                            id="filter0_f" width="20"
                                            x="0"
                                            y="0">
                                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                        <feBlend in="SourceGraphic" in2="BackgroundImageFix"
                                                 mode="normal" result="shape"/>
                                        <feGaussianBlur result="effect1_foregroundBlur"
                                                        stdDeviation="1"/>
                                    </filter>
                                </defs>
                            </svg>
                            <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.16671 0H2.83344C3.47679 0 4.00014 0.523333 4.00014 1.16667V1.41067C5.00818 0.897332 7.2976 0 11.0211 0H12.0878C13.6332 0 14.9452 1.10533 15.2072 2.628L15.9539 6.96133C16.1126 7.886 15.8572 8.826 15.2546 9.542C14.6525 10.2567 13.7698 10.6667 12.8345 10.6667H9.81036C9.99903 11.1733 10.1877 11.912 10.1877 12.8333C10.1877 15.338 8.77032 16 8.02096 16C6.63691 16 6.5209 14.7093 6.5209 13.5C6.5209 12.024 4.86551 10.7973 3.99148 10.252C3.94681 10.8547 3.44746 11.3333 2.83344 11.3333H1.16671C0.523352 11.3333 0 10.81 0 10.1667V1.16667C0 0.523333 0.523352 0 1.16671 0V0ZM7.52094 13.5C7.52094 15 7.77495 15 8.02096 15C8.49031 15 9.18767 14.4227 9.18767 12.8333C9.18767 11.356 8.60898 10.4507 8.60498 10.444C8.50297 10.2907 8.49297 10.0933 8.58031 9.93067C8.66698 9.768 8.83699 9.66667 9.02099 9.66667H12.8345C13.4745 9.66667 14.0778 9.38667 14.4899 8.898C14.9025 8.40867 15.0772 7.76467 14.9685 7.132L14.2218 2.79867C14.0425 1.75733 13.1451 1.00067 12.0878 1.00067H11.0211C6.48557 0.999999 4.21349 2.41867 4.00014 2.55933V9.09933C4.5555 9.394 7.52094 11.0807 7.52094 13.5V13.5ZM1.00004 10.1667C1.00004 10.2587 1.07471 10.3333 1.16671 10.3333H2.83344C2.92544 10.3333 3.00011 10.2587 3.00011 10.1667V1.16667C3.00011 1.076 2.92411 1 2.83344 1H1.16671C1.07471 1 1.00004 1.07467 1.00004 1.16667V10.1667Z"
                                      fill="#353535"/>
                            </svg>
                            <span> 1</span></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php foreach ($model->answers as $answer): ?>
            <div class="review-answer">
                <div class="review-answer__body">
                    <div class="review-answer__title">
                        <div class="review-answer__name">
                            <?= $answer->user->info->name ?>
                            <?php if ($answer->user->info->city): ?>
                                , <?= $answer->user->info->city ?>
                            <?php endif; ?>
                        </div>
                        <div class="review-answer__data"><?= $answer->date ?></div>
                    </div>
                    <div class="review-answer__text">
                        <?= Html::encode($answer->comment) ?>
                    </div>
                    <div class="review-answer__footer">
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <div class="review__answer">
                                <img alt="" src="/assets/img/icon/arow_back.svg">
                                <span><?= Yii::t('app', 'Ответить') ?></span>
                            </div>
                            <div class="review_like_dizlike">
                                <div class="review__like">
                                    <svg fill="none" height="20" viewBox="0 0 20 20" width="20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g filter="url(#filter0_f)">
                                            <path d="M3.16671 18H4.83344C5.47679 18 6.00014 17.4767 6.00014 16.8333V16.5893C7.00818 17.1027 9.2976 18 13.0211 18H14.0878C15.6332 18 16.9452 16.8947 17.2072 15.372L17.9539 11.0387C18.1126 10.114 17.8572 9.174 17.2546 8.458C16.6525 7.74333 15.7698 7.33333 14.8345 7.33333H11.8104C11.999 6.82667 12.1877 6.088 12.1877 5.16667C12.1877 2.662 10.7703 2 10.021 2C8.63691 2 8.5209 3.29067 8.5209 4.5C8.5209 5.976 6.86551 7.20267 5.99148 7.748C5.94681 7.14533 5.44746 6.66667 4.83344 6.66667H3.16671C2.52335 6.66667 2 7.19 2 7.83333V16.8333C2 17.4767 2.52335 18 3.16671 18V18ZM9.52094 4.5C9.52094 3 9.77495 3 10.021 3C10.4903 3 11.1877 3.57733 11.1877 5.16667C11.1877 6.644 10.609 7.54933 10.605 7.556C10.503 7.70933 10.493 7.90667 10.5803 8.06933C10.667 8.232 10.837 8.33333 11.021 8.33333H14.8345C15.4745 8.33333 16.0778 8.61333 16.4899 9.102C16.9025 9.59133 17.0772 10.2353 16.9685 10.868L16.2218 15.2013C16.0425 16.2427 15.1451 16.9993 14.0878 16.9993H13.0211C8.48557 17 6.21349 15.5813 6.00014 15.4407V8.90067C6.5555 8.606 9.52094 6.91933 9.52094 4.5V4.5ZM3.00004 7.83333C3.00004 7.74133 3.07471 7.66667 3.16671 7.66667H4.83344C4.92544 7.66667 5.00011 7.74133 5.00011 7.83333V16.8333C5.00011 16.924 4.92411 17 4.83344 17H3.16671C3.07471 17 3.00004 16.9253 3.00004 16.8333V7.83333Z"
                                                  fill="#84EB8E"/>
                                        </g>
                                        <defs>
                                            <filter color-interpolation-filters="sRGB" filterUnits="userSpaceOnUse"
                                                    height="20" id="filter0_f"
                                                    width="20" x="0"
                                                    y="0">
                                                <feFlood flood-opacity="0"
                                                         result="BackgroundImageFix"/>
                                                <feBlend in="SourceGraphic" in2="BackgroundImageFix"
                                                         mode="normal" result="shape"/>
                                                <feGaussianBlur result="effect1_foregroundBlur"
                                                                stdDeviation="1"/>
                                            </filter>
                                        </defs>
                                    </svg>
                                    <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.16671 16H2.83344C3.47679 16 4.00014 15.4767 4.00014 14.8333V14.5893C5.00818 15.1027 7.2976 16 11.0211 16H12.0878C13.6332 16 14.9452 14.8947 15.2072 13.372L15.9539 9.03867C16.1126 8.114 15.8572 7.174 15.2546 6.458C14.6525 5.74333 13.7698 5.33333 12.8345 5.33333H9.81036C9.99903 4.82667 10.1877 4.088 10.1877 3.16667C10.1877 0.662 8.77032 0 8.02096 0C6.63691 0 6.5209 1.29067 6.5209 2.5C6.5209 3.976 4.86551 5.20267 3.99148 5.748C3.94681 5.14533 3.44746 4.66667 2.83344 4.66667H1.16671C0.523352 4.66667 0 5.19 0 5.83333V14.8333C0 15.4767 0.523352 16 1.16671 16ZM7.52094 2.5C7.52094 1 7.77495 1 8.02096 1C8.49031 1 9.18767 1.57733 9.18767 3.16667C9.18767 4.644 8.60898 5.54933 8.60498 5.556C8.50297 5.70933 8.49297 5.90667 8.58031 6.06933C8.66698 6.232 8.83699 6.33333 9.02099 6.33333H12.8345C13.4745 6.33333 14.0778 6.61333 14.4899 7.102C14.9025 7.59133 15.0772 8.23533 14.9685 8.868L14.2218 13.2013C14.0425 14.2427 13.1451 14.9993 12.0878 14.9993H11.0211C6.48557 15 4.21349 13.5813 4.00014 13.4407V6.90067C4.5555 6.606 7.52094 4.91933 7.52094 2.5ZM1.00004 5.83333C1.00004 5.74133 1.07471 5.66667 1.16671 5.66667H2.83344C2.92544 5.66667 3.00011 5.74133 3.00011 5.83333V14.8333C3.00011 14.924 2.92411 15 2.83344 15H1.16671C1.07471 15 1.00004 14.9253 1.00004 14.8333V5.83333Z"
                                              fill="#353535"/>
                                    </svg>
                                    <span> 1</span></div>
                                <div class="review__dizlike">
                                    <svg fill="none" height="20" viewBox="0 0 20 20" width="20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g filter="url(#filter0_f)">
                                            <path d="M3.16671 2H4.83344C5.47679 2 6.00014 2.52333 6.00014 3.16667V3.41067C7.00818 2.89733 9.2976 2 13.0211 2H14.0878C15.6332 2 16.9452 3.10533 17.2072 4.628L17.9539 8.96133C18.1126 9.886 17.8572 10.826 17.2546 11.542C16.6525 12.2567 15.7698 12.6667 14.8345 12.6667H11.8104C11.999 13.1733 12.1877 13.912 12.1877 14.8333C12.1877 17.338 10.7703 18 10.021 18C8.63691 18 8.5209 16.7093 8.5209 15.5C8.5209 14.024 6.86551 12.7973 5.99148 12.252C5.94681 12.8547 5.44746 13.3333 4.83344 13.3333H3.16671C2.52335 13.3333 2 12.81 2 12.1667V3.16667C2 2.52333 2.52335 2 3.16671 2V2ZM9.52094 15.5C9.52094 17 9.77495 17 10.021 17C10.4903 17 11.1877 16.4227 11.1877 14.8333C11.1877 13.356 10.609 12.4507 10.605 12.444C10.503 12.2907 10.493 12.0933 10.5803 11.9307C10.667 11.768 10.837 11.6667 11.021 11.6667H14.8345C15.4745 11.6667 16.0778 11.3867 16.4899 10.898C16.9025 10.4087 17.0772 9.76467 16.9685 9.132L16.2218 4.79867C16.0425 3.75733 15.1451 3.00067 14.0878 3.00067H13.0211C8.48557 3 6.21349 4.41867 6.00014 4.55933V11.0993C6.5555 11.394 9.52094 13.0807 9.52094 15.5V15.5ZM3.00004 12.1667C3.00004 12.2587 3.07471 12.3333 3.16671 12.3333H4.83344C4.92544 12.3333 5.00011 12.2587 5.00011 12.1667V3.16667C5.00011 3.076 4.92411 3 4.83344 3H3.16671C3.07471 3 3.00004 3.07467 3.00004 3.16667V12.1667Z"
                                                  fill="#F98C79"/>
                                        </g>
                                        <defs>
                                            <filter color-interpolation-filters="sRGB" filterUnits="userSpaceOnUse"
                                                    height="20" id="filter0_f"
                                                    width="20" x="0"
                                                    y="0">
                                                <feFlood flood-opacity="0"
                                                         result="BackgroundImageFix"/>
                                                <feBlend in="SourceGraphic" in2="BackgroundImageFix"
                                                         mode="normal" result="shape"/>
                                                <feGaussianBlur result="effect1_foregroundBlur"
                                                                stdDeviation="1"/>
                                            </filter>
                                        </defs>
                                    </svg>
                                    <svg fill="none" height="16" viewBox="0 0 16 16" width="16"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.16671 0H2.83344C3.47679 0 4.00014 0.523333 4.00014 1.16667V1.41067C5.00818 0.897332 7.2976 0 11.0211 0H12.0878C13.6332 0 14.9452 1.10533 15.2072 2.628L15.9539 6.96133C16.1126 7.886 15.8572 8.826 15.2546 9.542C14.6525 10.2567 13.7698 10.6667 12.8345 10.6667H9.81036C9.99903 11.1733 10.1877 11.912 10.1877 12.8333C10.1877 15.338 8.77032 16 8.02096 16C6.63691 16 6.5209 14.7093 6.5209 13.5C6.5209 12.024 4.86551 10.7973 3.99148 10.252C3.94681 10.8547 3.44746 11.3333 2.83344 11.3333H1.16671C0.523352 11.3333 0 10.81 0 10.1667V1.16667C0 0.523333 0.523352 0 1.16671 0V0ZM7.52094 13.5C7.52094 15 7.77495 15 8.02096 15C8.49031 15 9.18767 14.4227 9.18767 12.8333C9.18767 11.356 8.60898 10.4507 8.60498 10.444C8.50297 10.2907 8.49297 10.0933 8.58031 9.93067C8.66698 9.768 8.83699 9.66667 9.02099 9.66667H12.8345C13.4745 9.66667 14.0778 9.38667 14.4899 8.898C14.9025 8.40867 15.0772 7.76467 14.9685 7.132L14.2218 2.79867C14.0425 1.75733 13.1451 1.00067 12.0878 1.00067H11.0211C6.48557 0.999999 4.21349 2.41867 4.00014 2.55933V9.09933C4.5555 9.394 7.52094 11.0807 7.52094 13.5V13.5ZM1.00004 10.1667C1.00004 10.2587 1.07471 10.3333 1.16671 10.3333H2.83344C2.92544 10.3333 3.00011 10.2587 3.00011 10.1667V1.16667C3.00011 1.076 2.92411 1 2.83344 1H1.16671C1.07471 1 1.00004 1.07467 1.00004 1.16667V10.1667Z"
                                              fill="#353535"/>
                                    </svg>
                                    <span> 1</span></div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <?php if (isset($can_edit) && $can_edit === true): ?>
        <div class="review__adit">
            <img alt="edit" src="/assets/img/personal-cart/pen.svg">
            <p><?= Yii::t('app', 'Редактировать') ?></p>
        </div>
    <?php endif; ?>
</div>
