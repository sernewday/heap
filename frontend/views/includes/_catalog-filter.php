<?php

/* @var $this \yii\web\View */

use common\models\Brands;
use common\models\ProductParams;
use frontend\widgets\FilterParamWidget;

?>

<form action="#" id="catalog-filter-form" class="catalog-filter">
    <div class="catalog-filter__body">
        <div class="filter-catalog-filter-container">

            <?= FilterParamWidget::widget([
                'param_id' => ProductParams::PARAM_MATERIAL,
                'name' => Yii::t('app', 'Материал'),
                'name_input' => 'params[' . ProductParams::PARAM_MATERIAL . '][]',
            ]) ?>

            <?= FilterParamWidget::widget([
                'param_id' => ProductParams::PARAM_COLOR,
                'name' => Yii::t('app', 'Цвет'),
                'name_input' => 'params[' . ProductParams::PARAM_COLOR . '][]',
            ]) ?>

            <?php if (Yii::$app->controller->id != 'brands'): ?>
                <?= FilterParamWidget::widget([
                    'param_id' => 0,
                    'name' => Yii::t('app', 'Бренд'),
                    'name_input' => 'brand_id[]',
                    'values' => Brands::find()->cache(3600 * 24)->all(),
                    'value' => 'name',
                ]) ?>
            <?php endif; ?>

            <div class="catalog-filter__filter filter-catalog-filter">
                <div class="filter-catalog-filter__name">
                    <?= Yii::t('app', 'Цена') ?>
                    <i class="fas fa-chevron-up"></i>
                </div>
                <div class="filter-catalog-filter__body">
                    <div class="filter-catalog-filter__column">
                        <div class="filter-catalog-filter__row">
                            <input class="filter-catalog-filter__range"
                                   name="price_start" placeholder="<?= Yii::t('app', 'От') ?>" type="number" min="0">
                            <input class="filter-catalog-filter__range"
                                   name="price_end" placeholder="До" type="number" min="0">
                        </div>
                        <button class="filter-catalog-filter__btn hover-button price-confirm">
                            <div class="hover-button__front"><span><?= Yii::t('app', 'Применить') ?></span></div>
                            <div class="hover-button__back"><span><?= Yii::t('app', 'Применить') ?></span></div>
                        </button>
                    </div>
                </div>
            </div>

            <?= FilterParamWidget::widget([
                'param_id' => ProductParams::PARAM_LENGTH,
                'name' => Yii::t('app', 'Длина'),
                'name_input' => 'params[' . ProductParams::PARAM_LENGTH . '][]',
            ]) ?>

            <?= FilterParamWidget::widget([
                'param_id' => ProductParams::PARAM_CLASP,
                'name' => Yii::t('app', 'Застежка'),
                'name_input' => 'params[' . ProductParams::PARAM_CLASP . '][]',
            ]) ?>

            <?= FilterParamWidget::widget([
                'param_id' => ProductParams::PARAM_STYLE,
                'name' => Yii::t('app', 'Стиль'),
                'name_input' => 'params[' . ProductParams::PARAM_STYLE . '][]',
            ]) ?>

            <?= FilterParamWidget::widget([
                'param_id' => ProductParams::PARAM_SEASON,
                'name' => Yii::t('app', 'Сезон'),
                'name_input' => 'params[' . ProductParams::PARAM_SEASON . '][]',
            ]) ?>

        </div>
        <div class="catalog-filter__pay"><label class="checkbox-filter"><input
                    class="checkbox-filter__input" type="checkbox"><span
                    class="checkbox-filter__fake"> <span></span></span><span
                    class="checkbox-filter__text"><?= Yii::t('app', 'Оплата частями') ?></span></label><label
                class="checkbox-filter"><input class="checkbox-filter__input"
                                               type="checkbox"><span
                    class="checkbox-filter__fake"> <span></span></span><span
                    class="checkbox-filter__text"><?= Yii::t('app','Одежда'). 'Plus Size' ?></span></label></div>
    </div>
    <div class="catalog-filter__container">
        <div class="catalog-filter__filter-select">

        </div>
        <div class="catalog-filter__remove_all"><?= Yii::t('app', 'Сбросить фильтры') ?></div>
    </div>
</form>
