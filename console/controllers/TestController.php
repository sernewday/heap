<?php

namespace console\controllers;

use common\models\Brands;
use common\models\BrandsInfo;
use common\models\Categories;
use common\models\ImageManager;
use common\models\Infos\CategoriesInfo;
use common\models\Infos\ProductsInfo;
use common\models\OrderHistory;
use common\models\Orders;
use common\models\Payments;
use common\models\Products;
use common\models\ProductsCategoriesAssoc;
use common\models\Reviews;
use common\models\SellersBalance;
use common\models\User;
use common\models\UserInfo;
use Faker\Factory;
use frontend\components\Crypt;
use frontend\components\Helper;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class TestController
 * @package console\controllers
 */
class TestController extends Controller
{
    public function actionReplaceBrandImages()
    {
        $images = ImageManager::find()->where(['module' => Brands::tableName()])->all();

        $base_path = dirname(dirname(__DIR__)) . '/frontend/web/uploads/' . Brands::tableName() . '/';

        foreach ($images as $image) {
            if (!file_exists($image->fullPath))
                continue;

            $new_dir_part = mt_rand(0, 100);
            $new_path = $base_path . $new_dir_part . '/';
            FileHelper::createDirectory($new_path);

            $image_new_path = $new_dir_part . '/' . $image->path . '_' . $image->record_id . '.' . $image->extension;
            $new_file_path = $base_path . $image_new_path;
            copy($image->fullPath, $new_file_path);

            $image->path = $image_new_path;
            $image->save();

            $this->stdout($new_file_path . PHP_EOL);

        }

        return ExitCode::OK;
    }

    public function actionFakeBrandReviews()
    {
        $brands = Brands::find()->all();

        foreach ($brands as $brand) {
            for ($i = 0; $i < 15; $i++) {
                $faker = Factory::create();

                $review = new Reviews(['module' => Brands::tableName(), 'record_id' => $brand->id]);
                $review->comment = $faker->sentence(mt_rand(5, 10));
                $review->perfect_quality = mt_rand(0, 1);
                $review->delivery_in_time = mt_rand(0, 1);
                $review->is_online = mt_rand(0, 1);
                $review->is_polite = mt_rand(0, 1);
                $review->user_id = 27;
                $review->reply_to = 0;
                $review->score = mt_rand(0, 5);
                $review->confirmed = 1;

                $review->save();

                $this->stdout($review->comment . PHP_EOL);
            }
        }

        return ExitCode::OK;
    }

    public function actionCrypt($str = 'This is a test string')
    {
        $encrypted = Crypt::encrypt($str);

        $this->stdout($str . PHP_EOL);
        $this->stdout($encrypted . PHP_EOL);
        $this->stdout(Crypt::decrypt($encrypted) . PHP_EOL);

        return ExitCode::OK;
    }

    public function actionEmulatePayments()
    {
        $orders = Orders::find()->all();
        foreach ($orders as $order) {
            $data = [
                'payment_type' => Payments::TYPE_PAY_FOR_SALES,
                'order_id' => $order->id,
                'order_cost' => $order->orderSum,
                'balance_add' => $order->orderSum,
            ];

            $order->seller->addPayment($data);

            $data = [
                'payment_type' => Payments::TYPE_COMMISSION_FOR_SALES,
                'order_id' => $order->id,
                'order_cost' => $order->orderSum,
                'balance_charge' => round($order->orderSum * 10 / 100, 2),
            ];

            $order->seller->addPayment($data);
        }

        return ExitCode::OK;
    }

    public function actionSetBalances()
    {
        $brands = Brands::find()->all();

        foreach ($brands as $brand) {
            $balance = new SellersBalance();
            $balance->seller_id = $brand->id;
            $balance->balance = 0;

            $balance->save();
        }

        return ExitCode::OK;
    }

    public function actionUpdateReviews()
    {
        $faker = Factory::create();

        $user = new User();
        $user->email = $faker->email;
        $user->username = $user->email;
        $user->setPassword(123456);
        $user->generateAuthKey();
        $user->status = User::STATUS_ACTIVE;
        $user->user_type = User::TYPE_CUSTOMER;
        $user->phone = '+3800457856398';
        $user->confirmed_email = 1;
        $user->confirmed_phone = 1;

        if ($user->save()) {

            $user_info = new UserInfo(['user_id' => $user->id]);
            $user_info->name = $faker->firstName;
            $user_info->surname = $faker->lastName;

            $user_info->save();

            Reviews::updateAll(['user_id' => $user->id]);
        }
    }

    public function actionUndoReplace()
    {
        $images = ImageManager::find()->where(['module' => Products::tableName()])->all();

        foreach ($images as $image) {
            $data = explode('/', $image->path);

            if (count($data) <= 1)
                continue;

            $f_name_data = explode('_', $data[1]);

            unset($f_name_data[count($f_name_data) - 1]);

            $this->stdout($image->path . PHP_EOL);
            $this->stdout(implode('_', $f_name_data) . PHP_EOL);

            $image->path = implode('_', $f_name_data);
            $image->save();
        }

        return ExitCode::OK;
    }

    public function actionReplaceImages()
    {
        $images = ImageManager::find()->where(['module' => Products::tableName()])->all();

        $base_path = dirname(dirname(__DIR__)) . '/frontend/web/uploads/' . Products::tableName() . '/';

        foreach ($images as $image) {
            if (!file_exists($image->fullPath))
                continue;

            $new_dir_part = mt_rand(0, 100);
            $new_path = $base_path . $new_dir_part . '/';
            FileHelper::createDirectory($new_path);

            $image_new_path = $new_dir_part . '/' . $image->path . '_' . $image->record_id . '.' . $image->extension;
            $new_file_path = $base_path . $image_new_path;
            copy($image->fullPath, $new_file_path);

            $image->path = $image_new_path;
            $image->save();

            $this->stdout($new_file_path . PHP_EOL);

        }

        return ExitCode::OK;
    }

    public function actionProcessLeft()
    {
        $products = Products::find()->all();
        foreach ($products as $product) {
            $product->amount_left = mt_rand(0, 10);
            $product->save();
        }

        return ExitCode::OK;
    }

    /**
     * Generates random brands
     */
    public function actionBrands()
    {
        for ($i = 0; $i < 100; $i++) {
            $faker = Factory::create();
            $brand = new Brands();
            $brand->name = $faker->firstName;

            $brand->save();

            $this->stdout($brand->name . PHP_EOL);
        }
    }

    public function actionCreateAdmin()
    {
        $user = new User();
        $user->setPassword('admin1');
        $user->username = 'admin@gmail.com';
        $user->email = 'admin@gmail.com';
        $user->status = User::STATUS_ACTIVE;

        $user->save();

        $auth = \Yii::$app->authManager;
        $authorRole = $auth->getRole('admin');
        $auth->assign($authorRole, $user->getId());

        $this->stdout('OK' . PHP_EOL);
    }

    public function actionBrandsDesc()
    {
        $brands = Brands::find()->all();
        foreach ($brands as $brand) {
            $faker = Factory::create();

            $info = new BrandsInfo(['record_id' => $brand->id, 'lang' => 1]);
            $info->description = $faker->sentence(200);

            $info->save();
        }

        $this->stdout('OK' . PHP_EOL);
    }

    public function actionProducts()
    {
        $brands = Brands::find()->all();
        $brands_ids = ArrayHelper::getColumn($brands, 'id');
        $c_brands = count($brands_ids);
        for ($i = 0; $i < 2000; $i++) {
            $faker = Factory::create();
            $product = new Products();
            $product->brand_id = $brands_ids[mt_rand(0, $c_brands - 1)];
            $product->category_id = 0;

            if (mt_rand(1, 100) % 3 == 0) {
                $product->discount = mt_rand(5, 15);
                $product->discount_type = Products::DISCOUNT_PERCENT;
                $product->discount_due = time() + (3600 * 24 * mt_rand(5, 30));
            }

            $product->is_new = mt_rand(0, 1);
            $product->price = mt_rand(100, 2000);
            $product->currency_id = Products::CURRENCY_UAH;

            $product->created_at = time();
            $product->updated_at = time();

            if ($product->save()) {
                $info = new ProductsInfo(['record_id' => $product->id, 'lang' => 1]);
                $info->name = $faker->sentence(mt_rand(3, 6));
                $info->description = $faker->sentence(100);

                $info->save();

                $this->stdout($info->name . PHP_EOL);
            }

        }
    }

    public function actionProductImages()
    {
        $list = [
            'product_img_1.png',
            'product_img_2.png',
            'product_img_3.png',
            'product_img_4.png',
            'product_img_5.png',
            'product_img_6.png',
            'product_slider-img_1.png',
            'product_slider-img_2.png',
            'product_slider-img_4_test.png',
        ];

        $products = Products::find()->all();
        foreach ($products as $product) {
            $im = new ImageManager();
            $im->module = Products::tableName();
            $im->record_id = $product->id;
            $im->path = $list[mt_rand(0, 8)];

            $im->save();

            $this->stdout($im->path . PHP_EOL);
        }
    }

    public function actionBrandImages()
    {
        $list = [
            'befree.png',
            'bershka.png',
            'calvinklein.png',
            'h_m.png',
            'stradivarius.png',
            'topshop.png',
        ];

        $brands = Brands::find()->all();
        foreach ($brands as $brand) {
            $im = new ImageManager();
            $im->module = Brands::tableName();
            $im->record_id = $brand->id;
            $im->path = $list[mt_rand(0, 5)];

            $im->save();

            $this->stdout($im->path . PHP_EOL);
        }
    }

    public function actionFakeReviews()
    {
        $products = Products::find()->all();
        foreach ($products as $product) {
            for ($i = 0; $i < 5; $i++) {
                $faker = Factory::create();

                $review = new Reviews();
                $review->module = Products::tableName();
                $review->record_id = $product->id;
                $review->comment = $faker->sentence(mt_rand(5, 20));
                $review->small_size = mt_rand(0, 1);
                $review->correct_color = mt_rand(0, 1);
                $review->perfect_quality = mt_rand(0, 1);
                $review->delivery_in_time = mt_rand(0, 1);
                $review->user_id = 0;
                $review->reply_to = 0;
                $review->score = mt_rand(1, 5);

                $review->save();

                $this->stdout($review->comment . PHP_EOL);
            }
        }
    }

    public function actionFakeCategories()
    {
        for ($i = 0; $i < 5; $i++) {
            $faker_main = Factory::create();

            $name = $faker_main->firstName;

            $category_main = new Categories();
            $category_main->name_alt = mb_strtolower($name);
            $category_main->parent_id = -1;
            $category_main->depth = 1;
            $category_main->path = '-1';

            if ($category_main->save()) {

                $main_info = new CategoriesInfo(['record_id' => $category_main->id, 'lang' => 1]);
                $main_info->name = $name;
                $main_info->save();

                $this->stdout($name . PHP_EOL);

                for ($j = 0; $j < 22; $j++) {
                    $faker_two = Factory::create();

                    $name_two = $faker_two->firstName;

                    $category_two = new Categories();
                    $category_two->name_alt = mb_strtolower($name_two);
                    $category_two->parent_id = $category_main->id;
                    $category_two->depth = 2;
                    $category_two->path = (string)$category_main->id;

                    if ($category_two->save()) {
                        $two_info = new CategoriesInfo(['record_id' => $category_two->id, 'lang' => 1]);
                        $two_info->name = $name_two;
                        $two_info->save();

                        $this->stdout($name_two . PHP_EOL);

                        for ($k = 0; $k < 4; $k++) {
                            $faker_3 = Factory::create();

                            $name_3 = $faker_3->firstName;

                            $category_3 = new Categories();
                            $category_3->name_alt = mb_strtolower($name_3);
                            $category_3->parent_id = $category_two->id;
                            $category_3->depth = 3;
                            $category_3->path = $category_main->id . '.' . $category_two->id;

                            $category_3->save();

                            $three_info = new CategoriesInfo(['record_id' => $category_3->id, 'lang' => 1]);
                            $three_info->name = $name_3;
                            $three_info->save();

                            $this->stdout($name_3 . PHP_EOL);
                        }
                    }
                }
            }
        }

        $this->renameExisting();
    }

    public function renameExisting()
    {
        $cats = Categories::find()->all();
        foreach ($cats as $cat) {
            $existing = Categories::find()->where(['name_alt' => $cat->name_alt])->all();
            if (count($existing) > 1) {
                $c = 1;
                foreach ($existing as $item) {
                    $item->name_alt .= '-' . $c;
                    $item->save();

                    $this->stdout('Renamed to ' . $item->name_alt . PHP_EOL);

                    $c++;
                }
            }
        }
    }

    public function actionTranslit($text)
    {
        $this->stdout(Helper::transliterate($text) . PHP_EOL);
    }

    public function actionAssocProducts()
    {
        $products = Products::find()->all();
        $categories = Categories::find()->where(['depth' => 3])->all();

        $prods_index = 0;
        $count_prods = count($products);
        foreach ($categories as $category) {
            $c = 0;
            $path_parts = explode('.', $category->path);
            while ($prods_index < $count_prods) {
                if ($c > 5) break;

                foreach ($path_parts as $path_part) {
                    $assoc = new ProductsCategoriesAssoc();
                    $assoc->product_id = $products[$prods_index]->id;
                    $assoc->category_id = $path_part;
                    $assoc->save();
                }

                $assoc = new ProductsCategoriesAssoc();
                $assoc->product_id = $products[$prods_index]->id;
                $assoc->category_id = $category->id;
                $assoc->save();

                $prods_index++;
                $c++;
            }
        }
    }

    public function actionAssocCatProd()
    {
        $prods = Products::find()->all();
        foreach ($prods as $prod) {
            $assoc = ProductsCategoriesAssoc::find()->where(['product_id' => $prod->id])->all();
            $prod->category_id = $assoc[count($assoc) - 1]->category_id;
            $prod->save();
        }

        $this->stdout('OK' . PHP_EOL);
    }

    public function actionFakeAnswers()
    {
        $reviews = Reviews::find()->main()->all();
        foreach ($reviews as $review) {
            if (mt_rand(1, 100) % 3 != 0)
                continue;

            $faker = Factory::create();

            $answer = new Reviews();
            $answer->module = $review->module;
            $answer->record_id = $review->record_id;
            $answer->comment = $faker->sentence(mt_rand(5, 10));
            $answer->user_id = 0;
            $answer->reply_to = $review->id;
            $answer->score = 0;

            $answer->save();

            $this->stdout($answer->comment . PHP_EOL);

        }
    }

    public function actionOrderHistory()
    {
        $orders = Orders::find()->all();

        foreach ($orders as $order) {
            $order_history = new OrderHistory(['order_id' => $order->id]);

            $order_history->order_status = Orders::STATUS_NEW;
            $order_history->created_at = $order->created_at;
            $order_history->save();
        }

        $this->stdout('OK');
    }

    public function actionOrderStatus()
    {
        $orders = Orders::find()->all();

        foreach ($orders as $order) {
            $order->setNewStatus(Orders::STATUS_PROCESSING);
        }

        $this->stdout('OK');
    }
}