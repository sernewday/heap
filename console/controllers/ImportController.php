<?php


namespace console\controllers;


use common\models\Brands;
use common\models\Categories;
use common\models\Infos\CategoriesInfo;
use common\models\Infos\ProductsInfo;
use common\models\Products;
use frontend\components\Csv;
use frontend\components\Helper;
use SimpleXMLElement;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class ImportController
 * @package console\controllers
 */
class ImportController extends Controller
{
    public function actionCategory()
    {
        $file_path = dirname(dirname(__DIR__)) . '/frontend/import/categories.csv';
        $csv_data = file_get_contents($file_path);

        $parsed_data = Csv::parse($csv_data);

        $parent_main = -1;
        $parent_second = -1;
        foreach ($parsed_data as $item) {
            if (!$item[2] && !$item[3] && !$item[4] || in_array($item[2], ['Мужская/женская одежда', 'Мужская/женская обувь', 'Детская обувь', 'Детская одежда'])) continue;
            $isMain = false;
            $category_name = trim($item[4]);

            $category = new Categories();

            if ($item[2]) {
                $isMain = true;
                $category_name = trim($item[2]);

                $category->parent_id = -1;
                $category->path = '-1';
            } else if ($item[3])
                $category_name = trim($item[3]);

            if ($item[3])
                $category->parent_id = $parent_main;
            else if ($item[4])
                $category->parent_id = $parent_second;

            $category->name_alt = Helper::transliterate($category_name);

            if ($category->save()) {
                if ($isMain)
                    $parent_main = $category->id;
                else if ($item[3])
                    $parent_second = $category->id;

                $info = new CategoriesInfo(['record_id' => $category->id, 'lang' => 1]);
                $info->name = trim($category_name);

                $info->save();

                $this->stdout($category_name . PHP_EOL);
            }

        }

        return ExitCode::OK;
    }

    public function actionProducts()
    {
        $file_data = file_get_contents(dirname(dirname(__DIR__)) . '/frontend/import/products_2.xml');
        $xml = new SimpleXMLElement($file_data, LIBXML_NOCDATA);

        $categories = $xml->children()->shop->categories->category;

//        ---------------------------  ASSOC CATEGORIES
        $explode_name = false;
        $index_take = 1;
        foreach ($categories as $category) {
            $import_id = $category->attributes();
            $name = (string)$category;

            $category_data = Categories::findByImportId($import_id);
            if (!$category_data) {
                if ($explode_name) {
                    $name_parts = explode(' ', $name);

                    $category_data = Categories::findByName($name_parts[$index_take]);
                    if ($category_data)
                        $category_data->addImportId($import_id);
                } else {
                    $category_data = Categories::findByName($name);
                    if ($category_data)
                        $category_data->addImportId($import_id);
                }
            }

            $this->stdout("{$name} - $import_id" . PHP_EOL);
        }

//            ---------------------------

        //            IMPORT PRODUCTS
        $this->stdout(PHP_EOL . PHP_EOL);

        $products = $xml->children()->shop->offers->offer;

        $cur_dir = dirname(__DIR__) . '/controllers/import_time.json';

        if (!file_exists($cur_dir))
            file_put_contents($cur_dir, '{}');

        $import_time = json_decode(file_get_contents($cur_dir), true);
        if (!$import_time)
            $import_time = [];

        $c_time = time();

        $products_counter = 0;

        foreach ($products as $product_item) {
            if ($products_counter >= 200) {
                file_put_contents($cur_dir, json_encode($import_time, JSON_PRETTY_PRINT));
                $products_counter = 0;
            }

            $import_id = (string)$product_item->attributes()->id;
            $available = (string)$product_item->attributes()->available;

            if (isset($import_time[$import_id]) && ($c_time - $import_time[$import_id] < 3600))
                continue;

            $products_counter += 1;
            $import_time[$import_id] = $c_time;

            $product = Products::findOne(['import_id' => "$import_id"]);
            if (!$product)
                $product = new Products(['import_id' => "$import_id"]);

            $brand = null;
            if ($product_item->vendorCode)
                $brand = Brands::findOne(['import_id' => (string)$product_item->vendorCode]);

            if (!$brand) {
                $brand = Brands::findOne(['name' => trim((string)$product_item->vendor)]);

                if (!$brand)
                    $brand = Brands::createByName((string)$product_item->vendor, (string)$product_item->vendorCode ?: null);
            }

            $product->brand_id = $brand->id;
            $product->category_id = Categories::findByImportId($product_item->categoryId)->id ?: 0;

            $price = (string)$product_item->price;
            $product->price = $price;

            $price_old = (string)$product_item->price_old;
            if ($price_old && $price_old > $price) {
                $product->price = $price_old;

                $product->discount = $price_old - $price;
                $product->discount_type = Products::DISCOUNT_CASH;
            }

            $product->currency_id = Products::CURRENCY_UAH;
            $product->status_id = Products::STATUS_ACTIVE;

            $product->available = 0;
            if ($available == 'true')
                $product->available = 1;

            $product->amount_left = $product_item->stock_quantity ?: 0;

            if ($product->save()) {
                $product->addNewCategory($product->category_id, true);

                $info = ProductsInfo::findOne(['record_id' => $product->id, 'lang' => 1]);
                if (!$info)
                    $info = new ProductsInfo(['record_id' => $product->id, 'lang' => 1]);

                $info->name = (string)$product_item->name;
                $info->description = (string)$product_item->description;

                foreach ($product_item->param as $param) {
                    if ($param->attributes() == 'Размер') {
                        $product->addSize((explode(':', $param))[0], 'M');
                    }

                    if ($param->attributes() != 'Дополнительные характеристики') {
                        $product->addNewParam((string)$param->attributes(), ucfirst(trim(str_replace(['-', '_'], '', (string)$param))));
                    }
                }

                foreach ($product_item->picture as $picture) {
                    try {
                        $product->addNewImageByUrl((string)$picture);
                    } catch (\Exception $e) {}
                }

                $info->save();
            }

            $this->stdout((string)$product_item->name . PHP_EOL);
        }

        file_put_contents($cur_dir, json_encode($import_time, JSON_PRETTY_PRINT));

        return ExitCode::OK;
    }
}